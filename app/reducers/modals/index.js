const initialState = {
	networkModalVisible: false,
	accountsModalVisible: false,
	collectibleContractModalVisible: false,
	receiveModalVisible: false,
	receiveAsset: undefined,
	dappTransactionModalVisible: false,
	approveModalVisible: false,
	importFromSeedModalVisible: false,
	detailTokenModalVisible: false,
	detailNFTModalVisible: false,
	tokenDetail: {},
	collectibles: {}
};

const modalsReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'TOGGLE_NETWORK_MODAL':
			return {
				...state,
				networkModalVisible: !state.networkModalVisible,
			};
		case 'TOGGLE_RECEIVE_MODAL': {
			return {
				...state,
				receiveModalVisible: !state.receiveModalVisible,
				receiveAsset: action.asset,
			};
		}
		case 'TOGGLE_IMPORT_FROM_SEED_MODAL': {
			return {
				...state,
				importFromSeedModalVisible: !state.importFromSeedModalVisible,
			}
		}
		case 'TOGGLE_ACCOUNT_MODAL':
			return {
				...state,
				accountsModalVisible: !state.accountsModalVisible,
			};
		case 'TOGGLE_DETAIL_TOKEN_MODAL':
			return {
				...state,
				detailTokenModalVisible: !state.detailTokenModalVisible,
				tokenDetail: action.token
			};
		case 'TOGGLE_DETAIL_NFT_MODAL':
			return {
				...state,
				detailNFTModalVisible: !state.detailNFTModalVisible,
				collectibles: action.collectibles
			};
		case 'TOGGLE_COLLECTIBLE_CONTRACT_MODAL':
			return {
				...state,
				collectibleContractModalVisible: !state.collectibleContractModalVisible,
			};
		case 'TOGGLE_DAPP_TRANSACTION_MODAL':
			if (action.show === false) {
				return {
					...state,
					dappTransactionModalVisible: false,
				};
			}
			return {
				...state,
				dappTransactionModalVisible: action.show === null ? !state.dappTransactionModalVisible : action.show,
			};
		case 'TOGGLE_APPROVE_MODAL':
			if (action.show === false) {
				return {
					...state,
					approveModalVisible: false,
				};
			}
			return {
				...state,
				approveModalVisible: !state.approveModalVisible,
			};
		default:
			return state;
	}
};
export default modalsReducer;
