const initialState = {
	editWalletAddress: null,
};

export function myWalletReducer(state = initialState, action) {
	switch (action.type) {
		case 'EDIT_MY_WALLET':
			return { ...state, editWalletAddress: action.payload.address };

		case 'CANCEL_EDIT_WALLET':
			return { ...state, editWalletAddress: null };

		default:
			return state;
	}
}
