import {
	ADD_FAVORITE_COLLECTIBLE,
	REMOVE_FAVORITE_COLLECTIBLE,
	GET_ALL_COLLECTIBLE,
	REMOVE_COLLECTIBLE,
	PIN_COLLECTIBLE,
	UNPIN_COLLECTIBLE
} from '../../reducers/collectibles';

export const addFavoriteCollectible = (selectedAddress, chainId, collectible) => ({
	type: ADD_FAVORITE_COLLECTIBLE,
	selectedAddress,
	chainId,
	collectible,
});

export const removeFavoriteCollectible = (selectedAddress, chainId, collectible) => ({
	type: REMOVE_FAVORITE_COLLECTIBLE,
	selectedAddress,
	chainId,
	collectible,
});

export const getCollectible = (collectibles) => ({
	type: GET_ALL_COLLECTIBLE,
	collectibles,
});

export const removeCollectible = (collectibles) => ({
	type: REMOVE_COLLECTIBLE,
	collectibles,
});

export const pinCollectible = (collectibles) => ({
	type: PIN_COLLECTIBLE,
	collectibles,
});

export const unPinCollectible = (collectibles) => ({
	type: UNPIN_COLLECTIBLE,
	collectibles,
});
