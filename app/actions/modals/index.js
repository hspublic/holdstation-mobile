// eslint-disable-next-line import/prefer-default-export
export function toggleNetworkModal() {
	return {
		type: 'TOGGLE_NETWORK_MODAL',
	};
}

export function toggleAccountsModal() {
	return {
		type: 'TOGGLE_ACCOUNT_MODAL',
	};
}

export function toggleCollectibleContractModal() {
	return {
		type: 'TOGGLE_COLLECTIBLE_CONTRACT_MODAL',
	};
}

// Toggle modal detail token list in home pgae
export function toggleDetailTokenModal(token) {
	return {
		type: 'TOGGLE_DETAIL_TOKEN_MODAL',
		token: token || {},
	};
}

// Toggle modal detail nft list in home page
export function toggleDetailNftModal(token) {
	return {
		type: 'TOGGLE_DETAIL_NFT_MODAL',
		collectibles: token || {},
	};
}

export function toggleReceiveModal(asset) {
	return {
		type: 'TOGGLE_RECEIVE_MODAL',
		asset,
	};
}

export function toggleImportFromSeedModalVisible() {
	return {
		type: 'TOGGLE_IMPORT_FROM_SEED_MODAL',
	};
}

export function toggleDappTransactionModal(show) {
	return {
		type: 'TOGGLE_DAPP_TRANSACTION_MODAL',
		show,
	};
}

export function toggleApproveModal(show) {
	return {
		type: 'TOGGLE_APPROVE_MODAL',
		show,
	};
}
