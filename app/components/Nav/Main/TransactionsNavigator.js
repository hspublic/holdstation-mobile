import React from "react";
import { StyleSheet, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";

import { mockTheme, useAppThemeFromContext } from "../../../util/theme";
import { colors as importedColors } from "../../../styles/common";
import ThemeSettings from "../../Views/ThemeSettings";

import TransactionsView from "../../Views/TransactionsView";
import SimpleWebview from "../../Views/SimpleWebview";
import Settings from "../../Views/Settings";
import GeneralSettings from "../../Views/Settings/GeneralSettings";
import AdvancedSettings from "../../Views/Settings/AdvancedSettings";
import SecuritySettings from "../../Views/Settings/SecuritySettings";
import ExperimentalSettings from "../../Views/Settings/ExperimentalSettings";
import NetworksSettings from "../../Views/Settings/NetworksSettings";
import NetworkSettings from "../../Views/Settings/NetworksSettings/NetworkSettings";
import AppInformation from "../../Views/Settings/AppInformation";
import Contacts from "../../Views/Settings/Contacts";
import ContactForm from "../../Views/Settings/Contacts/ContactForm";
import RevealAccountList from "../../Views/RevealAccountList";
import RevealPrivateCredential from "../../Views/RevealPrivateCredential";
import WalletConnectSessions from "../../Views/WalletConnectSessions";
import ResetPassword from "../../Views/ResetPassword";
import AccountBackupStep1B from "../../Views/AccountBackupStep1B";
import ManualBackupStep1 from "../../Views/ManualBackupStep1";
import ManualBackupStep2 from "../../Views/ManualBackupStep2";
import ManualBackupStep3 from "../../Views/ManualBackupStep3";
import EnterPasswordSimple from "../../Views/EnterPasswordSimple";

const createStyles = (colors) =>
	StyleSheet.create({
		flex: {
			flex: 1,
		},
		loader: {
			backgroundColor: colors.background.default,
			flex: 1,
			justifyContent: 'center',
			alignItems: 'center',
		},
	});

const Stack = createStackNavigator();
const TransactionTabHome = () => (
	<Stack.Navigator initialRouteName={'TransactionView'}>
		<Stack.Screen name="TransactionView" component={TransactionsView} />
	</Stack.Navigator>
);

const Webview = () => (
	<Stack.Navigator>
		<Stack.Screen
			name="SimpleWebview"
			component={SimpleWebview}
			mode={'modal'}
			options={SimpleWebview.navigationOptions}
		/>
	</Stack.Navigator>
);

const SettingsFlow = () => (
	<Stack.Navigator initialRouteName={'Settings'}>
		<Stack.Screen name="Settings" component={Settings} options={Settings.navigationOptions} />
		<Stack.Screen name="GeneralSettings" component={GeneralSettings} options={GeneralSettings.navigationOptions} />
		<Stack.Screen
			name="AdvancedSettings"
			component={AdvancedSettings}
			options={AdvancedSettings.navigationOptions}
		/>
		<Stack.Screen
			name="SecuritySettings"
			component={SecuritySettings}
			options={SecuritySettings.navigationOptions}
		/>
		<Stack.Screen
			name="ExperimentalSettings"
			component={ExperimentalSettings}
			options={ExperimentalSettings.navigationOptions}
		/>
		<Stack.Screen
			name="NetworksSettings"
			component={NetworksSettings}
			options={NetworksSettings.navigationOptions}
		/>
		<Stack.Screen name="NetworkSettings" component={NetworkSettings} options={NetworkSettings.navigationOptions} />
		<Stack.Screen name="CompanySettings" component={AppInformation} options={AppInformation.navigationOptions} />
		<Stack.Screen name="ContactsSettings" component={Contacts} options={Contacts.navigationOptions} />
		<Stack.Screen name="ContactForm" component={ContactForm} options={ContactForm.navigationOptions} />
		<Stack.Screen
			name="RevealAccountList"
			component={RevealAccountList}
			options={RevealAccountList.navigationOptions, { headerShown: false }}
		/>
		<Stack.Screen
			name="RevealPrivateCredentialView"
			component={RevealPrivateCredential}
			options={RevealPrivateCredential.navigationOptions}
		/>
		<Stack.Screen
			name="WalletConnectSessionsView"
			component={WalletConnectSessions}
			options={WalletConnectSessions.navigationOptions}
		/>
		<Stack.Screen name="ResetPassword" component={ResetPassword} options={ResetPassword.navigationOptions} />
		<Stack.Screen
			name="AccountBackupStep1B"
			component={AccountBackupStep1B}
			options={AccountBackupStep1B.navigationOptions}
		/>
		<Stack.Screen
			name="ManualBackupStep1"
			component={ManualBackupStep1}
			options={ManualBackupStep1.navigationOptions}
		/>
		<Stack.Screen
			name="ManualBackupStep2"
			component={ManualBackupStep2}
			options={ManualBackupStep2.navigationOptions}
		/>
		<Stack.Screen
			name="ManualBackupStep3"
			component={ManualBackupStep3}
			options={ManualBackupStep3.navigationOptions}
		/>
		<Stack.Screen
			name="EnterPasswordSimple"
			component={EnterPasswordSimple}
			options={EnterPasswordSimple.navigationOptions}
		/>
	</Stack.Navigator>
);

const SettingsModalStack = () => (
	<Stack.Navigator
		initialRouteName={'SettingsFlow'}
		mode={'modal'}
		screenOptions={{ headerShown: false, cardStyle: { backgroundColor: importedColors.transparent } }}
	>
		<Stack.Screen name={'SettingsFlow'} component={SettingsFlow} />
		<Stack.Screen name={'ThemeSettings'} component={ThemeSettings} options={{ animationEnabled: false }} />
	</Stack.Navigator>
);

const TransactionsNavigator = () => {
	const { colors } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);
	return (
		<React.Fragment>
			<View style={styles.flex}>
				<Stack.Navigator
					screenOptions={{
						headerShown: false,
					}}
					mode={'modal'}
					initialRouteName={'TransactionHome'}
				>
					<Stack.Screen name="TransactionHome" tabBarVisible={false} component={TransactionTabHome} />
					<Stack.Screen name="Webview" component={Webview} />
					<Stack.Screen name="SettingsView" component={SettingsModalStack} />
				</Stack.Navigator>
			</View>
		</React.Fragment>
	);
};

export default TransactionsNavigator;
