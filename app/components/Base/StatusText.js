import React from 'react';
import PropTypes from 'prop-types';
import Text from './Text';
import { View, Image, Dimensions } from 'react-native';
import { StyleSheet } from 'react-native';
import { FIAT_ORDER_STATES } from '../../constants/on-ramp';
import { strings } from '../../../locales/i18n';
import { useAppThemeFromContext, mockTheme } from '../../util/theme';

import pendingIcon from '../../images-new/transaction-icons/status-pending.png';
import confirmedIcon from '../../images-new/transaction-icons/status-confirmed.png';
import failIcon from '../../images-new/transaction-icons/status-failed.png';

const win = Dimensions.get('window');

const styles = StyleSheet.create({
	wrapper: {
		flexDirection: 'row',
		alignItems: 'center',
		// marginTop: win.height/ 100,
	},
	icon: {
		width: 16,
		height: 16,
	},
	status: {
		marginTop: 4,
		fontSize: 14,
		fontWeight: '400',
		letterSpacing: 0.5,
		marginLeft: 1
	},
});

// export const ConfirmedText = (props) => <Text bold green style={styles.status} {...props} />;
// export const PendingText = (props) => {
// 	const { colors } = useAppThemeFromContext() || mockTheme;
// 	return <Text bold style={[styles.status, { color: colors.secondary.default }]} {...props} />;
// };
// export const FailedText = (props) => {
// 	const { colors } = useAppThemeFromContext() || mockTheme;
// 	return <Text bold style={[styles.status, { color: colors.error.default }]} {...props} />;
// };

export const ConfirmedText = (props) => {
	return (
		<View style={styles.wrapper}>
			<Image source={confirmedIcon} style={styles.icon} />
			<Text bold style={[styles.status, { color: '#A0A0B1' }]} {...props} />
		</View>
	);
};
export const PendingText = (props) => {
	return (
		<View style={styles.wrapper}>
			<Image source={pendingIcon} style={styles.icon} />
			<Text bold style={[styles.status, { color: '#A0A0B1' }]} {...props} />
		</View>
	);
};
export const FailedText = (props) => {
	return (
		<View style={styles.wrapper}>
			<Image source={failIcon} style={styles.icon} />
			<Text bold style={[styles.status, { color: '#EB5252' }]} {...props} />
		</View>
	);
};

function StatusText({ status, context, ...props }) {
	switch (status) {
		case 'Confirmed':
		case 'confirmed':
			return <ConfirmedText>{strings(`${context}.${status}`)}</ConfirmedText>;
		case 'Pending':
		case 'pending':
		case 'Submitted':
		case 'submitted':
			return <PendingText>{strings(`${context}.${status}`)}</PendingText>;
		case 'Failed':
		case 'Cancelled':
		case 'failed':
		case 'cancelled':
			return <FailedText>{strings(`${context}.${status}`)}</FailedText>;

		case FIAT_ORDER_STATES.COMPLETED:
			return <ConfirmedText>{strings(`${context}.completed`)}</ConfirmedText>;
		case FIAT_ORDER_STATES.PENDING:
			return <PendingText>{strings(`${context}.pending`)}</PendingText>;
		case FIAT_ORDER_STATES.FAILED:
			return <FailedText>{strings(`${context}.failed`)}</FailedText>;
		case FIAT_ORDER_STATES.CANCELLED:
			return <FailedText>{strings(`${context}.cancelled`)}</FailedText>;

		default:
			return (
				<Text bold style={styles.status}>
					{status}
				</Text>
			);
	}
}

StatusText.defaultProps = {
	context: 'transaction',
};

StatusText.propTypes = {
	status: PropTypes.string.isRequired,
	context: PropTypes.string,
};

export default StatusText;
