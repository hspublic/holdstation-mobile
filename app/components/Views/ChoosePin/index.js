import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
	ActivityIndicator,
	View,
	StyleSheet,
	InteractionManager,
	Dimensions,
} from 'react-native';
// eslint-disable-next-line import/no-unresolved
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { logIn, passwordSet, passwordUnset, seedphraseNotBackedUp, seedphraseBackedUp } from '../../../actions/user';
import { setLockTime } from '../../../actions/settings';
import Engine from '../../../core/Engine';
import Device from '../../../util/device';
import { strings } from '../../../../locales/i18n';
import { getOnboardingNavbarOptions } from '../../UI/Navbar';
import SecureKeychain from '../../../core/SecureKeychain';
import AppConstants from '../../../core/AppConstants';
import Logger from '../../../util/Logger';
import { fontStyles } from '../../../styles/common';

const win = Dimensions.get('window')

import {
	EXISTING_USER,
	PASSCODE,
	NEXT_MAKER_REMINDER,
	TRUE,
	SEED_PHRASE_HINTS,
	BIOMETRY_CHOICE_DISABLED,
} from '../../../constants/storage';
import AnalyticsV2 from '../../../util/analyticsV2';
import { ThemeContext, mockTheme } from '../../../util/theme';
import PINCode, { hasUserSetPinCode } from '@haskkor/react-native-pincode';

const createStyles = (colors) =>
	StyleSheet.create({
		// eslint-disable-next-line react-native/no-color-literals
		container: {
			flex: 1,
			backgroundColor: '#1B1B23',
			opacity:1
		},
		title: {
			fontSize: 20,
			textAlign: 'center',
			margin: 10,
			marginTop: 20,
		},
		
		bold: {
			fontWeight: 'bold',
		},
		dots: {
			width: 12,
			height: 12,
			borderRadius: 6,
			backgroundColor: '#1B1B23',
			borderColor: '#FFFFFF',
			borderWidth: 2,
		},
		titlePinCode: {
			fontSize: 24,
			...fontStyles.bold,
			lineHeight: 29,
			color: '#FFFFFF',
			opacity:1
		},
		subtitlePinCode: {
			fontSize: 16,
			...fontStyles.normal,
			lineHeight: 20,
			color: '#58586C',
			opacity:1
		},
		pinCodeTextButtonCircle:{
			fontSize: 20,
			...fontStyles.normal,
			color: '#FFFFFF',
			opacity:1
		},
		pinCodeButtonCircle:{
			backgroundColor: '#1B1B23',
			borderWidth: 1,
			borderColor: '#3D3D4B',
			// width: 72,
			// height: 72,
			borderRadius: 36,
			opacity:1
		},
		pinCodeDeleteButtonText:{
			color:'#FFFFFF', 
			fontSize:16, 
			lineHeight:20, 
			...fontStyles.normal
		}
	});

const IOS_DENY_BIOMETRIC_ERROR = 'The user name or passphrase you entered is not correct.';

/**
 * View where users can set their password for the first time
 */
class ChoosePin extends PureComponent {
	static propTypes = {
		/**
		 * The navigator object
		 */
		navigation: PropTypes.object,
		/**
		 * The action to update the password set flag
		 * in the redux store
		 */
		passwordSet: PropTypes.func,
		/**
		 * The action to update the password set flag
		 * in the redux store to false
		 */
		passwordUnset: PropTypes.func,
		/**
		 * The action to update the lock time
		 * in the redux store
		 */
		setLockTime: PropTypes.func,
		/**
		 * A string representing the selected address => account
		 */
		selectedAddress: PropTypes.string,
		/**
		 * Action to reset the flag seedphraseBackedUp in redux
		 */
		seedphraseNotBackedUp: PropTypes.func,
		/**
		 * Object that represents the current route info like params passed to it
		 */
		route: PropTypes.object,
		logIn: PropTypes.func,
		/**
		 * The action to update the seedphrase backed up flag
		 * in the redux store
		 */
		seedphraseBackedUp: PropTypes.func,
	};

	state = {
		showPinLock: true,
		PINCodeStatus: 'choose',
		isSelected: false,
		password: '',
		confirmPassword: '',
		secureTextEntry: true,
		biometryType: null,
		biometryChoice: false,
		rememberMe: true,
		loading: false,
		error: null,
		inputWidth: { width: '99%' },
	};

	mounted = true;

	// Flag to know if password in keyring was set or not
	keyringControllerPasswordSet = false;

	updateNavBar = () => {
		const { route, navigation } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(getOnboardingNavbarOptions(route, {}, colors));
	};

	async componentDidMount() {
		this.updateNavBar();
		//console.log(this.props.route.params.WALLET_NAME);
		const biometryType = await SecureKeychain.getSupportedBiometryType();
		if (biometryType) {
			this.setState({ biometryType: Device.isAndroid() ? 'biometrics' : biometryType, biometryChoice: true });
		}
		setTimeout(() => {
			this.setState({
				inputWidth: { width: '100%' },
			});
		}, 100);
	}

	componentDidUpdate(prevProps, prevState) {
		this.updateNavBar();
		const prevLoading = prevState.loading;
		const { loading } = this.state;
		const { navigation } = this.props;
		Logger.error(navigation);

		if (!prevLoading && loading) {
			// update navigationOptions
			navigation.setParams({
				headerLeft: () => <View />,
			});
		}
	}

	componentWillUnmount() {
		this.mounted = false;
	}

	createNewVaultAndKeychain = async (password) => {
		const { KeyringController } = Engine.context;
		await Engine.resetState();
		await KeyringController.createNewVaultAndKeychain(password);
		await SecureKeychain.setGenericPassword(password, SecureKeychain.TYPES.REMEMBER_ME);
		this.keyringControllerPasswordSet = true;
		Logger.log(this.props.selectedAddress);
	};

	createNewVaultAndRestore = async (password, parsedSeed) => {
		const { KeyringController } = Engine.context;
		await Engine.resetState();
		await KeyringController.createNewVaultAndRestore(password, parsedSeed);
		await SecureKeychain.setGenericPassword(password, SecureKeychain.TYPES.REMEMBER_ME);
		this.keyringControllerPasswordSet = true;
		Logger.log(this.props.selectedAddress);
	};

	/**
	 * This function handles the case when the user rejects the OS prompt for allowing use of biometrics.
	 * It resets the state and and prompts the user to both set the "Remember Me" state and to try again.
	 * @param {*} error - error provide from try catch wrapping the biometric set password attempt
	 */
	handleRejectedOsBiometricPrompt = async (error) => {
		const biometryType = await SecureKeychain.getSupportedBiometryType();
		if (error.toString().includes(IOS_DENY_BIOMETRIC_ERROR) && !biometryType) {
			this.setState({
				biometryType,
				biometryChoice: true,
			});
			this.updateBiometryChoice();
			throw Error(strings('choose_password.disable_biometric_error'));
		}
	};

	/**
	 * Recreates a vault
	 *
	 * @param password - Password to recreate and set the vault with
	 */
	recreateVault = async (password) => {
		const { KeyringController, PreferencesController } = Engine.context;
		const seedPhrase = await this.getSeedPhrase();

		let importedAccounts = [];
		try {
			const keychainPassword = this.keyringControllerPasswordSet ? this.state.password : '';
			// Get imported accounts
			const simpleKeyrings = KeyringController.state.keyrings.filter(
				(keyring) => keyring.type === 'Simple Key Pair'
			);
			for (let i = 0; i < simpleKeyrings.length; i++) {
				const simpleKeyring = simpleKeyrings[i];
				const simpleKeyringAccounts = await Promise.all(
					simpleKeyring.accounts.map((account) => KeyringController.exportAccount(keychainPassword, account))
				);
				importedAccounts = [...importedAccounts, ...simpleKeyringAccounts];
			}
		} catch (e) {
			Logger.error(e, 'error while trying to get imported accounts on recreate vault');
		}

		// Recreate keyring with password given to this method
		await KeyringController.createNewVaultAndRestore(password, seedPhrase);
		// Keyring is set with empty password or not
		this.keyringControllerPasswordSet = password !== '';

		// Get props to restore vault
		const hdKeyring = KeyringController.state.keyrings[0];
		const existingAccountCount = hdKeyring.accounts.length;
		const selectedAddress = this.props.selectedAddress;
		let preferencesControllerState = PreferencesController.state;

		// Create previous accounts again
		for (let i = 0; i < existingAccountCount - 1; i++) {
			await KeyringController.addNewAccount();
		}

		try {
			// Import imported accounts again
			for (let i = 0; i < importedAccounts.length; i++) {
				await KeyringController.importAccountWithStrategy('privateKey', [importedAccounts[i]]);
			}
		} catch (e) {
			Logger.error(e, 'error while trying to import accounts on recreate vault');
		}

		// Reset preferencesControllerState
		preferencesControllerState = PreferencesController.state;

		// Set preferencesControllerState again
		await PreferencesController.update(preferencesControllerState);
		// Reselect previous selected account if still available
		if (hdKeyring.accounts.includes(selectedAddress)) {
			PreferencesController.setSelectedAddress(selectedAddress);
		} else {
			PreferencesController.setSelectedAddress(hdKeyring.accounts[0]);
		}
	};

	/**
	 * Returns current vault seed phrase
	 * It does it using an empty password or a password set by the user
	 * depending on the state the app is currently in
	 */
	getSeedPhrase = async () => {
		const { KeyringController } = Engine.context;
		const { password } = this.state;
		const keychainPassword = this.keyringControllerPasswordSet ? password : '';
		const mnemonic = await KeyringController.exportSeedPhrase(keychainPassword);
		return JSON.stringify(mnemonic).replace(/"/g, '');
	};

	updateBiometryChoice = async (biometryChoice) => {
		if (!biometryChoice) {
			await AsyncStorage.setItem(BIOMETRY_CHOICE_DISABLED, TRUE);
		} else {
			await AsyncStorage.removeItem(BIOMETRY_CHOICE_DISABLED);
		}
		this.setState({ biometryChoice });
	};

	_finishProcess = async (pin, type) => {
		const hasPin = await hasUserSetPinCode();
		if (hasPin) {
			InteractionManager.runAfterInteractions(() => {
				AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_CREATION_ATTEMPTED);
			});
			try {
				this.setState({ loading: true });
				const seedPhrase = this.props.route.params.seed;
				const type = this.props.route.params?.type;
				if (seedPhrase == null) {
					await this.createNewVaultAndKeychain(pin);
					this.props.seedphraseNotBackedUp();
				} else {
					await this.createNewVaultAndRestore(pin, seedPhrase);
					this.props.seedphraseBackedUp();
				}

				await AsyncStorage.removeItem(NEXT_MAKER_REMINDER);
				await AsyncStorage.setItem(EXISTING_USER, TRUE);
				await AsyncStorage.removeItem(SEED_PHRASE_HINTS);
				const { PreferencesController } = Engine.context;
				const { selectedAddress } = this.props;
				let accountLabel = this.props.route.params.WALLET_NAME;
				if (accountLabel == null || accountLabel === '') {
					accountLabel =
						selectedAddress.slice(0, 4) + '...' + selectedAddress.slice(selectedAddress.length - 4);
				}
				PreferencesController.setAccountLabel(selectedAddress, accountLabel);
				// Set state in app as it was with password
				await SecureKeychain.setGenericPassword(pin, SecureKeychain.TYPES.REMEMBER_ME);
				await AsyncStorage.setItem(PASSCODE, pin);
				await AsyncStorage.setItem(EXISTING_USER, TRUE);
				await AsyncStorage.removeItem(SEED_PHRASE_HINTS);
				this.props.passwordSet();
				this.props.logIn();
				this.props.setLockTime(AppConstants.DEFAULT_LOCK_TIMEOUT);
				this.setState({ loading: false });

				//this.props.navigation.replace('AccountBackupStep1');

				const biometryType = await SecureKeychain.getSupportedBiometryType();

				if (biometryType) {
					if (biometryType === 'FaceID') {
						this.props.navigation.reset({ routes: [{ name: 'FaceId' }] });
					}

					if (biometryType === 'TouchID' || biometryType === 'Fingerprint') {
						this.props.navigation.reset({ routes: [{ name: 'TouchId' }] });
					}
				} else {
					!type ? this.props.navigation.reset({ routes: [{ name: 'Welcome' }] }):
					this.props.navigation.navigate('Welcome', { type: 'import' });

				}

				InteractionManager.runAfterInteractions(() => {
					AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_CREATED, {
						biometrics_enabled: Boolean(this.state.biometryType),
					});
					AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SETUP_COMPLETED, {
						wallet_setup_type: 'new',
						new_wallet: true,
					});
				});
			} catch (error) {
				await this.recreateVault('');
				// Set state in app as it was with no password
				await SecureKeychain.resetGenericPassword();
				await AsyncStorage.removeItem(NEXT_MAKER_REMINDER);
				await AsyncStorage.setItem(EXISTING_USER, TRUE);
				await AsyncStorage.removeItem(SEED_PHRASE_HINTS);
				this.props.passwordUnset();
				this.props.setLockTime(-1);
				this.setState({ loading: false, error: error.toString() });
				InteractionManager.runAfterInteractions(() => {
					AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SETUP_FAILURE, {
						wallet_setup_type: 'new',
						error_type: error.toString(),
					});
				});
			}
		}
	};

	render() {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		const { loading } = this.state;

		return (
			<View style={styles.container}>
				{loading ? (
					<ActivityIndicator size="small" color={colors.primary.inverse} />
				) : (
					<PINCode
						titleChoose={strings('pin_code.title_choose')}
						subtitleChoose={strings('pin_code.subtitle')}
						titleConfirm={strings('pin_code.title_confirm')}
						subtitleConfirm={strings('pin_code.subtitle_confirm')}
						titleConfirmFailed={strings('pin_code.title_confirm_failed')}
						subtitleError={strings('pin_code.subtitl_error')}
						buttonDeleteText={strings('pin_code.button_delete_text')}
						passwordLength={6}
						disableLockScreen
						status={this.state.PINCodeStatus}
						touchIDDisabled
						finishProcess={(pin, type) => this._finishProcess(pin, type)}
						stylePinCodeTextTitle={styles.titlePinCode}
						stylePinCodeTextSubtitle={styles.subtitlePinCode}
						stylePinCodeCircle={styles.dots}
						stylePinCodeTextButtonCircle={styles.pinCodeTextButtonCircle}
						stylePinCodeButtonCircle={styles.pinCodeButtonCircle}
						stylePinCodeDeleteButtonText={styles.pinCodeDeleteButtonText}					
					/>
				)}
			</View>
		);
	}
}

ChoosePin.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
});

const mapDispatchToProps = (dispatch) => ({
	passwordSet: () => dispatch(passwordSet()),
	passwordUnset: () => dispatch(passwordUnset()),
	setLockTime: (time) => dispatch(setLockTime(time)),
	seedphraseNotBackedUp: () => dispatch(seedphraseNotBackedUp()),
	seedphraseBackedUp: () => dispatch(seedphraseBackedUp()),
	logIn: () => dispatch(logIn()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChoosePin);
