import React, { PureComponent } from 'react';
import { SafeAreaView, View, StyleSheet, Text } from 'react-native';
import { fontStyles } from '../../../styles/common';
import { connect } from 'react-redux';
import DefaultTabBar from 'react-native-scrollable-tab-view/DefaultTabBar';
import AddCustomToken from '../../UI/AddCustomToken';
import SearchTokenAutocomplete from '../../UI/SearchTokenAutocomplete';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import PropTypes from 'prop-types';
import { strings } from '../../../../locales/i18n';
import AddCustomNFT from '../../UI/AddCustomNFT';
import { getNetworkNavbarOptions } from '../../UI/Navbar';
import { NetworksChainId } from '@metamask/controllers';
import CollectibleDetectionModal from '../../UI/CollectibleDetectionModal';
import { isMainNet } from '../../../util/networks';
import { ThemeContext, mockTheme } from '../../../util/theme';

const createStyles = (colors) =>
    StyleSheet.create({
        wrapper: {
            flex: 1,
            backgroundColor: '#1B1B23',
        },
        titleWrapper: {
            width: '100%',
            height: 33,
            alignItems: 'center',
            justifyContent: 'center',
            //borderBottomWidth: StyleSheet.hairlineWidth,
            //borderColor: colors.border.muted,
        },
        dragger: {
            width: 48,
            height: 5,
            borderRadius: 4,
            backgroundColor: colors.border.default,
            opacity: 0.6,
        },
        infoWrapper: {
            alignItems: 'center',
            marginTop: 10,
        },
        tabUnderlineStyle: {
            height: '100%',
            zIndex: -1,
            backgroundColor: '#3D3D4B',
            borderRadius: 10,
        },
        tabBar: {
            borderColor: colors.border.muted,
        },
        tabStyle: {
            paddingBottom: 0,
        },
        textStyle: {
            fontSize: 16,
            letterSpacing: 0.5,
            ...fontStyles.bold,
        },
    });

/**
 * PureComponent that provides ability to add assets.
 */
class AddNFT extends PureComponent {
    state = {
        address: '',
        symbol: '',
        decimals: '',
        dismissNftInfo: false,
    };

    static propTypes = {
        /**
        /* navigation object required to push new views
        */
        navigation: PropTypes.object,
        /**
         * Chain id
         */
        chainId: PropTypes.string,
        /**
         * Object that represents the current route info like params passed to it
         */
        route: PropTypes.object,
        /**
         * Boolean to show if NFT detection is enabled
         */
        useCollectibleDetection: PropTypes.bool,
    };

    updateNavBar = () => {
        const { navigation, route } = this.props;
        const colors = this.context.colors || mockTheme.colors;
        navigation.setOptions(
            getNetworkNavbarOptions(
                `add_asset.${route.params.assetType === 'token' ? 'title' : 'title_nft'}`,
                true,
                navigation,
                colors
            )
        );
    };

    componentDidMount = () => {
        this.updateNavBar();
    };

    componentDidUpdate = () => {
        this.updateNavBar();
    };

    goToPage(pageId) {
        this.tabView.goToPage(pageId);
    }

    renderTabBar() {
        const colors = this.context.colors || mockTheme.colors;
        const styles = createStyles(colors);

        return (
            <DefaultTabBar
                underlineStyle={styles.tabUnderlineStyle}
                activeTextColor={'#FFFFFF'}
                inactiveTextColor={colors.text.muted}
                backgroundColor={'#282832'}
                tabStyle={styles.tabStyle}
                textStyle={styles.textStyle}
                style={styles.tabBar}
            />
        );
    }

    dismissNftInfo = async () => {
        this.setState({ dismissNftInfo: true });
    };

    render = () => {
        const {
            route: {
                params: { assetType, collectibleContract },
            },
            navigation,
            chainId,
            useCollectibleDetection,
        } = this.props;
        const { dismissNftInfo } = this.state;
        const colors = this.context.colors || mockTheme.colors;
        const styles = createStyles(colors);

        return (
            <SafeAreaView style={styles.wrapper} testID={`add-${assetType}-screen`}>
                <View style={styles.titleWrapper}>
                    <View style={styles.dragger} testID={'addasset-dragger'} />
                </View>
                <View style={styles.titleWrapper}>
                    <Text style={{ color: '#FFFFFF', fontSize: 16, fontWeight: '500', paddingBottom: 24 }}>{strings('add_asset.title_nft')}</Text>
                </View>

                {/*{isMainNet(chainId) && assetType !== 'token' && !dismissNftInfo && !useCollectibleDetection && (*/}
                {/*	<View style={styles.infoWrapper}>*/}
                {/*		<CollectibleDetectionModal onDismiss={this.dismissNftInfo} navigation={navigation} />*/}
                {/*	</View>*/}
                {/*)}*/}
                <AddCustomNFT
                    navigation={navigation}
                    collectibleContract={collectibleContract}
                    testID={'add-custom-collectible'}
                />
            </SafeAreaView>
        );
    };
}

AddNFT.contextType = ThemeContext;

const mapStateToProps = (state) => ({
    chainId: state.engine.backgroundState.NetworkController.provider.chainId,
    useCollectibleDetection: state.engine.backgroundState.PreferencesController.useCollectibleDetection,
});

export default connect(mapStateToProps)(AddNFT);
