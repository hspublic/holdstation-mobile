import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
	Switch,
	ActivityIndicator,
	Alert,
	TouchableOpacity,
	Text,
	Image,
	View,
	TextInput,
	SafeAreaView,
	StyleSheet,
	Linking,
	InteractionManager,
	Platform,
} from 'react-native';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { getOnboardingNavbarOptions } from '../../UI/Navbar';
import { connect } from 'react-redux';
import { logIn, passwordSet, seedphraseBackedUp } from '../../../actions/user';
import { setLockTime } from '../../../actions/settings';
import StyledButton from '../../UI/StyledButton';
import Engine from '../../../core/Engine';
import { fontStyles, colors as importedColors } from '../../../styles/common';
import { strings } from '../../../../locales/i18n';
import SecureKeychain from '../../../core/SecureKeychain';
import AppConstants from '../../../core/AppConstants';
import setOnboardingWizardStep from '../../../actions/wizard';
import DetectFaceIdModal from '../../UI/DetectFaceIdModal';
import TermsAndConditions from '../TermsAndConditions';
import zxcvbn from 'zxcvbn';
import Icon from 'react-native-vector-icons/FontAwesome';
import Device from '../../../util/device';
import {
	failedSeedPhraseRequirements,
	failedPrivateKeyRequirements,
	isValidMnemonic,
	parseSeedPhrase,
	parseVaultValue,
} from '../../../util/validators';
import { OutlinedTextField } from 'react-native-material-textfield';
import {
	SEED_PHRASE_HINTS,
	BIOMETRY_CHOICE_DISABLED,
	ENCRYPTION_LIB,
	NEXT_MAKER_REMINDER,
	ONBOARDING_WIZARD,
	EXISTING_USER,
	ORIGINAL,
	TRUE,
} from '../../../constants/storage';
import Logger from '../../../util/Logger';
import { tlc, toLowerCaseEquals } from '../../../util/general';
import { getPasswordStrengthWord, passwordRequirementsMet, MIN_PASSWORD_LENGTH } from '../../../util/password';
import importAdditionalAccounts from '../../../util/importAdditionalAccounts';
import AnalyticsV2 from '../../../util/analyticsV2';
import DefaultPreference from 'react-native-default-preference';
import Clipboard from '@react-native-clipboard/clipboard';
import ClipboardManager from '../../../core/ClipboardManager';
import { ThemeContext, mockTheme } from '../../../util/theme';

const MINIMUM_SUPPORTED_CLIPBOARD_VERSION = 9;

const createStyles = (colors) =>
	StyleSheet.create({
		mainWrapper: {
			backgroundColor: '#1B1B23',
			flex: 1,
		},
		wrapper: {
			flex: 1,
			paddingHorizontal: 32,
		},
		title: {
			fontSize: Device.isAndroid() ? 20 : 25,
			marginTop: 50,
			marginBottom: 50,
			color: '#FFFFFF',
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
		},
		field: {
			marginVertical: 5,
			position: 'relative',
		},
		fieldRow: {
			flexDirection: 'row',
			alignItems: 'flex-end',
			justifyContent: 'center',
			textAlign: 'center'
		},
		container: {
		    justifyContent: 'center',
		    alignItems: 'center',
		  },
		faceIdIcon: {
        	marginTop: 150,
        	marginBottom: 150,
		},

		faceIdFailedIcon: {
        	marginTop: 25,
        	marginBottom: 25,
		},
		warning: {
			fontSize: 15,
			marginTop: 10,
			marginBottom: 10,
			color: colors.text.default,
			justifyContent: 'center',
			textAlign: 'center',
        	...fontStyles.bold,
		},
		fieldCol: {
			width: '70%',
		},
		fieldColRight: {
			flexDirection: 'row-reverse',
			width: '30%',
		},
		label: {
			color: '#FF4B4B',
			fontSize: 16,
			marginBottom: 12,
			...fontStyles.normal,
			justifyContent: 'center',
			textAlign: 'center',
		},
		ctaWrapper: {
			marginTop: 10,
		},
		offFaceIdButton: {
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
			fontSize: 15,
			color: '#FFFFFF',
		},
		errorMsg: {
			color: colors.error.default,
			textAlign: 'center',
			...fontStyles.normal,
		},
		seedPhrase: {
			marginTop: 80,
			marginBottom: 80,
			paddingTop:50,
			paddingBottom: 50,
			paddingHorizontal: 20,
			fontSize: 12,
			borderRadius: 10,
			minHeight: 110,
			height: 'auto',
			borderWidth: 1,
			borderColor: colors.border.default,
			backgroundColor: colors.background.default,
			...fontStyles.normal,
			color: colors.text.default,
		},
		errorPhrase: {
			fontSize: 12,
			borderRadius: 10,
			backgroundColor: 'rgba(222, 44, 44, 0.13)',
			...fontStyles.normal,
			paddingTop:10
		},
		padding: {
			paddingRight: 46,
		},
		biometrics: {
			alignItems: 'flex-start',
			marginTop: 10,
		},
		biometryLabel: {
			flex: 1,
			fontSize: 16,
			color: colors.text.default,
			...fontStyles.normal,
		},
		biometrySwitch: {
			marginTop: 10,
			flex: 0,
		},
		termsAndConditions: {
			paddingVertical: 10,
		},
		passwordStrengthLabel: {
			height: 20,
			fontSize: 15,
			color: colors.text.default,
			...fontStyles.normal,
		},
		// eslint-disable-next-line react-native/no-unused-styles
		strength_weak: {
			color: colors.error.default,
		},
		// eslint-disable-next-line react-native/no-unused-styles
		strength_good: {
			color: colors.primary.default,
		},
		// eslint-disable-next-line react-native/no-unused-styles
		strength_strong: {
			color: colors.success.default,
		},
		showMatchingPasswords: {
			position: 'absolute',
			top: 52,
			right: 17,
			alignSelf: 'flex-end',
		},
		qrCode: {
			width: 80,
			marginRight: 120,
			marginLeft: 120,
			marginTop: -80,
			marginBottom: 30,
			alignSelf: 'flex-end',
			flexDirection:'row',
		},
		paste: {
			width: 80,
			marginRight: 120,
			marginLeft: 120,
			marginTop: -70,
			alignSelf: 'flex-end',
			flexDirection:'row',
		},
		pasteTxt: {
			marginLeft: 5
		},
		inputFocused: {
			borderColor: colors.primary.default,
			borderWidth: 2,
		},
		input: {
			...fontStyles.normal,
			fontSize: 16,
			paddingTop: 2,
			color: colors.text.default,
		},
	});

const PASSCODE_NOT_SET_ERROR = 'Error: Passcode not set.';
const WRONG_PASSWORD_ERROR = 'Error: Decrypt failed';
const WRONG_PASSWORD_ERROR_ANDROID = 'Error: error:1e000065:Cipher functions:OPENSSL_internal:BAD_DECRYPT';
const VAULT_ERROR = 'Error: Cannot unlock without a previous vault.';

/**
 * View where users can set restore their account
 * using a seed phrase
 */
class TouchId extends PureComponent {
	static propTypes = {
		/**
		 * The navigator object
		 */
		navigation: PropTypes.object,
		/**
		 * The action to update the password set flag
		 * in the redux store
		 */
		passwordSet: PropTypes.func,
		/**
		 * The action to set the locktime
		 * in the redux store
		 */
		setLockTime: PropTypes.func,
		/**
		 * The action to update the seedphrase backed up flag
		 * in the redux store
		 */
		seedphraseBackedUp: PropTypes.func,
		/**
		 * Action to set onboarding wizard step
		 */
		setOnboardingWizardStep: PropTypes.func,
		logIn: PropTypes.func,
		route: PropTypes.object,
	};

	state = {
		password: '',
		confirmPassword: '',
		seed: '',
		biometryType: null,
		rememberMe: false,
		secureTextEntry: true,
		biometryChoice: false,
		loading: false,
		error: null,
		seedphraseInputFocused: false,
		inputWidth: { width: '99%' },
		hideSeedPhraseInput: true,
	};

	fieldRef = React.createRef();

	updateNavBar = () => {
		const { route, navigation } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(getOnboardingNavbarOptions(route, {}, colors));
	};

	async componentDidMount() {
		this.updateNavBar();
		const biometryType = await SecureKeychain.getSupportedBiometryType();
		if (biometryType) {
			let enabled = true;
			const previouslyDisabled = await AsyncStorage.removeItem(BIOMETRY_CHOICE_DISABLED);
			if (previouslyDisabled && previouslyDisabled === TRUE) {
				enabled = false;
			}
			this.setState({ biometryType: Device.isAndroid() ? 'biometrics' : biometryType, biometryChoice: enabled });
		}
		// Workaround https://github.com/facebook/react-native/issues/9958
		setTimeout(() => {
			this.setState({ inputWidth: { width: '100%' } });
		}, 100);
	}

	componentDidUpdate = () => {
		this.updateNavBar();
	};

	tryBiometric = async (e) => {
		this.setState({ loading: true }, async () => {
			let credentials;
			try {
				credentials = await SecureKeychain.getGenericPassword();
			} catch (error) {
				Logger.error(error);
			}
			if (credentials && credentials.password !== '') {
				this.storeCredentials(credentials.password, true, 'biometryChoice');
			} else {
				this.setState({ loading: false });
			}
		});
	};

	storeCredentials = async (password, enabled, type) => {
		try {
			await SecureKeychain.resetGenericPassword();

			await Engine.context.KeyringController.exportSeedPhrase(password);

			await AsyncStorage.setItem(EXISTING_USER, TRUE);

			if (!enabled) {
				this.setState({ [type]: false, loading: false });
				if (type === 'passcodeChoice') {
					await AsyncStorage.setItem(PASSCODE_DISABLED, TRUE);
				} else if (type === 'biometryChoice') {
					await AsyncStorage.setItem(BIOMETRY_CHOICE_DISABLED, TRUE);
				}

				return;
			}

			if (type === 'passcodeChoice')
				await SecureKeychain.setGenericPassword(password, SecureKeychain.TYPES.PASSCODE);
			else if (type === 'biometryChoice')
				await SecureKeychain.setGenericPassword(password, SecureKeychain.TYPES.BIOMETRICS);

			this.props.passwordSet();

			if (this.props.lockTime === -1) {
				this.props.setLockTime(AppConstants.DEFAULT_LOCK_TIMEOUT);
			}

			this.setState({ [type]: true, loading: false });
			this.props.navigation.replace('Welcome');

		} catch (e) {
			if (e.message === 'Invalid password') {
				Alert.alert(strings('app_settings.invalid_password'), strings('app_settings.invalid_password_message'));
				trackErrorAsAnalytics('SecuritySettings: Invalid password', e?.message);
			} else {
				Logger.error(e, 'SecuritySettings:biometrics');
			}
			this.setState({ [type]: !enabled, loading: false });
		}
	};


	toggleWarningModal = () => this.setState((state) => ({ warningModalVisible: !state.warningModalVisible }));

	onBiometryChoiceChange = (value) => {
		this.setState({ biometryChoice: value });
	};

	clearSecretRecoveryPhrase = async (seed) => {
		// get clipboard contents
		const clipboardContents = await Clipboard.getString();
		const parsedClipboardContents = parseSeedPhrase(clipboardContents);
		if (
			// only clear clipboard if contents isValidMnemonic
			!failedSeedPhraseRequirements(parsedClipboardContents) &&
			isValidMnemonic(parsedClipboardContents) &&
			// only clear clipboard if the seed phrase entered matches what's in the clipboard
			parseSeedPhrase(seed) === parsedClipboardContents
		) {
			await Clipboard.clearString();
		}
	};

	onSeedWordsChange = async (seed) => {
		this.setState({ error: null});
		this.setState({ seed });
		// Only clear on android since iOS will notify users when we getString()
		if (Device.isAndroid()) {
			const androidOSVersion = parseInt(Platform.constants.Release, 10);
			// This conditional is necessary to avoid an error in Android 8.1.0 or lower
			if (androidOSVersion >= MINIMUM_SUPPORTED_CLIPBOARD_VERSION) {
				await this.clearSecretRecoveryPhrase(seed);
			}
		}
	};

	onPasswordChange = (val) => {
		const passInfo = zxcvbn(val);

		this.setState({ password: val, passwordStrength: passInfo.score });
	};

	onPasswordConfirmChange = (val) => {
		this.setState({ confirmPassword: val });
	};

	jumpToPassword = () => {
		const { current } = this.passwordInput;
		current && current.focus();
	};

	jumpToConfirmPassword = () => {
		const { current } = this.confirmPasswordInput;
		current && current.focus();
	};

	updateBiometryChoice = async (biometryChoice) => {
		if (!biometryChoice) {
			await AsyncStorage.setItem(BIOMETRY_CHOICE_DISABLED, TRUE);
		} else {
			await AsyncStorage.removeItem(BIOMETRY_CHOICE_DISABLED);
		}
		this.setState({ biometryChoice });
	};

	renderSwitch = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (this.state.biometryType) {
			return (
				<View style={styles.biometrics}>
					<Text style={styles.biometryLabel}>
						{strings(`biometrics.enable_${this.state.biometryType.toLowerCase()}`)}
					</Text>
					<Switch
						onValueChange={this.updateBiometryChoice}
						value={this.state.biometryChoice}
						style={styles.biometrySwitch}
						trackColor={{ true: colors.primary.default, false: colors.border.muted }}
						thumbColor={importedColors.white}
						ios_backgroundColor={colors.border.muted}
					/>
				</View>
			);
		}

		return (
			<View style={styles.biometrics}>
				<Text style={styles.biometryLabel}>{strings(`choose_password.remember_me`)}</Text>
				<Switch
					onValueChange={(rememberMe) => this.setState({ rememberMe })} // eslint-disable-line react/jsx-no-bind
					value={this.state.rememberMe}
					style={styles.biometrySwitch}
					trackColor={{ true: colors.primary.default, false: colors.border.muted }}
					thumbColor={importedColors.white}
					ios_backgroundColor={colors.border.muted}
					testID={'remember-me-toggle'}
				/>
			</View>
		);
	};

	render() {
		const {
			password,
			passwordStrength,
			confirmPassword,
			seed,
			seedphraseInputFocused,
			inputWidth,
			secureTextEntry,
			error,
			loading,
			hideSeedPhraseInput,
		} = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);

		const passwordStrengthWord = getPasswordStrengthWord(passwordStrength);

		return (
			<SafeAreaView style={styles.mainWrapper}>
				<KeyboardAwareScrollView style={styles.wrapper} resetScrollToCoords={{ x: 0, y: 0 }}>
					<View testID={'import-from-seed-screen'}>
						<Text style={styles.title}>{strings('touchid.title')}</Text>

						<View style={styles.container}>
							<Image
								style={styles.faceIdIcon}
								source={require('../../../images-new/touch.png')}
							/>
						</View>


						<DetectFaceIdModal
							warningModalVisible={this.state.warningModalVisible}
							cancelText={strings('login.i_understand')}
							onCancelPress={this.toggleWarningModal}
							onRequestClose={this.toggleWarningModal}
							onConfirmPress={this.toggleWarningModal}
						>
								<View style={styles.wrapper}>
									<View style={styles.container}>
										<Image
										style={styles.faceIdFailedIcon}
										source={require('../../../images-new/touch-failed.png')}
									/>
									<Text style={styles.warning}>{strings('touchid.canot_detect_touch')}</Text>
									</View>
								</View>

						</DetectFaceIdModal>


						<View style={styles.ctaWrapper}>
							<StyledButton
								type={'blue'}
								onPress={this.tryBiometric}
								testID={'submit'}
								 ref={this.fieldRef}
								disabled={error}
							>
								{loading ? (
									<ActivityIndicator size="small" color={colors.primary.inverse} />
								) : (
									strings('touchid.on_touch_button')
								)}
							</StyledButton>
						</View>

						<View style={styles.ctaWrapper}>

							<Text style={styles.offFaceIdButton} onPress={ ()=> this.props.navigation.replace('Welcome') } >
								{strings('touchid.off_touch_button')}
							</Text>

						</View>
					</View>
				</KeyboardAwareScrollView>

			</SafeAreaView>
		);
	}
}

TouchId.contextType = ThemeContext;

const mapDispatchToProps = (dispatch) => ({
	setLockTime: (time) => dispatch(setLockTime(time)),
	setOnboardingWizardStep: (step) => dispatch(setOnboardingWizardStep(step)),
	passwordSet: () => dispatch(passwordSet()),
	seedphraseBackedUp: () => dispatch(seedphraseBackedUp()),
	logIn: () => dispatch(logIn()),
});

export default connect(null, mapDispatchToProps)(TouchId);
