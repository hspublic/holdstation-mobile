import React, { PureComponent } from 'react';
import { KeyringTypes } from '@metamask/controllers';
import Engine from '../../../core/Engine';
import PropTypes from 'prop-types';
import {
	Alert,
	ActivityIndicator,
	InteractionManager,
	FlatList,
	TouchableOpacity,
	StyleSheet,
	Text,
	TextInput,
	Image,
	View,
	SafeAreaView,
	Dimensions,
} from 'react-native';
import { fontStyles, GLOBAL_ITEM_FONT_SIZE } from '../../../styles/common';
import Device from '../../../util/device';
import { strings } from '../../../../locales/i18n';
import { toChecksumAddress } from 'ethereumjs-util';
import Logger from '../../../util/Logger';
import Analytics from '../../../core/Analytics';
import AnalyticsV2 from '../../../util/analyticsV2';
import { ANALYTICS_EVENT_OPTS } from '../../../util/analytics';
import { doENSReverseLookup } from '../../../util/ENSUtils';
import { connect } from 'react-redux';
import { ThemeContext, mockTheme } from '../../../util/theme';
import {
	BIOMETRY_CHOICE_DISABLED,
	ONBOARDING_WIZARD,
	ENCRYPTION_LIB,
	TRUE,
	ORIGINAL,
	EXISTING_USER,
} from '../../../constants/storage';
import AsyncStorage from '@react-native-community/async-storage';
import StyledButton from '../../UI/StyledButton';
import NotificationManager from '../../../core/NotificationManager';
import Identicon from '../../UI/Identicon';
import EthereumAddress from '../../UI/EthereumAddress';
import Modal from 'react-native-modal';
import AccountList from '../../UI/AccountList';
import Avatar from '../../../util/avatar';

const {width, height} = Dimensions.get('window')

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			backgroundColor: '#1B1B23',
			borderTopLeftRadius: 10,
			borderTopRightRadius: 10,
			minHeight: '90%',
		},
		title: {
			justifyContent: 'center',
			textAlign: 'center',
			width: '100%',
			height: 33,
			alignItems: 'center',
			marginTop: height / 40,
			marginBottom: height / 40
		},
		titleWrapper: {
			width: '100%',
			// height: 33,
			alignItems: 'center',
			justifyContent: 'center',
			// marginTop: 50,
			//borderBottomWidth: StyleSheet.hairlineWidth,
			//borderColor: colors.border.muted,
		},
		wrapperModal: {
			flex: 1,
			paddingHorizontal: 32,
		},
		titleModal: {
			width: '100%',
			height: 33,
			alignItems: 'center',
			justifyContent: 'center',
			borderBottomWidth: StyleSheet.hairlineWidth,
			borderColor: colors.border.muted,
		},
		input: {
			...fontStyles.normal,
			fontSize: 12,
			paddingTop: 2,
			color: 'white',
			borderRadius: 10,
			borderWidth: 1,
			borderColor: colors.border.default,
			textAlign: 'center',
			minHeight: 40,
		},
		container: {
			justifyContent: 'center',
			alignItems: 'center',
			marginTop: 20,
			marginBottom: 20,
		},
		mainWrapper: {
			backgroundColor: '#1B1B23',
			flex: 1,
		},
		wrapperWellcome: {
			flex: 1,
			paddingHorizontal: 32,
		},
		containerWellcome: {
			justifyContent: 'center',
			alignItems: 'center',
		},
		likeIcon: {
			marginTop: 250,
			marginBottom: 30,
		},
		ctaWrapper: {
			marginTop: 250,
			marginBottom: 30,
		},
		walletTitle: {
			fontSize: 15,
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
			color: 'white',
		},
		errorPhrase: {
			fontSize: 12,
			borderRadius: 10,
			...fontStyles.normal,
			paddingTop: 10,
		},
		errorLabel: {
			color: 'red',
		},
		fieldRow: {
			flexDirection: 'row',
			alignItems: 'flex-end',
			justifyContent: 'center',
			textAlign: 'center',
		},
		dragger: {
			width: 48,
			height: 5,
			borderRadius: 4,
			backgroundColor: colors.border.default,
			opacity: 1,
		},
		accountsWrapper: {
			flex: 1,
		},
		footer: {
			height: Device.isIphoneX() ? 200 : 170,
			paddingBottom: Device.isIphoneX() ? 30 : 0,
			justifyContent: 'flex-start',
			flexDirection: 'column',
			alignItems: 'flex-start',
		},
		btnText: {
			fontSize: 14,
			color: colors.primary.default,
			...fontStyles.normal,
		},
		footerButton: {
			width: '100%',
			marginLeft: 20,
			padding: 10,
			height: 55,
			alignItems: 'flex-start',
			justifyContent: 'flex-start',
			//borderTopWidth: StyleSheet.hairlineWidth,
			borderColor: colors.border.muted,
		},
		walletInfo: {
			borderColor: colors.border.muted,
			flexDirection: 'row',
			paddingHorizontal: 20,
			paddingVertical: 20,
			height: 80,
		},
		accountInfo: {
			marginLeft: width /40,
			marginRight: 0,
			flex: 1,
			flexDirection: 'row',
			marginTop: height / 120
		},
		accountMain: {
			flex: 1,
			flexDirection: 'column',
		},
		accountNameWrapper: {
			flexDirection: 'row',
			justifyContent: 'space-between',
		},
		accountLabelConfirm: {
			fontSize: 16,
			color: '#FFFFFF',
			...fontStyles.bold,
		},
		accountBalanceWrapper: {
			display: 'flex',
			flexDirection: 'row',
		},
		address: {
			fontSize: 10,
			color: '#A0A0B1',
			...fontStyles.normal,
			letterSpacing: 0.8,
		},
		bottomModal: {
			justifyContent: 'flex-end',
			margin: 0,
		},
		wrapperModalView: {
			backgroundColor: '#1B1B23',
			borderTopLeftRadius: 10,
			borderTopRightRadius: 10,
			minHeight: '40%',
		},
		titleWrapperView: {
			width: '100%',
			// height: 33,
			alignItems: 'center',
			justifyContent: 'center',
			//borderBottomWidth: StyleSheet.hairlineWidth,
			//borderColor: colors.border.muted,
		},
		textTitleWrapper: {
			color: '#FFFFFF', 
			fontSize: 16, 
			...fontStyles.semiBold,
			// padding: 10 
		},
		wrapperLockString: {
			borderRadius: 20, 
			alignItems:'center', 
			justifyContent: 'center', 
			backgroundColor: '#deac2c26', 
			height: 22, 
			marginTop: height / 70, 
			marginRight: width / 50
		},
		wrapperTextLockString: { 
			color: '#FFBC39', 
			paddingHorizontal: 8, 
			fontSize: 12, 
			...fontStyles.semiBold, 
			lineHeight: 14.52
		},
		wrapperPrivateKey: {
			borderRadius: 20, 
			alignItems:'center', 
			justifyContent: 'center', 
			backgroundColor: '#0F2646', 
			height: 22, 
			marginTop: height / 70
		},
		wrapperPrivateKeyString: { 
			color: '#2F80ED', 
			paddingHorizontal: 8, 
			fontSize: 12, 
			...fontStyles.semiBold, 
			lineHeight: 14.52
		},
		wrapperSelecTion: { 
			color: '#FFFFFF', 
			fontSize: 16, 
			...fontStyles.bold 
		},
		borderBlockHorizontal: {
			width: '100%',
			height: 1,
			opacity: 1,
			backgroundColor: '#282832'
		},
		chooseText: { 
			color: '#FFFFFF', 
			fontSize: GLOBAL_ITEM_FONT_SIZE, 
			...fontStyles.bold 
		}
	});

/**
 * View that contains the list of all the available accounts
 */
class RevealAccountList extends PureComponent {
	static propTypes = {
		/**
		/* navigation object required to push new views
		*/
		navigation: PropTypes.object,
		/**
		 * Map of accounts to information objects including balances
		 */
		accounts: PropTypes.object,
		/**
		 * An object containing each identity in the format address => account
		 */
		identities: PropTypes.object,
		/**
		 * A string representing the selected address => account
		 */
		selectedAddress: PropTypes.string,
		/**
		 * An object containing all the keyrings
		 */
		keyrings: PropTypes.array,
		/**
		 * function to be called when switching accounts
		 */
		onAccountChange: PropTypes.func,
		/**
		 * function to be called when importing an account
		 */
		onImportAccount: PropTypes.func,
		/**
		 * function to be called when connect to a QR hardware
		 */
		onConnectHardware: PropTypes.func,
		/**
		 * Current provider ticker
		 */
		ticker: PropTypes.string,
		/**
		 * Whether it will show options to create or import accounts
		 */
		enableAccountsAddition: PropTypes.bool,
		/**
		 * function to generate an error string based on a passed balance
		 */
		getBalanceError: PropTypes.func,
		/**
		 * Indicates whether third party API mode is enabled
		 */
		thirdPartyApiMode: PropTypes.bool,
		/**
		 * ID of the current network
		 */
		network: PropTypes.string,
		hideModal: PropTypes.func,
	};

	state = {
		selectedAccountIndex: 0,
		loading: false,
		error: null,
		orderedAccounts: {},
		accountsENS: {},
		modalVisible: false,
		showWellcomeScreen: false,
		selectOptionModalVisible: false,
	};

	flatList = React.createRef();
	lastPosition = 0;
	updating = false;

	getInitialSelectedAccountIndex = () => {
		const { identities, selectedAddress } = this.props;
		Object.keys(identities).forEach((address, i) => {
			if (selectedAddress === address) {
				this.mounted && this.setState({ selectedAccountIndex: i });
			}
		});
	};

	componentDidMount() {
		this.mounted = true;
		this.getInitialSelectedAccountIndex();
		const orderedAccounts = this.getAccounts();
		InteractionManager.runAfterInteractions(() => {
			this.assignENSToAccounts(orderedAccounts);
			if (orderedAccounts.length > 4) {
				this.scrollToCurrentAccount();
			}
		});
		this.mounted && this.setState({ orderedAccounts });
	}

	componentWillUnmount = () => {
		this.mounted = false;
	};

	scrollToCurrentAccount() {
		// eslint-disable-next-line no-unused-expressions
		this.flatList?.current?.scrollToIndex({ index: this.state.selectedAccountIndex, animated: true });
	}

	isImported(allKeyrings, address) {
		let ret = false;
		for (const keyring of allKeyrings) {
			if (keyring.accounts.includes(address)) {
				ret = keyring.type === KeyringTypes.simple;
				break;
			}
		}

		return ret;
	}

	isQRHardware(allKeyrings, address) {
		let ret = false;
		for (const keyring of allKeyrings) {
			if (keyring.accounts.includes(address)) {
				ret = keyring.type === KeyringTypes.qr;
				break;
			}
		}

		return ret;
	}

	onStartButton = () => {
		this.setState({ showWellcomeScreen: false });
		this.toggleWalletModal();
	};

	onReveal(revealType, item) {
		// this.props.navigation.navigate('RevealPrivateCredentialView', { privateCredentialName: revealType });
		if (item == null) {
			const { orderedAccounts } = this.state;
			item = orderedAccounts[this.state.selectedAccountIndex];
		}
		this.props.hideModal();
		this.toggleSelectOptionModal();
		this.props.navigation.navigate('RevealPrivateCredentialView', { revealType, account: item });
	}

	onRowPress(item) {
		this.setState({ selectedAccountIndex: item.index });
		if (item.isContainSeedphrase) {
			this.toggleSelectOptionModal();
		} else {
			this.toggleSelectOptionModal();
			this.onReveal('private_key', item);
		}
	}

	renderItem = ({ item }) => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		return (
			<View>
				<TouchableOpacity style={styles.walletInfo} onPress={() => this.onRowPress(item)}>
					{/* <Identicon address={item.address} diameter={38} /> */}
					<Avatar address={item.address}/>
					<View style={styles.accountInfo}>
						<View style={styles.accountMain}>
							<View style={styles.accountNameWrapper}>
								<Text style={styles.accountLabelConfirm} numberOfLines={1}>
									{item.name}
								</Text>
							</View>
							<View style={styles.accountBalanceWrapper}>
								<EthereumAddress address={item.address} style={styles.address} type={'short'} />
							</View>
						</View>
					</View>
					{item.isContainSeedphrase && (
						<View style={styles.wrapperLockString}>
							<Text style={styles.wrapperTextLockString}>{'Chuỗi khoá'}</Text>
						</View>
					)}
					<View style={styles.wrapperPrivateKey}>
						<Text style={styles.wrapperPrivateKeyString}>{'Private key'}</Text>
					</View>
				</TouchableOpacity>
			</View>
		);
	};

	isContainSeedphrase(addr) {
		const keyrings = Engine.context.KeyringController.state.keyrings;
		const keyring = keyrings.find((k) => k.accounts.indexOf(addr) > -1);
		return keyring?.accounts?.indexOf(addr) === 0;
	}

	getAccounts() {
		const { accounts, identities, selectedAddress, keyrings, getBalanceError } = this.props;
		// This is a temporary fix until we can read the state from @metamask/controllers
		const allKeyrings = keyrings && keyrings.length ? keyrings : Engine.context.KeyringController.state.keyrings;

		const accountsOrdered = allKeyrings.reduce((list, keyring) => list.concat(keyring.accounts), []);
		return accountsOrdered
			.filter((address) => !!identities[toChecksumAddress(address)])
			.map((addr, index) => {
				const checksummedAddress = toChecksumAddress(addr);
				const identity = identities[checksummedAddress];
				const { name, address } = identity;
				const identityAddressChecksummed = toChecksumAddress(address);
				const isSelected = identityAddressChecksummed === selectedAddress;
				const isImported = this.isImported(allKeyrings, identityAddressChecksummed);
				const isQRHardware = this.isQRHardware(allKeyrings, identityAddressChecksummed);
				let balance = 0x0;
				if (accounts[identityAddressChecksummed]) {
					balance = accounts[identityAddressChecksummed].balance;
				}
				const isContainSeedphrase = this.isContainSeedphrase(addr);
				const balanceError = getBalanceError ? getBalanceError(balance) : null;
				return {
					index,
					name,
					address: identityAddressChecksummed,
					balance,
					isSelected,
					isImported,
					isQRHardware,
					balanceError,
					isContainSeedphrase,
				};
			});
	}

	assignENSToAccounts = (orderedAccounts) => {
		const { network } = this.props;
		orderedAccounts.forEach(async (account) => {
			try {
				const ens = await doENSReverseLookup(account.address, network);
				this.setState((state) => ({
					accountsENS: {
						...state.accountsENS,
						[account.address]: ens,
					},
				}));
			} catch {
				// Error
			}
		});
	};

	toggleWalletModal = () => this.setState((state) => ({ modalVisible: !state.modalVisible }));

	toggleSelectOptionModal = () => {
		this.setState((state) => ({ selectOptionModalVisible: !state.selectOptionModalVisible }));
	};

	keyExtractor = (item) => item.address;

	render() {
		const { orderedAccounts, error, wallet } = this.state;
		const { enableAccountsAddition } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		const themeAppearance = this.context.themeAppearance || 'light';
		const onReach_MAX_Length = (temp) => {
			this.setState({ wallet: temp });
			const tempLength = temp.length.toString();
			let error = null;
			if (tempLength === 18) {
				error = strings('accounts.input_max_length');
			}
			this.setState({ error });
		};
		return (
			<>
				<View style={styles.titleWrapper}>
						<View style={styles.dragger} testID={'account-list-dragger'} />
				</View>
				<SafeAreaView style={styles.wrapper} testID={'account-list'}>
				
					<View style={styles.title}>
						<Text style={styles.textTitleWrapper}>{strings('accounts.choose_wallet')}</Text>
					</View>
					<FlatList
						data={orderedAccounts}
						keyExtractor={this.keyExtractor}
						renderItem={this.renderItem}
						ref={this.flatList}
						style={styles.accountsWrapper}
						testID={'account-number-button'}
						getItemLayout={(_, index) => ({ length: 80, offset: 80 * index, index })} // eslint-disable-line
					/>
					<Modal
						isVisible={this.state.selectOptionModalVisible}
						style={styles.bottomModal}
						onBackdropPress={this.toggleSelectOptionModal}
						onBackButtonPress={this.toggleSelectOptionModal}
						onSwipeComplete={this.toggleSelectOptionModal}
						swipeDirection={'down'}
						propagateSwipe
						backdropColor={colors.overlay.default}
						backdropOpacity={1}
					>
						<>
							<View style={styles.titleWrapperView}>
									<View style={styles.dragger} />
								</View>
							<SafeAreaView style={styles.wrapperModalView}>
								<View style={styles.title}>
									<Text style={styles.wrapperSelecTion}>Chọn hiển thị</Text>
								</View>
								<View style={styles.borderBlockHorizontal}/>
								<View style={styles.title}>
									<TouchableOpacity onPress={() => this.onReveal('seed_phrase')} 
										style={{
											width:'100%', 
											height: height/ 30,
											justifyContent: 'center',
											alignItems: 'center',
										}}>
										<Text style={styles.chooseText}>Chuỗi Khoá</Text>
									</TouchableOpacity>
								</View>
								<View style={styles.borderBlockHorizontal}/>
								<View style={styles.title}>
									<TouchableOpacity onPress={() => this.onReveal('private_key')}>
										<Text style={styles.chooseText}>Private Key</Text>
									</TouchableOpacity>
								</View>
								<View style={styles.borderBlockHorizontal}/>
							</SafeAreaView>
						</>
					</Modal>
				</SafeAreaView>
			</>
		);
	}
}

RevealAccountList.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	thirdPartyApiMode: state.privacy.thirdPartyApiMode,
	network: state.engine.backgroundState.NetworkController.network,
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
	accounts: state.engine.backgroundState.AccountTrackerController.accounts,
	identities: state.engine.backgroundState.PreferencesController.identities,
	keyrings: state.engine.backgroundState.KeyringController.keyrings,
});

export default connect(mapStateToProps)(RevealAccountList);
