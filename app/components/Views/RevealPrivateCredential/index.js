import React, { PureComponent } from 'react';
import {
	Text,
	View,
	SafeAreaView,
	StyleSheet,
	ActivityIndicator,
	InteractionManager,
	Appearance,
	Image,
	TouchableOpacity,
	Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fontStyles, GLOBAL_ITEM_FONT_SIZE } from '../../../styles/common';
import StyledButton from '../../UI/StyledButton';
import { strings } from '../../../../locales/i18n';
import FeatherIcons from 'react-native-vector-icons/Feather';
import { BlurView } from '@react-native-community/blur';
import ActionView from '../../UI/ActionView';
import Device from '../../../util/device';
import Engine from '../../../core/Engine';
import PreventScreenshot from '../../../core/PreventScreenshot';
import SecureKeychain from '../../../core/SecureKeychain';
import { getOnboardingNavbarOptions, getSetNameWallet } from '../../UI/Navbar';
import {
	MANUAL_BACKUP_STEPS,
	SEED_PHRASE,
	CONFIRM_PASSWORD,
	WRONG_PASSWORD_ERROR,
} from '../../../constants/onboarding';
import AnalyticsV2 from '../../../util/analyticsV2';
import { ThemeContext, mockTheme } from '../../../util/theme';
import ClipboardManager from '../../../core/ClipboardManager';
import Logger from '../../../util/Logger';
import Button from 'react-native-button';
import CheckBox from '@react-native-community/checkbox';
import { seedphraseBackedUp } from '../../../actions/user';
import { showAlert } from '../../../actions/alert';
import { isNull } from 'url/util';
import AsyncStorage from '@react-native-community/async-storage';
import { BIOMETRY_CHOICE } from '../../../constants/storage';
import PINCode, { hasUserSetPinCode } from '@haskkor/react-native-pincode';
import IonicIcon from 'react-native-vector-icons/Ionicons';
import Identicon from '../../UI/Identicon';
import EthereumAddress from '../../UI/EthereumAddress';
import Avatar from '../../../util/avatar';

const {width, height} = Dimensions.get('window')

const createStyles = (colors) =>
	StyleSheet.create({
		// eslint-disable-next-line react-native/no-color-literals
		mainWrapper: {
			backgroundColor: '#1B1B23',
			flex: 1,
		},
		titleWrapper: {
			width: '100%',
			height: 33,
			alignItems: 'center',
			justifyContent: 'center',
			borderBottomWidth: StyleSheet.hairlineWidth,
			borderColor: colors.border.muted,
		},
		btnDone: {
			flexDirection: 'row',
			width: '100%',
		},
		account: {
			justifyContent: 'center',
			marginTop: height / 30,
			flexDirection: 'row',
			marginBottom: height / 20
		},
		copyButtonWrap: {
			width: '100%',
			height: 50,
			alignItems: 'center',
			justifyContent: 'center',
		},
		dragger: {
			width: 48,
			height: 5,
			borderRadius: 4,
			backgroundColor: colors.border.default,
			opacity: 1
		},
		// eslint-disable-next-line react-native/no-color-literals
		wrapper: {
			flex: 1,
			paddingHorizontal: 32,
			backgroundColor: '#1B1B23',
		},
		onBoardingWrapper: {
			paddingHorizontal: 20,
		},
		loader: {
			backgroundColor: colors.background.default,
			flex: 1,
			minHeight: 300,
			justifyContent: 'center',
			alignItems: 'center',
		},
		// eslint-disable-next-line react-native/no-color-literals
		action: {
			flex: 10,
			fontSize: 16,
			marginVertical: height / 40,
			color: '#FFFFFF',
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.semiBold,
			marginLeft : width / 9.5
		},
		privateKeyTitle: {
			flex: 10,
			fontSize: 16,
			marginVertical: height / 40,
			color: '#FFFFFF',
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.semiBold,
			marginLeft : width / 9.5
		},
		infoWrapper: {
			marginBottom: 16,
			justifyContent: 'center',
		},
		info: {
			fontSize: 14,
			color: colors.text.default,
			textAlign: 'center',
			...fontStyles.normal,
			paddingHorizontal: 6,
		},
		seedPhraseConcealerContainer: {
			position: 'absolute',
			width: '100%',
			height: '100%',
			borderRadius: 8,
		},
		seedPhraseConcealer: {
			position: 'absolute',
			width: '100%',
			height: '100%',
			backgroundColor: colors.overlay.alternative,
			alignItems: 'center',
			borderRadius: 8,
			paddingHorizontal: 24,
			paddingVertical: 45,
		},
		blurView: {
			position: 'absolute',
			top: 0,
			left: 0,
			bottom: 0,
			right: 0,
			borderRadius: 8,
		},
		textDoneWrapper: { color: '#B1B5FF', marginTop: height/ 40, fontSize: GLOBAL_ITEM_FONT_SIZE, ...fontStyles.semiBold },
		icon: {
			width: 24,
			height: 24,
			color: colors.overlay.inverse,
			textAlign: 'center',
			marginBottom: 32,
		},
		reveal: {
			fontSize: Device.isMediumDevice() ? 13 : 16,
			...fontStyles.bold,
			color: colors.overlay.inverse,
			lineHeight: 22,
			marginBottom: 8,
			textAlign: 'center',
		},
		watching: {
			fontSize: Device.isMediumDevice() ? 10 : 12,
			color: colors.overlay.inverse,
			lineHeight: 17,
			marginBottom: 32,
			textAlign: 'center',
		},
		viewButtonContainer: {
			width: 155,
			padding: 12,
		},
		seedPhraseWrapper: {
			borderRadius: 12,
			flexDirection: 'row',
			borderColor: '#FFFFFF26',
			borderWidth: 1,
			marginBottom: height / 40,
			minHeight: height / 3.3,
			width: width/ 1.5,
			marginLeft: width / 8.6,
			shadowColor: 'red',
			shadowOpacity: 1,
			shadowRadius: 39,
			shadowOffset: {
				width: 0,
				height: 4
			},
		},
		seedPhraseWrapperPrivate: {
			borderRadius: 8,
			flexDirection: 'row',
			borderColor: '#FFFFFF26',
			borderWidth: 1,
			marginBottom: height / 40,
			minHeight: height / 12,
			width: width * 0.85,
			// marginLeft: - width / 30,
			shadowColor: "#000000",
			shadowOpacity: 1,
			shadowRadius: 39,
			shadowOffset: {
				width: 0,
				height: 4
			},
			elevation: 20,
			marginTop: height / 10,
			alignItems: 'center',
			justifyContent: 'center'
		},
		wordColumn: {
			flex: 1,
			alignItems: 'flex-start',
			paddingHorizontal: Device.isMediumDevice() ? 18 : 24,
			paddingVertical: 18,
			justifyContent: 'center',
			marginLeft: 5,
			// backgroundColor: 'red'
		},
		wordWrapper: {
			flexDirection: 'row',
			// alignItems: 'flex-start'
			// marginBottom : - height / 100,
		},
		word: {
			// paddingHorizontal: 8,
			paddingVertical: 12,
			fontSize: 14,
			color: '#FFFFFF',
			...fontStyles.semiBold,
			
		},
		wordTxt:{
			paddingVertical: 16,
			paddingHorizontal: 12,
			fontSize: 14,
			color: '#FFFFFF',
			...fontStyles.semiBold,
			textAlign: 'center'
		},
		wordIndex: {
			fontSize: 14,
			color: '#58586C',
			paddingVertical: 12,
			// borderColor: colors.primary.default,
			// borderRadius: 13,
			// textAlign: 'center',
			// textAlignVertical: 'center',
			...fontStyles.normal,
			marginLeft:10,
			marginRight: 5
			// lineHeight: 14,
		},
		confirmPasswordWrapper: {
			flex: 1,
			padding: 30,
			paddingTop: 0,
		},
		passwordRequiredContent: {
			marginBottom: 20,
		},
		content: {
			alignItems: 'flex-start',
		},
		title: {
			fontSize: 32,
			marginTop: 20,
			marginBottom: 10,
			color: colors.text.default,
			justifyContent: 'center',
			textAlign: 'left',
			...fontStyles.normal,
		},
		text: {
			marginBottom: 10,
			marginTop: 20,
			justifyContent: 'center',
		},
		label: {
			fontSize: 16,
			lineHeight: 23,
			color: colors.text.default,
			textAlign: 'left',
			...fontStyles.normal,
		},
		buttonWrapper: {
			flex: 1,
			marginTop: 20,
			justifyContent: 'flex-end',
		},
		input: {
			borderWidth: 2,
			borderRadius: 5,
			width: '100%',
			borderColor: colors.border.default,
			padding: 10,
			height: 40,
		},
		warningMessageText: {
			paddingVertical: 10,
			color: '#FFFFFF',
			...fontStyles.normal,
		},
		keyboardAvoidingView: {
			flex: 1,
			flexDirection: 'row',
			alignSelf: 'center',
		},
		copyAction: {
			flexDirection: 'row',
			marginVertical: 14,
			color: '#B1B5FF',
			textAlign: 'center',
			alignItems: 'center',
			...fontStyles.normal,
		},
		copyText: {
			color: '#FFFFFF',
			margin: 10,
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			lineHeight: 17,
			...fontStyles.semiBold,
		},
		saveSeedphrase: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		saveSeedphraseCheckbox: {
			height: 18,
			width: 18,
			marginRight: 12,
			marginTop: 3,
		},
		saveSeedphraseText: {
			color: '#FFFFFF',
		},
		accountInfo: {
			marginLeft: 15,
			marginRight: 0,
			flex: 1,
			flexDirection: 'row',
		},
		accountMain: {
			flex: 1,
			flexDirection: 'column',
		},
		accountNameWrapper: {
			flexDirection: 'column',
			marginTop: height /90
		},
		accountLabelConfirm: {
			fontSize: 16,
			color: '#FFFFFF',
			...fontStyles.bold,
			marginLeft : width/ 40,
		},
		accountBalanceWrapper: {
			
		},
		address: {
			fontSize: 10,
			color: '#A0A0B1',
			...fontStyles.normal,
			letterSpacing: 0.8,
			marginLeft: width / 40
		},
		dots: {
			width: 12,
			height: 12,
			borderRadius: 6,
			backgroundColor: '#1B1B23',
			borderColor: '#FFFFFF',
			borderWidth: 2,
		},
		titlePinCode: {
			fontSize: 24,
			...fontStyles.bold,
			lineHeight: 29,
			color: '#FFFFFF',
			opacity:1
		},
		subtitlePinCode: {
			fontSize: 16,
			...fontStyles.normal,
			lineHeight: 20,
			color: '#58586C',
			opacity:1
		},
		pinCodeTextButtonCircle:{
			fontSize: 20,
			...fontStyles.normal,
			color: '#FFFFFF',
			opacity:1
		},
		pinCodeButtonCircle:{
			backgroundColor: '#1B1B23',
			borderWidth: 1,
			borderColor: '#3D3D4B',
			// width: 72,
			// height: 72,
			borderRadius: 36,
			opacity:1
		},
		pinCodeDeleteButtonText:{
			color:'#FFFFFF', 
			fontSize:16, 
			lineHeight:20, 
			...fontStyles.normal
		}
	});

/**
 * View that's shown during the second step of
 * the backup seed phrase flow
 */
class RevealPrivateCredential extends PureComponent {
	static propTypes = {
		/**
		/* navigation object required to push and pop other views
		 */
		navigation: PropTypes.object,
		/**
		 * Object that represents the current route info like params passed to it
		 */
		route: PropTypes.object,
		/**
		 * Theme that app is set to
		 */
		appTheme: PropTypes.string,
		seedphraseBackedUp: PropTypes.func,
		showAlert: PropTypes.func,
		selectedAddress: PropTypes.string,
	};

	steps = MANUAL_BACKUP_STEPS;

	state = {
		seedPhraseHidden: true,
		currentStep: 1,
		password: undefined,
		warningIncorrectPassword: undefined,
		ready: false,
		view: SEED_PHRASE,
		skipCheckbox: false,
	};

	updateNavBar = () => {
		const { route, navigation } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(getSetNameWallet(route, {}, colors));
	};

	async componentDidMount() {
		this.updateNavBar();

		const biometryType = await SecureKeychain.getSupportedBiometryType();
		const biometryChoice = await AsyncStorage.getItem(BIOMETRY_CHOICE);
		Logger.log(biometryType);
		if (isNull(biometryType) || isNull(biometryChoice) || !biometryChoice) {
			this.setState({ view: CONFIRM_PASSWORD, ready: true });
		} else {
			this.words = this.props.route.params?.words ?? [];
			const credentials = await SecureKeychain.getGenericPassword();
			this.privateKey = await this.tryExportPrivateKey(credentials.password);
			if (!this.words.length) {
				try {
					if (credentials) {
						this.words = await this.tryExportSeedPhrase(credentials.password);
					} else {
						this.setState({ view: CONFIRM_PASSWORD });
					}
				} catch (e) {
					this.setState({ view: CONFIRM_PASSWORD });
				}
			}
			this.setState({ ready: true });
			InteractionManager.runAfterInteractions(() => PreventScreenshot.forbid());
		}
	}

	componentDidUpdate = () => {
		this.updateNavBar();
	};

	onPasswordChange = (password) => {
		this.setState({ password });
	};

	goNext = () => {
		const { seedphraseBackedUp } = this.props;
		seedphraseBackedUp();
		this.props.navigation.navigate('WalletView', {});
	};

	revealSeedPhrase = () => {
		this.setState({ seedPhraseHidden: false });
		InteractionManager.runAfterInteractions(() => {
			AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SECURITY_PHRASE_REVEALED);
		});
	};

	tryExportSeedPhrase = async (password) => {
		const { KeyringController } = Engine.context;
		const { selectedAddress } = this.props;
		let address = this.props.route?.params?.account.address;
		if (!address) address = selectedAddress;

		let mnemonic = await KeyringController.exportSeedPhrase(password);
		try {
			const accKeyring = await KeyringController.getAccountKeyring(address);
			// Dont know why it has to be like that
			let privKey = accKeyring.hdWallet._hdkey;
			privKey = JSON.parse(JSON.stringify(privKey));
			mnemonic = await KeyringController.exportSeedPhraseByWallet(password, privKey.xpriv);
		} finally {
			// const seed = JSON.stringify(mnemonic).replace(/"/g, '').split(' ');
			// return seed;
		}
		const seed = JSON.stringify(mnemonic).replace(/"/g, '').split(' ');
		return seed;
	};

	tryExportPrivateKey = async (password) => {
		const { KeyringController } = Engine.context;
		const { selectedAddress } = this.props;
		let address = this.props.route?.params?.account.address;
		if (!address) address = selectedAddress;

		const privateKey = await KeyringController.exportAccount(password, address);
		return privateKey;
	};

	tryUnlockWithPassword = async (password) => {
		this.setState({ ready: false });
		try {
			this.words = await this.tryExportSeedPhrase(password);
			this.privateKey = await this.tryExportPrivateKey(password);
			this.setState({ view: SEED_PHRASE, ready: true });
		} catch (e) {
			let msg = strings('reveal_credential.warning_incorrect_password');
			if (e.toString().toLowerCase() !== WRONG_PASSWORD_ERROR.toLowerCase()) {
				msg = strings('reveal_credential.unknown_error');
			}
			this.setState({
				warningIncorrectPassword: msg,
				ready: true,
			});
		}
	};

	tryUnlock = async (pin) => {
		//const { password } = this.state;
		const hasPin = await hasUserSetPinCode();
		if (hasPin) {
			this.tryUnlockWithPassword(pin);
		}
	};

	renderLoader = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<View style={styles.loader}>
				<ActivityIndicator size="small" />
			</View>
		);
	};

	getBlurType = () => {
		const { appTheme } = this.props;
		let blurType = 'light';
		switch (appTheme) {
			case 'light':
				blurType = 'light';
				break;
			case 'dark':
				blurType = 'dark';
				break;
			case 'os':
				blurType = Appearance.getColorScheme();
				break;
			default:
				blurType = 'light';
		}
		return blurType;
	};

	copySeedphraseToClipboard = async () => {
		const words = this.words || [];
		const wordStr = words.join(' ');
		Logger.log('this.words: ' + wordStr);

		await ClipboardManager.setString(wordStr);
		this.props.showAlert({
			isVisible: true,
			autodismiss: 1500,
			content: 'clipboard-alert',
			data: { msg: strings('account_details.account_copied_to_clipboard') },
		});
	};
	copyPrivateKeyToClipboard = async () => {
		const words = this.privateKey || [];
		await ClipboardManager.setString(words);
		this.props.showAlert({
			isVisible: true,
			autodismiss: 1500,
			content: 'clipboard-alert',
			data: { msg: strings('account_details.account_copied_to_clipboard') },
		});
	};

	toggleSkipCheckbox = () => {
		const { skipCheckbox } = this.state;
		this.setState(() => ({ skipCheckbox: !skipCheckbox }));
	};

	renderConfirmPassword() {
		const { warningIncorrectPassword } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);

		return (
			// <PINCode
			// 	titleEnter={'Nhập mã PIN'}
			// 	subtitleEnter={'Mã gồm 6 chữ số'}
			// 	titleAttemptFailed={'Mã PIN không khớp'}
			// 	subtitleError={'Vui lòng thử lại'}
			// 	passwordLength={6}
			// 	stylePinCodeCircle={styles.dots}
			// 	disableLockScreen
			// 	status={'enter'}
			// 	touchIDDisabled
			// 	finishProcess={(pin) => this.tryUnlock(pin)}
			// />
			<PINCode
                titleChoose={strings('pin_code.title_choose')}
                subtitleChoose={strings('pin_code.subtitle')}
                titleConfirm={strings('pin_code.title_confirm')}
                subtitleConfirm={strings('pin_code.subtitle_confirm')}
                titleConfirmFailed={strings('pin_code.title_confirm_failed')}
                subtitleError={strings('pin_code.subtitl_error')}
                buttonDeleteText={strings('pin_code.button_delete_text')}
                passwordLength={6}
                disableLockScreen
                status={'enter'}
                touchIDDisabled
                finishProcess={(pin) => this.tryUnlock(pin)}
                stylePinCodeTextTitle={styles.titlePinCode}
                stylePinCodeTextSubtitle={styles.subtitlePinCode}
                stylePinCodeCircle={styles.dots}
                stylePinCodeTextButtonCircle={styles.pinCodeTextButtonCircle}
                stylePinCodeButtonCircle={styles.pinCodeButtonCircle}
                stylePinCodeDeleteButtonText={styles.pinCodeDeleteButtonText}					
            />
		);
	}

	goBack = () => {
		this.props.navigation.navigate('Home');
	};

	renderSeedphraseView = () => {
		let revealType = this.props.route?.params?.revealType;
		if (!revealType) revealType = 'private_key';
		const account = this.props.route?.params?.account;
		const words = this.words || [];
		const wordLength = words.length;
		const half = wordLength / 2 || 6;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<View style={styles.wrapper} testID={'manual_backup_step_1-screen'}>
				<View style={styles.btnDone}>
					<Text style={revealType === 'seed_phrase' ? styles.action : styles.privateKeyTitle}>{revealType === 'seed_phrase' ? 'Chuỗi khoá' : 'Private Key'}</Text>
					<TouchableOpacity onPress={this.goBack}>
						<Text style={styles.textDoneWrapper}>{strings('token.done')}</Text>
					</TouchableOpacity>
				</View>

				<View style={styles.account}>
					{/* <Identicon address={account.address} diameter={38} /> */}
					<Avatar address={account.address} />
					<View style={styles.accountNameWrapper}>
						<Text style={styles.accountLabelConfirm} numberOfLines={1}>
							{account.name}
						</Text>
						<View style={styles.accountBalanceWrapper}>
							<EthereumAddress address={account.address} style={styles.address} type={'short'} />
						</View>
					</View>
					
				</View>
				{revealType === 'seed_phrase' && (
					<>
						<View style={styles.seedPhraseWrapper}>
							<View style={styles.wordColumn}>
								{this.words.slice(0, half).map((word, i) => (
									<View key={`word_${i}`} style={styles.wordWrapper}>
										<Text style={styles.wordIndex}>{`${i + 1}`}</Text>
										<Text style={styles.word}>{`${word}`}</Text>
									</View>
								))}
							</View>
							<View style={styles.wordColumn}>
								{this.words.slice(-half).map((word, i) => (
									<View key={`word_${i}`} style={styles.wordWrapper}>
										<Text style={styles.wordIndex}>{`${i + (half + 1)}`}</Text>
										<Text style={styles.word}>{`${word}`}</Text>
										{/* <Text style={styles.word}>{`${i + (half + 1)} ${word}`}</Text> */}
									</View>
								))}
							</View>
						</View>
						<View style={styles.copyButtonWrap}>
							<Button style={styles.copyAction} onPress={this.copySeedphraseToClipboard}>
								<Image source={require('../../../images-new/copy-key.png')} resizeMethod={'auto'} style={{width:20, height: 20}}/>
								<Text style={styles.copyText}>{strings('Copy')}</Text>
							</Button>
						</View>
					</>
				)}
				{revealType === 'private_key' && (
					<>
						<View style={styles.seedPhraseWrapperPrivate}>
							<Text style={styles.wordTxt}>{this.privateKey}</Text>
						</View>
						<View style={styles.copyButtonWrap}>
							<Button style={styles.copyAction} onPress={this.copyPrivateKeyToClipboard}>
							<Image source={require('../../../images-new/copy-key.png')} resizeMethod={'auto'} style={{width:20, height: 20}}/>
								<Text style={styles.copyText}>{strings('Copy')}</Text>
							</Button>
						</View>
					</>
				)}
			</View>
		);
	};

	render() {
		const { ready, currentStep, view } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (!ready) return this.renderLoader();
		return (
			<SafeAreaView style={styles.mainWrapper}>
				<View style={styles.titleWrapper}>
					<View style={styles.dragger} testID={'account-list-dragger'} />
				</View>
				{view === SEED_PHRASE ? this.renderSeedphraseView() : this.renderConfirmPassword()}
			</SafeAreaView>
		);
	}
}

const propTypes = {
	toggleSaveSeedphraseCheckbox: PropTypes.func,
	saveSeedphrase: PropTypes.bool,
};

const mapStateToProps = (state) => ({
	appTheme: state.user.appTheme,
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
});

const mapDispatchToProps = (dispatch) => ({
	seedphraseBackedUp: () => dispatch(seedphraseBackedUp()),
	showAlert: (config) => dispatch(showAlert(config)),
});

RevealPrivateCredential.contextType = ThemeContext;

export default connect(mapStateToProps, mapDispatchToProps)(RevealPrivateCredential);
