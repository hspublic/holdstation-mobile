import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react';
import { ActivityIndicator, Image, RefreshControl, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { useSelector } from 'react-redux';
// import ScrollableTabView from 'react-native-scrollable-tab-view';
// import DefaultTabBar from 'react-native-scrollable-tab-view/DefaultTabBar';
import { strings } from '../../../../locales/i18n';
import { baseStyles, fontStyles } from '../../../styles/common';
import {
	balanceToFiatNumber,
	hexToBN,
	renderFromTokenMinimalUnit,
	renderFromWei,
	weiToFiat,
} from '../../../util/number';
import AccountOverview from '../../UI/AccountOverview';
import { getWalletNavbarOptions } from '../../UI/Navbar';
import NoticeDeposit from '../../UI/NoticeDeposit';
import Tokens from '../../UI/Tokens';

import Engine from '../../../core/Engine';
import { mockTheme, useAppThemeFromContext } from '../../../util/theme';
import { getTicker, getTokenByTicker } from '../../../util/transactions';
import { DrawerContext } from '../../Nav/Main/MainNavigator';
import CollectibleContracts from '../../UI/CollectibleContracts';
import OnboardingWizard from '../../UI/OnboardingWizard';
import ErrorBoundary from '../ErrorBoundary';
import LottieView from 'lottie-react-native';
import { EventEmitter } from 'events';
import AsyncStorage from '@react-native-community/async-storage';

const hub = new EventEmitter();

const createStyles = (colors: any) =>
	StyleSheet.create({
		wrapper: {
			flex: 1,
			backgroundColor: '#1B1B23',
			paddingBottom: 40,
		},
		container: {
			flex: 1,
			backgroundColor: '#1B1B23',
		},
		tabUnderlineStyle: {
			height: 2,
			backgroundColor: colors.primary.default,
		},
		tabStyle: {
			paddingBottom: 0,
		},
		tabBar: {
			borderColor: colors.border.muted,
		},
		textStyle: {
			fontSize: 12,
			letterSpacing: 0.5,
			...(fontStyles.bold as any),
		},
		loader: {
			backgroundColor: colors.background.default,
			flex: 1,
			justifyContent: 'center',
			alignItems: 'center',
		},
		containButton: {
			flex: 1,
			position: 'absolute',
			right: 0,
			bottom: 0,
			marginBottom: 40,
			marginRight: 20,
			flexDirection: 'row',
		},
		zoomIn: {
			transform: [{ scale: 0.9 }],
		},
	});

/**
 * Main view for the wallet
 */
const Wallet = ({ navigation, route }: any) => {
	const { drawerRef } = useContext(DrawerContext);
	const [refreshing, setRefreshing] = useState(false);
	const accountOverviewRef = useRef(null);
	const sendAnimation = useRef<any>(null);
	const swapAnimation = useRef<any>(null);
	const { colors } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);
	const [isPressSend, setIsPressSend] = useState(false);
	const [isPressSwap, setIsPressSwap] = useState(false);
	const [swapAnmation, setSwapAnmation] = useState(true);
	/**
	 * Map of accounts to information objects including balances
	 */
	const accounts = useSelector((state: any) => state.engine.backgroundState.AccountTrackerController.accounts);
	/**
	 * ETH to current currency conversion rate
	 */
	const conversionRate = useSelector(
		(state: any) => state.engine.backgroundState.CurrencyRateController.conversionRate
	);
	/**
	 * Currency code of the currently-active currency
	 */
	const currentCurrency = useSelector(
		(state: any) => state.engine.backgroundState.CurrencyRateController.currentCurrency
	);
	/**
	 * An object containing each identity in the format address => account
	 */
	const identities = useSelector((state: any) => state.engine.backgroundState.PreferencesController.identities);
	/**
	 * A string that represents the selected address
	 */
	const selectedAddress = useSelector(
		(state: any) => state.engine.backgroundState.PreferencesController.selectedAddress
	);
	/**
	 * An array that represents the user tokens
	 */
	const tokens = useSelector((state: any) => state.engine.backgroundState.TokensController.tokens);
	const tokenList = useSelector((state: any) => state.engine.backgroundState.TokenListController.tokenList);
	const tokenExchangeRates = useSelector(
		(state: any) => state.engine.backgroundState.TokenRatesController.contractExchangeRates
	);
	const tokenBalances = useSelector(
		(state: any) => state.engine.backgroundState.TokenBalancesController.contractBalances
	);

	const primaryCurrency = useSelector((state: any) => state.settings.primaryCurrency);
	tokens.forEach(
		(token: { verified: any; address: string }) =>
			(token.verified = token?.verified
				? token.verified
				: token.address
				? token.address in tokenList
					? tokenList[token.address].verified
					: token.address.toLowerCase() in tokenList
					? tokenList[token.address.toLowerCase()].verified
					: null
				: null)
	);

	tokens.forEach(
		(token: {
			secondaryBalanceSort: any;
			address: string;
			balance: string | number;
			decimals: number;
			balanceFiat: string;
			symbol: any;
			balanceError: any;
			secondaryBalance: any;
			verified: any;
		}) => {
			const exchangeRate = token.address in tokenExchangeRates ? tokenExchangeRates[token.address] : undefined;
			const balance =
				token.balance ||
				(token.address in tokenBalances
					? renderFromTokenMinimalUnit(tokenBalances[token.address], token.decimals)
					: 0);
			const balanceFiat = token.balanceFiat || balanceToFiatNumber(balance, conversionRate, exchangeRate);
			let secondaryBalance;
			if (primaryCurrency === 'ETH') {
				secondaryBalance = balanceFiat;
			} else {
				secondaryBalance = !balanceFiat ? balanceFiat : balance;
			}
			if (token?.balanceError) {
				secondaryBalance = strings('wallet.unable_to_load');
			}
			token.secondaryBalanceSort = secondaryBalance;
		}
	);

	
	/**
	 * Current provider ticker
	 */
	const ticker = useSelector((state: any) => state.engine.backgroundState.NetworkController.provider.ticker);
	/**
	 * Current onboarding wizard step
	 */
	const wizardStep = useSelector((state: any) => state.wizard.step);

	const { colors: themeColors } = useAppThemeFromContext() || mockTheme;

	const [collectibleContracts, setCollectibleContracts] = useState([]);

	useEffect(
		() => {
			requestAnimationFrame(async () => {
				const { TokenDetectionController, CollectibleDetectionController, AccountTrackerController } =
					Engine.context as any;
				TokenDetectionController.detectTokens();
				CollectibleDetectionController.detectCollectibles();
				AccountTrackerController.refresh();
			});
		},
		/* eslint-disable-next-line */
		[navigation]
	);

	useEffect(() => {
		navigation.setOptions(getWalletNavbarOptions('wallet.title', navigation, drawerRef, themeColors));
		const { CollectibleDetectionController } = Engine.context as any;
		// const fetchCollectiblesContract = async () => {
		// 	const response = await CollectibleDetectionController.getOwnerCollectibles(selectedAddress);
		// 	// console.log("Holdstation");
		// 	// console.log(response);
		// 	setCollectibleContracts(response);
		// };
		// fetchCollectiblesContract().catch(console.error);
	}, [navigation, themeColors, selectedAddress]);

	const onRefresh = useCallback(async () => {
		requestAnimationFrame(async () => {
			setRefreshing(true);
			const {
				TokenDetectionController,
				CollectibleDetectionController,
				AccountTrackerController,
				CurrencyRateController,
				TokenRatesController,
			} = Engine.context as any;
			const actions = [
				TokenDetectionController.detectTokens(),
				CollectibleDetectionController.detectCollectibles(),
				AccountTrackerController.refresh(),
				CurrencyRateController.start(),
				TokenRatesController.poll(),
			];
			await Promise.all(actions);
			setRefreshing(false);
		});
	}, [setRefreshing]);

	// const renderTabBar = useCallback(
	// 	() => (
	// 		<DefaultTabBar
	// 			underlineStyle={styles.tabUnderlineStyle}
	// 			activeTextColor={colors.primary.default}
	// 			inactiveTextColor={colors.text.muted}
	// 			backgroundColor={colors.background.default}
	// 			tabStyle={styles.tabStyle}
	// 			textStyle={styles.textStyle}
	// 			style={styles.tabBar}
	// 		/>
	// 	),
	// 	[styles, colors]
	// );

	// const onChangeTab = useCallback((obj) => {
	// 	InteractionManager.runAfterInteractions(() => {
	// 		if (obj.ref.props.tabLabel === strings('wallet.tokens')) {
	// 			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_TOKENS);
	// 		} else {
	// 			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_COLLECTIBLES);
	// 		}
	// 	});
	// }, []);

	const onRef = useCallback((ref) => {
		accountOverviewRef.current = ref;
	}, []);

	// const updateTokenList = async(address, chainId, tokens) => {
    //     const tokenListStorage = JSON.parse(await AsyncStorage.getItem(`${address?.toLowerCase()}.${chainId}`)) || [];
    //     tokens.forEach(token => {
	// 		let update = false

    //         tokenListStorage.forEach(tokenStorage => {
    //             if (token?.address?.toLowerCase() === tokenStorage?.address?.toLowerCase()){
    //                 update = true
    //                 if (token?.isETH){
    //                     tokenStorage.balance = tokens.balance;
    //                     tokenStorage.balanceFiat = tokens.balanceFiat;
    //                     tokenStorage.coingeckoId = tokens.coingeckoId;
    //                     tokenStorage.fiatValue = tokens.fiatValue;
    //                     tokenStorage.thumbnails = tokens.thumbnails;
    //                     tokenStorage.verified = tokens.verified;
    //                 }
    //                 else{
    //                     tokenStorage.balanceError = tokens.balanceError;
    //                     tokenStorage.decimals = tokens.decimals;
    //                     tokenStorage.fiatValue = tokens.fiatValue;
    //                     tokenStorage.isERC721 = tokens.isERC721;
    //                     tokenStorage.pin = tokens.hasOwnProperty('pin') && token.pin !== undefined ? token.pin : tokenListStorage.pin;
    //                     tokenStorage.verified = tokens.verified;
    //                     tokenStorage.secondaryBalanceSort = tokens.secondaryBalanceSort;
    //                     tokenStorage.symbol = tokens.symbol;
    //                 }
    //             }
    //         })
	// 		if (!update){
	// 			tokenListStorage.push(token)
	// 		}
    //     })
	// 	// console.log('tokenListStorage', tokenListStorage.length)
    //     await AsyncStorage.setItem(`${address?.toLowerCase()}.${chainId}`, JSON.stringify(tokenListStorage));
    // }

	

	const renderContent = () => {
		let balance: any = 0;
		let assets = tokens;
		// console.log('accounts : ', accounts);
		let tokenMainnet = null;
		if (ticker) {
			tokenMainnet = getTokenByTicker(ticker);
		} else {
			tokenMainnet = getTokenByTicker('ETH');
		}

		if (accounts[selectedAddress]) {
			balance = renderFromWei(accounts[selectedAddress].balance);
			// @ts-ignore
			// @ts-ignore
			// @ts-ignore
			assets = [
				{
					name: tokenMainnet ? tokenMainnet.name : ticker,
					symbol: getTicker(ticker),
					isETH: true,
					balance,
					balanceFiat: weiToFiat(
						hexToBN(accounts[selectedAddress].balance) as any,
						conversionRate,
						currentCurrency
					),
					logo: '',
					coingeckoId: tokenMainnet ? tokenMainnet.coinGeckoId : ticker,
					address: tokenMainnet ? tokenMainnet.coinGeckoId : ticker,
					thumbnails: tokenMainnet ? tokenMainnet.thumbnail : '',
					verified: true,
				},
				...(tokens || []),
			];
		} else {
			assets = tokens;
		}

		const account = { address: selectedAddress, ...identities[selectedAddress], ...accounts[selectedAddress] };

		// console.log('assets : ', assets.length, collectibleContracts.length);
		if (assets.length === 0 && collectibleContracts.length === 0) {
			return (
				<View style={styles.wrapper}>
					<AccountOverview account={account} navigation={navigation} onRef={onRef} />
					{/* <GlobalAlert /> */}
					<NoticeDeposit navigation={navigation} />
				</View>
			);
		}
		// updateTokenList(account.address, tokenMainnet.chainId, assets)
		// console.log('render WAllet')
		return (
			<View style={styles.container}>
				<AccountOverview account={account} navigation={navigation} onRef={onRef} screen={'wallet-screen'} />
				{/* <GlobalAlert /> */}
				<Tokens
					// tabLabel={strings('wallet.tokens')}
					key={'tokens-tab'}
					navigation={navigation}
					tokens={assets}
					address={selectedAddress}
					chainId={tokenMainnet ? tokenMainnet.chainId : 1}
				/>
				<CollectibleContracts
					// tabLabel={strings('wallet.collectibles')}
					route={route}
					key={'nfts-tab'}
					navigation={navigation}
					assetOwner={selectedAddress}
					// assetOwner={'0xA622B86f5DF035641bd330BCA66F518aA139DF95'}
					hub={hub}
				/>
			</View>
		);
		// else if (collectibleContracts.length !== 0) {
		// 	return (
		// 		<View style={styles.wrapper}>
		// 			<AccountOverview account={account} navigation={navigation} onRef={onRef} />
		// 			<GlobalAlert />
		// 			<ScrollView
		// 			// renderTabBar={renderTabBar}
		// 			// eslint-disable-next-line react/jsx-no-bind
		// 			// onChangeTab={onChangeTab}
		// 			>
		// 				<Tokens
		// 					// tabLabel={strings('wallet.tokens')}
		// 					key={'tokens-tab'}
		// 					navigation={navigation}
		// 					tokens={assets}
		// 				/>
		// 				<CollectibleContracts
		// 					// tabLabel={strings('wallet.collectibles')}
		// 					key={'nfts-tab'}
		// 					navigation={navigation}
		// 					// assetOwner={'0xc1bb39ba8ab14b37d4dd59c457042f639dfef95d'}
		// 					assetOwner={selectedAddress}
		// 					// assetOwner={'0x0A1dB2a73647d5242FAce0D70365ee9d34A4f725'}
		// 				/>
		// 			</ScrollView>
		// 		</View>
		// 	);
		// }
		// return (
		// 	<View style={styles.wrapper}>
		// 		<AccountOverview account={account} navigation={navigation} onRef={onRef} />
		// 		<GlobalAlert />
		// 		{!assets.length ? (
		// 			<NoticeDeposit navigation={navigation} />
		// 		) : (
		// 			<ScrollView
		// 			// renderTabBar={renderTabBar}
		// 			// eslint-disable-next-line react/jsx-no-bind
		// 			// onChangeTab={onChangeTab}
		// 			>
		// 				<Tokens
		// 					// tabLabel={strings('wallet.tokens')}
		// 					key={'tokens-tab'}
		// 					navigation={navigation}
		// 					tokens={assets}
		// 				/>
		// 				<CollectibleContracts
		// 					// tabLabel={strings('wallet.collectibles')}
		// 					key={'nfts-tab'}
		// 					navigation={navigation}
		// 					// assetOwner={'0xc1bb39ba8ab14b37d4dd59c457042f639dfef95d'}
		// 					assetOwner={selectedAddress}
		// 					// assetOwner={'0x0A1dB2a73647d5242FAce0D70365ee9d34A4f725'}
		// 				/>
		// 			</ScrollView>
		// 		)}
		// 	</View>
		// );
	};
	// , [renderTabBar, accounts, identities, navigation, onChangeTab, onRef, selectedAddress, tokens, styles]

	const renderLoader = useCallback(
		() => (
			<View style={styles.loader}>
				<ActivityIndicator size="small" />
			</View>
		),
		[styles]
	);

	/**
	 * Return current step of onboarding wizard if not step 5 nor 0
	 */
	const renderOnboardingWizard = useCallback(
		() =>
			[1, 2, 3, 4].includes(wizardStep) && (
				<OnboardingWizard navigation={navigation} coachmarkRef={accountOverviewRef.current} />
			),
		[navigation, wizardStep]
	);

	const goToSwaps = () => {
		navigation.navigate('Swaps', {
			screen: 'TokenSelectView',
			// screen: 'SwapsAmountView',
			params: {
				// sourceToken: this.props.asset.isETH ? swapsUtils.NATIVE_SWAPS_TOKEN_ADDRESS : this.props.asset.address,
			},
		});
	};

	useEffect(() => {
		swapAnimation?.current.play();
		sendAnimation?.current.play(1, 7);
	}, []);

	useEffect(() => {
		const interval = setInterval(() => {
			setSwapAnmation((swapAnmation) => !swapAnmation);
			if (swapAnmation) {
				sendAnimation?.current.play();
			} else {
				swapAnimation?.current.play();
			}
		}, 30000);
		return () => clearInterval(interval);
	}, [swapAnmation]);
	return (
		<ErrorBoundary view="Wallet">
			<View style={baseStyles.flexGrow} testID="wallet-screen">
				<ScrollView
					style={styles.wrapper}
					refreshControl={
						<RefreshControl
							colors={[colors.primary.default]}
							tintColor={colors.icon.default}
							refreshing={refreshing}
							onRefresh={onRefresh}
						/>
					}
					onScroll={({ nativeEvent }) => {
						if (
							nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y >=
							nativeEvent.contentSize.height
						) {
							hub.emit('wallet_view::onEndReach');
						}
					}}
					scrollEventThrottle={400}
				>
					{selectedAddress ? renderContent() : renderLoader()}
				</ScrollView>
				<View style={styles.containButton}>
					<TouchableOpacity 
						// style={[isPressSwap && styles.zoomIn]}
						// onPressIn={() => setIsPressSwap(true)}
						// onPressOut={() => setIsPressSwap(false)}
						// activeOpacity={1}
					>
						{/* <Image
							style={{ height: 48, width: 48 }}
							resizeMode="contain"
							source={require('../../../images/floating-swap.png')}
						/> */}
						<LottieView
							ref={swapAnimation}
							style={{ height: 54, width: 54, marginRight: 5 }}
							source={require('../../../animations/swap_button.json')}
							autoPlay
							loop={false}
						></LottieView>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={() => {
							navigation.navigate('SendFlowView');
						}}
						// style={{
						// 	height: 154,
						// 	width: 154,
						// 	backgroundColor: 'blue',
						// 	justifyContent: 'center',
						// 	alignItems: 'center',
						// }}
						// style={[isPressSend && styles.zoomIn]}
						// onPressIn={() => setIsPressSend(true)}
						// onPressOut={() => setIsPressSend(false)}
						// activeOpacity={1}
					>
						<LottieView
							ref={sendAnimation}
							style={{ height: 54, width: 54 }}
							// style={{ height: 54, width: 54 }}
							source={require('../../../animations/send_button.json')}
							autoPlay={false}
							loop={false}
						></LottieView>
						{/* <Image
							style={{ height: 48, width: 48, marginLeft: 12 }}
							resizeMode="contain"
							source={require('../../../images/floating-send.png')}
						/> */}
					</TouchableOpacity>
				</View>
			</View>
		</ErrorBoundary>
	);
};

export default Wallet;
