import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { strings } from '../../../../../locales/i18n';
import { mockTheme, ThemeContext } from '../../../../util/theme';

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			margin: 0,
			width: '100%',
			padding: 10,
			justifyContent: 'center',
			alignItems: 'center',
		},
		content: {
			width: '100%',
			padding: 20,
			backgroundColor: '#1B1B23',
			borderRadius: 20,
			justifyContent: 'center',
			alignItems: 'center',
		},
		title: {
			fontSize: 16,
			fontWeight: '500',
			color: '#FFF',
		},
		input: {
			color: '#FFF',
			fontSize: 14,
			fontWeight: '400',
			borderWidth: 1,
			borderRadius: 12,
			borderColor: 'rgba(255, 255, 255, 0.15)',
			padding: 12,
			marginTop: 20,
			width: '100%',
			textAlign: 'center',
		},
		inputFocus: {
			borderColor: '#FFF',
		},
		inputError: {
			borderColor: '#EB5252',
		},
		errorText: {
			fontSize: 12,
			fontWeight: '400',
			color: '#EB5252',
			marginTop: 4,
			textAlign: 'center',
		},
		labelTitle: {
			fontSize: 14,
			fontWeight: '400',
			color: '#58586C',
			marginTop: 20,
			textAlign: 'center',
		},
		labelInformation: {
			fontSize: 14,
			fontWeight: '500',
			color: '#FFF',
			marginTop: 8,
			textAlign: 'center',
		},
		saveButton: {
			width: '100%',
			padding: 12,
			borderRadius: 12,
			backgroundColor: '#6A45FF',
			marginTop: 20,
			justifyContent: 'center',
			alignItems: 'center',
		},
		saveButtonText: {
			fontSize: 16,
			fontWeight: '500',
			color: '#FFF',
		},
		saveButtonDisabled: {
			backgroundColor: '#3D3D4B',
		},
		saveButtonDisabledText: {
			color: '#A0A0B1',
		},
		cancelButton: {
			width: '100%',
			padding: 12,
			marginTop: 8,
			justifyContent: 'center',
			alignItems: 'center',
		},
		cancelButtonText: {
			fontSize: 16,
			fontWeight: '500',
			color: '#FFF',
		},
	});

/**
 * View that contains app information
 */
class AddressSaveModal extends PureComponent {
	static propTypes = {
		network: PropTypes.string,
		/**
		 * An object containing each identity in the format address => account
		 */
		identities: PropTypes.object,
		/**
		 * Map representing the address book
		 */
		addressBook: PropTypes.object,
		/**
		/* contact address
		 */
		address: PropTypes.string,
		/**
		/* contact avatar
		 */
		avatar: PropTypes.string,
		/**
		/* isVisible
		 */
		isVisible: PropTypes.bool,
		/**
		/* onAddressSaveCancel
		 */
		onAddressSaveCancel: PropTypes.func,
		/**
		/* onAddressSaveSubmit
		 */
		onAddressSaveSubmit: PropTypes.func,
	};

	state = {
		newName: '',
		newNameFocus: false,
		newNameError: '',
	};

	getNewNameError = (newName) => {
		if (newName.length === 0) {
			return '';
		}

		if (!newName) {
			return strings('address_book.reminiscent_name_empty');
		}
		if (newName.length > 18) {
			return strings('address_book.reminiscent_name_too_long');
		}
		const { addressBook, network, identities } = this.props;
		const networkAddressBook = addressBook[network] || {};
		let addresses = Object.keys(networkAddressBook);
		for (let i = 0; i < addresses.length; i++) {
			const address = addresses[i];
			const contact = networkAddressBook[address];
			if (contact.name === newName.trim()) {
				return strings('address_book.reminiscent_name_existed');
			}
		}
		addresses = Object.keys(identities);
		for (let i = 0; i < addresses.length; i++) {
			const address = addresses[i];
			const contact = identities[address];
			if (contact.name === newName.trim()) {
				return strings('address_book.reminiscent_name_existed');
			}
		}
		return '';
	};
	onNewNameChanged = (text) => {
		this.setState({
			newName: text,
			newNameError: this.getNewNameError(text.trim()),
		});
	};

	render = () => {
		const { isVisible, address, onAddressSaveCancel, onAddressSaveSubmit } = this.props;
		const { newName, newNameFocus, newNameError } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || mockTheme.themeAppearance;
		const styles = createStyles(colors);

		return (
			<Modal
				isVisible={isVisible}
				style={styles.wrapper}
				onBackdropPress={onAddressSaveCancel}
				onBackButtonPress={onAddressSaveCancel}
				onSwipeComplete={onAddressSaveCancel}
				swipeDirection={'down'}
				backdropColor={'#000'}
				backdropOpacity={0.8}
				avoidKeyboard
			>
				<View style={styles.content}>
					<Text style={styles.title}>{strings('address_book.add_to_address_book')}</Text>
					<TextInput
						// ref={inputRef}
						autoCapitalize="none"
						autoCorrect={false}
						onChangeText={this.onNewNameChanged}
						placeholder={strings('address_book.input_reminiscent_name')}
						placeholderTextColor={'#58586C'}
						spellCheck={false}
						style={[
							styles.input,
							newNameFocus ? styles.inputFocus : {},
							newNameError ? styles.inputError : {},
						]}
						numberOfLines={1}
						onFocus={() => this.setState({ newNameFocus: true })}
						onBlur={() => this.setState({ newNameFocus: false })}
						onSubmitEditing={() => {}}
						value={newName}
						keyboardAppearance={themeAppearance}
						maxLength={19}
					/>
					{!!newNameError && <Text style={styles.errorText}>{newNameError}</Text>}
					<Text style={styles.labelTitle}>{strings('address_book.address')}</Text>
					<Text style={styles.labelInformation}>{address}</Text>
					<TouchableOpacity
						style={[styles.saveButton, !newName || !!newNameError ? styles.saveButtonDisabled : {}]}
						onPress={() => onAddressSaveSubmit({ address, newName: newName.trim() })}
						disabled={!newName || !!newNameError}
					>
						<Text
							style={[
								styles.saveButtonText,
								!newName || !!newNameError ? styles.saveButtonDisabledText : {},
							]}
						>
							{strings('address_book.save_address')}
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.cancelButton} onPress={onAddressSaveCancel}>
						<Text style={styles.cancelButtonText}>{strings('address_book.cancel')}</Text>
					</TouchableOpacity>
				</View>
			</Modal>
		);
	};
}

AddressSaveModal.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	addressBook: state.engine.backgroundState.AddressBookController.addressBook,
	identities: state.engine.backgroundState.PreferencesController.identities,
	network: state.engine.backgroundState.NetworkController.network,
});

export default connect(mapStateToProps)(AddressSaveModal);
