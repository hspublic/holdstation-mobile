import Fuse from 'fuse.js';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { ActivityIndicator, ScrollView, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { strings } from '../../../../../locales/i18n';
import { fontStyles } from '../../../../styles/common';
import { mockTheme, ThemeContext } from '../../../../util/theme';
import { isSmartContractAddress } from '../../../../util/transactions';
import AddressElement from '../AddressElement';

const createStyles = (colors) =>
	StyleSheet.create({
		root: {
			flex: 1,
			// backgroundColor: colors.background.default,
			padding: 25,
		},
		messageText: {
			...fontStyles.normal,
			color: '#A0A0B1',
			fontSize: 18,
			textAlign: 'center',
		},
		messageLeft: {
			textAlign: 'left',
		},
		messageMT: {
			marginTop: 20,
		},
		myAccountsWrapper: {
			flexGrow: 1,
		},
		myAccountsTouchable: {
			// borderBottomWidth: 1,
			// borderBottomColor: colors.border.muted,
			// padding: 15,
		},
		labelElementWrapper: {
			// backgroundColor: colors.background.default,
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
			// borderBottomWidth: 1,
			// borderBottomColor: colors.border.muted,
			padding: 8,
		},
		labelElementInitialText: {
			textTransform: 'uppercase',
		},
		labelElementText: {
			...fontStyles.normal,
			fontSize: 12,
			marginHorizontal: 8,
			color: colors.text.alternative,
		},
		activityIndicator: {
			color: colors.icon.default,
		},
	});

const LabelElement = (styles, label, checkingForSmartContracts = false, showLoading = false) => (
	<View key={label} style={styles.labelElementWrapper}>
		<Text style={[styles.labelElementText, label.length > 1 ? {} : styles.labelElementInitialText]}>{label}</Text>
		{showLoading && checkingForSmartContracts && (
			<ActivityIndicator size="small" style={styles.activityIndicator} />
		)}
	</View>
);
/**
 * View that wraps the wraps the "Send" screen
 */
class AddressList extends PureComponent {
	static propTypes = {
		/**
		 * List of accounts from the PreferencesController
		 */
		identities: PropTypes.object,
		/**
		 * Map representing the address book
		 */
		addressBook: PropTypes.object,
		/**
		 * Search input from parent component
		 */
		inputSearch: PropTypes.string,
		/**
		 * Network id
		 */
		network: PropTypes.string,
		/**
		 * Callback called when account in address book is pressed
		 */
		onAccountPress: PropTypes.func,
		/**
		 * Callback called when account in address book is long pressed
		 */
		onAccountLongPress: PropTypes.func,
		/**
		 * Callback called when account in address book edit button is pressed
		 */
		onAccountEditPress: PropTypes.func,
		/**
		 * Callback called when account in address book delete button is pressed
		 */
		onAccountDeletePress: PropTypes.func,
		/**
		 * Whether it only has to render address book
		 */
		onlyRenderAddressBook: PropTypes.bool,
		reloadAddressList: PropTypes.bool,
		/**
		 * An array that represents the user's recent toAddresses
		 */
		recents: PropTypes.array,

		hideRecents: PropTypes.bool,
	};

	state = {
		myAccountsOpened: false,
		processedAddressBookList: undefined,
		contactElements: [],
		checkingForSmartContracts: false,
	};

	networkAddressBook = {};

	componentDidMount = () => {
		const { addressBook, network } = this.props;
		this.networkAddressBook = addressBook[network] || {};
		const networkAddressBookList = Object.keys(this.networkAddressBook).map(
			(address) => this.networkAddressBook[address]
		);
		this.fuse = new Fuse(networkAddressBookList, {
			shouldSort: true,
			threshold: 0.45,
			location: 0,
			distance: 10,
			maxPatternLength: 32,
			minMatchCharLength: 1,
			keys: [
				{ name: 'name', weight: 0.5 },
				{ name: 'address', weight: 0.5 },
			],
		});
		this.parseAddressBook(networkAddressBookList);
	};

	componentDidUpdate = (prevProps) => {
		const { network, addressBook, reloadAddressList } = this.props;
		if (
			(prevProps.reloadAddressList && reloadAddressList !== prevProps.reloadAddressList) ||
			prevProps.inputSearch !== this.props.inputSearch ||
			JSON.stringify(prevProps.addressBook[network]) !== JSON.stringify(addressBook[network])
		) {
			let networkAddressBookList;
			if (this.props.inputSearch) {
				networkAddressBookList = this.fuse.search(this.props.inputSearch);
			} else {
				const { addressBook } = this.props;
				const networkAddressBook = addressBook[network] || {};
				networkAddressBookList = Object.keys(networkAddressBook).map((address) => networkAddressBook[address]);
			}
			this.parseAddressBook(networkAddressBookList);
		}
	};

	openMyAccounts = () => {
		this.setState({ myAccountsOpened: true });
	};

	parseAddressBook = (networkAddressBookList) => {
		const contactElements = [];
		const addressBookTree = {};
		networkAddressBookList.forEach((contact) => {
			this.setState({ checkingForSmartContracts: true });

			isSmartContractAddress(contact.address, contact.chainId)
				.then((isSmartContract) => {
					if (isSmartContract) {
						contact.isSmartContract = true;
						return this.setState({ checkingForSmartContracts: false });
					}

					contact.isSmartContract = false;
					return this.setState({ checkingForSmartContracts: false });
				})
				.catch(() => {
					contact.isSmartContract = false;
				});
		});

		networkAddressBookList.forEach((contact) => {
			const contactNameInitial = contact && contact.name && contact.name[0];
			const nameInitial = contactNameInitial && contactNameInitial.match(/[a-z]/i);
			const initial = nameInitial ? nameInitial[0] : strings('address_book.others');
			if (Object.keys(addressBookTree).includes(initial)) {
				addressBookTree[initial].push(contact);
			} else if (contact.isSmartContract && !this.props.onlyRenderAddressBook) {
				null;
			} else {
				addressBookTree[initial] = [contact];
			}
		});
		Object.keys(addressBookTree)
			.sort()
			.forEach((initial) => {
				contactElements.push(initial);
				addressBookTree[initial].forEach((contact) => {
					contactElements.push(contact);
				});
			});
		this.setState({ contactElements });
	};

	renderElement = (element) => {
		const { onAccountPress, onAccountLongPress, onAccountEditPress, onAccountDeletePress } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (typeof element === 'string') {
			// return LabelElement(styles, element);
			return <></>;
		}

		const key = element.address + element.name;

		return (
			<AddressElement
				key={key}
				address={element.address}
				name={element.name}
				avatar={element.avatar}
				onAccountPress={onAccountPress}
				onAccountLongPress={onAccountLongPress}
				onAccountEditPress={onAccountEditPress}
				onAccountDeletePress={onAccountDeletePress}
				testID={'account-address'}
			/>
		);
	};

	renderMyAccounts = () => {
		const {
			identities,
			addressBook,
			network,
			onAccountPress,
			inputSearch,
			onAccountLongPress,
			onAccountEditPress,
			onAccountDeletePress,
			addressList,
			ensNameList,
		} = this.props;
		const networkAddressBook = addressBook[network] || {};
		const { myAccountsOpened } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		// let showTitle = false
		if (inputSearch) {
			if (addressList.length > 0) {
				return (
					<View style={styles.myAccountsTouchable}>
						<Text style={[styles.messageText, styles.messageLeft]}>{strings('address_book.my_wallet')}</Text>
						{Object.keys(identities).map((address) => {
							if (addressList.includes(identities[address].address)) {
								return (
									<AddressElement
										key={address}
										address={address}
										name={identities[address].name}
										avatar={networkAddressBook[address]?.avatar || ''}
										onAccountPress={onAccountPress}
										onAccountLongPress={onAccountLongPress}
										onAccountEditPress={onAccountEditPress}
										onAccountDeletePress={onAccountDeletePress}
										testID={'account-identity'}
									/>
								);
							}
						})}
					</View>
				);
			}
			if (ensNameList) {
				return (
					<View style={styles.myAccountsTouchable}>
						{Object.keys(ensNameList).map((address) => (
							<AddressElement
								key={address}
								address={address}
								name={ensNameList[address].name}
								avatar={networkAddressBook[address]?.avatar || ''}
								onAccountPress={onAccountPress}
								onAccountLongPress={onAccountLongPress}
								onAccountEditPress={onAccountEditPress}
								onAccountDeletePress={onAccountDeletePress}
								testID={'account-identity'}
							/>
						))}
					</View>
				);
			}
			return null;
		}
		return (
			<View style={styles.myAccountsTouchable}>
				<Text style={[styles.messageText, styles.messageLeft]}>{strings('address_book.my_wallet')}</Text>
				{Object.keys(identities).map((address) => (
					<AddressElement
						key={address}
						address={address}
						name={identities[address].name}
						avatar={networkAddressBook[address]?.avatar || ''}
						onAccountPress={onAccountPress}
						onAccountLongPress={onAccountLongPress}
						onAccountEditPress={onAccountEditPress}
						onAccountDeletePress={onAccountDeletePress}
					/>
				))}
			</View>
		);
		// Lọc kết quả tìm kiếm
		// Từ input đầu vào
		// Lấy ra các tên ví có ký tự liên tiếp trùng với tên ví trong danh sách(Không phân biệt in hoa)
		// text.includes(str)

		// return !myAccountsOpened ? (
		// 	<TouchableOpacity
		// 		style={styles.myAccountsTouchable}
		// 		onPress={this.openMyAccounts}
		// 		testID={'my-accounts-button'}
		// 	>
		// 		<Text style={[styles.messageText, styles.messageLeft]}>{'Ví của tôi'}</Text>
		// 	</TouchableOpacity>
		// ) : (
		// 	<View>
		// 		{Object.keys(identities).map((address) => (
		// 			<AddressElement
		// 				key={address}
		// 				address={address}
		// 				name={identities[address].name}
		// 				onAccountPress={onAccountPress}
		// 				onAccountLongPress={onAccountLongPress}
		// 				testID={'account-identity'}
		// 			/>
		// 		))}
		// 	</View>
		// );
	};

	renderRecents = () => {
		const {
			recents,
			identities,
			addressBook,
			network,
			onAccountPress,
			onAccountLongPress,
			onAccountEditPress,
			onAccountDeletePress,
			inputSearch,
		} = this.props;
		const networkAddressBook = addressBook[network] || {};
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (!recents.length || inputSearch) return;
		return (
			<View style={[styles.myAccountsTouchable, { paddingTop: 0 }]}>
				{/* {LabelElement(styles, strings('address_book.recents'), this.state.checkingForSmartContracts, true)} */}
				<Text style={[styles.messageText, styles.messageLeft, styles.messageMT]}>
					{strings('address_book.sent_address')}
				</Text>
				{recents
					.filter((recent) => recent != null)
					.map((address, index) => (
						<AddressElement
							key={index}
							address={address}
							name={identities[address]?.name || networkAddressBook[address]?.name}
							avatar={networkAddressBook[address]?.avatar || ''}
							onAccountPress={onAccountPress}
							onAccountLongPress={onAccountLongPress}
							onAccountEditPress={onAccountEditPress}
							onAccountDeletePress={onAccountDeletePress}
						/>
					))}
			</View>
		);
	};

	renderSendFlowContacts = (contacts) => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (!contacts.length) return <></>;
		return (
			<View style={[styles.myAccountsTouchable, { paddingTop: 0 }]}>
				<Text style={[styles.messageText, styles.messageLeft, styles.messageMT]}>
					{strings('address_book.title')}
				</Text>
				{contacts.map(this.renderElement)}
			</View>
		);
	};

	render = () => {
		const { contactElements } = this.state;
		const { onlyRenderAddressBook } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		const sendFlowContacts = [];

		contactElements.filter((element) => {
			if (typeof element === 'object' && element.isSmartContract === false) {
				const nameInitial = element && element.name && element.name[0];
				if (sendFlowContacts.includes(nameInitial)) {
					sendFlowContacts.push(element);
				} else {
					sendFlowContacts.push(nameInitial);
					sendFlowContacts.push(element);
				}
			}
			return element;
		});

		return (
			<View style={styles.root}>
				<ScrollView style={styles.myAccountsWrapper}>
					{onlyRenderAddressBook ? (
						contactElements.map(this.renderElement)
					) : (
						<>
							{this.renderMyAccounts()}
							{!this.props.hideRecents && this.renderRecents()}
							{this.renderSendFlowContacts(sendFlowContacts)}
						</>
					)}
				</ScrollView>
			</View>
		);
	};
}

AddressList.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	recents: state.recents,
	addressBook: state.engine.backgroundState.AddressBookController.addressBook,
	identities: state.engine.backgroundState.PreferencesController.identities,
	network: state.engine.backgroundState.NetworkController.network,
	transactions: state.engine.backgroundState.TransactionController.transactions,
	chainId: state.engine.backgroundState.NetworkController.provider.chainId,
});

export default connect(mapStateToProps)(AddressList);
