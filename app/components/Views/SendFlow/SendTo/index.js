import { util } from '@metamask/controllers';
import { isValidAddress, toChecksumAddress } from 'ethereumjs-util';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { InteractionManager, KeyboardAvoidingView, StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';
import { strings } from '../../../../../locales/i18n';
import addRecent from '../../../../actions/recents';
import { newAssetTransaction, setRecipient, setSelectedAsset } from '../../../../actions/transaction';
import { ADD_ADDRESS_MODAL_CONTAINER_ID, ENTER_ALIAS_INPUT_BOX_ID } from '../../../../constants/test-ids';
import Analytics from '../../../../core/Analytics';
import Engine from '../../../../core/Engine';
import NotificationManager from '../../../../core/NotificationManager';
import { baseStyles, fontStyles } from '../../../../styles/common';
import { isENS, renderShortAddress } from '../../../../util/address';
import { ANALYTICS_EVENT_OPTS } from '../../../../util/analytics';
import AnalyticsV2 from '../../../../util/analyticsV2';
import { ensClient } from '../../../../util/apollo/client';
import { ENS_SUGGESTIONS } from '../../../../util/apollo/queries';
import { getConfusablesExplanations, hasZeroWidthPoints } from '../../../../util/confusables';
import { doENSLookup, doENSReverseLookup } from '../../../../util/ENSUtils';
import { renderFromWei } from '../../../../util/number';
import { mockTheme, ThemeContext } from '../../../../util/theme';
import { getEther, getTicker } from '../../../../util/transactions';
import Text from '../../../Base/Text';
import AccountList from '../../../UI/AccountList';
import ActionModal from '../../../UI/ActionModal';
import { allowedToBuy } from '../../../UI/FiatOrders';
import AddressSaveModal from '../AddressSave';
import { AddressTo } from './../AddressInputs';
import AddressList from './../AddressList';

const { hexToBN } = util;
const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			flex: 1,
			backgroundColor: '#1B1B23',
		},
		imputWrapper: {
			paddingHorizontal: 15,
		},
		bottomModal: {
			justifyContent: 'flex-end',
			margin: 0,
		},
		myAccountsText: {
			...fontStyles.normal,
			color: colors.primary.default,
			fontSize: 16,
			alignSelf: 'center',
		},
		myAccountsTouchable: {
			padding: 28,
		},
		addToAddressBookRoot: {
			flex: 1,
			padding: 24,
		},
		addToAddressBookWrapper: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		addTextTitle: {
			...fontStyles.normal,
			fontSize: 24,
			color: colors.text.default,
			marginBottom: 24,
		},
		addTextSubtitle: {
			...fontStyles.normal,
			fontSize: 16,
			color: colors.text.alternative,
			marginBottom: 24,
		},
		addTextInput: {
			...fontStyles.normal,
			color: colors.text.default,
			fontSize: 20,
		},
		addInputWrapper: {
			flexDirection: 'row',
			borderWidth: 1,
			borderRadius: 8,
			borderColor: colors.border.default,
			height: 50,
			width: '100%',
		},
		input: {
			flex: 1,
			flexDirection: 'row',
			alignItems: 'center',
			marginHorizontal: 6,
			width: '100%',
		},
		nextActionWrapper: {
			flex: 1,
			marginBottom: 16,
		},
		buttonNextWrapper: {
			flexDirection: 'row',
			alignItems: 'flex-end',
		},
		buttonNext: {
			flex: 1,
			marginHorizontal: 24,
		},
		addressErrorWrapper: {
			margin: 16,
		},
		footerContainer: {
			flex: 1,
			justifyContent: 'flex-end',
		},
		warningContainer: {
			marginTop: 20,
			marginHorizontal: 24,
			marginBottom: 32,
		},
		buyEth: {
			color: colors.primary.default,
			textDecorationLine: 'underline',
		},
		confusabeError: {
			display: 'flex',
			flexDirection: 'row',
			justifyContent: 'space-between',
			margin: 16,
			padding: 16,
			borderWidth: 1,
			borderColor: colors.error.default,
			backgroundColor: colors.error.muted,
			borderRadius: 8,
		},
		confusabeWarning: {
			borderColor: colors.warning.default,
			backgroundColor: colors.warning.muted,
		},
		confusableTitle: {
			marginTop: -3,
			color: colors.text.default,
			...fontStyles.bold,
			fontSize: 14,
		},
		confusableMsg: {
			color: colors.text.default,
			fontSize: 12,
			lineHeight: 16,
			paddingRight: 10,
		},
		warningIcon: {
			marginRight: 8,
		},
		titleWrapper: {
			width: '100%',
			height: 33,
			alignItems: 'center',
			justifyContent: 'center',
		},
		dragger: {
			width: 48,
			height: 5,
			borderRadius: 4,
			backgroundColor: colors.border.default,
			opacity: 0.6,
		},
		content: {
			color: '#FFFFFF',
			fontSize: 16,
		},
		buttonStyle: {
			height: 45,
			backgroundColor: '#6A45FF',
			marginHorizontal: 25,
			borderRadius: 10,
			justifyContent: 'center',
			alignItems: 'center',
			marginBottom: 10,
		},
		addressBookWrapper: {
			padding: 20,
		},
	});

const dummy = () => true;

/**
 * View that wraps the wraps the "Send" screen
 */
class SendFlow extends PureComponent {
	static propTypes = {
		/**
		 * Map of accounts to information objects including balances
		 */
		accounts: PropTypes.object,
		/**
		 * Map representing the address book
		 */
		addressBook: PropTypes.object,
		/**
		 * Network id
		 */
		network: PropTypes.string,
		/**
		 * Object that represents the navigator
		 */
		navigation: PropTypes.object,
		/**
		 * Start transaction with asset
		 */
		newAssetTransaction: PropTypes.func.isRequired,
		/**
		 * Selected address as string
		 */
		selectedAddress: PropTypes.string,
		/**
		 * List of accounts from the PreferencesController
		 */
		identities: PropTypes.object,
		/**
		 * List of keyrings
		 */
		keyrings: PropTypes.array,
		/**
		 * Current provider ticker
		 */
		ticker: PropTypes.string,
		/**
		 * Action that sets transaction to and ensRecipient in case is available
		 */
		setRecipient: PropTypes.func,
		/**
		 * Set selected in transaction state
		 */
		setSelectedAsset: PropTypes.func,
		/**
		 * Network provider type as mainnet
		 */
		providerType: PropTypes.string,
		/**
		 * Object that represents the current route info like params passed to it
		 */
		route: PropTypes.object,
		/**
		 * Indicates whether the current transaction is a deep link transaction
		 */
		isPaymentRequest: PropTypes.bool,
		addRecent: PropTypes.func,
	};

	addressToInputRef = React.createRef();

	state = {
		addressError: undefined,
		balanceIsZero: false,
		fromAccountModalVisible: false,
		addToAddressBookModalVisible: false,
		fromSelectedAddress: this.props.selectedAddress,
		fromAccountName: this.props.identities[this.props.selectedAddress].name,
		fromAccountBalance: undefined,
		toSelectedAddress: this.props.route.params?.toSelectedAddress || undefined,
		toSelectedAddressName: this.props.route.params?.toSelectedAddressName || undefined,
		toSelectedAddressReady: this.props.route.params?.toSelectedAddressReady || false,
		toEnsName: this.props.route.params?.toEnsName || undefined,
		addToAddressToAddressBook: false,
		alias: undefined,
		confusableCollection: [],
		inputWidth: { width: '99%' },
		addressList: [],
		ensNameList: null,
		saveAddressModal: null,
	};

	// updateNavBar = () => {
	// 	const { navigation, route } = this.props;
	// 	const colors = this.context.colors || mockTheme.colors;
	// 	navigation.setOptions(getSendFlowTitle('send.send_to', navigation, route, colors));
	// };

	componentDidMount = async () => {
		const {
			addressBook,
			selectedAddress,
			accounts,
			ticker,
			network,
			navigation,
			providerType,
			route,
			isPaymentRequest,
		} = this.props;
		const { fromAccountName, toSelectedAddress } = this.state;
		// this.updateNavBar();
		// For analytics
		navigation.setParams({ providerType, isPaymentRequest });
		const networkAddressBook = addressBook[network] || {};
		const ens = await doENSReverseLookup(selectedAddress, network);
		const fromAccountBalance = `${renderFromWei(accounts[selectedAddress].balance)} ${getTicker(ticker)}`;

		setTimeout(() => {
			this.setState({
				fromAccountName: ens || fromAccountName,
				fromAccountBalance,
				balanceIsZero: hexToBN(accounts[selectedAddress].balance).isZero(),
				inputWidth: { width: '100%' },
			});
		}, 100);
		if (!Object.keys(networkAddressBook).length) {
			setTimeout(() => {
				this.addressToInputRef && this.addressToInputRef.current && this.addressToInputRef.current.focus();
			}, 500);
		}
		if (toSelectedAddress) {
			this.onToAddressChange(toSelectedAddress);
		}
		//Fills in to address and sets the transaction if coming from QR code scan
		const targetAddress = route.params?.txMeta?.target_address;
		if (targetAddress) {
			this.props.newAssetTransaction(getEther(ticker));
			this.onToAddressChange(targetAddress);
		}
	};

	componentDidUpdate = () => {
		// this.updateNavBar();
	};

	toggleFromAccountModal = () => {
		const { fromAccountModalVisible } = this.state;
		this.setState({ fromAccountModalVisible: !fromAccountModalVisible });
	};

	toggleAddToAddressBookModal = () => {
		const { addToAddressBookModalVisible } = this.state;
		this.setState({ addToAddressBookModalVisible: !addToAddressBookModalVisible });
	};

	onAccountChange = async (accountAddress) => {
		const { identities, ticker, accounts } = this.props;
		const { name } = identities[accountAddress];
		const { PreferencesController } = Engine.context;
		const fromAccountBalance = `${renderFromWei(accounts[accountAddress].balance)} ${getTicker(ticker)}`;
		const ens = await doENSReverseLookup(accountAddress);
		const fromAccountName = ens || name;
		PreferencesController.setSelectedAddress(accountAddress);
		// If new account doesn't have the asset
		this.props.setSelectedAsset(getEther(ticker));
		this.setState({
			fromAccountName,
			fromAccountBalance,
			fromSelectedAddress: accountAddress,
			balanceIsZero: hexToBN(accounts[accountAddress].balance).isZero(),
		});
		this.toggleFromAccountModal();
	};

	// onToSelectedAddressChange = async (toSelectedAddress) => {
	// 	const { AssetsContractController } = Engine.context;
	// 	const { addressBook, network, identities, providerType } = this.props;
	// 	const networkAddressBook = addressBook[network] || {};
	// 	let addressError, toAddressName, toEnsName, errorContinue, isOnlyWarning, confusableCollection;
	// 	let [addToAddressToAddressBook, toSelectedAddressReady] = [false, false];
	// 	if (isValidAddress(toSelectedAddress)) {
	// 		const checksummedToSelectedAddress = toChecksumAddress(toSelectedAddress);
	// 		toSelectedAddressReady = true;
	// 		const ens = await doENSReverseLookup(toSelectedAddress);
	// 		if (ens) {
	// 			toAddressName = ens;
	// 			if (!networkAddressBook[checksummedToSelectedAddress] && !identities[checksummedToSelectedAddress]) {
	// 				addToAddressToAddressBook = true;
	// 			}
	// 		} else if (networkAddressBook[checksummedToSelectedAddress] || identities[checksummedToSelectedAddress]) {
	// 			toAddressName =
	// 				(networkAddressBook[checksummedToSelectedAddress] &&
	// 					networkAddressBook[checksummedToSelectedAddress].name) ||
	// 				(identities[checksummedToSelectedAddress] && identities[checksummedToSelectedAddress].name);
	// 		} else {
	// 			// If not in address book nor user accounts
	// 			addToAddressToAddressBook = true;
	// 		}

	// 		// Check if it's token contract address on mainnet
	// 		const networkId = NetworkList[providerType].networkId;
	// 		if (networkId === 1) {
	// 			try {
	// 				const symbol = await AssetsContractController.getERC721AssetSymbol(toSelectedAddress);
	// 				if (symbol) {
	// 					addressError = (
	// 						<Text>
	// 							{/* CẢNH BÁO: Địa chỉ này là */}
	// 							<Text>{strings('transaction.tokenContractAddressWarning_1')}</Text>
	// 							{/* địa chỉ hợp đồng token */}
	// 							<Text bold>{strings('transaction.tokenContractAddressWarning_2')}</Text>
	// 							{/* Nếu gửi token đến địa chỉ này, bạn sẽ mất token */}
	// 							<Text>{strings('transaction.tokenContractAddressWarning_3')}</Text>
	// 						</Text>
	// 					);
	// 					errorContinue = true;
	// 				}
	// 			} catch (e) {
	// 				// Not a token address
	// 			}
	// 		}

	// 		/**
	// 		 * Not using this for now; Import isSmartContractAddress from utils/transaction and use this for checking smart contract: await isSmartContractAddress(toSelectedAddress);
	// 		 * Check if it's smart contract address
	// 		 */
	// 		/*
	// 		const smart = false; //

	// 		if (smart) {
	// 			addressError = strings('transaction.smartContractAddressWarning');
	// 			isOnlyWarning = true;
	// 		}
	// 		*/
	// 	} else if (isENS(toSelectedAddress)) {
	// 		toEnsName = toSelectedAddress;
	// 		confusableCollection = collectConfusables(toEnsName);
	// 		const resolvedAddress = await doENSLookup(toSelectedAddress, network);
	// 		if (resolvedAddress) {
	// 			const checksummedResolvedAddress = toChecksumAddress(resolvedAddress);
	// 			toAddressName = toSelectedAddress;
	// 			toSelectedAddress = resolvedAddress;
	// 			toSelectedAddressReady = true;
	// 			if (!networkAddressBook[checksummedResolvedAddress] && !identities[checksummedResolvedAddress]) {
	// 				addToAddressToAddressBook = true;
	// 			}
	// 		} else {
	// 			// Không phân giải được ENS
	// 			addressError = strings('transaction.could_not_resolve_ens');
	// 		}
	// 	} else if (toSelectedAddress && toSelectedAddress.length >= 42) {
	// 		// Địa chỉ không hợp lệ
	// 		addressError = strings('transaction.invalid_address');
	// 	}
	// 	console.log('addressError : ', addressError)
	// 	this.setState({
	// 		addressError,
	// 		toSelectedAddress,
	// 		addToAddressToAddressBook,
	// 		toSelectedAddressReady,
	// 		toSelectedAddressName: toAddressName,
	// 		toEnsName,
	// 		errorContinue,
	// 		isOnlyWarning,
	// 		confusableCollection,
	// 	});
	// };

	// 0xB1197C3F470842AC95A16ef2FC528D56Be8747E7
	// 0x106dbd02F3395E7574f9F27B7A58336B13F4176b

	onToAddressChange = async (toSelectedAddress) => {
		const { identities } = this.props;
		if (!toSelectedAddress) {
			this.setState({ addressError: undefined });
		}
		this.setState({ toSelectedAddress });
		// this.setState({ toSelectedAddress, toSelectedAddressReady: isValidAddress(toSelectedAddress) });
		// Lọc tên địa chỉ và cập nhật
		const addressList = [];
		Object.keys(identities).forEach((address) => {
			if (identities[address].name.toLowerCase().includes(toSelectedAddress.toLowerCase())) {
				addressList.push(address);
			}
		});
		this.setState({ addressList });
		if (addressList.length === 0 && toSelectedAddress.length >= 3) {
			// Gọi API lấy tên ens
			const recpt = toSelectedAddress;
			const result = await ensClient.query({
				query: ENS_SUGGESTIONS,
				variables: {
					amount: 8,
					name: recpt,
				},
			});
			const data = {};

			if (result?.data?.domains) {
				result.data.domains.forEach((e) => {
					data[e.resolver.addr.id || e.owner.id] = {
						address: e.resolver.addr.id || e.owner.id,
						name: e.name,
					};
				});
			}
			this.setState({ ensNameList: data });
		}
		// this.props.navigation.replace('PaymentRequestView', {
		// 	screen: 'PaymentRequest',
		// 	params: { skipSelectAsset: this.props.route.params?.skipSelectAsset || false },
		// });
	};

	onToSelectedAddress = async ({ name, address }) => {
		// Xoá hiển thị lỗi
		this.setState({ addressError: undefined });
		// Cập nhật lại tên địa chỉ
		// this.setState({
		// 	toSelectedAddress: address,
		// 	toEnsName: name,
		// 	toSelectedAddressName: name,
		// });
		// Xác thực lại địa chỉ
		this.onTransactionDirectionSet(address, name);
	};

	validateToAddress = async (toSelectedAddress) => {
		// const { toSelectedAddress } = this.state;
		const { network } = this.props;
		let addressError;
		if (isENS(toSelectedAddress)) {
			const resolvedAddress = await doENSLookup(toSelectedAddress, network);
			if (!resolvedAddress) {
				// Không phân giải được ENS
				addressError = strings('transaction.could_not_resolve_ens');
			}
		} else if (!isValidAddress(toSelectedAddress)) {
			// Địa chỉ không hợp lệ
			addressError = strings('transaction.invalid_address');
		}
		this.setState({ addressError });
		return addressError;
	};

	onToClear = () => {
		this.onToAddressChange();
	};

	onChangeAlias = (alias) => {
		this.setState({ alias });
	};

	onSaveToAddressBook = () => {
		const { network } = this.props;
		const { toSelectedAddress, alias } = this.state;
		const { AddressBookController } = Engine.context;
		AddressBookController.set(toSelectedAddress, alias, network);
		this.toggleAddToAddressBookModal();
		this.setState({ toSelectedAddressName: alias, addToAddressToAddressBook: false, alias: undefined });
	};

	onSaveToAddressBookPress = () => {
		this.setState({
			saveAddressModal: {
				address: this.state.toSelectedAddress,
			},
		});
	};
	onSaveToAddressBookCancel = () => {
		this.setState({
			saveAddressModal: null,
		});
	};
	onSaveToAddressBookSubmit = ({ address, newName }) => {
		this.setState({
			saveAddressModal: null,
		});
		try {
			const { network } = this.props;
			const { AddressBookController } = Engine.context;
			AddressBookController.set(toChecksumAddress(address), newName, network, '', '');
			NotificationManager.showSimpleNotification({
				status: 'success',
				duration: 1500,
				title: strings('address_book.add_contact_success_title'),
				description: strings('address_book.add_contact_success_description').replace(
					'{name}',
					newName || renderShortAddress(address)
				),
			});
		} catch (error) {
			console.log('onSaveToAddressBookSubmit : ', error)
		}
	};

	onScan = () => {
		this.props.navigation.navigate('QRScanner', {
			onScanSuccess: (meta) => {
				if (meta.target_address) {
					this.onToAddressChange(meta.target_address);
				}
			},
		});
	};

	onTransactionDirectionSet = async (address, ensRecipient) => {
		// console.log('address, ensRecipient : ', address, ensRecipient)
		const { network } = this.props;
		const { setRecipient, navigation, providerType, addRecent } = this.props;
		const { fromSelectedAddress, toEnsName, toSelectedAddressName, fromAccountName } = this.state;
		let toSelectedAddress = address || this.state.toSelectedAddress;
		let ensName = ensRecipient || toEnsName;
		let selectedAddressName = ensRecipient || toSelectedAddressName;
		// validate address
		let addressError;
		// Nếu là ENS
		if (isENS(toSelectedAddress)) {
			const resolvedAddress = await doENSLookup(toSelectedAddress, network);
			if (!resolvedAddress) {
				// Không phân giải được ENS
				addressError = strings('transaction.could_not_resolve_ens');
			} else {
				// Lấy thông tin ens
				const ens = await doENSReverseLookup(resolvedAddress, network);
				// console.log('----ens ---- : ', toSelectedAddress, ens)
				ensName = ens || toSelectedAddress;
				selectedAddressName = ens || toSelectedAddress;
				toSelectedAddress = resolvedAddress;
			}
			// this.setState({
			// 	toSelectedAddress,
			// 	toEnsName: ensName,
			// 	toSelectedAddressName: selectedAddressName,
			// });
		} else if (!isValidAddress(toSelectedAddress)) {
			// Địa chỉ không hợp lệ
			addressError = strings('transaction.invalid_address');
		}

		if (addressError) {
			this.setState({ addressError });
			return;
		}
		// this.setState({ toSelectedAddressReady: true });
		addRecent(toSelectedAddress);
		// console.log('fromSelectedAddress, toSelectedAddress, ensName, selectedAddressName, fromAccountName : ', fromSelectedAddress, toSelectedAddress, ensName, selectedAddressName, fromAccountName)
		setRecipient(fromSelectedAddress, toSelectedAddress, ensName, selectedAddressName, fromAccountName);
		this.props.navigation.replace('PaymentRequestView', {
			screen: 'PaymentRequest',
			params: { skipSelectAsset: this.props.route.params?.skipSelectAsset || false },
		});
		// InteractionManager.runAfterInteractions(() => {
		// 	Analytics.trackEventWithParameters(ANALYTICS_EVENT_OPTS.SEND_FLOW_ADDS_RECIPIENT, {
		// 		network: providerType,
		// 	});
		// });

		// navigation.navigate('Amount');
	};

	renderAddToAddressBookModal = () => {
		const { addToAddressBookModalVisible, alias } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);

		return (
			<ActionModal
				modalVisible={addToAddressBookModalVisible}
				confirmText={strings('address_book.save')}
				cancelText={strings('address_book.cancel')}
				onCancelPress={this.toggleAddToAddressBookModal}
				onRequestClose={this.toggleAddToAddressBookModal}
				onConfirmPress={this.onSaveToAddressBook}
				cancelButtonMode={'normal'}
				confirmButtonMode={'confirm'}
				confirmDisabled={!alias}
			>
				<View style={styles.addToAddressBookRoot}>
					<View style={styles.addToAddressBookWrapper} testID={ADD_ADDRESS_MODAL_CONTAINER_ID}>
						<View style={baseStyles.flexGrow}>
							<Text style={styles.addTextTitle}>{strings('address_book.add_to_address_book')}</Text>
							<Text style={styles.addTextSubtitle}>{strings('address_book.enter_an_alias')}</Text>
							<View style={styles.addInputWrapper}>
								<View style={styles.input}>
									<TextInput
										autoFocus
										autoCapitalize="none"
										autoCorrect={false}
										onChangeText={this.onChangeAlias}
										placeholder={strings('address_book.enter_an_alias_placeholder')}
										placeholderTextColor={colors.text.muted}
										spellCheck={false}
										style={styles.addTextInput}
										numberOfLines={1}
										onBlur={this.onBlur}
										onFocus={this.onInputFocus}
										onSubmitEditing={this.onFocus}
										value={alias}
										keyboardAppearance={themeAppearance}
										testID={ENTER_ALIAS_INPUT_BOX_ID}
									/>
								</View>
							</View>
						</View>
					</View>
				</View>
			</ActionModal>
		);
	};

	renderFromAccountModal = () => {
		const { identities, keyrings, ticker } = this.props;
		const { fromAccountModalVisible, fromSelectedAddress } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<Modal
				isVisible={fromAccountModalVisible}
				style={styles.bottomModal}
				onBackdropPress={this.toggleFromAccountModal}
				onBackButtonPress={this.toggleFromAccountModal}
				onSwipeComplete={this.toggleFromAccountModal}
				swipeDirection={'down'}
				propagateSwipe
				backdropColor={colors.overlay.default}
				backdropOpacity={1}
			>
				<AccountList
					enableAccountsAddition={false}
					identities={identities}
					selectedAddress={fromSelectedAddress}
					keyrings={keyrings}
					onAccountChange={this.onAccountChange}
					ticker={ticker}
				/>
			</Modal>
		);
	};

	onToInputFocus = () => {
		const { toInputHighlighted } = this.state;
		this.setState({ toInputHighlighted: !toInputHighlighted });
	};

	goToBuy = () => {
		this.props.navigation.navigate('FiatOnRamp');
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_BUY_ETH);
			AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.ONRAMP_OPENED, {
				button_location: 'Send Flow warning',
				button_copy: 'Buy ETH',
			});
		});
	};

	renderBuyEth = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (!allowedToBuy(this.props.network)) {
			return null;
		}

		return (
			<>
				{'\n'}
				<Text bold style={styles.buyEth} onPress={this.goToBuy}>
					{strings('fiat_on_ramp.buy', { ticker: getTicker(this.props.ticker) })}
				</Text>
			</>
		);
	};

	render = () => {
		const { ticker } = this.props;
		const { addressBook, network } = this.props;
		const {
			fromSelectedAddress,
			fromAccountName,
			fromAccountBalance,
			toSelectedAddress,
			toSelectedAddressReady,
			toSelectedAddressName,
			addToAddressToAddressBook,
			addressError,
			balanceIsZero,
			toInputHighlighted,
			inputWidth,
			errorContinue,
			isOnlyWarning,
			confusableCollection,
			ensNameList,
			addressList,
			saveAddressModal,
		} = this.state;
		// console.log('fromSelectedAddress, toSelectedAddress : ', fromSelectedAddress, toSelectedAddress)
		// console.log('fromAccountName, toSelectedAddressName : ', fromAccountName, toSelectedAddressName)
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		const checksummedAddress = toSelectedAddress && toChecksumAddress(toSelectedAddress);
		const existingContact = checksummedAddress && addressBook[network] && addressBook[network][checksummedAddress];
		const displayConfusableWarning = !existingContact && confusableCollection && !!confusableCollection.length;
		const displayAsWarning =
			confusableCollection && confusableCollection.length && !confusableCollection.some(hasZeroWidthPoints);
		const explanations = displayConfusableWarning && getConfusablesExplanations(confusableCollection);

		return (
			<SafeAreaView style={styles.wrapper}>
				<View style={styles.titleWrapper}>
					<View style={styles.dragger} />
				</View>
				<View style={styles.titleWrapper}>
					<Text style={{ color: '#FFFFFF', fontSize: 18, paddingBottom: 20 }}>{'Gửi'}</Text>
				</View>
				<View style={styles.imputWrapper}>
					{/* <AddressFrom
						onPressIcon={this.toggleFromAccountModal}
						fromAccountAddress={fromSelectedAddress}
						fromAccountName={fromAccountName}
						fromAccountBalance={fromAccountBalance}
					/> */}
					<AddressTo
						addressError={addressError}
						inputRef={this.addressToInputRef}
						highlighted={toInputHighlighted}
						addressToReady={toSelectedAddressReady}
						toSelectedAddress={toSelectedAddress}
						toAddressName={toSelectedAddressName}
						onToAddressChange={this.onToAddressChange}
						onScan={this.onScan}
						onClear={this.onToClear}
						onInputFocus={this.onToInputFocus}
						onInputBlur={this.onToInputFocus}
						onSubmit={() => this.onTransactionDirectionSet(toSelectedAddress)}
						inputWidth={inputWidth}
						confusableCollection={(!existingContact && confusableCollection) || []}
						onSaveToAddressBookPress={this.onSaveToAddressBookPress}
					/>
					{addressError && (
						// <View style={styles.addressErrorWrapper}>
						// 	<ErrorMessage
						// 		errorMessage={addressError}
						// 		errorContinue={!!errorContinue}
						// 		onContinue={this.onTransactionDirectionSet}
						// 		isOnlyWarning={!!isOnlyWarning}
						// 	/>
						// </View>
						<Text style={{ color: '#EB5252', fontSize: 16 }}>{addressError}</Text>
					)}
				</View>
				<AddressList
					addressList={addressList}
					ensNameList={ensNameList}
					inputSearch={toSelectedAddress}
					onAccountPress={this.onToSelectedAddress}
					onAccountLongPress={dummy}
					hideRecents
				/>
				{/* {toSelectedAddressReady ? (
					<View style={styles.nextActionWrapper} />
				) : (
					<AddressList
						addressList={addressList}
						ensNameList={ensNameList}
						inputSearch={toSelectedAddress}
						onAccountPress={this.onToSelectedAddress}
						onAccountLongPress={dummy}
						hideRecents
					/>
				)} */}
				{/* {
					toSelectedAddressReady && <KeyboardAvoidingView behavior="padding">
						<TouchableOpacity
							disabled={!toSelectedAddressReady}
							style={[
								styles.buttonStyle,
								{ backgroundColor: toSelectedAddressReady ? '#6A45FF' : '#3D3D4B' },
							]}
							onPress={() => {
								// this.props.navigation.navigate('Amount')
								this.props.navigation.replace('PaymentRequestView', {
									screen: 'PaymentRequest',
									params: { skipSelectAsset: this.props.route.params?.skipSelectAsset || false },
								});
							}}
						>
							<Text style={styles.content}>Tiếp tục</Text>
						</TouchableOpacity>
					</KeyboardAvoidingView>
				} */}
				<AddressSaveModal
					isVisible={!!saveAddressModal}
					address={saveAddressModal?.address}
					onAddressSaveCancel={this.onSaveToAddressBookCancel}
					onAddressSaveSubmit={this.onSaveToAddressBookSubmit}
				/>
			</SafeAreaView>
		);
	};
}

SendFlow.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	accounts: state.engine.backgroundState.AccountTrackerController.accounts,
	addressBook: state.engine.backgroundState.AddressBookController.addressBook,
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
	selectedAsset: state.transaction.selectedAsset,
	identities: state.engine.backgroundState.PreferencesController.identities,
	keyrings: state.engine.backgroundState.KeyringController.keyrings,
	ticker: state.engine.backgroundState.NetworkController.provider.ticker,
	network: state.engine.backgroundState.NetworkController.network,
	providerType: state.engine.backgroundState.NetworkController.provider.type,
	isPaymentRequest: state.transaction.paymentRequest,
});

const mapDispatchToProps = (dispatch) => ({
	addRecent: (address) => dispatch(addRecent(address)),
	setRecipient: (from, to, ensRecipient, transactionToName, transactionFromName) =>
		dispatch(setRecipient(from, to, ensRecipient, transactionToName, transactionFromName)),
	newAssetTransaction: (selectedAsset) => dispatch(newAssetTransaction(selectedAsset)),
	setSelectedAsset: (selectedAsset) => dispatch(setSelectedAsset(selectedAsset)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SendFlow);
