import React from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, Image, Keyboard } from 'react-native';
import { fontStyles, baseStyles } from '../../../../styles/common';
import AntIcon from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import Identicon from '../../../UI/Identicon';
import { isQRHardwareAccount, renderShortAddress } from '../../../../util/address';
import { strings } from '../../../../../locales/i18n';
import Text from '../../../Base/Text';
import { hasZeroWidthPoints } from '../../../../util/confusables';
import { useAppThemeFromContext, mockTheme } from '../../../../util/theme';
import Clipboard from '@react-native-clipboard/clipboard';
import { toChecksumAddress } from "ethereumjs-util";
import { useSelector } from "react-redux";

// import saveAddressIcon from '../../../../images-new/address-book/save-address.png';

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			flexDirection: 'row',
		},
		selectWrapper: {
			flex: 1,
			flexDirection: 'row',
			borderRadius: 8,
			marginVertical: 8,
			alignItems: 'center',
			marginHorizontal: 8
		},
		inputWrapper: {
			flex: 1,
			marginLeft: 8,
			padding: 10,
			// minHeight: 52,
			flexDirection: 'row',
			borderRadius: 8,
			marginTop: 8,
		},
		input: {
			flex: 1,
			flexDirection: 'row',
			alignItems: 'center',
		},
		identiconWrapper: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		addressToInformation: {
			flex: 1,
			flexDirection: 'row',
			alignItems: 'center',
			position: 'relative',
		},
		exclamation: {
			backgroundColor: colors.background.default,
			borderRadius: 12,
			position: 'absolute',
			bottom: 8,
			left: 20,
		},
		address: {
			flexDirection: 'column',
			alignItems: 'flex-start',
			marginHorizontal: 8,
		},
		addressWrapper: { flexDirection: 'row' },
		textAddress: {
			...fontStyles.normal,
			color: colors.text.default,
			fontSize: 14,
		},
		accountNameLabel: {
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'center',
		},
		accountNameLabelText: {
			marginLeft: 4,
			paddingHorizontal: 8,
			...fontStyles.bold,
			color: colors.primary.alternative,
			borderWidth: 1,
			borderRadius: 8,
			borderColor: colors.primary.alternative,
			fontSize: 10,
		},
		textBalance: {
			...fontStyles.normal,
			fontSize: 12,
			color: colors.text.alternative,
		},
		label: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		labelTextWrapper: {
			flex: 1,
			flexDirection: 'row',
			justifyContent: 'space-between',
			alignItems: 'center',
		},
		labelText: {
			...fontStyles.normal,
			color: '#FFFFFF',
			fontSize: 16,
		},
		textInput: {
			...fontStyles.normal,
			paddingLeft: 0,
			paddingRight: 8,
			width: '100%',
			color: '#FFFFFF',
			fontWeight: "500",
			fontSize: 18,
		},
		scanIcon: {
			// flexDirection: 'column',
			// alignItems: 'center',
			height: 26,
			width: 26
		},
		iconOpaque: {
			color: colors.icon.default,
		},
		iconHighlighted: {
			color: colors.primary.default,
		},
		borderOpaque: {
			borderColor: colors.border.default,
		},
		borderHighlighted: {
			borderColor: colors.primary.default,
		},
		iconWrapper: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		dropdownIconWrapper: {
			height: 23,
			width: 23,
		},
		dropdownIcon: {
			alignSelf: 'center',
		},
		checkIconWrapper: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		checkAddress: {
			flex: 0.9,
		},
		toInputWrapper: {
			flexDirection: 'row',
		},
		saveButton: {
			padding: 10,
			marginRight: -10,
		},
		saveButtonImage: {
			height: 16,
			width: 16,
		},
	});

const AddressName = ({ toAddressName, confusableCollection = [] }) => {
	const { colors } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);

	if (confusableCollection.length) {
		const texts = toAddressName.split('').map((char, index) => {
			// if text has a confusable highlight it red
			if (confusableCollection.includes(char)) {
				// if the confusable is zero width, replace it with `?`
				const replacement = hasZeroWidthPoints(char) ? '?' : char;
				return (
					<Text red key={index}>
						{replacement}
					</Text>
				);
			}
			return (
				<Text black key={index}>
					{char}
				</Text>
			);
		});
		return (
			<Text style={styles.textAddress} numberOfLines={1}>
				{texts}
			</Text>
		);
	}
	return (
		<Text style={styles.textAddress} numberOfLines={1}>
			{toAddressName}
		</Text>
	);
};

AddressName.propTypes = {
	toAddressName: PropTypes.string,
	confusableCollection: PropTypes.array,
};

export const AddressTo = (props) => {
	const {
		addressToReady,
		highlighted,
		inputRef,
		toSelectedAddress,
		onToAddressChange,
		onScan,
		onClear,
		toAddressName,
		onInputFocus,
		onSubmit,
		onInputBlur,
		inputWidth,
		confusableCollection,
		displayExclamation,
		addressError,
		onSaveToAddressBookPress,
	} = props;
	// console.log('toAddressName : ', toAddressName, toSelectedAddress)
	const { colors, themeAppearance } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);

	const addressBook = useSelector((state) => state.engine.backgroundState.AddressBookController.addressBook);
	const identities = useSelector((state) => state.engine.backgroundState.PreferencesController.identities);
	const network = useSelector((state) => state.engine.backgroundState.NetworkController.network);

	const pateAddress = async () => {
		const text = await Clipboard.getString();
		onToAddressChange(text)
		inputRef.current.focus();
	}

	const isAddressSaved = () => {
		const checksummedResolvedAddress = toChecksumAddress(toSelectedAddress);
		const networkAddressBook = addressBook[network] || {};
		if (networkAddressBook[checksummedResolvedAddress] || identities[checksummedResolvedAddress]) {
			return true;
		}
		return false;
	};

	return (
		<View style={styles.wrapper}>
			<View style={styles.label}>
				<Text style={styles.labelText}>tới:</Text>
			</View>
			<View style={styles.selectWrapper}>
				<View style={styles.input}>
					<TextInput
						autoFocus
						ref={inputRef}
						autoCapitalize="none"
						autoCorrect={false}
						onChangeText={onToAddressChange}
						placeholder={'địa chỉ hoặc tên ví'}
						placeholderTextColor={'#58586C'}
						spellCheck={false}
						style={[styles.textInput, inputWidth, { color: addressError ? '#EB5252' : '#FFFFFF' }]}
						numberOfLines={1}
						onFocus={onInputFocus}
						onBlur={onInputBlur}
						onSubmitEditing={onSubmit}
						value={toSelectedAddress}
						testID={'txn-to-address-input'}
						keyboardAppearance={themeAppearance}
					/>
				</View>
				{!!onScan && (
					<TouchableOpacity onPress={onScan} style={styles.iconWrapper}>
						<Image source={require('../../../../images-new/scan2.png')} resizeMode={"contain"} style={styles.scanIcon}></Image>
						{/* <AntIcon
								name="scan1"
								size={20}
								style={[styles.scanIcon, highlighted ? styles.iconHighlighted : styles.iconOpaque]}
							/> */}
					</TouchableOpacity>
				)}
				<TouchableOpacity onPress={pateAddress} style={{
					backgroundColor: '#3D3D4B',
					height: 26, width: 40,
					justifyContent: 'center', alignItems: 'center',
					borderRadius: 13,
					marginLeft: 15
				}}>
					<Text style={{
						color: '#FFFFFF',
						fontSize: 14,
						fontWeight: '500'
					}}>{'Dán'}</Text>
				</TouchableOpacity>
			</View>
			{/* {!addressToReady ? (
				<View style={styles.selectWrapper}>
					<View style={styles.input}>
						<TextInput
							autoFocus
							ref={inputRef}
							autoCapitalize="none"
							autoCorrect={false}
							onChangeText={onToAddressChange}
							placeholder={'địa chỉ hoặc tên ví'}
							placeholderTextColor={'#58586C'}
							spellCheck={false}
							style={[styles.textInput, inputWidth, { color: addressError ? '#EB5252' : '#FFFFFF' }]}
							numberOfLines={1}
							onFocus={onInputFocus}
							onBlur={onInputBlur}
							onSubmitEditing={onSubmit}
							value={toSelectedAddress}
							testID={'txn-to-address-input'}
							keyboardAppearance={themeAppearance}
						/>
					</View>
					{!!onScan && (
						<TouchableOpacity onPress={onScan} style={styles.iconWrapper}>
							<Image source={require('../../../../images-new/scan2.png')} resizeMode={"contain"} style={styles.scanIcon}></Image>
						</TouchableOpacity>
					)}
					<TouchableOpacity onPress={pateAddress} style={{
						backgroundColor: '#3D3D4B',
						height: 26, width: 40,
						justifyContent: 'center', alignItems: 'center',
						borderRadius: 13,
						marginLeft: 15
					}}>
						<Text style={{
							color: '#FFFFFF',
							fontSize: 14,
							fontWeight: '500'
						}}>{'Dán'}</Text>
					</TouchableOpacity>
				</View>
			) : (
				<View style={styles.labelTextWrapper}>
					<Text
						style={[styles.labelText, { marginLeft: 8, color: '#2F80ED', fontWeight: '600' }]}
						numberOfLines={1}
					>
						{toAddressName || renderShortAddress(toSelectedAddress)}
					</Text>
					{!isAddressSaved() && (
						<TouchableOpacity
							style={styles.saveButton}
							onPress={onSaveToAddressBookPress}
						>
							<Image style={styles.saveButtonImage} source={saveAddressIcon} />
						</TouchableOpacity>
					)}
				</View>
			)} */}
		</View>
	);
};

AddressTo.propTypes = {
	/**
	 * Whether is a valid Ethereum address to send to
	 */
	addressToReady: PropTypes.bool,
	/**
	 * Whether the input is highlighted
	 */
	highlighted: PropTypes.bool,
	/**
	 * Object to use as reference for input
	 */
	inputRef: PropTypes.object,
	/**
	 * Address of selected address as string
	 */
	toSelectedAddress: PropTypes.string,
	/**
	 * Callback called when to selected address changes
	 */
	onToAddressChange: PropTypes.func,
	/**
	 * Callback called when scan icon is pressed
	 */
	onScan: PropTypes.func,
	/**
	 * Callback called when close icon is pressed
	 */
	onClear: PropTypes.func,
	/**
	 * Callback called when input onFocus
	 */
	onInputFocus: PropTypes.func,
	/**
	 * Callback called when input is submitted
	 */
	onSubmit: PropTypes.func,
	/**
	 * Callback called when input onBlur
	 */
	onInputBlur: PropTypes.func,
	/**
	 * Name of selected address as string
	 */
	toAddressName: PropTypes.string,
	/**
	 * Input width to solve android paste bug
	 * https://github.com/facebook/react-native/issues/9958
	 */
	inputWidth: PropTypes.object,
	/**
	 * Array of confusables
	 */
	confusableCollection: PropTypes.array,
	/**
	 * Display Exclamation Icon
	 */
	displayExclamation: PropTypes.bool,
	/**
	 * onSaveToAddressBookPress
	 */
	onSaveToAddressBookPress: PropTypes.func,
};

export const AddressFrom = (props) => {
	const { highlighted, onPressIcon, fromAccountName, fromAccountBalance, fromAccountAddress } = props;
	const isHardwareAccount = isQRHardwareAccount(fromAccountAddress);
	const { colors } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);

	return (
		<View style={styles.wrapper}>
			<View style={styles.label}>
				<Text style={styles.labelText}>From:</Text>
			</View>
			<View style={[styles.inputWrapper, highlighted ? styles.borderHighlighted : styles.borderOpaque]}>
				<View style={styles.identiconWrapper}>
					<Identicon address={fromAccountAddress} diameter={30} />
				</View>
				<View style={[baseStyles.flexGrow, styles.address]}>
					<View style={styles.accountNameLabel}>
						<Text style={styles.textAddress}>{fromAccountName}</Text>
						{isHardwareAccount && (
							<Text style={styles.accountNameLabelText}>{strings('transaction.hardware')}</Text>
						)}
					</View>
					<Text style={styles.textBalance}>{`${strings(
						'transactions.address_from_balance'
					)} ${fromAccountBalance}`}</Text>
				</View>

				{!!onPressIcon && (
					<TouchableOpacity onPress={onPressIcon} style={styles.iconWrapper}>
						<View style={styles.dropdownIconWrapper}>
							<FontAwesome
								name={'caret-down'}
								size={20}
								style={[styles.dropdownIcon, highlighted ? styles.iconHighlighted : styles.iconOpaque]}
							/>
						</View>
					</TouchableOpacity>
				)}
			</View>
		</View>
	);
};

AddressFrom.propTypes = {
	/**
	 * Whether the input is highlighted
	 */
	highlighted: PropTypes.bool,
	/**
	 * Callback to execute when icon is pressed
	 */
	onPressIcon: PropTypes.func,
	/**
	 * Address of selected address as string
	 */
	fromAccountAddress: PropTypes.string,
	/**
	 * Name of selected address as string
	 */
	fromAccountName: PropTypes.string,
	/**
	 * Account balance of selected address as string
	 */
	fromAccountBalance: PropTypes.string,
};
