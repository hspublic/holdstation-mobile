import React, { PureComponent } from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import PropTypes from "prop-types";
import { strings } from '../../../../../locales/i18n';
import { connect } from 'react-redux';
import { ThemeContext, mockTheme } from '../../../../util/theme';
import Modal from 'react-native-modal';
import AddressAvatar from "../../SendFlow/AddressAvatar";
import { renderShortAddress } from '../../../../util/address';

const createStyles = (colors) =>
	StyleSheet.create({
		deleteContactModal: {
			margin: 0,
			width: '100%',
			// padding: 20,
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
		},
		deleteContactModalContent: {
			flex: 1,
			padding: 20,
			justifyContent: 'center',
			alignItems: 'center',
			backgroundColor: '#1B1B23',
			borderRadius: 20,
		},
		deleteContactModalMesage: {
			fontSize: 16,
			fontWeight: '600',
			color: '#FFF',
			marginBottom: 20,
		},
		deleteContactModalName: {
			fontSize: 16,
			fontWeight: '600',
			color: '#FFF',
			marginTop: 8,
		},
		deleteContactModalAddress: {
			fontSize: 12,
			fontWeight: '500',
			color: '#A0A0B1',
			marginTop: 2,
			marginBottom: 20,
		},
		deleteContactModalButton: {
			width: '100%',
			justifyContent: 'center',
			alignItems: 'center',
			padding: 10,
		},
		deleteContactModalButtonText: {
			fontSize: 14,
			fontWeight: '500',
			color: '#FFF',
		},
		deleteContactModalButtonTextRed: {
			color: '#EB5252',
		},
	});

const EDIT = 'edit';
const ADD = 'add';

/**
 * View that contains app information
 */
class ContactDeleteModal extends PureComponent {
	static propTypes = {
		/**
		 * contact name
		 */
		name: PropTypes.string,
		/**
		/* contact address
		 */
		address: PropTypes.string,
		/**
		/* isVisible
		 */
		isVisible: PropTypes.bool,
		/**
		/* onAddressDeleteCancel
		 */
		onAddressDeleteCancel: PropTypes.func,
		/**
		/* onAddressDeleteConfirm
		 */
		onAddressDeleteConfirm: PropTypes.func,
	};

	render = () => {
		const { isVisible, name, address, avatar, onAddressDeleteCancel, onAddressDeleteConfirm } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<Modal
				isVisible={isVisible}
				style={styles.deleteContactModal}
				onBackdropPress={onAddressDeleteCancel}
				onBackButtonPress={onAddressDeleteCancel}
				onSwipeComplete={onAddressDeleteCancel}
				swipeDirection={'down'}
				backdropColor={'#000'}
				backdropOpacity={0.8}
				avoidKeyboard
			>
				<View style={styles.deleteContactModalContent}>
					<Text style={styles.deleteContactModalMesage}>
						{strings('address_book.delete_contact_message')}
					</Text>
					<AddressAvatar
						name={name}
						address={address}
						avatar={avatar}
						width={52}
						height={52}
					/>
					<Text style={styles.deleteContactModalName} numberOfLines={1}>
						{name || renderShortAddress(address)}
					</Text>
					<Text style={styles.deleteContactModalAddress} numberOfLines={1}>
						{address}
					</Text>
					<TouchableOpacity
						style={styles.deleteContactModalButton}
						onPress={onAddressDeleteConfirm}
					>
						<Text
							style={[
								styles.deleteContactModalButtonText,
								styles.deleteContactModalButtonTextRed,
							]}
						>
							{strings('address_book.delete')}
						</Text>
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.deleteContactModalButton}
						onPress={onAddressDeleteCancel}
					>
						<Text style={styles.deleteContactModalButtonText}>
							{strings('address_book.no')}
						</Text>
					</TouchableOpacity>
				</View>
			</Modal>
		);
	};
}

ContactDeleteModal.contextType = ThemeContext;

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(ContactDeleteModal);
