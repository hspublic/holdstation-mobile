import Clipboard from '@react-native-clipboard/clipboard';
import { isValidAddress, toChecksumAddress } from 'ethereumjs-util';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { launchImageLibrary } from 'react-native-image-picker';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { strings } from '../../../../../locales/i18n';
import { mockTheme, ThemeContext } from '../../../../util/theme';
import AddressAvatar from '../../SendFlow/AddressAvatar';

import avatarIcon from '../../../../images-new/address-book/avatar.png';
import { ensClient } from '../../../../util/apollo/client';
import { ENS_SUGGESTIONS } from '../../../../util/apollo/queries';
import AddressElement from '../AddressElement';

const createStyles = (colors) =>
	StyleSheet.create({
		daggerWrapper: {
			width: '100%',
			height: 40,
			alignItems: 'center',
			justifyContent: 'flex-end',
		},
		dagger: {
			width: 40,
			height: 4,
			borderRadius: 20,
			backgroundColor: '#A0A0B1',
			marginBottom: 4,
		},
		wrapper: {
			margin: 0,
			width: '100%',
			padding: 0,
			justifyContent: 'flex-end',
			alignItems: 'center',
		},
		content: {
			width: '100%',
			height: '80%',
			padding: 20,
			backgroundColor: '#1B1B23',
			borderTopRightRadius: 20,
			borderTopLeftRadius: 20,
			justifyContent: 'space-between',
			alignItems: 'center',
		},
		inputs: {
			width: '100%',
			justifyContent: 'center',
			alignItems: 'center',
		},
		title: {
			fontSize: 16,
			fontWeight: '500',
			color: '#FFF',
			marginBottom: 32,
		},
		avatarEmpty: {
			width: 52,
			height: 52,
			borderRadius: 26,
			backgroundColor: 'rgba(255, 255, 255, 0.15)',
			justifyContent: 'center',
			alignItems: 'center',
		},
		avatarEmptyImage: {
			width: 20,
			height: 20,
		},
		avatarActions: {
			marginTop: 12,
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
		},
		avatarActionsButton: {
			backgroundColor: '#58586C',
			borderRadius: 12,
			padding: 8,
		},
		avatarActionsButtonSecond: {
			marginLeft: 12,
		},
		avatarActionsButtonText: {
			fontSize: 12,
			fontWeight: '500',
			color: '#FFF',
		},
		inputGroup: {
			marginTop: 24,
			width: '100%',
		},
		label: {
			fontSize: 12,
			fontWeight: '500',
			color: '#FFF',
		},
		inputWrapper: {
			marginTop: 4,
		},
		input: {
			color: '#FFF',
			fontSize: 14,
			fontWeight: '400',
			borderWidth: 1,
			borderRadius: 12,
			borderColor: 'rgba(255, 255, 255, 0.15)',
			padding: 12,
		},
		inputWithButton: {
			paddingRight: 70,
		},
		inputFocus: {
			borderColor: '#FFF',
		},
		inputError: {
			borderColor: '#EB5252',
		},
		errorText: {
			fontSize: 12,
			fontWeight: '400',
			color: '#EB5252',
			marginTop: 4,
		},
		inputButton: {
			position: 'absolute',
			height: '100%',
			top: 0,
			right: 0,
			justifyContent: 'center',
			alignItems: 'center',
			paddingHorizontal: 10,
		},
		inputButtonText: {
			fontSize: 14,
			fontWeight: '500',
			color: '#7E73FF',
		},
		saveButton: {
			width: '100%',
			padding: 12,
			borderRadius: 12,
			backgroundColor: '#6A45FF',
			marginTop: 32,
			marginBottom: 20,
			justifyContent: 'center',
			alignItems: 'center',
		},
		saveButtonText: {
			fontSize: 16,
			fontWeight: '500',
			color: '#FFF',
		},
		saveButtonDisabled: {
			backgroundColor: '#3D3D4B',
		},
		saveButtonDisabledText: {
			color: '#A0A0B1',
		},
	});

/**
 * View that contains app information
 */
class ContactFormModal extends PureComponent {
	static propTypes = {
		network: PropTypes.string,
		/**
		 * An object containing each identity in the format address => account
		 */
		identities: PropTypes.object,
		/**
		 * Map representing the address book
		 */
		addressBook: PropTypes.object,
		/**
		 * contact name
		 */
		name: PropTypes.string,
		/**
		/* contact address
		 */
		address: PropTypes.string,
		/**
		/* contact avatar
		 */
		avatar: PropTypes.string,
		/**
		/* isVisible
		 */
		isVisible: PropTypes.bool,
		/**
		/* onAddressFormCancel
		 */
		onAddressFormCancel: PropTypes.func,
		/**
		/* onAddressFormSubmit
		 */
		onAddressFormSubmit: PropTypes.func,
	};

	state = {
		newName: this.props.name,
		newNameFocus: false,
		newNameError: '',
		newAddress: this.props.address,
		newAddressFocus: false,
		newAddressError: '',
		newAvatar: this.props.avatar,
		ensList: [],
	};

	getNewNameError = (newName) => {
		if (newName.length === 0) {
			return '';
		}

		if (!newName) {
			return strings('address_book.reminiscent_name_empty');
		}
		if (newName.length > 18) {
			return strings('address_book.reminiscent_name_too_long');
		}
		const { name } = this.props;
		if (newName !== name) {
			const { addressBook, network, identities } = this.props;
			const networkAddressBook = addressBook[network] || {};
			let addresses = Object.keys(networkAddressBook);
			for (let i = 0; i < addresses.length; i++) {
				const address = addresses[i];
				const contact = networkAddressBook[address];
				if (contact.name === newName.trim()) {
					return strings('address_book.reminiscent_name_existed');
				}
			}
			addresses = Object.keys(identities);
			for (let i = 0; i < addresses.length; i++) {
				const address = addresses[i];
				const contact = identities[address];
				if (contact.name === newName.trim()) {
					return strings('address_book.reminiscent_name_existed');
				}
			}
		}
		return '';
	};
	onNewNameChanged = (text) => {
		this.setState({
			newName: text,
			newNameError: this.getNewNameError(text),
		});
	};

	checkIfAlreadySaved = (newAddress) => {
		const { address } = this.props;
		const checksummedResolvedAddress = toChecksumAddress(newAddress);
		if (checksummedResolvedAddress !== address) {
			const { addressBook, network, identities } = this.props;
			const networkAddressBook = addressBook[network] || {};
			if (networkAddressBook[checksummedResolvedAddress] || identities[checksummedResolvedAddress]) {
				return strings('address_book.wallet_address_existed');
			}
		}
		return '';
	};
	getNewAddressError = (newAddress) => {
		if (!newAddress) {
			return strings('address_book.wallet_address_empty');
		}
		if (isValidAddress(newAddress)) {
			return this.checkIfAlreadySaved(newAddress);
		}
		if (newAddress.length > 42) {
			return strings('address_book.wallet_address_too_long');
		}

		return strings('address_book.wallet_address_invalid');
	};
	onNewAddressChanged = async (text) => {
		this.setState({
			newAddress: text,
			newAddressError: this.getNewAddressError(text),
		});

		const ensList = [];
		if (text.length >= 3) {
			const response = await ensClient.query({
				query: ENS_SUGGESTIONS,
				variables: {
					amount: 3,
					name: text,
				},
			});
			if (response?.data?.domains) {
				response.data.domains.forEach((domain) => {
					ensList.push({
						address: domain.resolver.addr.id || domain.owner.id,
						name: domain.name,
					});
				});
			}
		}
		this.setState({ ensList });
	};
	onNewAddressPaste = async () => {
		const text = await Clipboard.getString();
		this.onNewAddressChanged(text);
	};
	onNewAddressClear = () => {
		this.onNewAddressChanged('');
	};

	onNewAvatarPickImage = () => {
		launchImageLibrary(
			{
				mediaType: 'photo',
				quality: 1,
			},
			({ didCancel, errorCode, assets }) => {
				if (didCancel) {
					return;
				}
				if (errorCode) {
					return;
				}
				if (!assets.length) {
					return;
				}

				this.setState({
					newAvatar: assets[0].uri,
				});
			}
		);
	};
	onNewAvatarClearImage = () => {
		this.setState({
			newAvatar: '',
		});
	};

	onEnsDomainPress = ({ name, address }) => {
		this.setState({
			newName: name,
			newNameError: this.getNewNameError(name),
			newAddress: address,
			newAddressError: this.getNewAddressError(address),
			ensList: [],
		});
	};

	render = () => {
		const { isVisible, name, address, avatar, onAddressFormCancel, onAddressFormSubmit } = this.props;
		const {
			newName,
			newNameFocus,
			newNameError,
			newAddress,
			newAddressFocus,
			newAddressError,
			newAvatar,
			ensList,
		} = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || mockTheme.themeAppearance;
		const styles = createStyles(colors);

		return (
			<Modal
				isVisible={isVisible}
				style={styles.wrapper}
				onBackdropPress={onAddressFormCancel}
				onBackButtonPress={onAddressFormCancel}
				onSwipeComplete={onAddressFormCancel}
				swipeDirection={'down'}
				backdropColor={'#000'}
				backdropOpacity={0.8}
				avoidKeyboard
			>
				<View style={styles.daggerWrapper}>
					<View style={styles.dagger} />
				</View>
				<View style={styles.content}>
					<View style={styles.inputs}>
						<Text style={styles.title}>
							{strings(`address_book.${address ? 'edit_information' : 'add_to_list'}`)}
						</Text>
						{!newName && !newAvatar && (
							<View style={styles.avatarEmpty}>
								<Image style={styles.avatarEmptyImage} source={avatarIcon} />
							</View>
						)}
						{(!!newName || !!newAvatar) && (
							<AddressAvatar
								name={newName}
								address={newAddress}
								avatar={newAvatar}
								width={52}
								height={52}
							/>
						)}
						<View style={styles.avatarActions}>
							<TouchableOpacity
								style={styles.avatarActionsButton}
								onPress={this.onNewAvatarPickImage}
							>
								<Text style={styles.avatarActionsButtonText}>{strings(`address_book.${newAvatar ? 'change_image' : 'add_image'}`)}</Text>
							</TouchableOpacity>
							{!!newAvatar && (
								<TouchableOpacity
									style={[styles.avatarActionsButton, styles.avatarActionsButtonSecond]}
									onPress={this.onNewAvatarClearImage}
								>
									<Text style={styles.avatarActionsButtonText}>
										{strings('address_book.clear_image')}
									</Text>
								</TouchableOpacity>
							)}
						</View>
						<View style={styles.inputGroup}>
							<Text style={styles.label}>{`${strings('address_book.reminiscent_name')}*`}</Text>
							<View style={styles.inputWrapper}>
								<TextInput
									// ref={inputRef}
									autoCapitalize="none"
									autoCorrect={false}
									onChangeText={this.onNewNameChanged}
									placeholder={strings('address_book.input_information')}
									placeholderTextColor={'#58586C'}
									spellCheck={false}
									style={[
										styles.input,
										newNameFocus ? styles.inputFocus : {},
										newNameError ? styles.inputError : {},
									]}
									numberOfLines={1}
									onFocus={() => this.setState({ newNameFocus: true })}
									onBlur={() => this.setState({ newNameFocus: false })}
									onSubmitEditing={() => {}}
									value={newName}
									keyboardAppearance={themeAppearance}
									maxLength={19}
								/>
							</View>
							{!!newNameError && <Text style={styles.errorText}>{newNameError}</Text>}
						</View>
						<View style={styles.inputGroup}>
							<Text style={styles.label}>{`${strings('address_book.wallet_address')}*`}</Text>
							<View style={styles.inputWrapper}>
								<TextInput
									// ref={inputRef}
									autoCapitalize="none"
									autoCorrect={false}
									onChangeText={this.onNewAddressChanged}
									placeholder={strings('address_book.input_information')}
									placeholderTextColor={'#58586C'}
									spellCheck={false}
									style={[
										styles.input,
										styles.inputWithButton,
										newAddressFocus ? styles.inputFocus : {},
										newAddressError ? styles.inputError : {},
									]}
									numberOfLines={1}
									onFocus={() => this.setState({ newAddressFocus: true })}
									onBlur={() => this.setState({ newAddressFocus: false })}
									onSubmitEditing={() => {}}
									value={newAddress}
									keyboardAppearance={themeAppearance}
									maxLength={43}
								/>
								{!newAddress && (
									<TouchableOpacity style={styles.inputButton} onPress={this.onNewAddressPaste}>
										<Text style={styles.inputButtonText}>{strings('address_book.paste')}</Text>
									</TouchableOpacity>
								)}
								{!!newAddress && (
									<TouchableOpacity style={styles.inputButton} onPress={this.onNewAddressClear}>
										<Text style={styles.inputButtonText}>{strings('address_book.delete')}</Text>
									</TouchableOpacity>
								)}
							</View>
							{!!newAddressError && <Text style={styles.errorText}>{newAddressError}</Text>}
							{ensList.map((domain) => (
								<AddressElement
									key={domain.address + domain.name}
									address={domain.address}
									name={domain.name}
									avatar={''}
									onAccountPress={this.onEnsDomainPress}
								/>
							))}
						</View>
					</View>
					<TouchableOpacity
						style={[
							styles.saveButton,
							!newName || !newAddress || !!newNameError || !!newAddressError
								? styles.saveButtonDisabled
								: {},
						]}
						onPress={() => onAddressFormSubmit({ address, name, avatar, newAddress, newName: newName.trim(), newAvatar })}
						disabled={!newName || !newAddress || !!newNameError || !!newAddressError}
					>
						<Text
							style={[
								styles.saveButtonText,
								!newName || !newAddress || !!newNameError || !!newAddressError
									? styles.saveButtonDisabledText
									: {},
							]}
						>
							{strings('address_book.save')}
						</Text>
					</TouchableOpacity>
				</View>
			</Modal>
		);
	};
}

ContactFormModal.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	addressBook: state.engine.backgroundState.AddressBookController.addressBook,
	identities: state.engine.backgroundState.PreferencesController.identities,
	network: state.engine.backgroundState.NetworkController.network,
});

export default connect(mapStateToProps)(ContactFormModal);
