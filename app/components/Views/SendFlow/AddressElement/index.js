import React, { PureComponent } from 'react';
import { renderShortAddress } from '../../../../util/address';
import { StyleSheet, View, Text, SafeAreaView, Image } from "react-native";
import Identicon from '../../../UI/Identicon';
import { fontStyles } from '../../../../styles/common';
import PropTypes from 'prop-types';
import { doENSReverseLookup } from '../../../../util/ENSUtils';
import { connect } from 'react-redux';
import { ThemeContext, mockTheme } from '../../../../util/theme';

import { GestureHandlerRootView, Swipeable, TouchableOpacity } from "react-native-gesture-handler";
import AddressAvatar from '../AddressAvatar'

const createStyles = (colors) =>
	StyleSheet.create({
		addressElementWrapper: {
			paddingVertical: 12,
			flexDirection: 'row',
			alignItems: 'center',
			// borderBottomWidth: 1,
			// borderBottomColor: colors.border.muted,
		},
		addressElementInformation: {
			flex: 1,
			flexDirection: 'column',
			paddingLeft: 8,
		},
		addressIdenticon: {
			paddingRight: 16,
		},
		addressTextNickname: {
			// ...fontStyles.normal,
			// flex: 1,
			color: '#FFFFFF',
			fontSize: 16,
			fontWeight: '600',
		},
		addressTextAddress: {
			// ...fontStyles.normal,
			fontSize: 12,
			fontWeight: '500',
			color: '#A0A0B1',
			marginTop: 2,
		},
		rightAction: {
			backgroundColor: '#1B1B23',
			justifyContent: 'center',
			flexDirection: 'row',
			flexWrap: 'wrap',
			alignItems: 'center',
		},
		rightActionButton: {
			width: 20,
			height: 20,
			marginTop: 25,
			marginLeft: 5,
			marginRight: 5,
		},
	});

class AddressElement extends PureComponent {
	static propTypes = {
		/**
		 * Name to display
		 */
		name: PropTypes.string,
		/**
		 * Ethereum address
		 */
		address: PropTypes.string,
		/**
		 * Contact avatar
		 */
		avatar: PropTypes.string,
		/**
		 * Callback on account press
		 */
		onAccountPress: PropTypes.func,
		/**
		 * Callback on account long press
		 */
		onAccountLongPress: PropTypes.func,
		/**
		 * Callback on account edit press
		 */
		onAccountEditPress: PropTypes.func,
		/**
		 * Callback on account delete press
		 */
		onAccountDeletePress: PropTypes.func,
		/**
		 * Network id
		 */
		network: PropTypes.string,
		key: PropTypes.any,
	};

	state = {
		name: this.props.name,
		address: this.props.address,
	};

	componentDidMount = async () => {
		const { name, address } = this.state;
		const { network } = this.props;
		if (!name) {
			const ensName = await doENSReverseLookup(address, network);
			this.setState({ name: ensName });
		}
	};

	render = () => {
		const { key, name, address, avatar, onAccountPress, onAccountLongPress, onAccountEditPress, onAccountDeletePress } = this.props;
		const primaryLabel = name && name[0] !== ' ' ? name : renderShortAddress(address);
		const secondaryLabel = name && name[0] !== ' ' && renderShortAddress(address);
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		let swipeableRef = null;

		return (
			<SafeAreaView key={key}>
				<GestureHandlerRootView>
					<Swipeable
						ref={(swipe) => swipeableRef = swipe}
						rightThreshold={10}
						renderLeftActions={(progress, dragX) => null}
						renderRightActions={(progress, dragX) => {
							if (!onAccountEditPress || !onAccountDeletePress) {
								return null;
							}
							return (
								<View style={styles.rightAction}>
									<TouchableOpacity
										onPress={() => {
											swipeableRef.close();
											onAccountEditPress({ address, name, avatar });
										}}
									>
										<Image
											source={require('../../../../images/icon-pencil.png')}
											style={styles.rightActionButton}
											resizeMethod={'auto'}
										/>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() => {
											swipeableRef.close();
											onAccountDeletePress({ address, name, avatar });
										}}
									>
										<Image
											source={require('../../../../images/icon-trash.png')}
											style={styles.rightActionButton}
											resizeMethod={'auto'}
										/>
									</TouchableOpacity>
								</View>
							)
						}}
					>
						<TouchableOpacity
							onPress={() => onAccountPress({ name, address, avatar })}
							// onLongPress={() => onAccountLongPress(address)}
							key={address}
							style={styles.addressElementWrapper}
						>
							{/*<View style={styles.addressIdenticon}>*/}
							{/*	<Identicon address={address} diameter={32} />*/}
							{/*</View>*/}
							<AddressAvatar
								name={name}
								address={address}
								avatar={avatar}
								height={52}
								width={52}
							/>
							<View style={styles.addressElementInformation}>
								<Text style={styles.addressTextNickname} numberOfLines={1}>
									{primaryLabel}
								</Text>
								{!!secondaryLabel && (
									<Text style={styles.addressTextAddress} numberOfLines={1}>
										{secondaryLabel}
									</Text>
								)}
							</View>
						</TouchableOpacity>
					</Swipeable>
				</GestureHandlerRootView>
			</SafeAreaView>
		);
	};
}

AddressElement.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	network: state.engine.backgroundState.NetworkController.network,
});

export default connect(mapStateToProps)(AddressElement);
