import React, { PureComponent } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Image } from 'react-native';
import PropTypes from "prop-types";
import { strings } from '../../../../../locales/i18n';
import { connect } from 'react-redux';
import { ThemeContext, mockTheme, useAppThemeFromContext } from "../../../../util/theme";
import Modal from 'react-native-modal';
import AddressAvatar from "../../SendFlow/AddressAvatar";
import { renderShortAddress } from '../../../../util/address';
import ClipboardManager from "../../../../core/ClipboardManager";
import { showAlert } from "../../../../actions/alert";

import sendIcon from '../../../../images-new/address-book/send.png';
import editIcon from '../../../../images-new/address-book/edit.png';
import trashIcon from '../../../../images-new/address-book/trash.png';

const createStyles = (colors) =>
	StyleSheet.create({
		daggerWrapper: {
			width: '100%',
			height: 40,
			alignItems: 'center',
			justifyContent: 'flex-end',
		},
		dagger: {
			width: 40,
			height: 4,
			borderRadius: 20,
			backgroundColor: '#A0A0B1',
			marginBottom: 4,
		},
		wrapper: {
			margin: 0,
			width: '100%',
			padding: 0,
			justifyContent: 'flex-end',
			alignItems: 'center',
		},
		content: {
			width: '100%',
			padding: 20,
			backgroundColor: '#1B1B23',
			borderTopRightRadius: 20,
			borderTopLeftRadius: 20,
			justifyContent: 'center',
			alignItems: 'center',
		},
		name: {
			fontSize: 16,
			fontWeight: '500',
			color: '#FFF',
			marginBottom: 31,
		},
		label: {
			width: '100%',
			fontSize: 12,
			fontWeight: '500',
			color: '#FFF',
			marginTop: 32,
		},
		infor: {
			width: '100%',
			fontSize: 14,
			fontWeight: '400',
			color: '#FFF',
			marginTop: 8,
		},
		copyButtonWrapper: {
			width: '100%',
			marginTop: 8,
		},
		copyButtontext: {
			fontSize: 14,
			fontWeight: '500',
			color: '#7E73FF',
			// marginTop: 8,
		},
		button: {
			width: '100%',
			backgroundColor: '#3D3D4B',
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
			borderRadius: 12,
			padding: 12,
		},
		buttonImage: {
			width: 18,
			height: 18,
		},
		buttonText: {
			marginLeft: 10,
			fontSize: 16,
			fontWeight: '500',
			color: '#FFF',
		},
		buttonTextRed: {
			color: '#EB5252',
		},
		mt24: {
			marginTop: 24,
		},
		mt64: {
			marginTop: 64,
		},
		mt16: {
			marginTop: 16,
		},
		mb20: {
			marginBottom: 20,
		},
	});

/**
 * View that contains app information
 */
class ContactDetailModal extends PureComponent {
	static propTypes = {
		/**
		 * contact name
		 */
		name: PropTypes.string,
		/**
		/* contact address
		 */
		address: PropTypes.string,
		/**
		/* isVisible
		 */
		isVisible: PropTypes.bool,
		/**
		/* onAddressDetailCancel
		 */
		onAddressDetailCancel: PropTypes.func,
		/**
		/* onAddressDetailSend
		 */
		onAddressDetailSend: PropTypes.func,
		/**
		/* onAddressDetailEdit
		 */
		onAddressDetailEdit: PropTypes.func,
		/**
		/* onAddressDetailDelete
		 */
		onAddressDetailDelete: PropTypes.func,
		/**
		/* Triggers global alert
		 */
		showAlert: PropTypes.func,
	};

	copyToClipboard = () => {
		const { address, onAddressDetailCancel } = this.props;
		ClipboardManager.setString(address);
		onAddressDetailCancel();
		setTimeout(() => {
			this.props.showAlert({
				isVisible: true,
				autodismiss: 1500,
				content: 'clipboard-alert',
				data: { msg: strings('address_book.copied_to_clipboard') },
			});
		}, 1000);
	};

	render = () => {
		const { isVisible, name, address, avatar, onAddressDetailCancel, onAddressDetailSend, onAddressDetailEdit, onAddressDetailDelete } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<Modal
				isVisible={isVisible}
				style={styles.wrapper}
				onBackdropPress={onAddressDetailCancel}
				onBackButtonPress={onAddressDetailCancel}
				onSwipeComplete={onAddressDetailCancel}
				swipeDirection={'down'}
				backdropColor={'#000'}
				backdropOpacity={0.8}
				avoidKeyboard
			>
				<View style={styles.daggerWrapper}>
					<View style={styles.dagger} />
				</View>
				<View style={styles.content}>
					<Text style={styles.name}>
						{name || renderShortAddress(address)}
					</Text>
					<AddressAvatar
						name={name}
						address={address}
						avatar={avatar}
						width={52}
						height={52}
					/>
					<Text style={styles.label}>
						{strings('address_book.reminiscent_name')}
					</Text>
					<Text style={styles.infor}>
						{name || renderShortAddress(address)}
					</Text>
					<Text style={styles.label}>
						{strings('address_book.wallet_address')}
					</Text>
					<Text style={styles.infor}>
						{address}
					</Text>
					<View style={styles.copyButtonWrapper}>
						<TouchableOpacity onPress={this.copyToClipboard}>
							<Text style={styles.copyButtontext}>
								{strings('address_book.copy')}
							</Text>
						</TouchableOpacity>
					</View>
					<TouchableOpacity
						style={[styles.button, styles.mt24]}
						onPress={() => onAddressDetailSend({ address, name, avatar })}
					>
						<Image style={styles.buttonImage} source={sendIcon} />
						<Text style={styles.buttonText}>{strings('address_book.send')}</Text>
					</TouchableOpacity>
					<TouchableOpacity
						style={[styles.button, styles.mt64]}
						onPress={() => onAddressDetailEdit({ address, name, avatar })}
					>
						<Image style={styles.buttonImage} source={editIcon} />
						<Text style={styles.buttonText}>{strings('address_book.edit_information')}</Text>
					</TouchableOpacity>
					<TouchableOpacity
						style={[styles.button, styles.mt16, styles.mb20]}
						onPress={() => onAddressDetailDelete({ address, name, avatar })}
					>
						<Image style={styles.buttonImage} source={trashIcon} />
						<Text style={[styles.buttonText, styles.buttonTextRed]}>{strings('address_book.delete_address')}</Text>
					</TouchableOpacity>
				</View>
			</Modal>
		);
	};
}

ContactDetailModal.contextType = ThemeContext;

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
	showAlert: (config) => dispatch(showAlert(config)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactDetailModal);
