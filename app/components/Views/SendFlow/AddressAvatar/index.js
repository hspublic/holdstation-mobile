import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, SafeAreaView, Image } from "react-native";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ThemeContext, mockTheme } from '../../../../util/theme';

import LinearGradient from 'react-native-linear-gradient';

const createStyles = (colors) =>
	StyleSheet.create({
		addressAvatarWrapper: {},
		addressAvatar: {
			width: 52,
			height: 52,
			borderRadius: 26,
			justifyContent: 'center',
			alignItems: 'center',
		},
		addressAvatarImage: {
			width: 52,
			height: 52,
			borderRadius: 26,
		},
		addressAvatarText: {
			color: '#FFF',
			fontSize: 20,
			fontWeight: '500',
		},
	});

const shortNameUnknown = 'UN';
const alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
const avatarGradiens = [
	['rgba(200, 91, 209, 1)', 'rgba(255, 109, 109, 1)'],
	['rgba(205, 255, 204, 1)', 'rgba(62, 205, 237, 0.5365)', 'rgba(120, 149, 254, 1)'],
	['rgba(226, 149, 77, 1)', 'rgba(224, 105, 53, 0.5365)', 'rgba(148, 77, 182, 1)'],
	['rgba(226, 149, 77, 1)', 'rgba(224, 105, 53, 0.5365)', 'rgba(248, 29, 42, 1)'],
];
const DEFAULT_HEIGHT = 52;
const DEFAULT_WIDTH = 52;

class AddressAvatar extends PureComponent {
	static propTypes = {
		/**
		 * Name to display
		 */
		name: PropTypes.string,
		/**
		 * Ethereum address
		 */
		address: PropTypes.string,
		/**
		 * Avatar
		 */
		avatar: PropTypes.string,
		/**
		 * Width
		 */
		width: PropTypes.number,
		/**
		 * Height
		 */
		height: PropTypes.number,
	};

	state = {};

	getNameShort = () => {
		const { name } = this.props;
		if (!name) {
			return shortNameUnknown;
		}

		const nameParts = name.split(' ');
		if (nameParts.length > 1 && nameParts[0][0] && nameParts[1][0]) {
			return `${nameParts[0][0]}${nameParts[1][0]}`.toUpperCase();
		}
		return nameParts[0][0].toUpperCase();
	};
	getAvatarGradient = () => {
		let colorsIndex = alphabet.indexOf(this.getNameShort()[0]) % avatarGradiens.length;
		colorsIndex = colorsIndex >= 0 ? colorsIndex : 0;
		return avatarGradiens[colorsIndex];
	};

	render = () => {
		const { width, height, avatar } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<View style={styles.addressAvatarWrapper}>
				{!!avatar && (
					<View
						style={[
							styles.addressAvatar,
							{
								width: width || DEFAULT_WIDTH,
								height: height || DEFAULT_HEIGHT,
								borderRadius: (width || DEFAULT_WIDTH) / 2,
							},
						]}
					>
						<Image
							style={[
								styles.addressAvatarImage,
								{
									width: width || DEFAULT_WIDTH,
									height: height || DEFAULT_HEIGHT,
									borderRadius: (width || DEFAULT_WIDTH) / 2,
								},
							]}
							source={{ uri: avatar }}
						/>
					</View>
				)}
				{!avatar && (
					<LinearGradient
						colors={this.getAvatarGradient()}
						style={[
							styles.addressAvatar,
							{
								width: width || DEFAULT_WIDTH,
								height: height || DEFAULT_HEIGHT,
								borderRadius: (width || DEFAULT_WIDTH) / 2,
							},
						]}
					>
						<Text style={styles.addressAvatarText}>
							{this.getNameShort()}
						</Text>
					</LinearGradient>
				)}
			</View>
		);
	};
}

AddressAvatar.contextType = ThemeContext;

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(AddressAvatar);
