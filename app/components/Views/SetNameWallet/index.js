import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
	Text,
	View,
	TextInput,
	StyleSheet,
	ScrollView,
	ImageBackground, Image, Dimensions, KeyboardAvoidingView
} from "react-native";

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { setLockTime } from '../../../actions/settings';
import StyledButton from '../../UI/StyledButton';
import Device from '../../../util/device';
import { fontStyles, colors as importedColors, baseStyles } from '../../../styles/common';
import { strings } from '../../../../locales/i18n';
import { getSetNameWallet } from '../../UI/Navbar';
import { PREVIOUS_SCREEN, SETNAMEWALLET } from "../../../constants/navigation";
import { WALLET_NAME } from '../../../constants/storage';

import { ThemeContext, mockTheme } from '../../../util/theme';
import FadeOutOverlay from '../../UI/FadeOutOverlay';
import Svg, { ClipPath, Defs, G, Path } from 'react-native-svg';
import Logger from '../../../util/Logger';
import LinearGradient from 'react-native-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';

const win = Dimensions.get('window')

const createStyles = (colors) =>
	StyleSheet.create({
		// eslint-disable-next-line react-native/no-color-literals
		mainWrapper: {
			flex: 1,
			backgroundColor: '#1B1B23',
			shadowColor: importedColors.transparent,
			flexDirection: 'row',
		},
		image: {
			flex: 1,
			justifyContent: 'center',
				// width: win.width / 2,
				// height: win.height / 10
			
		},
		wrapper: {
			flex: 1,
			alignItems: 'center',
			justifyContent: 'flex-start',
		},
		scrollableWrapper: {
			flex: 1,
			paddingHorizontal: 1,
		},
		keyboardScrollableWrapper: {
			flex: 1,
		},
		logo: {
			marginTop: 200,
			alignSelf: 'center',
			justifyContent: 'center',
		},
		container: {
			marginTop: 20,
			padding: 10,
			borderWidth: 1,
			borderRadius: 12,
			// opacity: 0.5,
			backgroundColor: '#1B1B23',
			borderColor: '#FFFFFF26',
			// height: 150,
			width: win.width * 0.8,
			height: win.height * 0.23
		},
		content: {
			height : win.height / 30,
			textAlign: 'center',
			justifyContent: 'center',
			// padding: 5,
			marginBottom: win.height / 100
		},
		title: {
			fontSize: Device.isAndroid() ? 16 : 20,
			color: '#FFFFFF',
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
			fontSize: 16
		},
		text: {
			marginBottom: 10,
			justifyContent: 'center',
			...fontStyles.normal,
		},
		field: {
			marginBottom: win.height / 100
		},
		input: {
			borderWidth: 1,
			borderColor: '#58586C',
			padding: 10,
			borderRadius: 6,
			fontSize: 16,
			height: 40,
			...fontStyles.normal,
			textAlign: 'center',
			color: '#FFFFFF',
		},
		ctaWrapper: {
			marginTop: 10,
			width: win.width * 0.748,
			marginBottom: -  win.height / 200,
			// justifyContent: 'center',
			// alignItems: 'center'
		},
	
		errorMsg: {
			color: colors.error.default,
			...fontStyles.normal,
		},
		errorPhrase: {
			fontSize: 12,
			borderRadius: 10,
			...fontStyles.normal,
			paddingTop: 10,
			justifyContent: 'center',
			alignItems: 'center',
		},
		errorLabel: {
			color: 'red',
		},
		textWrapper: {
			color: '#FFFFFF',
			...fontStyles.semiBold,
			fontSize: 16,
			lineHeight: 20
		},
		buttonWrapper: {
			height: 40,
			justifyContent: 'center',
			alignItems : 'center'
		},
		buttonCancelWrapper: {
			height: 40,
			justifyContent: 'center',
			alignItems : 'center',
		},
		zoominItem: {
			transform:[{scale: 0.97}]
		},
		zoominCancel: {
			transform:[{scale: 0.9}]
		},
		logoImage: {
			width: win.width / 2,
			height: win.height / 10
		}
	});
/**
 * View where users can set their password for the first time
 */
class SetNameWallet extends PureComponent {
	static propTypes = {
		/**
		 * The navigator object
		 */
		navigation: PropTypes.object,
		/**
		 * Object that represents the current route info like params passed to it
		 */
		route: PropTypes.object,
	};

	state = {
		walletName: '',
		loading: true,
		error: null,
		inputWidth: { width: '99%' },
		onClickCreate: false,
		onClickCancel: false,
	};

	mounted = true;

	updateNavBar = () => {
		const { route, navigation } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(getSetNameWallet(route, {}, colors));
	};

	async componentDidMount() {
		this.updateNavBar();
		setTimeout(() => {
			this.setState({
				inputWidth: { width: '100%' },

			});
		}, 100);
	}

	componentDidUpdate(prevProps, prevState) {
		this.updateNavBar();
		const prevLoading = prevState.loading;
		const { loading } = this.state;
		const { navigation } = this.props;
		if (!prevLoading && loading) {
			// update navigationOptions
			navigation.setParams({
				headerLeft: () => <View />,
			});
		}
	}

	componentWillUnmount() {
		this.mounted = false;
	}

	render() {
		const { inputWidth, walletName, error } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);
		const image = require('../../../images/bgStart.png');

		const setWalletName = (val) => {
			this.setState({ walletName: val });
			let error = null;
			if (val.length > 18) {
				error = strings('accounts.input_max_length');
			}
			this.setState({ error });
		};


		return (
			<View style={styles.mainWrapper}>
				<ImageBackground source={image} resizeMode="cover" style={styles.image}>
					<View style={{
						flex: 1,
					}}>
						<View style={styles.wrapper} testID={'set-name-screen'}>
							<KeyboardAvoidingView
								style={styles.scrollableWrapper}
								// contentContainerStyle={styles.keyboardScrollableWrapper}
								// resetScrollToCoords={{ x: 0, y: 0 }}
							>
								<View style={styles.logo}>
									<Image
										style={styles.logoImage}
										source={require('../../../images-new/hs-logo.png')}
									/>
								</View>
								<View testID={'create-name-screen'} style={styles.container}>
									<View style={styles.content}>
										<Text style={styles.title}>{strings('accounts.naming_account_title')}</Text>
									</View>
									<View style={styles.field}>
										<TextInput
											style={[styles.input, inputWidth]}
											value={walletName}
											maxLength={19}
											//onChangeText={this.setConfirmPassword}
											onChangeText={(item) => setWalletName(item)}
											//secureTextEntry={secureTextEntry}
											placeholder={strings('accounts.naming_account_placeholder')}
											placeholderTextColor={'#58586C'}
											testID={'input-walllet-name'}
											onSubmitEditing={this.onPressSetName}
											returnKeyType={'done'}
											autoCapitalize="none"
											autoFocus
											keyboardAppearance={themeAppearance}
										/>
										{!!error && (
											<View style={styles.errorPhrase}>
												<Text style={styles.errorLabel}>{error}</Text>
											</View>
										)}
									</View>
									{/*<View>{this.renderSwitch()}</View>*/}
									{/*{!!error && <Text style={styles.errorMsg}>{error}</Text>}*/}
									<View style={styles.ctaWrapper}>
										<TouchableOpacity
											onPress={this.onPressSetName}
											testID={'submit-button'}
											disabled={error != null}
											onPressIn={() => this.setState({onClickCreate: true})}
											onPressOut={() => this.setState({onClickCreate: false})} 
											activeOpacity={1}
											style={this.state.onClickCreate && styles.zoominItem}
										>
											<LinearGradient
												colors={['#5E44FF', '#7785FF']}
												style={[{borderRadius: 12}, styles.buttonWrapper]}
												start={{x: 0, y: 0}}
												end={{x: 1, y: 0}}
											>
												<Text style={styles.textWrapper}>
													{strings('accounts.create_new_wallet')}
												</Text>
											</LinearGradient>
											
										</TouchableOpacity>
									</View>
									<View style={styles.ctaWrapper}>
										{/* <StyledButton
											type={'cancel'}
											testID={'cancel-button'}
											onPress={this.onPressCancel}
										>
											{strings('accounts.cancel')}
										</StyledButton> */}
										<TouchableOpacity
												onPress={this.onPressCancel}
												testID={'cancel-button'}
												disabled={error != null}
												style={[styles.buttonCancelWrapper, this.state.onClickCancel && styles.zoominCancel]}
												onPressIn={() => this.setState({onClickCancel: true})}
												onPressOut={() => this.setState({onClickCancel: false})} 
												activeOpacity={1}
											>
											<Text style={styles.textWrapper}>
												{strings('accounts.cancel')}
											</Text>
										</TouchableOpacity>
									</View>
								</View>
							</KeyboardAvoidingView>
						</View>
					</View>
				</ImageBackground>
				<FadeOutOverlay />
			</View>
		);
	}

	onPressCancel = async () => {
		this.props.navigation.navigate('Onboarding', {
			[PREVIOUS_SCREEN]: SETNAMEWALLET,
		});
	};

	onPressSetName = async () => {
			const walletName = this.state.walletName;

			if (walletName != null && walletName.length > 18) {
				this.setState({ error: strings('accounts.input_max_length') });
				return;
			}

			this.props.navigation.navigate('ChoosePin', {
				[PREVIOUS_SCREEN]: SETNAMEWALLET,
				WALLET_NAME: this.state.walletName,
			});
		};
}

SetNameWallet.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
});

const mapDispatchToProps = (dispatch) => ({
	setLockTime: (time) => dispatch(setLockTime(time)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SetNameWallet);
