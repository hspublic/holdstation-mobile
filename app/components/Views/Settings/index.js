import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, ScrollView, InteractionManager, View, Text, Switch, Alert } from 'react-native';
import SettingsDrawer from '../../UI/SettingsDrawer';
import { getClosableNavigationOptions } from "../../UI/Navbar";
import Analytics from '../../../core/Analytics';
import { ANALYTICS_EVENT_OPTS } from '../../../util/analytics';
import { connect } from 'react-redux';
import { ThemeContext, mockTheme } from '../../../util/theme';
import I18n, { strings, getLanguages, setLocale } from '../../../../locales/i18n';
import SelectComponent from '../../UI/SelectComponent';
import { setDarkMode, setPrimaryCurrency } from '../../../actions/settings';
import { colors as importedColors, fontStyles } from '../../../styles/common';
import CustomSwitch from '../../UI/CustomSwitch';
import Engine from '../../../core/Engine';
import SecureKeychain from '../../../core/SecureKeychain';
import { logOut } from '../../../actions/user';

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			backgroundColor: '#1B1B23',
			flex: 1,
			paddingLeft: 18,
			zIndex: 99999999999999,
		},
		settingNoBorder: {
			flexDirection: 'row',
			padding: 10,
			minHeight: 50,
			paddingVertical: 18,
		},
		setting: {
			flexDirection: 'row',
			minHeight: 50,
			padding: 10,
			paddingVertical: 18,
			borderBottomWidth: 1,
			borderBottomColor: colors.border.muted,
		},
		content: {
			flex: 1,
			justifyContent:'center'
		},
		title: {
			...fontStyles.normal,
			color: '#FFFFFF',
			fontSize: 16,
			//marginBottom: 8,
		},
		action: {
			flex: 0,
			paddingHorizontal: 16,
		},
		picker: {
			borderColor: colors.border.default,
			borderRadius: 5,
			borderWidth: 2,
			minWidth: 200,
			height: 44,
			marginRight: 20,
		},
		simplePicker: {
			minWidth: 200,
			height: 44,
			marginRight: 20,
		},
	});

/**
 * Main view for app configurations
 */
class Settings extends PureComponent {
	static propTypes = {
		/**
		/* navigation object required to push new views
		*/
		navigation: PropTypes.object,
		/**
		 * redux flag that indicates if the user
		 * completed the seed phrase backup flow
		 */
		seedphraseBackedUp: PropTypes.bool,
		darkMode: PropTypes.bool,
		setDarkMode: PropTypes.func,
		primaryCurrency: PropTypes.string,
		setPrimaryCurrency: PropTypes.func,
		logOut: PropTypes.func,
	};

	state = {
		currentLanguage: I18n.locale.substr(0, 2),
		languages: {},
	};

	updateNavBar = () => {
		const { navigation } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(
			getClosableNavigationOptions(strings('app_settings.title'), strings('navigation.close'), navigation, colors)
		);
	};

	componentDidMount = () => {
		this.updateNavBar();
		const languages = getLanguages();
		this.setState({ languages });
		this.languageOptions = Object.keys(languages).map((key) => ({ value: key, label: languages[key], key }));
		this.primaryCurrencyOptions = [
			// { value: 'ETH', label: strings('app_settings.primary_currency_text_first'), key: 'Native' },
			// { value: 'Fiat', label: strings('app_settings.primary_currency_text_second'), key: 'Fiat' },
			{ value: 'VND', label: 'VND', key: 'VND' },
			{ value: 'USD', label: 'USD', key: 'USD' },
		];
	};

	componentDidUpdate = () => {
		this.updateNavBar();
	};

	onPressGeneral = () => {
		InteractionManager.runAfterInteractions(() => Analytics.trackEvent(ANALYTICS_EVENT_OPTS.SETTINGS_GENERAL));
		this.props.navigation.navigate('GeneralSettings');
	};

	onPressAdvanced = () => {
		InteractionManager.runAfterInteractions(() => Analytics.trackEvent(ANALYTICS_EVENT_OPTS.SETTINGS_ADVANCED));
		this.props.navigation.navigate('AdvancedSettings');
	};

	onPressSecurity = () => {
		InteractionManager.runAfterInteractions(() =>
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.SETTINGS_SECURITY_AND_PRIVACY)
		);
		this.props.navigation.navigate('SecuritySettings');
	};

	onPressNetworks = () => {
		this.props.navigation.navigate('NetworksSettings');
	};

	onPressExperimental = () => {
		InteractionManager.runAfterInteractions(() => Analytics.trackEvent(ANALYTICS_EVENT_OPTS.SETTINGS_EXPERIMENTAL));
		this.props.navigation.navigate('ExperimentalSettings');
	};

	onPressInfo = () => {
		InteractionManager.runAfterInteractions(() => Analytics.trackEvent(ANALYTICS_EVENT_OPTS.SETTINGS_ABOUT));
		this.props.navigation.navigate('CompanySettings');
	};

	onPressContacts = () => {
		this.props.navigation.navigate('ContactsSettings');
	};

	toggleDarkMode = (darkMode) => {
		this.props.setDarkMode(darkMode);
	};

	selectLanguage = (language) => {
		if (language === this.state.currentLanguage) return;
		setLocale(language);
		this.setState({ currentLanguage: language });
		setTimeout(() => this.props.navigation.navigate('Home'), 100);
	};

	selectPrimaryCurrency = (primaryCurrency) => {
		this.props.setPrimaryCurrency(primaryCurrency);
	};

	logOut = () => {
		this.props.navigation.navigate('Login');
		this.props.logOut();
	};

	onPress = async () => {
		const { passwordSet } = this.props;
		const { KeyringController } = Engine.context;
		await SecureKeychain.resetGenericPassword();
		await KeyringController.setLocked();
		if (!passwordSet) {
			this.props.navigation.navigate('OnboardingRootNav', {
				screen: 'OnboardingNav',
				params: { screen: 'Onboarding' },
			});
		} else {
			this.logOut();
		}
	};

	logout = () => {
		Alert.alert(
			strings('drawer.lock_title'),
			'',
			[
				{
					text: strings('drawer.lock_cancel'),
					onPress: () => null,
					style: 'cancel',
				},
				{
					text: strings('drawer.lock_ok'),
					onPress: this.onPress,
				},
			],
			{ cancelable: false }
		);
	};

	render = () => {
		const { seedphraseBackedUp, darkMode, primaryCurrency } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<ScrollView style={styles.wrapper}>
				<SettingsDrawer
					// description={strings('app_settings.contacts_desc')}
					onPress={this.onPressContacts}
					title={strings('app_settings.address_book_title')}
				/>
				<View style={styles.setting}>
					<View style={styles.content}>
						<Text style={styles.title}>{strings('app_settings.dark_mode')}</Text>
					</View>
					<View style={styles.action}>
						<Switch
							value={darkMode}
							onValueChange={this.toggleDarkMode}
							trackColor={{ true: colors.primary.default, false: colors.border.muted }}
							thumbColor={importedColors.white}
							style={styles.switch}
							ios_backgroundColor={colors.border.muted}
						/>
					</View>
				</View>
				<View style={styles.settingNoBorder}>
					<View style={styles.content}>
						<Text style={styles.title}>{strings('app_settings.current_language')}</Text>
					</View>
					<View style={styles.picker}>
						{this.languageOptions && (
							<SelectComponent
								selectedValue={this.state.currentLanguage}
								onValueChange={this.selectLanguage}
								label={strings('app_settings.current_language')}
								options={this.languageOptions}
							/>
						)}
					</View>
				</View>
				<View style={styles.setting}>
					<View style={styles.content}>
						<Text style={styles.title}>{strings('app_settings.primary_currency_title')}</Text>
					</View>
					<View style={styles.simplePicker}>
						{this.primaryCurrencyOptions && (
							<CustomSwitch
								selectionMode={'VND'}
								roundCorner
								option1={this.primaryCurrencyOptions[0]}
								option2={this.primaryCurrencyOptions[1]}
								onSelectSwitch={this.selectPrimaryCurrency}
								selectionColor={'blue'}
							/>
						)}
					</View>
				</View>
				{/*<SettingsDrawer*/}
				{/*	description={strings('app_settings.general_desc')}*/}
				{/*	onPress={this.onPressGeneral}*/}
				{/*	title={strings('app_settings.general_title')}*/}
				{/*/>*/}
				<SettingsDrawer onPress={this.onPressSecurity} title={strings('app_settings.security_title')} />
				<View style={styles.settingNoBorder}>
					<View style={styles.content}>
						<Text style={styles.title}>{strings('app_settings.contact_support')}</Text>
					</View>
				</View>
				<View style={styles.settingNoBorder}>
					<View style={styles.content}>
						<Text style={styles.title}>{strings('app_settings.send_suggestions')}</Text>
					</View>
				</View>
				<View style={styles.settingNoBorder}>
					<View style={styles.content}>
						<Text style={styles.title}>{strings('app_settings.version')}</Text>
					</View>
					<View style={styles.action}>
						<Text style={{ color: 'rgba(177, 181, 255, 1)' }}>0.5.4</Text>
					</View>
				</View>
				{/*<SettingsDrawer*/}
				{/*	description={strings('app_settings.advanced_desc')}*/}
				{/*	onPress={this.onPressAdvanced}*/}
				{/*	title={strings('app_settings.advanced_title')}*/}
				{/*/>*/}
				{/*<SettingsDrawer*/}
				{/*	description={strings('app_settings.contacts_desc')}*/}
				{/*	onPress={this.onPressContacts}*/}
				{/*	title={strings('app_settings.contacts_title')}*/}
				{/*/>*/}
				{/*<SettingsDrawer*/}
				{/*	title={strings('app_settings.networks_title')}*/}
				{/*	description={strings('app_settings.networks_desc')}*/}
				{/*	onPress={this.onPressNetworks}*/}
				{/*/>*/}
				{/*<SettingsDrawer*/}
				{/*	title={strings('app_settings.experimental_title')}*/}
				{/*	description={strings('app_settings.experimental_desc')}*/}
				{/*	onPress={this.onPressExperimental}*/}
				{/*/>*/}
				{/*<SettingsDrawer title={strings('app_settings.info_title')} onPress={this.onPressInfo} />*/}
				{/* <SettingsDrawer title={'logout'} onPress={this.logout} /> */}
			</ScrollView>
		);
	};
}

Settings.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	seedphraseBackedUp: state.user.seedphraseBackedUp,
	darkMode: state.settings.darkMode,
	primaryCurrency: state.settings.primaryCurrency,
});

const mapDispatchToProps = (dispatch) => ({
	setDarkMode: (darkMode) => dispatch(setDarkMode(darkMode)),
	setPrimaryCurrency: (primaryCurrency) => dispatch(setPrimaryCurrency(primaryCurrency)),
	logOut: () => dispatch(logOut()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
