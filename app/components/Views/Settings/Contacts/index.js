import React, { PureComponent } from 'react';
import { SafeAreaView, StyleSheet, TouchableOpacity, Image, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { strings } from '../../../../../locales/i18n';
import { getNavigationOptionsTitle } from '../../../UI/Navbar';
import { connect } from 'react-redux';
import AddressList from '../../SendFlow/AddressList';
import StyledButton from '../../../UI/StyledButton';
import Engine from '../../../../core/Engine';
import ActionSheet from 'react-native-actionsheet';
import { ThemeContext, mockTheme } from '../../../../util/theme';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import AddressAvatar from "../../SendFlow/AddressAvatar";
import { isENS, renderShortAddress } from "../../../../util/address";
import NotificationManager from "../../../../core/NotificationManager";
import AddressDelete from "../../SendFlow/AddressDelete";
import AddressDetail from "../../SendFlow/AddressDetail";
import AddressForm from "../../SendFlow/AddressForm";
import { toChecksumAddress } from "ethereumjs-util";

import addContactIcon from '../../../../images-new/address-book/add-contacts.png';
import { doENSLookup, doENSReverseLookup } from "../../../../util/ENSUtils";

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			// backgroundColor: colors.background.default,
			backgroundColor: '#1B1B23',
			flex: 1,
		},
		addContact: {
			marginHorizontal: 24,
			marginBottom: 16,
		},
		addContactButton: {
			position: 'absolute',
			bottom: 42,
			right: 20,
			width: 48,
			height: 48,
		},
		addContactButtonContent: {
			width: 48,
			height: 48,
			borderRadius: 24,
			justifyContent: 'center',
			alignItems: 'center',
		},
		addContactButtonIcon: {
			width: 16,
			height: 16,
		},
	});

const EDIT = 'edit';
const ADD = 'add';

/**
 * View that contains app information
 */
class Contacts extends PureComponent {
	static propTypes = {
		/**
		 * Map representing the address book
		 */
		addressBook: PropTypes.object,
		/**
		/* navigation object required to push new views
		*/
		navigation: PropTypes.object,
		/**
		 * Network id
		 */
		network: PropTypes.string,
	};

	state = {
		reloadAddressList: false,
		contactToRemove: null,
		contactToDetail: null,
		contactToForm: null,
	};

	actionSheet;
	contactAddressToRemove;

	updateNavBar = () => {
		const { navigation } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(
			getNavigationOptionsTitle(strings('app_settings.contacts_title'), navigation, false, colors)
		);
	};

	componentDidMount = () => {
		this.updateNavBar();
	};

	componentDidUpdate = (prevProps) => {
		this.updateNavBar();
		const { network } = this.props;
		if (
			prevProps.addressBook &&
			this.props.addressBook &&
			JSON.stringify(prevProps.addressBook[network]) !== JSON.stringify(this.props.addressBook[network])
		)
			this.updateAddressList();
	};

	updateAddressList = () => {
		this.setState({ reloadAddressList: true });
		setTimeout(() => {
			this.setState({ reloadAddressList: false });
		}, 100);
	};

	onAddressLongPress = (address) => {
		this.contactAddressToRemove = address;
		this.actionSheet && this.actionSheet.show();
	};

	deleteContact = () => {
		this.setState({ reloadAddressList: true });
		const { AddressBookController } = Engine.context;
		const { network } = this.props;
		AddressBookController.delete(network, this.contactAddressToRemove);
		this.setState({ reloadAddressList: false });
	};

	onAddressPress = ({ address, name, avatar }) => {
		// this.props.navigation.navigate('ContactForm', {
		// 	mode: EDIT,
		// 	editMode: EDIT,
		// 	address,
		// 	onDelete: () => this.updateAddressList(),
		// });
		this.setState({
			contactToDetail: {
				address,
				name,
				avatar,
			},
		});
	};
	onAddressDetailCancel = () => {
		this.setState({ contactToDetail: null });
	};
	onAddressDetailSend = ({ address, name, avatar }) => {
		this.setState({ contactToDetail: null }, async () => {
			const { network } = this.props;
			const ens = await doENSReverseLookup(address, network);
			const toEnsName = ens || address;
			this.props.navigation.navigate('SendFlowView', {
				screen: 'SendTo',
				params: {
					toSelectedAddress: address,
					toSelectedAddressName: name,
					toSelectedAddressReady: true,
					toEnsName,
				},
			});
		});
	};
	onAddressDetailEdit = ({ address, name, avatar }) => {
		this.setState({ contactToDetail: null }, () => {
			setTimeout(() => {
				this.onAddressEditPress({ address, name, avatar });
			}, 500);
		});
	};
	onAddressDetailDelete = ({ address, name, avatar }) => {
		this.setState({ contactToDetail: null }, () => {
			setTimeout(() => {
				this.onAddressDeletePress({ address, name, avatar });
			}, 500);
		});
	};

	onAddressNewPress = () => {
		this.setState({
			contactToForm: {
				address: '',
				name: '',
				avatar: '',
			},
		});
	};
	onAddressEditPress = ({ address, name, avatar }) => {
		this.setState({
			contactToForm: {
				address,
				name,
				avatar,
			},
		});
	};
	onAddressFormCancel = () => {
		this.setState({ contactToForm: null });
	};
	onAddressFormSubmit = ({ address, name, avatar, newAddress, newName, newAvatar }) => {
		this.setState({ reloadAddressList: true,  contactToForm: null });
		const { network } = this.props;
		const { AddressBookController } = Engine.context;
		AddressBookController.set(toChecksumAddress(newAddress), newName, network, newAvatar, '');
		this.setState({ reloadAddressList: false });
		if (!!name || !!address) {
			NotificationManager.showSimpleNotification({
				status: 'success',
				duration: 1500,
				title: strings('address_book.edit_contact_success_title'),
				description: strings('address_book.edit_contact_success_description').replace('{name}', name || renderShortAddress(address)),
			});
		} else {
			NotificationManager.showSimpleNotification({
				status: 'success',
				duration: 1500,
				title: strings('address_book.add_contact_success_title'),
				description: strings('address_book.add_contact_success_description').replace('{name}', newName || renderShortAddress(address)),
			});
		}
	};

	onAddressDeletePress = ({ address, name, avatar }) => {
		this.setState({
			contactToRemove: {
				address,
				name,
				avatar,
			},
		});
	};
	onAddressDeleteCancel = () => {
		this.setState({ contactToRemove: null });
	};
	onAddressDeleteConfirm = () => {
		const { address, name } = this.state.contactToRemove;
		this.setState({ reloadAddressList: true, contactToRemove: null });
		const { AddressBookController } = Engine.context;
		const { network } = this.props;
		AddressBookController.delete(network, address);
		this.setState({ reloadAddressList: false });
		NotificationManager.showSimpleNotification({
			status: 'success',
			duration: 1500,
			title: strings('address_book.delete_contact_success_title'),
			description: strings('address_book.delete_contact_success_description').replace('{name}', name || renderShortAddress(address)),
		});
	};

	goToAddContact = () => {
		this.props.navigation.navigate('ContactForm', { mode: ADD });
	};

	createActionSheetRef = (ref) => {
		this.actionSheet = ref;
	};

	render = () => {
		const { reloadAddressList, contactToRemove, contactToDetail, contactToForm } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance;
		const styles = createStyles(colors);

		return (
			<SafeAreaView style={styles.wrapper} testID={'contacts-screen'}>
				<AddressList
					onlyRenderAddressBook
					reloadAddressList={reloadAddressList}
					onAccountPress={this.onAddressPress}
					onAccountLongPress={this.onAddressLongPress}
					onAccountEditPress={this.onAddressEditPress}
					onAccountDeletePress={this.onAddressDeletePress}
				/>
				{/*<StyledButton*/}
				{/*	type={'confirm'}*/}
				{/*	containerStyle={styles.addContact}*/}
				{/*	onPress={this.goToAddContact}*/}
				{/*	testID={'add-contact-button'}*/}
				{/*>*/}
				{/*	{strings('address_book.add_contact')}*/}
				{/*</StyledButton>*/}
				<TouchableOpacity
					style={styles.addContactButton}
					onPress={this.onAddressNewPress}
				>
					<LinearGradient colors={['#C85BD1', '#FF6D6D']} style={styles.addContactButtonContent}>
						<Image style={styles.addContactButtonIcon} source={addContactIcon} />
					</LinearGradient>
				</TouchableOpacity>
				<ActionSheet
					ref={this.createActionSheetRef}
					title={strings('address_book.delete_contact')}
					options={[strings('address_book.delete'), strings('address_book.cancel')]}
					cancelButtonIndex={1}
					destructiveButtonIndex={0}
					// eslint-disable-next-line react/jsx-no-bind
					onPress={(index) => (index === 0 ? this.deleteContact() : null)}
					theme={themeAppearance}
				/>
				<AddressDelete
					name={contactToRemove?.name}
					address={contactToRemove?.address}
					avatar={contactToRemove?.avatar}
					isVisible={!!contactToRemove}
					onAddressDeleteCancel={this.onAddressDeleteCancel}
					onAddressDeleteConfirm={this.onAddressDeleteConfirm}
				/>
				<AddressDetail
					name={contactToDetail?.name}
					address={contactToDetail?.address}
					avatar={contactToDetail?.avatar}
					isVisible={!!contactToDetail}
					onAddressDetailCancel={this.onAddressDetailCancel}
					onAddressDetailSend={this.onAddressDetailSend}
					onAddressDetailEdit={this.onAddressDetailEdit}
					onAddressDetailDelete={this.onAddressDetailDelete}
				/>
				{!!contactToForm && (
					<AddressForm
						name={contactToForm?.name}
						address={contactToForm?.address}
						avatar={contactToForm?.avatar}
						isVisible={!!contactToForm}
						onAddressFormCancel={this.onAddressFormCancel}
						onAddressFormSubmit={this.onAddressFormSubmit}
					/>
				)}
			</SafeAreaView>
		);
	};
}

Contacts.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	addressBook: state.engine.backgroundState.AddressBookController.addressBook,
	network: state.engine.backgroundState.NetworkController.network,
});

export default connect(mapStateToProps)(Contacts);
