import React, { PureComponent } from 'react';
import { SafeAreaView, View, StyleSheet, Text, Dimensions } from 'react-native';
import { fontStyles } from '../../../styles/common';
import { connect } from 'react-redux';
import DefaultTabBar from 'react-native-scrollable-tab-view/DefaultTabBar';
import AddCustomToken from '../../UI/AddCustomToken';
import SearchTokenAutocomplete from '../../UI/SearchTokenAutocomplete';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import PropTypes from 'prop-types';
import { strings } from '../../../../locales/i18n';
import AddCustomCollectible from '../../UI/AddCustomCollectible';
import { getNetworkNavbarOptions } from '../../UI/Navbar';
import { NetworksChainId } from '@metamask/controllers';
import CollectibleDetectionModal from '../../UI/CollectibleDetectionModal';
import { isMainNet } from '../../../util/networks';
import { ThemeContext, mockTheme } from '../../../util/theme';

win = Dimensions.get('window');

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			flex: 1,
			backgroundColor: '#1B1B23',
		},
		titleWrapper: {
			width: '100%',
			height: 33,
			alignItems: 'center',
			justifyContent: 'center',
			//borderBottomWidth: StyleSheet.hairlineWidth,
			//borderColor: colors.border.muted,
			marginTop: win.height / 100,
			marginBottom: win.height /100
		},
		dragger: {
			width: 48,
			height: 5,
			borderRadius: 4,
			backgroundColor: colors.border.default,
			opacity: 0.6,
		},
		infoWrapper: {
			alignItems: 'center',
			marginTop: 10,
		},
		tabUnderlineStyle: {
			height: '100%',
			zIndex: -1,
			backgroundColor: '#3D3D4B',
			borderRadius: 10,
		},
		tabBar: {
			borderColor: '#282832',
			borderRadius: 12,
			height: win.height / 21,
			shadowColor: "#00000066",
			shadowOpacity: 1,
			shadowRadius: 39,
			shadowOffset: {
				width: 0,
				height: 4
			},
			// alignItems: 'center',
			// justifyContent: 'center'
		},
		tabStyle: {
			paddingBottom: 0,
			
		},
		textStyle: {
			fontSize: 16,
			// letterSpacing: 0.5,
			...fontStyles.semiBold,
		},
	});

/**
 * PureComponent that provides ability to add assets.
 */
class AddAsset extends PureComponent {
	state = {
		address: '',
		symbol: '',
		decimals: '',
		dismissNftInfo: false,
	};

	static propTypes = {
		/**
		/* navigation object required to push new views
		*/
		navigation: PropTypes.object,
		/**
		 * Chain id
		 */
		chainId: PropTypes.string,
		/**
		 * Object that represents the current route info like params passed to it
		 */
		route: PropTypes.object,
		/**
		 * Boolean to show if NFT detection is enabled
		 */
		useCollectibleDetection: PropTypes.bool,
	};

	updateNavBar = () => {
		const { navigation, route } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(
			getNetworkNavbarOptions(
				`add_asset.${route.params.assetType === 'token' ? 'title' : 'title_nft'}`,
				true,
				navigation,
				colors
			)
		);
	};

	componentDidMount = () => {
		this.updateNavBar();
	};

	componentDidUpdate = () => {
		this.updateNavBar();
	};

	goToPage(pageId) {
		this.tabView.goToPage(pageId);
	}

	renderTabBar() {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<DefaultTabBar
				underlineStyle={styles.tabUnderlineStyle}
				activeTextColor={'#FFFFFF'}
				inactiveTextColor={colors.text.muted}
				backgroundColor={'#282832'}
				tabStyle={styles.tabStyle}
				textStyle={styles.textStyle}
				style={styles.tabBar}
			/>
		);
	}

	dismissNftInfo = async () => {
		this.setState({ dismissNftInfo: true });
	};

	render = () => {
		const {
			route: {
				params: { assetType, collectibleContract },
			},
			navigation,
			chainId,
			useCollectibleDetection,
		} = this.props;
		const { dismissNftInfo } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<SafeAreaView style={styles.wrapper} testID={`add-${assetType}-screen`}>
				<View style={styles.titleWrapper}>
					<View style={styles.dragger} testID={'addasset-dragger'} />
				</View>
				<View style={styles.titleWrapper}>
					<Text style={{ color: '#FFFFFF', fontSize: 16, ...fontStyles.semiBold
					, paddingBottom: 20 }}>Thêm Token</Text>
				</View>
				{/*{isMainNet(chainId) && assetType !== 'token' && !dismissNftInfo && !useCollectibleDetection && (*/}
				{/*	<View style={styles.infoWrapper}>*/}
				{/*		<CollectibleDetectionModal onDismiss={this.dismissNftInfo} navigation={navigation} />*/}
				{/*	</View>*/}
				{/*)}*/}
				{assetType === 'token' ?
					(
					<ScrollableTabView
						initialPage={0}
						renderTabBar={() => this.renderTabBar()}
						ref={(tabView) => {
							this.tabView = tabView;
						}}
						style={{width: win.width * 0.9, justifyContent: 'center', alignItems:'center', marginLeft: win.width / 20}}
					>
						{NetworksChainId.mainnet === this.props.chainId && (
							<SearchTokenAutocomplete
								navigation={navigation}
								goToPage={() => this.goToPage(1)}
								tabLabel={strings('add_asset.search_token')}
								testID={'tab-search-token'}
							/>
						)}
						<AddCustomToken
							navigation={navigation}
							tabLabel={strings('add_asset.custom_token')}
							testID={'tab-add-custom-token'}
						/>
					</ScrollableTabView>
				) : (
					<AddCustomCollectible
						navigation={navigation}
						collectibleContract={collectibleContract}
						testID={'add-custom-collectible'}
					/>
				)}
			</SafeAreaView>
		);
	};
}

AddAsset.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	chainId: state.engine.backgroundState.NetworkController.provider.chainId,
	useCollectibleDetection: state.engine.backgroundState.PreferencesController.useCollectibleDetection,
});

export default connect(mapStateToProps)(AddAsset);
