import React from 'react';
import { shallow } from 'enzyme';
import FaceId from './';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

const mockStore = configureMockStore();
const store = mockStore({});

describe('FaceId', () => {
	it('should render correctly', () => {
		const wrapper = shallow(
			<Provider store={store}>
				<FaceId route={{ params: {} }} />
			</Provider>
		);
		expect(wrapper).toMatchSnapshot();
	});
});
