import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
	ActivityIndicator,
	Alert,
	Text,
	Image,
	View,
	SafeAreaView,
	StyleSheet,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { getOnboardingNavbarOptions } from '../../UI/Navbar';
import { connect } from 'react-redux';
import { logIn, passwordSet, seedphraseBackedUp } from '../../../actions/user';
import { setLockTime } from '../../../actions/settings';
import StyledButton from '../../UI/StyledButton';
import Engine from '../../../core/Engine';
import { fontStyles} from '../../../styles/common';
import { strings } from '../../../../locales/i18n';
import SecureKeychain from '../../../core/SecureKeychain';
import setOnboardingWizardStep from '../../../actions/wizard';
import DetectFaceIdModal from '../../UI/DetectFaceIdModal';
import Device from '../../../util/device';
import {
	SEED_PHRASE_HINTS,
	BIOMETRY_CHOICE_DISABLED,
	NEXT_MAKER_REMINDER,
	ONBOARDING_WIZARD,
	EXISTING_USER,
	TRUE,
} from '../../../constants/storage';
import Logger from '../../../util/Logger';
import { ThemeContext, mockTheme } from '../../../util/theme';
import { trackErrorAsAnalytics } from "../../../util/analyticsV2";

const createStyles = (colors) =>
	StyleSheet.create({
		// eslint-disable-next-line react-native/no-color-literals
		mainWrapper: {
			flex: 1,
			backgroundColor: '#1B1B23',
		},
		wrapper: {
			flex: 1,
			paddingHorizontal: 32,
		},
		// eslint-disable-next-line react-native/no-color-literals
		title: {
			fontSize: Device.isAndroid() ? 20 : 25,
			marginTop: 50,
			marginBottom: 50,
			color: '#FFFFFF',
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
		},
		field: {
			marginVertical: 5,
			position: 'relative',
		},
		fieldRow: {
			flexDirection: 'row',
			alignItems: 'flex-end',
			justifyContent: 'center',
			textAlign: 'center',
		},
		container: {
			justifyContent: 'center',
			alignItems: 'center',
		},
		faceIdIcon: {
			marginTop: 150,
			marginBottom: 150,
		},

		faceIdFailedIcon: {
			marginTop: 25,
			marginBottom: 25,
		},
		warning: {
			fontSize: 15,
			marginTop: 10,
			marginBottom: 10,
			color: colors.text.default,
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
		},
		fieldCol: {
			width: '70%',
		},
		fieldColRight: {
			flexDirection: 'row-reverse',
			width: '30%',
		},
		// eslint-disable-next-line react-native/no-color-literals
		label: {
			color: '#FF4B4B',
			fontSize: 16,
			marginBottom: 12,
			...fontStyles.normal,
			justifyContent: 'center',
			textAlign: 'center',
		},
		ctaWrapper: {
			marginTop: 10,
		},
		// eslint-disable-next-line react-native/no-color-literals
		offFaceIdButton: {
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
			fontSize: 15,
			color: '#FFFFFF',
		},
		errorMsg: {
			color: colors.error.default,
			textAlign: 'center',
			...fontStyles.normal,
		},
		seedPhrase: {
			marginTop: 80,
			marginBottom: 80,
			paddingTop: 50,
			paddingBottom: 50,
			paddingHorizontal: 20,
			fontSize: 12,
			borderRadius: 10,
			minHeight: 110,
			height: 'auto',
			borderWidth: 1,
			borderColor: colors.border.default,
			backgroundColor: colors.background.default,
			...fontStyles.normal,
			color: colors.text.default,
		},
		// eslint-disable-next-line react-native/no-color-literals
		errorPhrase: {
			fontSize: 12,
			borderRadius: 10,
			backgroundColor: 'rgba(222, 44, 44, 0.13)',
			...fontStyles.normal,
			paddingTop: 10,
		},
		padding: {
			paddingRight: 46,
		},
		biometrics: {
			alignItems: 'flex-start',
			marginTop: 10,
		},
		biometryLabel: {
			flex: 1,
			fontSize: 16,
			color: colors.text.default,
			...fontStyles.normal,
		},
		biometrySwitch: {
			marginTop: 10,
			flex: 0,
		},
		termsAndConditions: {
			paddingVertical: 10,
		},
		passwordStrengthLabel: {
			height: 20,
			fontSize: 15,
			color: colors.text.default,
			...fontStyles.normal,
		},
		// eslint-disable-next-line react-native/no-unused-styles
		strength_weak: {
			color: colors.error.default,
		},
		// eslint-disable-next-line react-native/no-unused-styles
		strength_good: {
			color: colors.primary.default,
		},
		// eslint-disable-next-line react-native/no-unused-styles
		strength_strong: {
			color: colors.success.default,
		},
		showMatchingPasswords: {
			position: 'absolute',
			top: 52,
			right: 17,
			alignSelf: 'flex-end',
		},
		qrCode: {
			width: 80,
			marginRight: 120,
			marginLeft: 120,
			marginTop: -80,
			marginBottom: 30,
			alignSelf: 'flex-end',
			flexDirection: 'row',
		},
		paste: {
			width: 80,
			marginRight: 120,
			marginLeft: 120,
			marginTop: -70,
			alignSelf: 'flex-end',
			flexDirection: 'row',
		},
		pasteTxt: {
			marginLeft: 5,
		},
		inputFocused: {
			borderColor: colors.primary.default,
			borderWidth: 2,
		},
		input: {
			...fontStyles.normal,
			fontSize: 16,
			paddingTop: 2,
			color: colors.text.default,
		},
	});

/**
 * View where users can set restore their account
 * using a seed phrase
 */
class FaceId extends PureComponent {
	static propTypes = {
		/**
		 * The navigator object
		 */
		navigation: PropTypes.object,
		route: PropTypes.object,
	};

	state = {
		password: '',
		biometryType: null,
		biometryChoice: false,
		loading: false,
		error: null,
	};

	updateNavBar = () => {
		const { route, navigation } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(getOnboardingNavbarOptions(route, {}, colors));
	};

	async componentDidMount() {
		this.updateNavBar();
	}

	componentDidUpdate = () => {
		this.updateNavBar();
	};

	onEnableFaceId = () => {
		this.setState({ loading: true }, async () => {
			let credentials;
			try {
				credentials = await SecureKeychain.getGenericPassword();
			} catch (error) {
				Logger.error(error);
			}
			Logger.log(credentials.password);
			this.storeCredentials(credentials.password, 'biometryChoice');
		});
	};

	storeCredentials = async (password, type) => {
		try {
			await SecureKeychain.resetGenericPassword();

			await AsyncStorage.setItem(EXISTING_USER, TRUE);

			await SecureKeychain.setGenericPassword(password, SecureKeychain.TYPES.BIOMETRICS);

			this.props.navigation.reset({ routes: [{ name: 'Welcome' }] });

			this.setState({ [type]: true, loading: false });
		} catch (e) {
			if (e.message === 'Invalid password') {
				Alert.alert(strings('app_settings.invalid_password'), strings('app_settings.invalid_password_message'));
				trackErrorAsAnalytics('SecuritySettings: Invalid password', e?.message);
			} else {
				Logger.error(e, 'SecuritySettings:biometrics');
			}
			this.setState({ [type]: false, loading: false });
		}
	};

	toggleWarningModal = () => this.setState((state) => ({ warningModalVisible: !state.warningModalVisible }));

	onDisableFaceId = () => {
		this.props.navigation.navigate('Welcome');
	};

	render() {
		const { error, loading } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		//const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);

		return (
			<SafeAreaView style={styles.mainWrapper}>
				<KeyboardAwareScrollView style={styles.wrapper} resetScrollToCoords={{ x: 0, y: 0 }}>
					<View testID={'import-from-seed-screen'}>
						<Text style={styles.title}>{strings('faceid.title')}</Text>

						<View style={styles.container}>
							<Image style={styles.faceIdIcon} source={require('../../../images-new/faceid.png')} />
						</View>

						<DetectFaceIdModal
							warningModalVisible={this.state.warningModalVisible}
							cancelText={strings('login.i_understand')}
							onCancelPress={this.toggleWarningModal}
							onRequestClose={this.toggleWarningModal}
							onConfirmPress={this.toggleWarningModal}
						>
							<View style={styles.wrapper}>
								<View style={styles.container}>
									<Image
										style={styles.faceIdFailedIcon}
										source={require('../../../images-new/faceid-failed.png')}
									/>
									<Text style={styles.warning}>{strings('faceid.canot_detect_faceid')}</Text>
								</View>
							</View>
						</DetectFaceIdModal>
						<View style={styles.ctaWrapper}>
							<StyledButton
								type={'blue'}
								style={styles.onFaceIdButton}
								onPress={this.onEnableFaceId}
								testID={'submit'}
								disabled={error}
							>
								{loading ? (
									<ActivityIndicator size="small" color={colors.primary.inverse} />
								) : (
									strings('faceid.on_faceId_button')
								)}
							</StyledButton>
						</View>

						<View style={styles.ctaWrapper}>
							<Text style={styles.offFaceIdButton} onPress={this.onDisableFaceId } >
								{strings('faceid.off_faceId_button')}
							</Text>
						</View>
					</View>
				</KeyboardAwareScrollView>
			</SafeAreaView>
		);
	}
}

FaceId.contextType = ThemeContext;

const mapDispatchToProps = (dispatch) => ({
	setLockTime: (time) => dispatch(setLockTime(time)),
	setOnboardingWizardStep: (step) => dispatch(setOnboardingWizardStep(step)),
	passwordSet: () => dispatch(passwordSet()),
	seedphraseBackedUp: () => dispatch(seedphraseBackedUp()),
	logIn: () => dispatch(logIn()),
});

export default connect(null, mapDispatchToProps)(FaceId);
