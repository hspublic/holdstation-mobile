import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
	ActivityIndicator,
	BackHandler,
	Text,
	View,
	ScrollView,
	StyleSheet,
	Alert,
	InteractionManager,
	Dimensions,
	TouchableOpacity,
	
} from 'react-native';
import Modal from 'react-native-modal'
import AsyncStorage from '@react-native-community/async-storage';
import StyledButton from '../../UI/StyledButton';
import { fontStyles, baseStyles, colors as importedColors } from '../../../styles/common';
import OnboardingScreenWithBg from '../../UI/OnboardingScreenWithBg';
import { strings } from '../../../../locales/i18n';
import Button from 'react-native-button';
import { connect } from 'react-redux';
import SecureKeychain from '../../../core/SecureKeychain';
import Engine from '../../../core/Engine';
import FadeOutOverlay from '../../UI/FadeOutOverlay';
import Analytics from '../../../core/Analytics';
import { saveOnboardingEvent } from '../../../actions/onboarding';
import { toggleImportFromSeedModalVisible } from '../../../actions/modals';
import { getTransparentBackOnboardingNavbarOptions, getTransparentOnboardingNavbarOptions } from '../../UI/Navbar';
import Device from '../../../util/device';
import BaseNotification from '../../UI/Notification/BaseNotification';
import Animated, { EasingNode } from 'react-native-reanimated';
import ElevatedView from 'react-native-elevated-view';
import { loadingSet, loadingUnset } from '../../../actions/user';
import PreventScreenshot from '../../../core/PreventScreenshot';
import WarningExistingUserModal from '../../UI/WarningExistingUserModal';
import { PREVIOUS_SCREEN, ONBOARDING } from '../../../constants/navigation';
import { EXISTING_USER, METRICS_OPT_IN } from '../../../constants/storage';
import AnalyticsV2 from '../../../util/analyticsV2';
import DefaultPreference from 'react-native-default-preference';
import { ThemeContext, mockTheme } from '../../../util/theme';
import { ANALYTICS_EVENT_OPTS } from '../../../util/analytics';
import LinearGradient from 'react-native-linear-gradient';
import ImportFromSeed from '../ImportFromSeed';


const PUB_KEY = process.env.MM_PUBNUB_PUB_KEY;
const { width, height } = Dimensions.get('window');

const createStyles = (colors) =>
	StyleSheet.create({
		scroll: {
			flex: 1,
		},
		wrapper: {
			flex: 1,
			alignItems: 'center',
			paddingVertical: 30,
		},
		foxWrapper: {
			width: Device.isIos() ? 90 : 45,
			height: Device.isIos() ? 90 : 45,
			marginVertical: 20,
		},
		image: {
			alignSelf: 'center',
			width: Device.isIos() ? 90 : 45,
			height: Device.isIos() ? 90 : 45,
		},
		termsAndConditions: {
			paddingBottom: 30,
		},
		title: {
			fontSize: 24,
			color: colors.text.default,
			...fontStyles.bold,
			textAlign: 'center',
		},
		ctas: {
			flex: 1,
			position: 'relative',
		},
		footer: {
			marginTop: -20,
			marginBottom: 20,
		},
		login: {
			fontSize: 18,
			color: colors.primary.default,
			...fontStyles.normal,
		},
		buttonDescription: {
			...fontStyles.normal,
			fontSize: 14,
			textAlign: 'center',
			marginBottom: 16,
			color: colors.text.default,
			lineHeight: 20,
		},
		importWrapper: {
			marginVertical: 24,
		},
		startBtn: {
			width: width * 0.9,
			alignItems: 'center',
			justifyContent: 'center',
			height: height / 19,
		},
		buttonTxt: {
			...fontStyles.semiBold,
			fontSize: 16,
			color: '#FFFFFF',
			lineHeight: 20,
		},
		importBtn: {
			width: width * 0.9,
			alignItems: 'center',
			justifyContent: 'center',
			height: height / 19,
			backgroundColor: '#FFFFFF26',
			borderRadius: 12
		},
		createWrapper: {
			flex: 1,
			justifyContent: 'flex-end',
			marginBottom: 24,
		},
		buttonWrapper: {
			marginBottom: 10,
			zIndex: 0
		},
		loader: {
			marginTop: 180,
			justifyContent: 'center',
			textAlign: 'center',
		},
		loadingText: {
			marginTop: 30,
			fontSize: 14,
			textAlign: 'center',
			color: colors.text.default,
			...fontStyles.normal,
		},
		modalTypeView: {
			position: 'absolute',
			bottom: 0,
			paddingBottom: Device.isIphoneX() ? 20 : 10,
			left: 0,
			right: 0,
			backgroundColor: importedColors.transparent,
		},
		notificationContainer: {
			flex: 0.1,
			flexDirection: 'row',
			alignItems: 'flex-end',
		},
		zoominItem: {
			transform:[{scale: 0.97}]
		},
		bottomModal: {
			justifyContent: 'flex-end',
			margin: 0,
		}
	});

/**
 * View that is displayed to first time (new) users
 */
class Onboarding extends PureComponent {
	static propTypes = {
		/**
		 * The navigator object
		 */
		navigation: PropTypes.object,
		/**
		 * redux flag that indicates if the user set a password
		 */
		passwordSet: PropTypes.bool,
		/**
		 * Save onboarding event to state
		 */
		saveOnboardingEvent: PropTypes.func,
		/**
		 * loading status
		 */
		loading: PropTypes.bool,
		/**
		 * set loading status
		 */
		setLoading: PropTypes.func,
		/**
		 * unset loading status
		 */
		unsetLoading: PropTypes.func,
		/**
		 * loadings msg
		 */
		loadingMsg: PropTypes.string,
		/**
		 * Object that represents the current route info like params passed to it
		 */
		route: PropTypes.object,
		logOut: PropTypes.func,
		accountsModalVisible: PropTypes.bool.isRequired,
		importFromSeedModalVisible: PropTypes.bool,
		toggleImportFromSeedModalVisible: PropTypes.func
	};

	notificationAnimated = new Animated.Value(100);
	detailsYAnimated = new Animated.Value(0);
	actionXAnimated = new Animated.Value(0);
	detailsAnimated = new Animated.Value(0);

	animatedTimingStart = (animatedRef, toValue) => {
		Animated.timing(animatedRef, {
			toValue,
			duration: 500,
			easing: EasingNode.linear,
			useNativeDriver: true,
		}).start();
	};

	state = {
		warningModalVisible: false,
		loading: false,
		existingUser: false,
		onClickScale: false,
		onClickCancle: false,
	};

	seedwords = null;
	importedAccounts = null;
	channelName = null;
	incomingDataStr = '';
	dataToSync = null;
	mounted = false;
	animatingAccountsModal = false;

	warningCallback = () => true;

	showNotification = () => {
		// show notification
		this.animatedTimingStart(this.notificationAnimated, 0);
		// hide notification
		setTimeout(() => {
			this.animatedTimingStart(this.notificationAnimated, 200);
		}, 4000);
		this.disableBackPress();
	};

	disableBackPress = () => {
		// Disable back press
		const hardwareBackPress = () => true;
		BackHandler.addEventListener('hardwareBackPress', hardwareBackPress);
	};

	updateNavBar = () => {
		const { route, navigation } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(
			route.params?.delete
				? getTransparentOnboardingNavbarOptions(colors)
				: getTransparentBackOnboardingNavbarOptions(colors)
		);
	};

	componentDidMount() {
		this.updateNavBar();
		this.mounted = true;
		this.checkIfExistingUser();
		InteractionManager.runAfterInteractions(() => {
			PreventScreenshot.forbid();
			if (this.props.route.params?.delete) {
				this.props.setLoading(strings('onboarding.delete_current'));
				setTimeout(() => {
					this.showNotification();
					this.props.unsetLoading();
				}, 2000);
			}
		});
	}

	componentWillUnmount() {
		this.mounted = false;
		this.pubnubWrapper && this.pubnubWrapper.disconnectWebsockets();
		this.props.unsetLoading();
		InteractionManager.runAfterInteractions(PreventScreenshot.allow);
	}

	componentDidUpdate = () => {
		this.updateNavBar();
	};

	async checkIfExistingUser() {
		const existingUser = await AsyncStorage.getItem(EXISTING_USER);
		if (existingUser !== null) {
			this.setState({ existingUser: true });
		}
	}

	logOut = () => {
		this.props.navigation.navigate('Login');
		this.props.logOut();
	};

	onLogin = async () => {
		const { passwordSet } = this.props;
		if (!passwordSet) {
			const { KeyringController } = Engine.context;
			// Restore vault with empty password
			await KeyringController.submitPassword('');
			await SecureKeychain.resetGenericPassword();
			this.props.navigation.navigate('HomeNav');
		} else {
			this.logOut();
		}
	};

	handleExistingUser = (action) => {
		if (this.state.existingUser) {
			this.alertExistingUser(action);
		} else {
			action();
		}
	};

	onPressCreate = () => {
		const action = async () => {
			// const metricsOptIn = await DefaultPreference.get(METRICS_OPT_IN);
			// if (metricsOptIn) {
			// 	this.props.navigation.navigate('SetNameWallet', {
			// 		[PREVIOUS_SCREEN]: ONBOARDING,
			// 	});
			// 	this.track(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SETUP_STARTED);
			// } else {
			// 	this.props.navigation.navigate('OptinMetrics', {
			// 		onContinue: () => {
			// 			this.props.navigation.replace('SetNameWallet', {
			// 				[PREVIOUS_SCREEN]: ONBOARDING,
			// 			});
			// 			this.track(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SETUP_STARTED);
			// 		},
			// 	});
			// }
			// console.log('aaa')
			this.props.navigation.navigate('SetNameWallet', {
				[PREVIOUS_SCREEN]: ONBOARDING,
			});
			this.track(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SETUP_STARTED);
		};
		this.handleExistingUser(action);
	};

	onPressSync = () => {
		if (!PUB_KEY) {
			// Dev message
			Alert.alert(
				'This feature has been disabled',
				`Because you did not set the .js.env file. Look at .js.env.example for more information`
			);
			return false;
		}
		const action = async () => {
			const metricsOptIn = await DefaultPreference.get(METRICS_OPT_IN);
			if (metricsOptIn) {
				this.props.navigation.navigate('ExtensionSync', {
					[PREVIOUS_SCREEN]: ONBOARDING,
				});
				this.track(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SYNC_STARTED);
			} else {
				this.props.navigation.navigate('OptinMetrics', {
					onContinue: () => {
						this.props.navigation.replace('ExtensionSync', {
							[PREVIOUS_SCREEN]: ONBOARDING,
						});
						this.track(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SYNC_STARTED);
					},
				});
			}
		};
		this.handleExistingUser(action);
	};

	toggleAccountsModal = async () => {
		if (!this.animatingAccountsModal) {
			this.animatingAccountsModal = true;
			setTimeout(() => {
				this.animatingAccountsModal = false;
			}, 500);
		}

		!this.props.accountsModalVisible && this.trackEvent(ANALYTICS_EVENT_OPTS.NAVIGATION_TAPS_ACCOUNT_NAME);
	};

	trackEvent = (event) => {
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(event);
		});
	};

	onPressImport = () => {
		this.toggleAccountsModal();
		// console.log('this.props.importFromSeedModalVisible', )
		// this.props.navigation.navigate('ImportFromSeed');
		this.toggleImportFromSeedModalVisible()

		// const action = async () => {
		// 	const metricsOptIn = await DefaultPreference.get(METRICS_OPT_IN);
		// 	this.props.navigation.push('ImportFromSeed');
		// 	this.track(AnalyticsV2.ANALYTICS_EVENTS.WALLET_IMPORT_STARTED);
		// 	// if (metricsOptIn) {
		// 	// 	this.props.navigation.push('ImportFromSeed');
		// 	// 	this.track(AnalyticsV2.ANALYTICS_EVENTS.WALLET_IMPORT_STARTED);
		// 	// } else {
		// 	// 	this.props.navigation.navigate('OptinMetrics', {
		// 	// 		onContinue: () => {
		// 	// 			this.props.navigation.replace('ImportFromSeed');
		// 	// 			this.track(AnalyticsV2.ANALYTICS_EVENTS.WALLET_IMPORT_STARTED);
		// 	// 		},
		// 	// 	});
		// 	// }
		// };
		// this.handleExistingUser(action);
	};

	track = (...eventArgs) => {
		InteractionManager.runAfterInteractions(async () => {
			if (Analytics.getEnabled()) {
				AnalyticsV2.trackEvent(...eventArgs);
				return;
			}
			const metricsOptIn = await DefaultPreference.get(METRICS_OPT_IN);
			if (!metricsOptIn) {
				this.props.saveOnboardingEvent(eventArgs);
			}
		});
	};

	alertExistingUser = (callback) => {
		this.warningCallback = () => {
			callback();
			this.toggleWarningModal();
		};
		this.toggleWarningModal();
	};

	toggleWarningModal = () => {
		const warningModalVisible = this.state.warningModalVisible;
		this.setState({ warningModalVisible: !warningModalVisible });
	};

	renderLoader = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<View style={styles.wrapper}>
				<View style={styles.loader}>
					<ActivityIndicator size="small" />
					<Text style={styles.loadingText}>{this.props.loadingMsg}</Text>
				</View>
			</View>
		);
	};

	renderContent() {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<View style={styles.ctas}>
				<View style={styles.createWrapper}>
					<View style={styles.buttonWrapper}>
						
							<TouchableOpacity
								// style={styles.startBtn}
								onPress={this.onPressCreate}
								testID={'create-wallet-button'}
								onPressIn={() => this.setState({onClickScale: true})}
								onPressOut={() => this.setState({onClickScale: false})} 
								activeOpacity={1}
								style={this.state.onClickScale && styles.zoominItem}
							>	
								<LinearGradient
										colors={['#5239EC', '#6658FF']}
										style={[{borderRadius: 12}, styles.startBtn]}
										start={{x: 0, y: 0}}
										end={{x: 0, y: 1}}
								>
									<Text style={styles.buttonTxt}>
										{strings('onboarding.start_exploring_now')}
									</Text>
								</LinearGradient>		
							</TouchableOpacity>

					</View>
					<View style={styles.buttonWrapper}>
						<TouchableOpacity
								style={[styles.importBtn, this.state.onClickCancle && styles.zoominItem]}
								onPress={this.onPressImport}
								testID={'import-wallet-import-from-seed-button'}
								onPressIn={() => this.setState({onClickCancle: true})}
								onPressOut={() => this.setState({onClickCancle: false})} 
								activeOpacity={1}
							>
								<Text style={styles.buttonTxt}>
									{strings('import_wallet.import_from_seed_button')}
								</Text>
							</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}

	handleSimpleNotification = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (!this.props.route.params?.delete) return;
		return (
			<Animated.View
				style={[styles.notificationContainer, { transform: [{ translateY: this.notificationAnimated }] }]}
			>
				<ElevatedView style={styles.modalTypeView} elevation={100}>
					<BaseNotification
						closeButtonDisabled
						status="success"
						data={{ title: strings('onboarding.success'), description: strings('onboarding.your_wallet') }}
					/>
				</ElevatedView>
			</Animated.View>
		);
	};
	
	toggleImportFromSeedModalVisible = () => {
		this.props.toggleImportFromSeedModalVisible()
	}

	showImportFromSeedModal = () => {
		this.toggleImportFromSeedModalVisible();
	};

	render() {
		const { loading } = this.props;
		const { existingUser } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<View style={baseStyles.flexGrow} testID={'onboarding-screen'}>
				<OnboardingScreenWithBg>
					<View style={baseStyles.flexGrow} contentContainerStyle={styles.scroll}>
						<View style={styles.wrapper}>
							{loading && (
								<View style={styles.foxWrapper}>
								</View>
							)}
							{loading ? this.renderLoader() : this.renderContent()}
						</View>
						{existingUser && !loading && (
							<View style={styles.footer}>
								<Button style={styles.login} onPress={this.onLogin}>
									{strings('onboarding.unlock')}
								</Button>
							</View>
						)}
					</View>
					{/*<View style={styles.termsAndConditions}>*/}
					{/*	<TermsAndConditions navigation={this.props.navigation} />*/}
					{/*</View>*/}
					{/* <Modal
						isVisible={this.props.receiveModalVisible}
						onBackdropPress={this.toggleReceiveModal}
						onBackButtonPress={this.toggleReceiveModal}
						onSwipeComplete={this.toggleReceiveModal}
						swipeDirection={'down'}
						propagateSwipe
						style={styles.bottomModal}
						backdropColor={colors.overlay.default}
						backdropOpacity={1}
				>
						<ReceiveRequest
							navigation={this.props.navigation}
							hideModal={this.toggleReceiveModal}
							showReceiveModal={this.showReceiveModal}
						/>
					</Modal> */}
					<Modal
						isVisible={this.props.importFromSeedModalVisible}
						onBackdropPress={this.toggleImportFromSeedModalVisible}
						onBackButtonPress={this.toggleImportFromSeedModalVisible}
						onSwipeComplete={this.toggleImportFromSeedModalVisible}
						swipeDirection={'down'}
						propagateSwipe
						style={styles.bottomModal}
						backdropColor={colors.overlay.default}
						backdropOpacity={1}
					>
						<ImportFromSeed 
							navigation={this.props.navigation}
							hideModal={this.toggleImportFromSeedModalVisible}
							showReceiveModal={this.showImportFromSeedModal}
							route={this.props.route}
						/>
					</Modal>
				</OnboardingScreenWithBg>
				<FadeOutOverlay />

				<View>{this.handleSimpleNotification()}</View>

				<WarningExistingUserModal
					warningModalVisible={this.state.warningModalVisible}
					onCancelPress={this.warningCallback}
					onRequestClose={this.toggleWarningModal}
					onConfirmPress={this.toggleWarningModal}
				/>
			</View>
		);
	}
}

Onboarding.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	accounts: state.engine.backgroundState.AccountTrackerController.accounts,
	passwordSet: state.user.passwordSet,
	loading: state.user.loadingSet,
	loadingMsg: state.user.loadingMsg,
	accountsModalVisible: state.modals.accountsModalVisible,
	importFromSeedModalVisible: state.modals.importFromSeedModalVisible,
});

const mapDispatchToProps = (dispatch) => ({
	setLoading: (msg) => dispatch(loadingSet(msg)),
	unsetLoading: () => dispatch(loadingUnset()),
	saveOnboardingEvent: (event) => dispatch(saveOnboardingEvent(event)),
	toggleImportFromSeedModalVisible: () => dispatch(toggleImportFromSeedModalVisible()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Onboarding);
