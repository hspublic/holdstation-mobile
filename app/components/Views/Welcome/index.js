import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
	Switch,
	ActivityIndicator,
	Alert,
	TouchableOpacity,
	Text,
	Image,
	View,
	TextInput,
	SafeAreaView,
	StyleSheet,
	Linking,
	InteractionManager,
	Platform,
	Dimensions,
	KeyboardAvoidingView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { getOnboardingNavbarOptions } from '../../UI/Navbar';
import { connect } from 'react-redux';
import { setLockTime } from '../../../actions/settings';
import StyledButton from '../../UI/StyledButton';
import Engine from '../../../core/Engine';
import { fontStyles, colors as importedColors } from '../../../styles/common';
import { strings } from '../../../../locales/i18n';
import AppConstants from '../../../core/AppConstants';
import TermsAndConditions from '../TermsAndConditions';
import zxcvbn from 'zxcvbn';
import Icon from 'react-native-vector-icons/FontAwesome';
import Device from '../../../util/device';
import { OutlinedTextField } from 'react-native-material-textfield';

import Logger from '../../../util/Logger';
import importAdditionalAccounts from '../../../util/importAdditionalAccounts';
import AnalyticsV2 from '../../../util/analyticsV2';
import DefaultPreference from 'react-native-default-preference';
import { ThemeContext, mockTheme } from '../../../util/theme';
import Lottie from 'lottie-react-native';

const MINIMUM_SUPPORTED_CLIPBOARD_VERSION = 9;

const win = Dimensions.get('window');

const createStyles = (colors) =>
	StyleSheet.create({
		mainWrapper: {
			backgroundColor: '#1B1B23',
			flex: 1,
		},
		wrapper: {
			flex: 1,
			paddingHorizontal: 32,
			// justifyContent: 'center',
			// alignContent: 'center'
		},
		title: {
			fontSize: 16,
			color: '#FFFFFF',
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
			lineHeight: 19,
			marginBottom: win.height / 100
			
		},
		marignTopTitle: {
			marginTop: win.height / 6
		},
		container: {
		    justifyContent: 'center',
		    alignItems: 'center',
		  },
		likeIcon: {
			// flex:1,
        	marginTop: win.height / 6,
        	// marginBottom: 30,
			width: 100,
			justifyContent: 'center'
		},
		ctaWrapper: {
			marginTop: 250,
			marginBottom: 30,
			marginLeft: - win.width / 33,
			justifyContent: 'flex-end',
			flex: 1,
		},
		offFaceIdButton: {
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
			fontSize: 15,
			color: '#FFFFFF',
		},
		padding: {
			paddingRight: 46,
		},
		biometrics: {
			alignItems: 'flex-start',
			marginTop: 10,
		},
		biometryLabel: {
			flex: 1,
			fontSize: 16,
			color: colors.text.default,
			...fontStyles.normal,
		},
		biometrySwitch: {
			marginTop: 10,
			flex: 0,
		},
		termsAndConditions: {
			paddingVertical: 10,
		},
		passwordStrengthLabel: {
			height: 20,
			fontSize: 15,
			color: colors.text.default,
			...fontStyles.normal,
		},
		textWrapper:{
			fontSize: 16,
			...fontStyles.semiBold,
			color: '#FFFFFF',
			textAlign:'center'
		},
		buttonWrapper: {
			justifyContent:'center',
			alignItems: 'center',
			backgroundColor: '#7E73FF',
			width: win.width * 0.9,
			height: 44,
			borderRadius: 12
		},
		zoominItem: {
			transform:[{scale: 0.97}]
		},
	});

const PASSCODE_NOT_SET_ERROR = 'Error: Passcode not set.';

/**
 * View where users can set restore their account
 * using a seed phrase
 */
class Welcome extends PureComponent {
	static propTypes = {
		/**
		 * The navigator object
		 */
		navigation: PropTypes.object,
		/**
		 * The action to update the password set flag
		 * in the redux store
		 */
		passwordSet: PropTypes.func,
		/**
		 * The action to set the locktime
		 * in the redux store
		 */
		setLockTime: PropTypes.func,
		/**
		 * The action to update the seedphrase backed up flag
		 * in the redux store
		 */
		seedphraseBackedUp: PropTypes.func,
		/**
		 * Action to set onboarding wizard step
		 */
		setOnboardingWizardStep: PropTypes.func,
		logIn: PropTypes.func,
		route: PropTypes.object,
	};

	state = {
		loading: false,
		error: null,
		inputWidth: { width: '99%' },
		onClickUse: false,
	};

	updateNavBar = () => {
		const { route, navigation } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(getOnboardingNavbarOptions(route, {}, colors));
	};

	componentDidUpdate = () => {
		this.updateNavBar();
	};

	onStartButton = () => {
		this.setState({ loading: true });
		// this.props.navigation.navigate('HomeNav');
		this.props.navigation.replace('HomeNav');
	};

	render() {
		const {
			inputWidth,
			error,
			loading,
		} = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);
		const { route } = this.props;
		const type = this.props.route.params?.type;

		return (
			<SafeAreaView style={styles.mainWrapper}>
				<KeyboardAvoidingView style={styles.wrapper}>
					<View testID={'import-from-seed-screen'} style={{flex: 1}}>
						<View style={styles.container}>
							{/* <Image
								style={styles.likeIcon}
								source={require('../../../images-new/like.png')}
							/> */}
							<Lottie source={require('../../../animations/success.json')} loop={false} autoPlay style={styles.likeIcon} />
							<Text style={[styles.title, styles.marignTopTitle]}>{type == null ? strings('wellcome.finish_wallet_1') : strings('wellcome.finish_wallet_3')}</Text>
							<Text style={styles.title}>{strings('wellcome.finish_wallet_2')}</Text>
						</View>

						<View style={styles.ctaWrapper}>
							{/* <StyledButton
								type={'blue'}
								style={styles.onStartButton}
								onPress={this.onStartButton}
								testID={'submit'}
								disabled={error}
							>
								{loading ? (
									<ActivityIndicator size="small" color={colors.primary.inverse} />
								) : (
									strings('wellcome.start_button')
								)}
							</StyledButton> */}
							<TouchableOpacity
								onPress={this.onStartButton}
								testID={'submit'}
								disabled={error}
								style={[styles.buttonWrapper, this.state.onClickUse && styles.zoominItem]}
								onPressIn={() => this.setState({onClickUse: true})}
								onPressOut={() => this.setState({onClickUse: false})} 
								activeOpacity={1}
							>	
								{loading ? (
									<ActivityIndicator size="small" color={colors.primary.inverse} />
								) : (
									<Text style={styles.textWrapper}>
										{strings('wellcome.start_button')}
									</Text>
								)}
								
							</TouchableOpacity>
						</View>
					</View>
				</KeyboardAvoidingView>

			</SafeAreaView>
		);
	}
}

Welcome.contextType = ThemeContext;

const mapDispatchToProps = (dispatch) => ({
	setLockTime: (time) => dispatch(setLockTime(time)),
	setOnboardingWizardStep: (step) => dispatch(setOnboardingWizardStep(step)),
	logIn: () => dispatch(logIn()),
});

export default connect(null, mapDispatchToProps)(Welcome);
