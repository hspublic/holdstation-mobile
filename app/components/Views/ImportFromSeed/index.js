import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
	Switch,
	ActivityIndicator,
	Alert,
	TouchableOpacity,
	Text,
	View,
	TextInput,
	SafeAreaView,
	StyleSheet,
	InteractionManager,
	Platform, Image,
	Dimensions,
	Modal,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { getOnboardingNavbarOptions } from '../../UI/Navbar';
import { connect } from 'react-redux';
import { logIn, passwordSet, seedphraseBackedUp } from '../../../actions/user';
import { setLockTime } from '../../../actions/settings';
import StyledButton from '../../UI/StyledButton';
import Engine from '../../../core/Engine';
import { fontStyles, colors as importedColors, GLOBAL_ITEM_FONT_SIZE } from '../../../styles/common';
import { strings } from '../../../../locales/i18n';
import SecureKeychain from '../../../core/SecureKeychain';
import setOnboardingWizardStep from '../../../actions/wizard';
import zxcvbn from 'zxcvbn';
import Icon from 'react-native-vector-icons/FontAwesome';
import Device from '../../../util/device';
import {
	failedSeedPhraseRequirements,
	failedPrivateKeyRequirements,
	isValidMnemonic,
	parseSeedPhrase,
	parseVaultValue,
} from '../../../util/validators';
import {
	BIOMETRY_CHOICE_DISABLED,
	TRUE,
} from '../../../constants/storage';
import AnalyticsV2 from '../../../util/analyticsV2';
import Clipboard from '@react-native-clipboard/clipboard';
import { ThemeContext, mockTheme } from '../../../util/theme';
import { importAccountFromPrivateKey } from '../../../util/address';
import { win32 } from 'path';
const MINIMUM_SUPPORTED_CLIPBOARD_VERSION = 9;

const { width, height } = Dimensions.get('window');
const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			backgroundColor: '#1B1B23',
			// borderTopLeftRadius: 10,
			// borderTopRightRadius: 10,
			flexDirection: 'column',
			// marginTop: height / 2
		},
		wrapperHigh:{
			minHeight: '65%'
		},
		wrapperLow:{
			minHeight: '100%',
		},
		dragger: {
			width: width/8.625,
			height: 5,
			borderRadius: 4,
			backgroundColor: colors.border.default,
			opacity: 1
		},
		titleWrapper: {
			width: '100%',
			height: 40,
			alignItems: 'center',
			justifyContent: 'flex-end',
		},
		title: {
			fontSize: Device.isAndroid() ? 17 : 16,
			color: '#FFFFFF',
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.semiBold,
			lineHeight: 20,
			justifyContent: 'flex-start',
			marginTop: height /50,
			marginBottom: height / 60
			// marginBottom: height / 40
		},
		field: {
			marginVertical: 5,
			position: 'relative',
		},
		fieldRow: {
			alignItems: 'center',
			justifyContent: 'center',
			textAlign: 'center',
			marginBottom: height/ 15,
		},
		fieldCol: {
			width: '70%',
		},
		fieldColRight: {
			flexDirection: 'row-reverse',
			width: '30%',
		},
		label: {
			color: '#FF4B4B',
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			marginBottom: 12,
			...fontStyles.normal,
			textAlign: 'center',
			marginTop: height / 200
		},

		txtWrapper: {
			// paddingLeft: width/20,
			// paddingRight: width/20,
			width: '90%',
			height: height/2.7,
			marginLeft: width / 20,
			marginTop: height / 60,
			flexDirection: 'column',
			borderWidth: 1,
			borderRadius: 10,
			borderColor: '#FFFFFF26',
			backgroundColor: '#1B1B23',
			alignItems: 'center',
			justifyContent: 'center',
			shadowColor: "#000000",
			shadowOpacity: 1,
			shadowRadius: 37,
			shadowOffset: {
				width: 0,
				height: 9
			},

		},
		ctaWrapper: {
			// marginTop: 280,
			flex: 1,
			justifyContent:'flex-end',
		},
		errorMsg: {
			color: colors.error.default,
			textAlign: 'center',
			...fontStyles.normal,
		},
		seedPhrase: {
			// padding: width / 20,
			// flex: 13,
			// justifyContent: 'flex-end',
			// alignItems: 'flex-end',
			...fontStyles.normal,
			fontSize: 16,
			color: '#FFFFFF',
			paddingHorizontal: 20,
			marginTop: height / 23,
			marginBottom: width / 9.5,
			// backgroundColor: 'red'
		},
		errorPhrase: {
			fontSize: 12,
			...fontStyles.normal,
			backgroundColor: 'rgba(222, 44, 44, 0.13)',
			alignItems: 'center',
			justifyContent: 'center',
			textAlign:'center',
			borderRadius: 10,
			width: width * 0.9,
			marginTop : height / 40,
			marginBottom: height/ 15,
			marginLeft: width / 20,
			paddingHorizontal: 5,
			paddingTop: 3,
		},
		viewPhrase: {
			fontSize: 12,
			borderRadius: 10,
			backgroundColor: '#1B1B23',
			...fontStyles.normal,
			paddingTop: 10,
			marginTop : -height / 5
		},
		padding: {
			paddingRight: 46,
		},
		biometrics: {
			alignItems: 'flex-start',
			marginTop: 10,
		},
		biometryLabel: {
			flex: 1,
			fontSize: 16,
			color: colors.text.default,
			...fontStyles.normal,
		},
		biometrySwitch: {
			marginTop: 10,
			flex: 0,
		},
		passwordStrengthLabel: {
			height: 20,
			fontSize: 15,
			color: colors.text.default,
			...fontStyles.normal,
		},
		// eslint-disable-next-line react-native/no-unused-styles
		strength_weak: {
			color: colors.error.default,
		},
		// eslint-disable-next-line react-native/no-unused-styles
		strength_good: {
			color: colors.primary.default,
		},
		// eslint-disable-next-line react-native/no-unused-styles
		strength_strong: {
			color: colors.success.default,
		},
		showMatchingPasswords: {
			position: 'absolute',
			top: 52,
			right: 17,
			alignSelf: 'flex-end',
		},
		qrCode: {
			width: 80,
			marginRight: 120,
			marginLeft: 120,
			marginTop: -80,
			marginBottom: 30,
			alignSelf: 'flex-end',
			flexDirection: 'row',
		},
		pasteWrapper: {
			flexDirection: 'row',
			color: '#B1B5FF',
			position: 'absolute',
			top : height / 3
		},
		pasteTxt: {
			marginLeft: 5,
			marginTop: 1,
			color: '#FFFFFF',
			...fontStyles.semiBold,
			fontSize: GLOBAL_ITEM_FONT_SIZE
		},
		inputFocused: {
			borderColor: colors.primary.default,
			borderWidth: 2,
		},
		input: {
			...fontStyles.normal,
			fontSize: 16,
			paddingTop: 2,
			color: colors.text.default,
		},
		warningText: {
			color: 'red',
			fontSize: 16,
			textAlign: "center",
			marginBottom : -height / 256
		},
		continueButton: {
			backgroundColor: '#6A45FF', 	
		},
		buttonText: {
			justifyContent:'center', 
			width: width * 0.9, 
			alignContent: 'center', 
			height: height /23,
			marginLeft: width / 20, 
			borderRadius: 12,
			marginBottom: height / 31
		},
		disabledButton: {
			backgroundColor:'#FFFFFF26',
		},
		zoominItem: {
			transform:[{scale: 0.97}]
		},
		zoominPaste: {
			transform:[{scale: 0.9}]
		},
		textWrapper: {
			textAlign: 'center', 
			...fontStyles.semiBold, 
			fontSize: 16, 
			lineHeight:20
		},
		textContinue: {
			color: '#FFFFFF', 
		},
		textDisable: {
			color: '#FFFFFF26'
		}
	});

const PASSCODE_NOT_SET_ERROR = 'Error: Passcode not set.';

/**
 * View where users can set restore their account
 * using a seed phrase
 */
class ImportFromSeed extends PureComponent {
	static propTypes = {
		/**
		 * The navigator object
		 */
		navigation: PropTypes.object,
		/**
		 * The action to update the password set flag
		 * in the redux store
		 */
		passwordSet: PropTypes.func,
		/**
		 * The action to set the locktime
		 * in the redux store
		 */
		setLockTime: PropTypes.func,
		/**
		 * The action to update the seedphrase backed up flag
		 * in the redux store
		 */
		seedphraseBackedUp: PropTypes.func,
		/**
		 * Action to set onboarding wizard step
		 */
		setOnboardingWizardStep: PropTypes.func,
		logIn: PropTypes.func,
		route: PropTypes.object,
		hideModal: PropTypes.func,
	};

	state = {
		password: '',
		confirmPassword: '',
		seed: '',
		biometryType: null,
		rememberMe: false,
		secureTextEntry: true,
		biometryChoice: false,
		loading: false,
		error: null,
		seedphraseInputFocused: false,
		inputWidth: { width: '99%' },
		hideSeedPhraseInput: true,
		onClickScale: false,
	};

	passwordInput = React.createRef();
	confirmPasswordInput = React.createRef();

	updateNavBar = () => {
		const { route, navigation } = this.props;
		// console.log(route)
		const colors = this.context.colors || mockTheme.colors;
		navigation.setOptions(getOnboardingNavbarOptions(route, {}, colors));
	};

	async componentDidMount() {
		this.updateNavBar();
		const biometryType = await SecureKeychain.getSupportedBiometryType();
		if (biometryType) {
			let enabled = true;
			const previouslyDisabled = await AsyncStorage.removeItem(BIOMETRY_CHOICE_DISABLED);
			if (previouslyDisabled && previouslyDisabled === TRUE) {
				enabled = false;
			}
			this.setState({ biometryType: Device.isAndroid() ? 'biometrics' : biometryType, biometryChoice: enabled });
		}
		// Workaround https://github.com/facebook/react-native/issues/9958
		setTimeout(() => {
			this.setState({ inputWidth: { width: '100%' } });
		}, 100);
	}

	componentDidUpdate = () => {
		this.updateNavBar();
	};

	createNewVaultAndAppend = async (parsedSeed) => {
		const { KeyringController } = Engine.context;
		await Engine.resetState();
		await KeyringController.createNewVaultAndAppend(parsedSeed);
		this.keyringControllerPasswordSet = true;
	};

	importNewPrivate = async (privateKey) => {
		console.log('importNewPrivate')
		privateKey = privateKey.trim();
		this.setState({ loading: true });
		// Import private key
		if (privateKey.length === 66) {
			privateKey = privateKey.slice(2);
		}
		try {
			await importAccountFromPrivateKey(privateKey);
			if (typeof this.props.hideModal !== "undefined") { 
				this.props.hideModal()
			}
			this.props.navigation.navigate('Welcome', { type: 'import' });
			this.setState({ loading: false });
		} catch (error) {
			if (error.toString().includes('duplicate')){
				this.setState({loading:false, error: strings('import_from_seed.duplicate_key')})
			}
			else this.setState({ loading: false, error: 'import_from_seed.invalid_seed_phrase' });
		}
	};

	importNewSeed = async (seedPhrase) => {
		console.log('importNewSeed')
		InteractionManager.runAfterInteractions(() => {
			AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_CREATION_ATTEMPTED);
		});
		try {
			this.setState({ loading: true });
			await this.createNewVaultAndAppend(seedPhrase);
			if (typeof this.props.hideModal !== "undefined") { 
				this.props.hideModal()
			}
			this.setState({ loading: false });
			this.props.navigation.navigate('Welcome', { type: 'import' });
			InteractionManager.runAfterInteractions(() => {
				AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_CREATED, {
					biometrics_enabled: Boolean(this.state.biometryType),
				});
				AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SETUP_COMPLETED, {
					wallet_setup_type: 'new',
					new_wallet: true,
				});
			});
		} catch (error) {
			if (error.toString().includes('duplicate')){
				this.setState({loading:false, error: strings('import_from_seed.duplicate_key')})
			}
			else {
				this.setState({ loading: false, error: error.toString() });
			}
			InteractionManager.runAfterInteractions(() => {
				AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SETUP_FAILURE, {
					wallet_setup_type: 'new',
					error_type: error.toString(),
				});
			});
		}
	};

	isValidPrivateKey = function (key) {
		return (key.length === 66 || key.length === 64) && key.indexOf(' ') === -1;
	};

	onPressImport = async () => {
		const { loading, seed, password } = this.state;

		const vaultSeed = await parseVaultValue(password, seed);
		const parsedSeed = parseSeedPhrase(vaultSeed || seed);
		//Set the seed state with a valid parsed seed phrase (handle vault scenario)
		this.setState({ seed: parsedSeed });

		if (loading) return;
		InteractionManager.runAfterInteractions(() => {
			AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_IMPORT_ATTEMPTED);
		});
		if (this.isValidPrivateKey(parsedSeed)) {
			// Add more account
			this.setState({ loading: true });
			await this.importNewPrivate(parsedSeed);
		} else {

			let error = null;
			if (parsedSeed === '') {
				// error = strings('import_from_seed.error_empty_message');
				error = strings('import_from_seed.invalid_seed_phrase');
			} else if (failedPrivateKeyRequirements(parsedSeed)) {
				// error = strings('import_private_key.error_message');
				error = strings('import_from_seed.invalid_seed_phrase');
			} else if (failedSeedPhraseRequirements(parsedSeed)) {
				// error = strings('import_from_seed.seed_phrase_requirements');
				error = strings('import_from_seed.invalid_seed_phrase');
			} else if (!isValidMnemonic(parsedSeed)) {
				error = strings('import_from_seed.invalid_seed_phrase');
			}
			if (error) {
				// Alert.alert(strings('import_from_seed.error'), error);
				this.setState({ error });
				InteractionManager.runAfterInteractions(() => {
					AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.WALLET_SETUP_FAILURE, {
						wallet_setup_type: 'import',
						error_type: error,
					});
				});
			} else if (!this.props.route?.params?.multiWallet) {
				if (typeof this.props.hideModal !== "undefined") { 
					// safe to use the function
					this.props.hideModal()
				}
				this.setState({ loading: true });
				this.props.navigation.navigate('ChoosePin', {
					seed: parsedSeed,
					type: 'import'
				});
			} else {
				// Add more wallet
				this.setState({ loading: true });
				await this.importNewSeed(parsedSeed);
			}
		}
	};

	onBiometryChoiceChange = (value) => {
		this.setState({ biometryChoice: value });
	};

	clearSecretRecoveryPhrase = async (seed) => {
		// get clipboard contents
		const clipboardContents = await Clipboard.getString();
		const parsedClipboardContents = parseSeedPhrase(clipboardContents);
		if (
			// only clear clipboard if contents isValidMnemonic
			!failedSeedPhraseRequirements(parsedClipboardContents) &&
			isValidMnemonic(parsedClipboardContents) &&
			// only clear clipboard if the seed phrase entered matches what's in the clipboard
			parseSeedPhrase(seed) === parsedClipboardContents
		) {
			await Clipboard.clearString();
		}
	};

	validateSeedOrPriv = (text) => {
		const privateRegex = /^(0x)?([A-Fa-f0-9]{64})$/;
		const alphaRegex = /^[a-zA-Z ]*$/;
		if (privateRegex.test(text)) {
			return true;
		}
		if (alphaRegex.test(text)) {
			const arrText = text.trim().split(' ');
			if (arrText.length === 12 || arrText.length === 18 || arrText.length === 24) {
				return true;
			}
		}
		return false;
	};

	onSeedWordsChange = async (seed) => {
		if (!this.validateSeedOrPriv(seed)) {
			this.setState({ error: '' });
		} else {
			this.setState({ error: null });
		}

		this.setState({ seed });
		// Only clear on android since iOS will notify users when we getString()
		if (Device.isAndroid()) {
			const androidOSVersion = parseInt(Platform.constants.Release, 10);
			// This conditional is necessary to avoid an error in Android 8.1.0 or lower
			if (androidOSVersion >= MINIMUM_SUPPORTED_CLIPBOARD_VERSION) {
				await this.clearSecretRecoveryPhrase(seed);
			}
		}
	};

	onPasswordChange = (val) => {
		const passInfo = zxcvbn(val);

		this.setState({ password: val, passwordStrength: passInfo.score });
	};

	onPasswordConfirmChange = (val) => {
		this.setState({ confirmPassword: val });
	};

	jumpToPassword = () => {
		const { current } = this.passwordInput;
		current && current.focus();
	};

	jumpToConfirmPassword = () => {
		const { current } = this.confirmPasswordInput;
		current && current.focus();
	};

	updateBiometryChoice = async (biometryChoice) => {
		if (!biometryChoice) {
			await AsyncStorage.setItem(BIOMETRY_CHOICE_DISABLED, TRUE);
		} else {
			await AsyncStorage.removeItem(BIOMETRY_CHOICE_DISABLED);
		}
		this.setState({ biometryChoice });
	};

	renderSwitch = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (this.state.biometryType) {
			return (
				<View style={styles.biometrics}>
					<Text style={styles.biometryLabel}>
						{strings(`biometrics.enable_${this.state.biometryType.toLowerCase()}`)}
					</Text>
					<Switch
						onValueChange={this.updateBiometryChoice}
						value={this.state.biometryChoice}
						style={styles.biometrySwitch}
						trackColor={{ true: colors.primary.default, false: colors.border.muted }}
						thumbColor={importedColors.white}
						ios_backgroundColor={colors.border.muted}
					/>
				</View>
			);
		}

		return (
			<View style={styles.biometrics}>
				<Text style={styles.biometryLabel}>{strings(`choose_password.remember_me`)}</Text>
				<Switch
					onValueChange={(rememberMe) => this.setState({ rememberMe })} // eslint-disable-line react/jsx-no-bind
					value={this.state.rememberMe}
					style={styles.biometrySwitch}
					trackColor={{ true: colors.primary.default, false: colors.border.muted }}
					thumbColor={importedColors.white}
					ios_backgroundColor={colors.border.muted}
					testID={'remember-me-toggle'}
				/>
			</View>
		);
	};

	toggleShowHide = () => {
		this.setState({ secureTextEntry: !this.state.secureTextEntry });
	};

	toggleHideSeedPhraseInput = () => {
		this.setState(({ hideSeedPhraseInput }) => ({ hideSeedPhraseInput: !hideSeedPhraseInput }));
	};

	onQrCodePress = () => {
		setTimeout(this.toggleHideSeedPhraseInput, 100);
		this.props.navigation.navigate('QRScanner', {
			onScanSuccess: ({ seed = undefined }) => {
				if (seed) {
					this.setState({ seed });
				} else {
					Alert.alert(
						strings('import_from_seed.invalid_qr_code_title'),
						strings('import_from_seed.invalid_qr_code_message')
					);
				}
				this.toggleHideSeedPhraseInput();
			},
			onScanError: (error) => {
				this.toggleHideSeedPhraseInput();
			},
		});
	};

	onPastePress = async (seed) => {
		// get clipboard contents
		const clipboardContents = await Clipboard.getString();
		if (!clipboardContents) return;
		seed = clipboardContents;
		this.setState({ seed, error: null });
	};

	seedphraseInputFocused = () => this.setState({ seedphraseInputFocused: !this.state.seedphraseInputFocused });

	render() {
		const {
			seed,
			seedphraseInputFocused,
			inputWidth,
			error,
			loading,
			hideSeedPhraseInput,
		} = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);

		// const SecretTextArea = styled(Input).attrs({
		// 	align: 'center',
		// 	autoCapitalize: 'none',
		// 	autoComplete: 'off',
		// 	autoCorrect: false,
		// 	autoFocus: true,
		// 	dataDetectorTypes: 'none',
		// 	enablesReturnKeyAutomatically: true,
		// 	keyboardType: android ? 'visible-password' : 'default',
		// 	lineHeight: 'looser',
		// 	multiline: true,
		// 	numberOfLines: 3,
		// 	placeholder,
		// 	returnKeyType: 'done',
		// 	size: 'large',
		// 	spellCheck: false,
		// 	textContentType: 'none',
		// 	weight: 'semibold',
		// 	})({
		// 	marginBottom: android ? 55 : 0,
		// 	minHeight: android ? 100 : 50,
		// 	width: '100%',
		// 	});

		return (
			<>
				<View style={[styles.titleWrapper, typeof this.props.hideModal === "undefined" ? {backgroundColor: '#1B1B23'}: null]}>
						<View style={styles.dragger} testID={'account-list-dragger'} />
				</View>
				<SafeAreaView style={[styles.wrapper, typeof this.props.hideModal !== "undefined" ? styles.wrapperHigh: styles.wrapperLow]} testID={'account-list'}>
					<Text style={styles.title}>{strings('import_from_seed.title')}</Text>
					<View testID={'import-from-seed-screen'} style={[styles.txtWrapper, typeof this.props.hideModal === "undefined" && {marginTop: height / 5}]}>
						<TextInput
							value={seed}
							numberOfLines={10}
							style={[styles.seedPhrase, inputWidth, seedphraseInputFocused && styles.inputFocused]}
							multiline={true}
							placeholder={strings('import_from_seed.seed_phrase_placeholder')}
							placeholderTextColor={'#58586C'}
							onChangeText={this.onSeedWordsChange}
							testID="input-seed-phrase"
							blurOnSubmit
							returnKeyType="next"
							keyboardType={(!hideSeedPhraseInput && Device.isAndroid() && 'visible-password') || 'default'}
							autoCapitalize="none"
							autoCorrect={false}
							onFocus={(!hideSeedPhraseInput && this.seedphraseInputFocused) || null}
							onBlur={(!hideSeedPhraseInput && this.seedphraseInputFocused) || null}
							keyboardAppearance={themeAppearance}
							textAlign={'center'}
							textAlignVertical={'middle'}
						/>
						<TouchableOpacity style={styles.pasteWrapper} onPress={() => this.onPastePress(seed)}>
							<Image source={require('../../../images-new/copy-address.png')} style={{width:20, height: 20}} />
							<Text style={styles.pasteTxt}>{strings('Paste')}</Text>
						</TouchableOpacity>
					</View>					
					{!!error && (
								<View style={[styles.errorPhrase]}>
									<Text style={styles.label}>{error}</Text>
								</View>)
							}
					<View style={styles.ctaWrapper}>
							<TouchableOpacity
								onPress={this.onPressImport}
								testID={'submit'}
								disabled={Boolean(error) || seed === ''}
								style={[Boolean(error) || seed === '' ? styles.disabledButton : styles.continueButton, 
									styles.buttonText, this.state.onClickScale && styles.zoominItem]}
								onPressIn={() => this.setState({onClickScale: true})}
								onPressOut={() => this.setState({onClickScale: false})} 
								activeOpacity={1}
							>
								{loading ? (
									<ActivityIndicator size="small" color={colors.primary.inverse} />
								) : (
									<Text style={[Boolean(error) || seed === '' ? styles.textDisable : styles.textContinue, styles.textWrapper]}>
										{strings('import_from_seed.import_button')}
									</Text>
								)}
							</TouchableOpacity>
							{/* <StyledButton
								type={'blue'}
								onPress={this.onPressImport}
								testID={'submit'}
								disabled={Boolean(error)}
							>
								
							</StyledButton> */}
					</View>
				</SafeAreaView>		
			</>	
		);
	}
}

ImportFromSeed.contextType = ThemeContext;

const mapDispatchToProps = (dispatch) => ({
	setLockTime: (time) => dispatch(setLockTime(time)),
	setOnboardingWizardStep: (step) => dispatch(setOnboardingWizardStep(step)),
	passwordSet: () => dispatch(passwordSet()),
	seedphraseBackedUp: () => dispatch(seedphraseBackedUp()),
	logIn: () => dispatch(logIn()),
});

export default connect(null, mapDispatchToProps)(ImportFromSeed);
