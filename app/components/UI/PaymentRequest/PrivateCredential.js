import React, { PureComponent } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    StyleSheet,
    ActivityIndicator,
    InteractionManager,
    Appearance,
    Image,
    TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fontStyles } from '../../../styles/common';
import StyledButton from '../../UI/StyledButton';
import { strings } from '../../../../locales/i18n';
import FeatherIcons from 'react-native-vector-icons/Feather';
import { BlurView } from '@react-native-community/blur';
import ActionView from '../../UI/ActionView';
import Device from '../../../util/device';
import Engine from '../../../core/Engine';
import PreventScreenshot from '../../../core/PreventScreenshot';
import SecureKeychain from '../../../core/SecureKeychain';
import { getOnboardingNavbarOptions, getSetNameWallet } from '../../UI/Navbar';
import {
    MANUAL_BACKUP_STEPS,
    SEED_PHRASE,
    CONFIRM_PASSWORD,
    WRONG_PASSWORD_ERROR,
} from '../../../constants/onboarding';
// import AnalyticsV2 from '../../../util/analyticsV2';
import { ThemeContext, mockTheme } from '../../../util/theme';
// import ClipboardManager from '../../../core/ClipboardManager';
import Logger from '../../../util/Logger';
// import Button from 'react-native-button';
// import CheckBox from '@react-native-community/checkbox';
import { seedphraseBackedUp } from '../../../actions/user';
import { showAlert } from '../../../actions/alert';
import { isNull } from 'url/util';
import AsyncStorage from '@react-native-community/async-storage';
import { BIOMETRY_CHOICE } from '../../../constants/storage';
import PINCode, { hasUserSetPinCode } from '@haskkor/react-native-pincode';
// import IonicIcon from 'react-native-vector-icons/Ionicons';
// import Identicon from '../../UI/Identicon';
// import EthereumAddress from '../../UI/EthereumAddress';

const createStyles = (colors) =>
    StyleSheet.create({
        // eslint-disable-next-line react-native/no-color-literals
        mainWrapper: {
            backgroundColor: '#1B1B23',
            flex: 1,
        },
        titleWrapper: {
            width: '100%',
            height: 33,
            alignItems: 'center',
            justifyContent: 'center',
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderColor: colors.border.muted,
        },
        btnDone: {
            flexDirection: 'row',
            width: '100%',
        },
        account: {
            width: '100%',
            minHeight: 100,
            alignItems: 'center',
            justifyContent: 'center',
        },
        copyButtonWrap: {
            width: '100%',
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
        },
        dragger: {
            width: 48,
            height: 5,
            borderRadius: 4,
            backgroundColor: colors.border.default,
            opacity: Device.isAndroid() ? 0.6 : 0.5,
        },
        // eslint-disable-next-line react-native/no-color-literals
        wrapper: {
            flex: 1,
            paddingHorizontal: 32,
            backgroundColor: '#1B1B23',
        },
        onBoardingWrapper: {
            paddingHorizontal: 20,
        },
        loader: {
            backgroundColor: colors.background.default,
            flex: 1,
            minHeight: 300,
            justifyContent: 'center',
            alignItems: 'center',
        },
        // eslint-disable-next-line react-native/no-color-literals
        action: {
            flex: 10,
            fontSize: 18,
            marginVertical: 16,
            color: '#FFFFFF',
            justifyContent: 'center',
            textAlign: 'center',
            ...fontStyles.bold,
        },
        infoWrapper: {
            marginBottom: 16,
            justifyContent: 'center',
        },
        info: {
            fontSize: 14,
            color: colors.text.default,
            textAlign: 'center',
            ...fontStyles.normal,
            paddingHorizontal: 6,
        },
        seedPhraseConcealerContainer: {
            position: 'absolute',
            width: '100%',
            height: '100%',
            borderRadius: 8,
        },
        seedPhraseConcealer: {
            position: 'absolute',
            width: '100%',
            height: '100%',
            backgroundColor: colors.overlay.alternative,
            alignItems: 'center',
            borderRadius: 8,
            paddingHorizontal: 24,
            paddingVertical: 45,
        },
        blurView: {
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            borderRadius: 8,
        },
        icon: {
            width: 24,
            height: 24,
            color: colors.overlay.inverse,
            textAlign: 'center',
            marginBottom: 32,
        },
        reveal: {
            fontSize: Device.isMediumDevice() ? 13 : 16,
            ...fontStyles.bold,
            color: colors.overlay.inverse,
            lineHeight: 22,
            marginBottom: 8,
            textAlign: 'center',
        },
        watching: {
            fontSize: Device.isMediumDevice() ? 10 : 12,
            color: colors.overlay.inverse,
            lineHeight: 17,
            marginBottom: 32,
            textAlign: 'center',
        },
        viewButtonContainer: {
            width: 155,
            padding: 12,
        },
        seedPhraseWrapper: {
            borderRadius: 8,
            flexDirection: 'row',
            borderColor: colors.border.default,
            borderWidth: 1,
            marginBottom: 64,
            minHeight: 275,
        },
        wordColumn: {
            flex: 1,
            alignItems: 'center',
            paddingHorizontal: Device.isMediumDevice() ? 18 : 24,
            paddingVertical: 18,
            justifyContent: 'space-between',
        },
        wordWrapper: {
            flexDirection: 'row',
        },
        word: {
            paddingHorizontal: 8,
            paddingVertical: 6,
            fontSize: 14,
            color: '#FFFFFF',
            borderColor: colors.primary.default,
            borderRadius: 13,
            textAlign: 'center',
            textAlignVertical: 'center',
            lineHeight: 14,
            flex: 1,
        },
        confirmPasswordWrapper: {
            flex: 1,
            padding: 30,
            paddingTop: 0,
        },
        passwordRequiredContent: {
            marginBottom: 20,
        },
        content: {
            alignItems: 'flex-start',
        },
        title: {
            fontSize: 32,
            marginTop: 20,
            marginBottom: 10,
            color: colors.text.default,
            justifyContent: 'center',
            textAlign: 'left',
            ...fontStyles.normal,
        },
        text: {
            marginBottom: 10,
            marginTop: 20,
            justifyContent: 'center',
        },
        label: {
            fontSize: 16,
            lineHeight: 23,
            color: colors.text.default,
            textAlign: 'left',
            ...fontStyles.normal,
        },
        buttonWrapper: {
            flex: 1,
            marginTop: 20,
            justifyContent: 'flex-end',
        },
        input: {
            borderWidth: 2,
            borderRadius: 5,
            width: '100%',
            borderColor: colors.border.default,
            padding: 10,
            height: 40,
        },
        warningMessageText: {
            paddingVertical: 10,
            color: '#FFFFFF',
            ...fontStyles.normal,
        },
        keyboardAvoidingView: {
            flex: 1,
            flexDirection: 'row',
            alignSelf: 'center',
        },
        copyAction: {
            flexDirection: 'row',
            marginVertical: 14,
            color: '#B1B5FF',
            textAlign: 'center',
            alignItems: 'center',
            ...fontStyles.normal,
        },
        copyText: {
            color: '#FFFFFF',
            margin: 10,
        },
        saveSeedphrase: {
            flexDirection: 'row',
            alignItems: 'center',
        },
        saveSeedphraseCheckbox: {
            height: 18,
            width: 18,
            marginRight: 12,
            marginTop: 3,
        },
        saveSeedphraseText: {
            color: '#FFFFFF',
        },
        accountInfo: {
            marginLeft: 15,
            marginRight: 0,
            flex: 1,
            flexDirection: 'row',
        },
        accountMain: {
            flex: 1,
            flexDirection: 'column',
        },
        accountNameWrapper: {
            flexDirection: 'row',
            justifyContent: 'space-between',
        },
        accountLabelConfirm: {
            fontSize: 18,
            color: 'white',
            ...fontStyles.normal,
        },
        accountBalanceWrapper: {
            display: 'flex',
            flexDirection: 'row',
        },
        address: {
            fontSize: 10,
            color: '#A0A0B1',
            ...fontStyles.normal,
            letterSpacing: 0.8,
        },
        dots: {
			width: 12,
			height: 12,
			borderRadius: 6,
			backgroundColor: '#1B1B23',
			borderColor: '#FFFFFF',
			borderWidth: 2,
		},
		titlePinCode: {
			fontSize: 24,
			...fontStyles.bold,
			lineHeight: 29,
			color: '#FFFFFF',
			opacity:1
		},
		subtitlePinCode: {
			fontSize: 16,
			...fontStyles.normal,
			lineHeight: 20,
			color: '#58586C',
			opacity:1
		},
		pinCodeTextButtonCircle:{
			fontSize: 20,
			...fontStyles.normal,
			color: '#FFFFFF',
			opacity:1
		},
		pinCodeButtonCircle:{
			backgroundColor: '#1B1B23',
			borderWidth: 1,
			borderColor: '#3D3D4B',
			// width: 72,
			// height: 72,
			borderRadius: 36,
			opacity:1
		},
		pinCodeDeleteButtonText:{
			color:'#FFFFFF', 
			fontSize:16, 
			lineHeight:20, 
			...fontStyles.normal
		}
    });

/**
 * View that's shown during the second step of
 * the backup seed phrase flow
 */
class PrivateCredential extends PureComponent {
    static propTypes = {
        /**
        /* navigation object required to push and pop other views
         */
        navigation: PropTypes.object,
        /**
         * Object that represents the current route info like params passed to it
         */
        route: PropTypes.object,
        /**
         * Theme that app is set to
         */
        appTheme: PropTypes.string,
        seedphraseBackedUp: PropTypes.func,
        showAlert: PropTypes.func,
        selectedAddress: PropTypes.string,
    };

    steps = MANUAL_BACKUP_STEPS;

    state = {
        seedPhraseHidden: true,
        currentStep: 1,
        password: undefined,
        warningIncorrectPassword: undefined,
        ready: false,
        view: SEED_PHRASE,
        skipCheckbox: false,
    };

    async componentDidMount() {
        const biometryType = await SecureKeychain.getSupportedBiometryType();
        const biometryChoice = await AsyncStorage.getItem(BIOMETRY_CHOICE);
        if (isNull(biometryType) || isNull(biometryChoice) || !biometryChoice) {
            this.setState({ view: CONFIRM_PASSWORD, ready: true });
        } else {
            const credentials = await SecureKeychain.getGenericPassword();
            this.privateKey = await this.tryExportPrivateKey(credentials.password);
            try {
                if (credentials) {
                    this.props.onSend()
                    this.props.onBack()
                } else {
                    this.setState({ view: CONFIRM_PASSWORD, ready: true });
                }
            } catch (e) {
                this.setState({ view: CONFIRM_PASSWORD, ready: true });
            }
            this.setState({ ready: true });
            InteractionManager.runAfterInteractions(() => PreventScreenshot.forbid());
        }
    }

    onPasswordChange = (password) => {
        this.setState({ password });
    };

    tryExportSeedPhrase = async (password) => {
        const { KeyringController } = Engine.context;
        const { selectedAddress } = this.props;
        let address = this.props.route?.params?.account.address;
        if (!address) address = selectedAddress;

        let mnemonic = await KeyringController.exportSeedPhrase(password);
        try {
            const accKeyring = await KeyringController.getAccountKeyring(address);
            // Dont know why it has to be like that
            let privKey = accKeyring.hdWallet._hdkey;
            privKey = JSON.parse(JSON.stringify(privKey));
            mnemonic = await KeyringController.exportSeedPhraseByWallet(password, privKey.xpriv);
        } finally {
            // const seed = JSON.stringify(mnemonic).replace(/"/g, '').split(' ');
            // return seed;
        }
        const seed = JSON.stringify(mnemonic).replace(/"/g, '').split(' ');
        return seed;
    };

    tryExportPrivateKey = async (password) => {
        const { KeyringController } = Engine.context;
        const { selectedAddress } = this.props;
        let address = this.props.route?.params?.account.address;
        if (!address) address = selectedAddress;

        const privateKey = await KeyringController.exportAccount(password, address);
        return privateKey;
    };

    tryUnlockWithPassword = async (password) => {
        this.setState({ ready: false });
        try {
            this.words = await this.tryExportSeedPhrase(password);
            this.privateKey = await this.tryExportPrivateKey(password);
            this.setState({ view: SEED_PHRASE, ready: true });
            this.props.onSend()
            this.props.onBack()
        } catch (e) {
            let msg = strings('reveal_credential.warning_incorrect_password');
            if (e.toString().toLowerCase() !== WRONG_PASSWORD_ERROR.toLowerCase()) {
                msg = strings('reveal_credential.unknown_error');
            }
            this.setState({
                warningIncorrectPassword: msg,
                ready: true,
            });
        }
    };

    tryUnlock = async (pin) => {
        //const { password } = this.state;
        const hasPin = await hasUserSetPinCode();
        if (hasPin) {
            this.tryUnlockWithPassword(pin);
        }
    };

    renderLoader = () => {
        const colors = this.context.colors || mockTheme.colors;
        const styles = createStyles(colors);

        return (
            <View style={styles.loader}>
                <ActivityIndicator size="small" />
            </View>
        );
    };

    renderConfirmPassword() {
        const { warningIncorrectPassword } = this.state;
        const colors = this.context.colors || mockTheme.colors;
        const themeAppearance = this.context.themeAppearance || 'light';
        const styles = createStyles(colors);

        return (
            // <PINCode
            //     titleEnter={'Nhập mã PIN'}
            //     subtitleEnter={'Mã gồm 6 chữ số'}
            //     titleAttemptFailed={'Mã PIN không khớp'}
            //     subtitleError={'Vui lòng thử lại'}
            //     passwordLength={6}
            //     stylePinCodeCircle={styles.dots}
            //     disableLockScreen
            //     status={'enter'}
            //     touchIDDisabled
            //     finishProcess={(pin) => this.tryUnlock(pin)}
            // />
            <PINCode
                titleChoose={strings('pin_code.title_choose')}
                subtitleChoose={strings('pin_code.subtitle')}
                titleConfirm={strings('pin_code.title_confirm')}
                subtitleConfirm={strings('pin_code.subtitle_confirm')}
                titleConfirmFailed={strings('pin_code.title_confirm_failed')}
                subtitleError={strings('pin_code.subtitl_error')}
                buttonDeleteText={strings('pin_code.button_delete_text')}
                passwordLength={6}
                disableLockScreen
                status={'enter'}
                touchIDDisabled
                finishProcess={(pin) => this.tryUnlock(pin)}
                stylePinCodeTextTitle={styles.titlePinCode}
                stylePinCodeTextSubtitle={styles.subtitlePinCode}
                stylePinCodeCircle={styles.dots}
                stylePinCodeTextButtonCircle={styles.pinCodeTextButtonCircle}
                stylePinCodeButtonCircle={styles.pinCodeButtonCircle}
                stylePinCodeDeleteButtonText={styles.pinCodeDeleteButtonText}					
            />
        );
    }

    // goBack = () => {
    //     this.props.navigation.navigate('RevealAccountList');
    // };

    render() {
        const { ready, currentStep, view } = this.state;
        const colors = this.context.colors || mockTheme.colors;
        const styles = createStyles(colors);

        if (!ready) return this.renderLoader();
        return (
            <View style={styles.mainWrapper}>
                {/* <View style={styles.titleWrapper}>
                    <View style={styles.dragger} testID={'account-list-dragger'} />
                </View> */}
                {this.renderConfirmPassword()}
            </View>
        );
    }
}

const propTypes = {
    toggleSaveSeedphraseCheckbox: PropTypes.func,
    saveSeedphrase: PropTypes.bool,
};

const mapStateToProps = (state) => ({
    appTheme: state.user.appTheme,
    selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
});

const mapDispatchToProps = (dispatch) => ({
    seedphraseBackedUp: () => dispatch(seedphraseBackedUp()),
    showAlert: (config) => dispatch(showAlert(config)),
});

PrivateCredential.contextType = ThemeContext;

export default connect(mapStateToProps, mapDispatchToProps)(PrivateCredential);
