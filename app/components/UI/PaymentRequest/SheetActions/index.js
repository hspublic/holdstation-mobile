import React, { useState } from 'react';
import {
    Pressable, StyleSheet, Text, View, Dimensions,
    Image, TextInput, KeyboardAvoidingView, Keyboard
} from 'react-native';
import Modal from 'react-native-modal';
import { fontStyles, baseStyles } from '../../../../styles/common';
import Device from '../../../../util/device';
import WalletModal from '../../../UI/WalletModal';
import { strings } from '../../../../../locales/i18n';
import GasFeeInput from '../GasFeeInput'
import BigNumber from 'bignumber.js';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const KEYBOARD_OFFSET = 120;

const gasInfo = [{
    title: "Phí gas hiện tại",
    content: "Phí gas hiện tại là mức phí tối thiểu để giao dịch của bạn được ghi lại trên Ethereum. Mức phí thay đổi phụ thuộc vào mật độ giao dịch từng thời điểm.",
},
{
    title: "Giới hạn gas",
    content: "Giới hạn gas là số đơn vị gas tối đa mà bạn sẵn sàng sử dụng. Kết quả của giới hạn gas nhân với giá gas (gồm phí cơ bản và phí ưu tiên)‘ là phí giao dịch tối đa bạn phải trả.",
},
{
    title: "Phí gas",
    content: "Phí gas là mức phí cao nhất bạn sẵn sàng trả cho giao dịch. Đặt mức phí cao giúp giao dịch của bạn không bị kẹt lại trong trường hợp phí cơ bản tăng lên.",
},
{
    title: "Phí ưu tiên",
    content: "Phí ưu tiên sẽ được trả cho người khai thác, người trực tiếp xác nhận giao dịch của bạn. Mức phí cao hơn sẽ đảm bảo giao dịch của bạn được xác nhận nhanh chóng."
}]

const optionText = ["Chuẩn", "Nhanh", "Nhanh hơn", "Tuỳ chỉnh"]


const GAS_LIMIT_INCREMENT = new BigNumber(1000);
const GAS_INCREMENT = new BigNumber(1);
const GAS_LIMIT_MIN = new BigNumber(21000);
const GAS_MIN = new BigNumber(0);

function SheetActions({ isVisible, closeModal, gasFeeOptions, gasOptions, updateOption, warningMinimumEstimateOption, onCalculateGasFee, transactionGasFee, onSaveCustom }) {
    const [gasIndex, selectGasIndex] = useState(3)
    const [showGasInfo, setShowGasInfo] = useState(0)
    const [gasCustomFee, setGasCustomFee] = useState(gasFeeOptions[2]) // Option gas tuỳ chọn, mặc định là Option nhanh nhất

    const [customWarning, showCustomWarning] = useState(false)

    const [maxPriorityFeeError, setMaxPriorityFeeError] = useState(null);
    const [maxFeeError, setMaxFeeError] = useState(null);

    function handleUnhandledTouches() {
        Keyboard.dismiss();
        return false;
    }
    // Option phí gas hiện tại
    var feeGas = gasFeeOptions[gasIndex] // .concat([gasCustomFee])[gasIndex]  // gasFeeOptions[gasIndex === 3 ? 2 : gasIndex]
    if (gasIndex === 3) {
        var feeGas = transactionGasFee || gasCustomFee
    }

    const onCalculateGas = (newGas) => {
        setGasCustomFee(newGas);
        onCalculateGasFee(newGas)
    }

    const changedGasLimit = (value) => {
        // Nếu gasIndex !== 3 thì chuyển sang trạng thái tuỳ chỉnh
        const newGas = { ...feeGas, suggestedGasLimit: value };
        onCalculateGas(newGas)
        if (gasIndex !== 3) {
            // Đặt lại trạng thái tuỳ chỉnh gas
            selectGasIndex(3)
        }
    }

    const changedMaxPriorityFee = (value) => {

        const lowerValue = new BigNumber(gasOptions?.[warningMinimumEstimateOption]?.suggestedMaxPriorityFeePerGas);
        const higherValue = new BigNumber(gasOptions?.high?.suggestedMaxPriorityFeePerGas).multipliedBy(
            new BigNumber(1.5)
        );
        const updateFloor = new BigNumber(updateOption?.maxPriortyFeeThreshold);

        const valueBN = new BigNumber(value);

        if (updateFloor && !updateFloor.isNaN() && valueBN.lt(updateFloor)) {
            setMaxPriorityFeeError(
                updateOption?.isCancel
                    ? strings('edit_gas_fee_eip1559.max_priority_fee_cancel_low', {
                        cancel_value: updateFloor,
                    })
                    : strings('edit_gas_fee_eip1559.max_priority_fee_speed_up_low', {
                        speed_up_floor_value: updateFloor,
                    })
            );
        } else if (!lowerValue.isNaN() && valueBN.lt(lowerValue)) {
            setMaxPriorityFeeError(strings('edit_gas_fee_eip1559.max_priority_fee_low'));
        } else if (!higherValue.isNaN() && valueBN.gt(higherValue)) {
            setMaxPriorityFeeError(strings('edit_gas_fee_eip1559.max_priority_fee_high'));
        } else {
            setMaxPriorityFeeError('');
        }

        const newGas = { ...feeGas, suggestedMaxPriorityFeePerGas: value };
        onCalculateGas(newGas)
        if (gasIndex !== 3) {
            // Đặt lại trạng thái tuỳ chỉnh gas
            selectGasIndex(3)
        }
    }

    const changedMaxFeePerGas = (value) => {
        const lowerValue = new BigNumber(gasOptions?.[warningMinimumEstimateOption]?.suggestedMaxFeePerGas);
        const higherValue = new BigNumber(gasOptions?.high?.suggestedMaxFeePerGas).multipliedBy(new BigNumber(1.5));
        const updateFloor = new BigNumber(updateOption?.maxFeeThreshold);

        const valueBN = new BigNumber(value);

        if (updateFloor && !updateFloor.isNaN() && valueBN.lt(updateFloor)) {
            setMaxFeeError(
                updateOption?.isCancel
                    ? strings('edit_gas_fee_eip1559.max_fee_cancel_low', {
                        cancel_value: updateFloor,
                    })
                    : strings('edit_gas_fee_eip1559.max_fee_speed_up_low', {
                        speed_up_floor_value: updateFloor,
                    })
            );
        } else if (!lowerValue.isNaN() && valueBN.lt(lowerValue)) {
            setMaxFeeError(strings('edit_gas_fee_eip1559.max_fee_low'));
        } else if (!higherValue.isNaN() && valueBN.gt(higherValue)) {
            setMaxFeeError(strings('edit_gas_fee_eip1559.max_fee_high'));
        } else {
            setMaxFeeError('');
        }

        const newGas = { ...feeGas, suggestedMaxFeePerGas: value };
        onCalculateGas(newGas)
        if (gasIndex !== 3) {
            // Đặt lại trạng thái tuỳ chỉnh gas
            selectGasIndex(3)
        }
    }
    return (
        <Modal
            hideModalContentWhileAnimating={true}
            useNativeDriver={true}
            isVisible={isVisible}
            animationIn={'fadeInUp'}
            animationOut={'fadeOutDown'}
            animationInTiming={500}
            animationOutTiming={500}
            onBackdropPress={() => closeModal(false)}
            onBackButtonPress={() => closeModal(false)}
            backdropOpacity={0.5}
            statusBarTranslucent
            deviceHeight={screenHeight}
            style={{
                flex: 1,
                margin: 0,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
            <KeyboardAvoidingView
                behavior={'padding'}
                // keyboardVerticalOffset={KEYBOARD_OFFSET}
                enabled={Device.isIos()}>
                <View style={{ width: screenWidth }} onStartShouldSetResponder={handleUnhandledTouches}>
                    <View style={{
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15,
                        backgroundColor: '#1B1B23',
                        paddingBottom: 30
                    }}>
                        <Pressable style={styles.view}>
                            <Text style={styles.title}>{'Tuỳ chỉnh phí gas'}</Text>
                        </Pressable>

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                            marginHorizontal: 15,
                            marginTop: 10
                        }}>
                            {
                                optionText.map((item, index) => {
                                    return <Pressable onPress={() => {
                                        selectGasIndex(index)
                                    }} key={`ii${index}`} style={[styles.btnStyle,
                                    { backgroundColor: index === gasIndex ? '#6A45FF' : 'transparent' }]}>
                                        <Text style={styles.content}>
                                            {item}
                                        </Text>
                                    </Pressable>
                                })
                            }
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 20 }}>
                            <Pressable style={{ flexDirection: 'row', alignItems: 'center' }}
                                onPress={() => setShowGasInfo(1)}>
                                <Text style={styles.fee}>{'Phí gas hiện tại'}</Text>
                                <Image source={require('../../../../images/info.png')} resizeMode={"contain"} style={styles.iconInfo}></Image>
                            </Pressable>
                            <Text style={styles.fee}>{`${Number(feeGas?.estimatedBaseFee).toFixed(5)} Gwei`}</Text>
                        </View>

                        <View style={styles.line} />
                        <GasFeeInput
                            setShowGasInfo={() => setShowGasInfo(2)}
                            style={[styles.input, { textAlign: 'center', marginRight: 0 }]}
                            value={feeGas?.suggestedGasLimit}
                            name={'Giới hạn gas'}
                            unit={'Gwei'}
                            changeLimit={true}
                            min={GAS_LIMIT_MIN}
                            increment={1000}
                            error={false} // maxFeeError
                            onChangeValue={changedGasLimit}
                            color={'#D94343'}

                        // onChangeValue={changedMaxFeePerGas}
                        // inputInsideLabel={maxFeePerGasPrimary && `≈ ${maxFeePerGasPrimary}`}
                        />

                        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 20 }}>
                            <View>
                                <Pressable style={{ flexDirection: 'row', alignItems: 'center' }}
                                    onPress={() => setShowGasInfo(2)}>
                                    <Text style={styles.fee}>{'Giới hạn gas'}</Text>
                                    <Image source={require('../../../../images/info.png')} resizeMode={"contain"} style={styles.iconInfo}></Image>
                                </Pressable>
                                <Text style={[styles.fee, { color: '#D94343' }]}>{'Phải lớn hơn 21000'}</Text>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Pressable hitSlop={10}>
                                    <Image source={require('../../../../images/minus.png')} resizeMode={"contain"} style={styles.iconMinus}></Image>
                                </Pressable>
                                <View style={styles.inputContainer}>
                                    <TextInput
                                        style={[styles.input, { textAlign: 'center', marginRight: 0 }]}
                                        value={feeGas?.suggestedGasLimit}
                                        keyboardType="numeric"
                                    />
                                </View>
                                <Pressable hitSlop={10}>
                                    <Image source={require('../../../../images/add.png')} resizeMode={"contain"} style={styles.iconAdd}></Image>
                                </Pressable>
                            </View>
                        </View> */}


                        <GasFeeInput
                            setShowGasInfo={() => setShowGasInfo(3)}
                            style={styles.input}
                            value={feeGas?.suggestedMaxFeePerGas}
                            name={'Phí gas'}
                            unit={'Gwei'}
                            min={GAS_MIN}
                            increment={3}
                            error={gasIndex === 3 ? maxFeeError : null} // maxFeeError // Phí thấp so với thị trường
                            onChangeValue={changedMaxFeePerGas}
                            color={'#EDA045'}
                        // onChangeValue={changedMaxFeePerGas}
                        // inputInsideLabel={maxFeePerGasPrimary && `≈ ${maxFeePerGasPrimary}`}
                        />
                        {/* 
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 20 }}>
                            <View>
                                <Pressable style={{ flexDirection: 'row', alignItems: 'center' }}
                                    onPress={() => setShowGasInfo(3)}>
                                    <Text style={styles.fee}>{'Phí gas'}</Text>
                                    <Image source={require('../../../../images/info.png')} resizeMode={"contain"} style={[styles.iconInfo, { tintColor: '#EDA045' }]}></Image>
                                </Pressable>
                                <Text style={[styles.fee, { color: '#EDA045' }]}>{'Phí thấp so với thị trường'}</Text>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Pressable hitSlop={10}>
                                    <Image source={require('../../../../images/minus.png')} resizeMode={"contain"} style={styles.iconMinus}></Image>
                                </Pressable>
                                <View style={styles.inputContainer}>
                                    <TextInput
                                        style={styles.input}
                                        value={feeGas?.suggestedMaxFeePerGas}
                                        keyboardType="numeric"
                                    />
                                    <Text style={styles.unit}>
                                        {"Gwei"}
                                    </Text>
                                </View>
                                <Pressable hitSlop={10}>
                                    <Image source={require('../../../../images/add.png')} resizeMode={"contain"} style={styles.iconAdd}></Image>
                                </Pressable>
                            </View>
                        </View> */}

                        <GasFeeInput
                            setShowGasInfo={() => setShowGasInfo(4)}
                            style={styles.input}
                            value={feeGas?.suggestedMaxPriorityFeePerGas}
                            name={'Phí ưu tiên'}
                            unit={'Gwei'}
                            min={GAS_MIN}
                            increment={1}
                            error={gasIndex === 3 ? maxPriorityFeeError : null} // maxFeeError // 'Phí cao so với thị trường'
                            onChangeValue={changedMaxPriorityFee}
                            color={'#EDA045'}
                        // onChangeValue={changedMaxFeePerGas}
                        // inputInsideLabel={maxFeePerGasPrimary && `≈ ${maxFeePerGasPrimary}`}
                        />

                        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 20 }}>
                            <Pressable style={{ flexDirection: 'row', alignItems: 'center' }}
                                onPress={() => setShowGasInfo(4)}>
                                <Text style={styles.fee}>{'Phí ưu tiên'}</Text>
                                <Image source={require('../../../../images/info.png')} resizeMode={"contain"} style={styles.iconInfo}></Image>
                            </Pressable>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Pressable hitSlop={10}>
                                    <Image source={require('../../../../images/minus.png')} resizeMode={"contain"} style={styles.iconMinus}></Image>
                                </Pressable>
                                <View style={styles.inputContainer}>
                                    <TextInput
                                        style={styles.input}
                                        // style={styles.input(Boolean(error))}
                                        // onChangeText={changeValue}
                                        // onBlur={checkLimits}
                                        value={feeGas?.suggestedMaxPriorityFeePerGas}
                                        keyboardType="numeric"
                                    // ref={textInput}
                                    // keyboardAppearance={themeAppearance}
                                    />
                                    <Text style={styles.unit}>
                                        {"Gwei"}
                                    </Text>
                                </View>
                                <Pressable hitSlop={10}>
                                    <Image source={require('../../../../images/add.png')} resizeMode={"contain"} style={styles.iconAdd}></Image>
                                </Pressable>
                            </View>
                        </View> */}

                        <View style={styles.line} />

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 20 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={styles.fee}>{'Phí giao dịch tối đa'}</Text>
                            </View>
                            <Text style={[styles.fee, { color: '#2F80ED' }]}>{`${feeGas?.renderableGasFeeMaxNative} ($${feeGas?.gasFeeMaxConversion})`}</Text>
                        </View>

                        <Pressable
                            style={[styles.saveStyle, { backgroundColor: '#6A45FF' }]}
                            // disabled={Boolean(error)}
                            onPress={() => onSaveCustom(feeGas)}>
                            <Text style={styles.save}>
                                {'Lưu'}
                            </Text>
                        </Pressable>
                    </View>
                </View>
            </KeyboardAvoidingView>
            <WalletModal
                warningModalVisible={customWarning}
                onCancelPress={() => showCustomWarning(false)}
                displayConfirmButton={false}
                displayCancelButton={false}
                confirmText={strings('accounts.ok')}
                cancelText={strings('accounts.cancel')}>
                <View style={{
                    flex: 1,
                    padding: 25
                }}>
                    <View style={{
                        alignItems: 'center'
                    }}>
                        <Image source={require('../../../../images/info.png')} resizeMode={"contain"} style={styles.iconWarning}></Image>
                    </View>
                    <Text style={styles.titleWarning}>
                        {'Phí gas quá thấp, giao dịch của bạn có thể bị chậm'}</Text>
                    <Text style={[styles.titleWarning, { fontWeight: '100', fontSize: 16 }]}>
                        {'Hãy điều chỉnh phí gas cao hơn để tránh lỗi xảy ra'}</Text>
                    <Pressable style={[styles.btnWarning, { backgroundColor: '#6A45FF' }]} onPress={() => showCustomWarning(false)}>
                        <Text style={styles.contentBtnWarning}>
                            {'Điều chỉnh phí gas'}
                        </Text>
                    </Pressable>
                    <Pressable style={styles.btnWarning} onPress={() => showCustomWarning(false)}>
                        <Text style={styles.contentBtnWarning}>
                            {'Tôi đã hiểu và vẫn muốn thực hiện'}
                        </Text>
                    </Pressable>
                </View>
            </WalletModal>

            <WalletModal
                warningModalVisible={showGasInfo !== 0}
                onCancelPress={() => showCustomWarning(false)}
                displayConfirmButton={false}
                displayCancelButton={false}
                confirmText={strings('accounts.ok')}
                cancelText={strings('accounts.cancel')}>
                <View style={{
                    flex: 1,
                    padding: 25
                }}>
                    <Text style={styles.titleWarning}>
                        {gasInfo[showGasInfo ? showGasInfo - 1 : 0].title}</Text>
                    <Text style={[styles.titleWarning, { fontWeight: '100', fontSize: 16 }]}>
                        {gasInfo[showGasInfo ? showGasInfo - 1 : 0].content}</Text>
                    <Pressable style={[styles.btnWarning, { backgroundColor: '#6A45FF' }]} onPress={() => setShowGasInfo(0)}>
                        <Text style={styles.contentBtnWarning}>
                            {'Ok'}
                        </Text>
                    </Pressable>
                </View>
            </WalletModal>
        </Modal>
    );
}

const styles = StyleSheet.create({
    view: {
        paddingVertical: 12,
        alignItems: 'center',
    },
    line: {
        height: 1,
        backgroundColor: 'rgba(255, 255, 255, 0.15)',
        margin: 20,
        marginBottom: 0
    },
    title: {
        ...fontStyles.normal,
        fontSize: 18,
        color: '#FFFFFF',
        marginTop: 5,
        fontWeight: '500'
    },
    content: {
        ...fontStyles.normal,
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: '500',
        padding: 10
    },
    btnStyle: {
        borderColor: 'rgba(255, 255, 255, 0.1)',
        borderWidth: 1,
        borderRadius: 16,
    },
    feeEstimate: {
        fontSize: 14,
        color: '#A0A0B1',
        marginTop: 5,
        fontWeight: '500'
    },
    fee: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500'
    },
    iconSetting: {
        width: 30,
        height: 30,
        tintColor: '#6A45FF' // #58586C
    },
    iconCheck: {
        marginRight: 15,
        width: 25,
        height: 25,
        tintColor: '#3D3D4B', // '#2BEB4A'
    },
    iconInfo: {
        width: 20,
        height: 20,
        marginTop: 2,
        marginLeft: 3 // #EDA045
    },
    iconAdd: {
        width: 25,
        height: 25,
    },
    iconMinus: {
        width: 25,
        height: 25,
    },
    input: {
        ...fontStyles.normal,
        fontSize: 15,
        color: '#FFFFFF',
        paddingVertical: 10,
        marginRight: 5,
        maxWidth: 60,
        textAlign: 'right'
    },
    unit: {
        ...fontStyles.normal,
        fontSize: 15,
        color: '#FFFFFF',
        textAlign: 'center',
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'rgba(255, 255, 255, 0.1)',
        borderWidth: 1,
        borderRadius: 10,
        marginHorizontal: 5,
        paddingHorizontal: 10,
        width: 120,
    },
    saveStyle: {
        backgroundColor: '#3D3D4B',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
        marginBottom: 0,
        padding: 15
    },
    save: {
        ...fontStyles.normal,
        fontSize: 18,
        color: '#FFFFFF',
        fontWeight: '500'
    },
    titleWarning: {
        ...fontStyles.normal,
        fontSize: 18,
        color: '#FFFFFF',
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 15
    },
    btnWarning: {
        backgroundColor: '#3D3D4B',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        marginTop: 15
    },
    contentBtnWarning: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
    },
    iconWarning: {
        width: 30,
        height: 30,
        tintColor: '#EDA045'
    },
});
export default SheetActions;
