import React, { useCallback, useRef, useEffect, useState } from 'react';
import { View, TextInput, StyleSheet, Pressable, Image, Text } from 'react-native';
// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import Text from './Text';
// import PropTypes from 'prop-types';
import BigNumber from 'bignumber.js';
import { useAppThemeFromContext, mockTheme } from '../../../../util//theme';
import { fontStyles, baseStyles } from '../../../../styles/common';

const createStyles = (colors) =>
    StyleSheet.create({
        inputContainer: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            borderColor: 'rgba(255, 255, 255, 0.1)',
            borderWidth: 1,
            borderRadius: 10,
            marginHorizontal: 5,
            paddingHorizontal: 10,
            width: 120,
        },
        iconAdd: {
            width: 25,
            height: 25,
        },
        iconMinus: {
            width: 25,
            height: 25,
        },
        fee: {
            ...fontStyles.normal,
            fontSize: 15,
            color: '#FFFFFF',
            fontWeight: '500'
        },
        iconInfo: {
            width: 20,
            height: 20,
            marginTop: 2,
            marginLeft: 3 // #EDA045
        },
        unit: {
            ...fontStyles.normal,
            fontSize: 15,
            color: '#FFFFFF',
            textAlign: 'center',
        },
    });

const GasFeeInput = ({
    value,
    unit,
    increment,
    onChangeValue,
    min,
    max,
    name,
    setShowGasInfo,
    error,
    style,
    changeLimit,
    color
}) => {
    const textInput = useRef(null);
    const [errorState, setErrorState] = useState();
    const { colors, themeAppearance } = useAppThemeFromContext() || mockTheme;
    const styles = createStyles(colors);

    const handleClickUnit = useCallback(() => {
        textInput?.current?.focus?.();
    }, []);

    const changeValue = useCallback(
        (newValue, dontEmptyError) => {
            if (!dontEmptyError) setErrorState('');
            const cleanValue = newValue?.replace?.(',', '.');
            if (cleanValue && new BigNumber(cleanValue).isNaN()) {
                setErrorState(`${name} phải là một số`);
                return;
            }

            onChangeValue?.(cleanValue);
        },
        [name, onChangeValue]
    );

    const increaseNumber = useCallback(() => {
        const newValue = new BigNumber(value).plus(new BigNumber(increment));
        if (!new BigNumber(max).isNaN() && newValue.gt(max)) return;
        changeValue(newValue.toString());
    }, [changeValue, increment, max, value]);

    const decreaseNumber = useCallback(() => {
        const newValue = new BigNumber(value).minus(new BigNumber(increment));
        if (!new BigNumber(min).isNaN() && newValue.lt(min)) return;
        changeValue(newValue.toString());
    }, [changeValue, increment, min, value]);

    // const renderLabelComponent = useCallback((component) => {
    //     if (!component) return null;
    //     if (typeof component === 'string')
    //         return (
    //             <Text noMargin black bold>
    //                 {component}
    //             </Text>
    //         );
    //     return component;
    // }, []);

    const checkLimits = useCallback(() => {
        if (new BigNumber(value || 0).lt(min)) {
            setErrorState(`${name} phải lớn hơn ${min}`);
            return changeValue(min.toString(), true);
        }
        if (new BigNumber(value || 0).gt(max)) {
            setErrorState(`${name} giới hạn ${max}`);
            return changeValue(max.toString());
        }
    }, [changeValue, max, min, name, value]);

    useEffect(() => {
        if (textInput?.current?.isFocused?.()) return;
        checkLimits();
    }, [checkLimits]);

    const hasError = Boolean(error) || Boolean(errorState);

    return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 20 }}>
            <View style={{ justifyContent: 'center' }}>
                <Pressable style={{ flexDirection: 'row', alignItems: 'center' }}
                    onPress={setShowGasInfo}
                >
                    <Text style={styles.fee}>{name}</Text>
                    <Image source={require('../../../../images/info.png')} resizeMode={"contain"} style={styles.iconInfo}></Image>
                </Pressable>
                {
                    hasError && <Text style={[styles.fee, { color: color, width: 150 }]}>{error || errorState}</Text>
                }
                {/* {error || errorState}
                {
                    error !== "" && error !== null && <Text style={[styles.fee, { color: color, width: 150 }]}>{error || errorState}</Text>
                } */}
            </View>
            {/* <Text style={styles.fee}>{'20 Gwei'}</Text> */}
            <View style={{
                flexDirection: 'row',
                alignItems: 'center'
            }}>
                <Pressable hitSlop={10} onPress={decreaseNumber}>
                    <Image source={require('../../../../images/minus.png')} resizeMode={"contain"} style={styles.iconMinus}></Image>
                </Pressable>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={style}
                        onChangeText={changeValue}
                        onBlur={checkLimits}
                        value={value}
                        keyboardType="numeric"
                        ref={textInput}
                        keyboardAppearance={themeAppearance}
                    />
                    {
                        !changeLimit && <Text onPress={handleClickUnit} style={styles.unit}>
                            {unit}
                        </Text>
                    }
                </View>
                {/* <TextInput
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    keyboardType="numeric"
                                    numberOfLines={1}
                                    // onChangeText={this.updateAmount}
                                    // placeholder={`Nhập số ${symbol}`}
                                    placeholderTextColor={'#58586C'}
                                    spellCheck={false}
                                    style={styles.input}
                                    value={feeGas?.suggestedGasLimit}
                                // onSubmitEditing={this.onNext}
                                // autoFocus
                                // ref={this.amountInput}
                                // testID={'request-amount-input'}
                                // keyboardAppearance={themeAppearance}
                                /> */}
                <Pressable hitSlop={10} onPress={increaseNumber}>
                    <Image source={require('../../../../images/add.png')} resizeMode={"contain"} style={styles.iconAdd}></Image>
                </Pressable>
            </View>
        </View>
        // <View>
        // 	<View style={styles.labelContainer}>
        // 		{renderLabelComponent(leftLabelComponent)}
        // 		{renderLabelComponent(rightLabelComponent)}
        // 	</View>

        // 	<View style={styles.rangeInputContainer(Boolean(error))}>
        // 		<View style={styles.buttonContainerLeft}>
        // 			<TouchableOpacity style={styles.button} hitSlop={styles.hitSlop} onPress={decreaseNumber}>
        // 				<FontAwesomeIcon name="minus" size={10} style={styles.buttonText} />
        // 			</TouchableOpacity>
        // 		</View>
        // 		<View style={styles.inputContainer}>
        // 			<TextInput
        // 				style={styles.input(Boolean(error))}
        // 				onChangeText={changeValue}
        // 				onBlur={checkLimits}
        // 				value={value}
        // 				keyboardType="numeric"
        // 				ref={textInput}
        // 				keyboardAppearance={themeAppearance}
        // 			/>
        // 			{!!unit && (
        // 				<Text onPress={handleClickUnit} black={!error} red={Boolean(error)}>
        // 					{unit}
        // 				</Text>
        // 			)}
        // 		</View>
        // 		<View style={styles.buttonContainerRight}>
        // 			<Text style={styles.conversionEstimation} adjustsFontSizeToFit numberOfLines={2}>
        // 				{inputInsideLabel}
        // 			</Text>
        // 			<TouchableOpacity style={styles.button} hitSlop={styles.hitSlop} onPress={increaseNumber}>
        // 				<FontAwesomeIcon name="plus" size={10} style={styles.buttonText} />
        // 			</TouchableOpacity>
        // 		</View>
        // 	</View>
        // 	{hasError && (
        // 		<View style={styles.errorContainer}>
        // 			<FontAwesomeIcon name="exclamation-circle" size={14} style={styles.errorIcon} />
        // 			<Text red noMargin small style={styles.errorText}>
        // 				{error || errorState}
        // 			</Text>
        // 		</View>
        // 	)}
        // </View>
    );
};

GasFeeInput.defaultProps = {
    increment: new BigNumber(1),
};

export default GasFeeInput;
