import React, { useCallback, useMemo } from 'react';
import { Text, View, StyleSheet, Pressable, Image } from 'react-native';
import StyledButton from '../../StyledButton';
import { fontStyles } from '../../../../styles/common';
import Identicon from '../../Identicon';
import NetworkMainAssetLogo from '../../NetworkMainAssetLogo';
import AssetIcon from '../../AssetIcon';
import { useSelector } from 'react-redux';
import { getTokenList } from '../../../../reducers/tokens';
import { toChecksumAddress } from 'ethereumjs-util';
import { useAppThemeFromContext, mockTheme } from '../../../../util/theme';
import {
	renderFromTokenMinimalUnit,
	balanceToFiat,
	renderFromWei,
	weiToFiat,
	hexToBN
} from '../../../../util/number';
import { safeToChecksumAddress } from '../../../../util/address';
import { collectibleContractsSelector, collectiblesSelector } from '../../../../reducers/collectibles';
import { setSelectedAsset, prepareTransaction, setTransactionObject } from '../../../../actions/transaction';
import { connect } from 'react-redux';
import TokenIcon from '../../../../components/UI/Swaps/components/TokenIcon';

const createStyles = (colors) =>
	StyleSheet.create({
		item: {
			// borderWidth: 1,
			// borderColor: colors.border.default,
			padding: 8,
			marginBottom: 8,
			borderRadius: 8,
		},
		assetListElement: {
			flex: 1,
			flexDirection: 'row',
			justifyContent: 'space-between',
			marginVertical: 10,
			alignItems: 'center',
		},
		assetNameElement: {
			flexDirection: 'row',
			marginVertical: 4
		},
		text: {
			...(fontStyles.normal),
			color: '#828282',
			fontSize: 18
		},
		textSymbol: {
			...fontStyles.normal,
			marginLeft: 5,
			fontSize: 16,
			color: '#FFFFFF',
			fontWeight: "500",
			// width: 200
		},
		balance: {
			...fontStyles.normal,
			marginLeft: 5,
			fontSize: 18,
			width: 200,
			color: '#828282',
			marginTop: 5
		},
		assetInfo: {
			// flex: 1,
			alignSelf: 'center',
			justifyContent: 'center',
		},
		assetIcon: {
			marginRight: 12,
		},
		ethLogo: {
			width: 35,
			height: 35,
		},
		checkedIcon: {
			width: 15,
			height: 15,
			marginTop: 0,
			marginLeft: 6
		},
	});

// interface Props {
// 	/**
// 	 * Array of assets objects returned from the search
// 	 */
// 	searchResults: any;
// 	/**
// 	 * Callback triggered when a token is selected
// 	 */
// 	handleSelectAsset: any;
// 	/**
// 	 * Message string to display when searchResults is empty
// 	 */
// 	emptyMessage: string;
// }

const AssetList = ({
	searchResults, handleSelectAsset, emptyMessage,
	accounts, selectedAddress, conversionRate,
	currentCurrency, contractBalances, contractExchangeRates
}) => {
	// const tokenList = useSelector(getTokenList);
	const { colors } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);
	/**
	 * Render logo according to asset. Could be ETH, Identicon or contractMap logo
	 *
	 * @param {object} asset - Asset to generate the logo to render
	 */
	// const renderLogo = (asset) => {
	// 	const { address, isETH, symbol } = asset;
	// 	if (isETH) {
	// 		return <NetworkMainAssetLogo style={styles.ethLogo} />; // big
	// 		// return <TokenIcon medium icon={asset.iconUrl} symbol={symbol} />
	// 	}
	// 	const token = tokenList?.[toChecksumAddress(address)] || tokenList?.[address.toLowerCase()];
	// 	const iconUrl = token?.iconUrl;
	// 	if (!iconUrl) {
	// 		return <Identicon address={address} customStyle={styles.ethLogo} />;
	// 	}
	// 	return <AssetIcon logo={iconUrl} customStyle={styles.ethLogo} />;
	// }

	renderLogo = (asset) => {
		const { address, isETH, iconUrl, thumbnails } = asset;
		if (isETH) {
			return <NetworkMainAssetLogo url={thumbnails || iconUrl} big style={styles.ethLogo} />;
		}
		if (!iconUrl && !thumbnails) {
			return <Identicon address={address} customStyle={styles.ethLogo} />;
		}
		return <AssetIcon logo={thumbnails || iconUrl} customStyle={styles.ethLogo} />;
	}

	// const RenderBalance = ({ secondaryBalance, balanceError }) => {
	// 	const styles = StyleSheet.create({
	// 		balanceFiat: {
	// 			fontSize: 12,
	// 			color: '#FFFFFF',
	// 			...fontStyles.normal,
	// 			textTransform: 'uppercase',
	// 		},
	// 		balanceFiatTokenError: {
	// 			textTransform: 'capitalize',
	// 		},
	// 	})
	// 	return useMemo(() =>
	// 		secondaryBalance ? <Text style={[styles.balanceFiat, balanceError && styles.balanceFiatTokenError]}>
	// 			{secondaryBalance}
	// 		</Text> : <Text style={[styles.balanceFiat, balanceError && styles.balanceFiatTokenError]}>
	// 			{'$0.00'}
	// 		</Text>
	// 		, [secondaryBalance, balanceError])
	// }

	return (
		<View>
			{searchResults.map((_, i) => {
				const { iconUrl, name, address, decimals, symbol, isETH, verified } = searchResults[i] || {};
				let balance, balanceFiat;
				if (isETH) {
					balance = renderFromWei(accounts[selectedAddress].balance);
					balanceFiat = weiToFiat(hexToBN(accounts[selectedAddress].balance), conversionRate, currentCurrency);
				} else {
					balance = renderFromTokenMinimalUnit(contractBalances[address], decimals);
					const exchangeRate = contractExchangeRates[address];
					balanceFiat = balanceToFiat(balance, conversionRate, exchangeRate, currentCurrency);
				}
				return (
					<Pressable
						onPress={() => {
							// setSelectedAsset(selectedAsset);
							handleSelectAsset({
								...searchResults[i],
								balance, balanceFiat
							})
						}} // eslint-disable-line
						style={styles.assetListElement}>
						<View style={styles.assetNameElement}>
							{/* <View style={styles.assetIcon}>
								<TokenIcon medium icon={iconUrl} symbol={symbol} />
							</View> */}
							<View style={styles.assetIcon}>{renderLogo(searchResults[i])}</View>
							{/* <View style={styles.assetIcon}>
								<TokenIcon medium icon={iconUrl} symbol={symbol} />
							</View> */}
							<View style={styles.assetInfo}>
								<View style={{ flexDirection: 'row', alignItems: 'center' }}>
									<Text style={styles.textSymbol}>{name}</Text>
									{verified && <Image source={require('../../../../images-new/checked-icon.png')} style={styles.checkedIcon}></Image>}
								</View>
								<Text style={styles.balance}>{`${balance} ${symbol.toUpperCase()}`}</Text>
							</View>
						</View>
						{/* <RenderBalance secondaryBalance={balanceFiat} balanceError={balanceError} /> */}
						{
							balanceFiat
								? <Text style={styles.text}>{`${balanceFiat}`}</Text>
								: <Text style={styles.text}>{`${balance} ${symbol.toUpperCase()}`}</Text>
						}

					</Pressable>
					// <StyledButton
					// 	type={'normal'}
					// 	containerStyle={styles.item}
					// 	onPress={() => handleSelectAsset(searchResults[i])} // eslint-disable-line
					// 	key={i}
					// >
					// 	<View style={styles.assetListElement}>
					// 		<View style={styles.assetIcon}>{renderLogo(searchResults[i])}</View>
					// 		<View style={styles.assetInfo}>
					// 			<Text style={styles.textSymbol}>{symbol}</Text>
					// 			{!!name && <Text style={styles.text}>{name}</Text>}
					// 		</View>
					// 	</View>
					// </StyledButton>
				);
			})}
			{searchResults.length === 0 && <Text style={styles.text}>{emptyMessage}</Text>}
		</View>
	);
};

const mapStateToProps = (state, ownProps) => ({
	accounts: state.engine.backgroundState.AccountTrackerController.accounts,
	contractBalances: state.engine.backgroundState.TokenBalancesController.contractBalances,
	contractExchangeRates: state.engine.backgroundState.TokenRatesController.contractExchangeRates,
	collectibles: collectiblesSelector(state),
	collectibleContracts: collectibleContractsSelector(state),
	currentCurrency: state.engine.backgroundState.CurrencyRateController.currentCurrency,
	conversionRate: state.engine.backgroundState.CurrencyRateController.conversionRate,
	providerType: state.engine.backgroundState.NetworkController.provider.type,
	primaryCurrency: state.settings.primaryCurrency,
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
	chainId: state.engine.backgroundState.NetworkController.provider.chainId,
	ticker: state.engine.backgroundState.NetworkController.provider.ticker,
	tokens: state.engine.backgroundState.TokensController.tokens,
	transactionState: ownProps.transaction || state.transaction,
	selectedAsset: state.transaction.selectedAsset,
	isPaymentRequest: state.transaction.paymentRequest,

});

const mapDispatchToProps = (dispatch) => ({
	setTransactionObject: (transaction) => dispatch(setTransactionObject(transaction)),
	prepareTransaction: (transaction) => dispatch(prepareTransaction(transaction)),
	setSelectedAsset: (selectedAsset) => dispatch(setSelectedAsset(selectedAsset)),
});


export default connect(mapStateToProps, mapDispatchToProps)(AssetList);
