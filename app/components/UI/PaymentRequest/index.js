import React, { PureComponent, useMemo } from 'react';
import {
	TextInput,
	Text,
	StyleSheet,
	View,
	TouchableOpacity,
	KeyboardAvoidingView,
	InteractionManager,
	Image,
	Pressable, Keyboard, Alert
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context'
import { connect } from 'react-redux';
import { fontStyles, baseStyles } from '../../../styles/common';
import { getPaymentRequestOptionsTitle } from '../../UI/Navbar';
import FeatherIcon from 'react-native-vector-icons/Feather';
import Fuse from 'fuse.js';
import AssetList from './AssetList';
import PropTypes from 'prop-types';
import {
	weiToFiat,
	toWei,
	balanceToFiat,
	renderFromWei,
	fiatNumberToWei,
	fromWei,
	isDecimal,
	fiatNumberToTokenMinimalUnit,
	renderFromTokenMinimalUnit,
	fromTokenMinimalUnit,
	toTokenMinimalUnit,
	toHexadecimal,
	fromTokenMinimalUnitString
} from '../../../util/number';
import { strings } from '../../../../locales/i18n';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import StyledButton from '../StyledButton';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { generateETHLink, generateERC20Link, generateUniversalLinkRequest } from '../../../util/payment-link-generator';
import Device from '../../../util/device';
import currencySymbols from '../../../util/currency-symbols.json';
import { NetworksChainId, util, WalletDevice, GAS_ESTIMATE_TYPES } from '@metamask/controllers';
import { getTokenByTicker, getTicker, parseTransactionEIP1559, parseTransactionLegacy, generateTransferData, getEther, calculateEIP1559GasFeeHexes } from '../../../util/transactions';
import { toLowerCaseEquals, shallowEqual } from '../../../util/general';
import { getTokenListArray } from '../../../reducers/tokens';
import { utils as ethersUtils } from 'ethers';
import { ThemeContext, mockTheme } from '../../../util/theme';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import AddCustomToken from '../../UI/AddCustomToken';
import SearchToken from '../../UI/SearchToken';
import DefaultTabBar from 'react-native-scrollable-tab-view/DefaultTabBar';

import Identicon from '../Identicon';
import NetworkMainAssetLogo from '../NetworkMainAssetLogo';
import AssetIcon from '../AssetIcon';
import Tooltip from './Tooltip';
import SheetActions from './SheetActions'
import SheetActionsLegacy from './SheetActionsLegacy'
import PaymentDetail from './PaymentDetail'
import OrderExecuted from './OrderExecuted'
import WalletModal from '../../UI/WalletModal';
import { setSelectedAsset, prepareTransaction, setTransactionObject, resetTransaction } from '../../../actions/transaction';

import { getGasLimit } from '../../../util/custom-gas';
import Engine from '../../../core/Engine';
import AppConstants from '../../../core/AppConstants';
import { isMainnetByChainId } from '../../../util/networks';
import Networks from '../../../util/networks';


import TransactionTypes from '../../../core/TransactionTypes';
import NotificationManager from '../../../core/NotificationManager';
import { removeFavoriteCollectible } from '../../../actions/collectibles';
import { addHexPrefix, toChecksumAddress } from 'ethereumjs-util';
import collectiblesTransferInformation from '../../../util/collectibles-transfer';
import { isQRHardwareAccount, renderShortAddress } from '../../../util/address';
import PINCode, { hasUserSetPinCode } from '@haskkor/react-native-pincode';

import {
	MANUAL_BACKUP_STEPS,
	SEED_PHRASE,
	CONFIRM_PASSWORD,
	WRONG_PASSWORD_ERROR,
} from '../../../constants/onboarding';
import { KEYSTONE_TX_CANCELED } from '../../../constants/error';
import SecureKeychain from '../../../core/SecureKeychain';
import AsyncStorage from '@react-native-community/async-storage';
import { BIOMETRY_CHOICE } from '../../../constants/storage';
import { isNull } from 'url/util';
import PreventScreenshot from '../../../core/PreventScreenshot';
import PrivateCredential from './PrivateCredential'
import { decGWEIToHexWEI } from '../../../util/conversions';
import Clipboard from '@react-native-clipboard/clipboard';
import { swapsUtils } from '@metamask/swaps-controller';
import { BN } from 'ethereumjs-util';
import { gte } from '../../../util/lodash';
import {
	swapsTokensSelector,
} from '../../../reducers/swaps';
import saveAddressIcon from '../../../images-new/address-book/save-address.png';
import AddressSaveModal from '../../Views/SendFlow/AddressSave';

const { hexToBN, BNToHex } = util;

const KEYBOARD_OFFSET = 120;
const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			flex: 1,
			backgroundColor: '#1B1B23',
			paddingHorizontal: 20
		},
		authWrapper: {
			backgroundColor: '#1B1B23',
			flex: 1,
		},
		contentWrapper: {
			paddingTop: 24,
		},
		title: {
			...fontStyles.normal,
			fontSize: 16,
			color: colors.text.default,
		},
		amountWrapper: {
			flex: 1,
			marginVertical: 10
		},
		searchWrapper: {
			marginBottom: 10,
			borderColor: '#FFFFFF26',
			color: '#FFFFFF',
			borderWidth: 1,
			borderRadius: 12,
			flexDirection: 'row',
			alignItems: 'center',
			height: 56,
		},
		searchInput: {
			paddingLeft: 8,
			fontSize: 16,
			// height: 56,
			flex: 1,
			color: '#FFFFFF', // colors.text.default,
			...fontStyles.normal,
			paddingRight: 15
		},
		searchIcon: {
			textAlignVertical: 'center',
			marginLeft: 12,
			alignSelf: 'center',
		},
		clearButton: { paddingHorizontal: 12, justifyContent: 'center' },
		input: {
			...fontStyles.normal,
			fontSize: 16,
			color: '#FFFFFF',
			maxWidth: '45%'
		},
		inputAddress: {
			...fontStyles.normal,
			color: '#FFFFFF',  // colors.text.default,
			maxWidth: '90%',
			color: '#2F80ED',
			fontSize: 18,
			fontWeight: '600'
		},
		eth: {
			...fontStyles.normal,
			fontSize: 16,
			paddingTop: Device.isAndroid() ? 3 : 0,
			paddingLeft: 10,
			textTransform: 'uppercase',
			color: '#FFFFFF' // colors.text.default,
		},
		max: {
			...fontStyles.normal,
			fontSize: 12,
			color: '#FFFFFF' // colors.text.default,
		},
		fiatValue: {
			...fontStyles.normal,
			fontSize: 16,
			color: '#FFFFFF' // colors.text.default,
		},
		split: {
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
			marginTop: 15
		},
		ethContainer: {
			flex: 1,
			flexDirection: 'row',
			paddingLeft: 6,
			paddingRight: 10,
		},
		container: {
			// flex: 1,
			flexDirection: 'row',
			// paddingRight: 10,
			paddingVertical: 20,
			paddingHorizontal: 10,
			// paddingLeft: 14,
			// position: 'relative',
			borderColor: 'rgba(255, 255, 255, 0.1)', // colors.border.default,
			borderWidth: 1,
			borderRadius: 15,
			shadowColor: 'rgba(0, 0, 0, 0.4)',
			shadowOffset: {
				width: 0,
				height: 2,
			},
			shadowOpacity: 0.25,
			shadowRadius: 3.84,
			elevation: 5,
			backgroundColor: '#1B1B23', // colors.border.default,
		},
		amounts: {
			width: '100%',
		},
		switchContainer: {
			flex: 1,
			flexDirection: 'column',
			alignSelf: 'center',
			right: 0,
		},
		switchTouchable: {
			flexDirection: 'row',
			alignSelf: 'flex-end',
			right: 0,
		},
		enterAmountWrapper: {
			flex: 1,
			flexDirection: 'column',
		},
		button: {
			marginBottom: 16,
		},
		buttonsWrapper: {
			flex: 1,
			flexDirection: 'row',
			alignSelf: 'center',
		},
		buttonsContainer: {
			flex: 1,
			flexDirection: 'column',
			alignSelf: 'flex-end',
		},
		scrollViewContainer: {
			flexGrow: 1,
		},
		errorWrapper: {
			// backgroundColor: colors.error.muted,
			borderRadius: 4,
			marginTop: 8,
		},
		errorText: {
			color: colors.text.default,
			alignSelf: 'center',
		},
		assetsWrapper: {
			// marginTop: 16,
		},
		assetsTitle: {
			...fontStyles.normal,
			fontSize: 16,
			marginBottom: 12,
			color: "#FFFFFF",
			fontWeight: '400'
		},
		secondaryAmount: {
			marginTop: 15,
			flexDirection: 'row',
			justifyContent: 'space-between'
		},
		currencySymbol: {
			...fontStyles.normal,
			fontSize: 24,
			color: colors.text.default,
		},
		currencySymbolSmall: {
			...fontStyles.normal,
			fontSize: 16,
			color: '#FFFFFF' // colors.text.default,
		},
		titleWrapper: {
			width: '100%',
			height: 33,
			alignItems: 'center',
			justifyContent: 'center',
		},
		dragger: {
			width: 48,
			height: 5,
			borderRadius: 4,
			backgroundColor: '#3D3D4B',
			opacity: 0.6,
		},
		tabUnderlineStyle: {
			height: '100%',
			zIndex: -1,
			backgroundColor: '#3D3D4B',
			borderRadius: 12,
			alignItems: 'center',
			shadowColor: "rgba(0, 0, 0, 0.4)",
			shadowOffset: {
				width: 0,
				height: 4,
			},
			shadowOpacity: 0.30,
			shadowRadius: 4.65,
			elevation: 8,
		},
		tabStyle: {
			padding: 0,
		},
		textStyle: {
			fontSize: 16,
			marginTop: 10,
			fontWeight: '400',
		},
		tabBar: {
			borderColor: 'transparent',
			marginTop: 15,
			borderRadius: 12,
			height: 45
		},
		label: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		labelText: {
			...fontStyles.normal,
			color: '#FFFFFF',
			fontSize: 16,
			// width: '60%'
		},
		ethLogo: {
			width: 28,
			height: 28,
		},
		ethLogoReview: {
			width: 36,
			height: 36,
		},
		assetContainer: {
			// backgroundColor: '#282832',
			borderColor: 'rgba(255, 255, 255, 0.2)', // colors.border.default,
			borderWidth: 1,
			borderRadius: 15,
			padding: 6,
			paddingHorizontal: 8,
			flexDirection: 'row',
			alignItems: 'center'
		},
		assetInfo: {
			alignSelf: 'center',
			justifyContent: 'center',
			marginLeft: 10
		},
		textName: {
			...fontStyles.normal,
			fontSize: 16,
			color: '#FFFFFF',
			fontWeight: "500",
			marginHorizontal: 8
		},
		textSymbol: {
			...fontStyles.normal,
			fontSize: 14,
			color: '#A0A0B1',
			marginTop: 8,
			textAlign: 'right'
		},
		feeContainer: {
			justifyContent: 'space-between',
			alignItems: 'center',
			flexDirection: 'row',
			paddingHorizontal: 20,
			marginTop: 15
		},
		feeEstimate: {
			// ...fontStyles.normal,
			fontSize: 14,
			color: '#A0A0B1',
			marginTop: 5,
			fontWeight: '500'
		},
		fee: {
			...fontStyles.normal,
			fontSize: 16,
			color: '#FFFFFF',
			marginTop: 5,
			fontWeight: '500'
		},
		iconSetting: {
			width: 30,
			height: 30,
			tintColor: '#D0D0D8'
		},
		iconCheck: {
			marginRight: 15,
			width: 25,
			height: 25,
			tintColor: '#3D3D4B', // '#2BEB4A' // #3D3D4B
		},
		buttonStyle: {
			height: 45,
			backgroundColor: '#4A369A',
			// marginHorizontal: 15,
			borderRadius: 10,
			justifyContent: 'center',
			alignItems: 'center',
			marginBottom: 10
		},
		content: {
			color: "#FFFFFF",
			fontSize: 16,
		},
		titleWarning: {
			...fontStyles.normal,
			fontSize: 18,
			color: '#FFFFFF',
			fontWeight: '500',
			textAlign: 'center',
			marginTop: 15
		},
		btnWarning: {
			backgroundColor: '#1B1B23',
			borderColor: 'rgba(255, 255, 255, 0.1)',
			borderWidth: 1,
			borderRadius: 15,
			justifyContent: 'center',
			alignItems: 'center',
			padding: 15,
			marginTop: 20,
			shadowOffset: {
				width: 0,
				height: 1,
			},
			shadowOpacity: 0.22,
			shadowRadius: 2.22,
			elevation: 3,
			shadowColor: 'rgba(0, 0, 0, 0.4)'
		},
		contentBtnWarning: {
			...fontStyles.normal,
			fontSize: 16,
			color: '#FFFFFF',
			textAlign: 'center'
		},
		iconWarning: {
			width: 30,
			height: 30,
			// tintColor: '#B22828'
		},
		warning: {
			width: 50,
			height: 50,
		},
		iconArrow: {
			width: 16,
			height: 16,
		},
		dots: {
			width: 12,
			height: 12,
			borderRadius: 6,
			backgroundColor: '#1B1B23',
			borderColor: '#FFFFFF',
			borderWidth: 2,
		},
		titlePinCode: {
			fontSize: 24,
			...fontStyles.bold,
			lineHeight: 29,
			color: '#FFFFFF',
			opacity: 1
		},
		subtitlePinCode: {
			fontSize: 16,
			...fontStyles.normal,
			lineHeight: 20,
			color: '#58586C',
			opacity: 1
		},
		pinCodeTextButtonCircle: {
			fontSize: 20,
			...fontStyles.normal,
			color: '#FFFFFF',
			opacity: 1
		},
		pinCodeButtonCircle: {
			backgroundColor: '#1B1B23',
			borderWidth: 1,
			borderColor: '#3D3D4B',
			// width: 72,
			// height: 72,
			borderRadius: 36,
			opacity: 1
		},
		pinCodeDeleteButtonText: {
			color: '#FFFFFF',
			fontSize: 16,
			lineHeight: 20,
			...fontStyles.normal
		},
		labelTextWrapper: {
			flexDirection: 'row',
			justifyContent: 'space-between',
			alignItems: 'center',
		},
		saveButton: {
			padding: 10,
			marginRight: -10,
		},
		saveButtonImage: {
			height: 16,
			width: 16,
		},
	});

const fuse = new Fuse([], {
	shouldSort: true,
	threshold: 0.45,
	location: 0,
	distance: 100,
	maxPatternLength: 32,
	minMatchCharLength: 1,
	keys: [
		{ name: 'name', weight: 0.5 },
		{ name: 'symbol', weight: 0.5 },
	],
});

const defaultEth = {
	symbol: 'ETH',
	name: 'Ether',
	isETH: true,
};
const defaultAssets = [
	defaultEth,
	{
		address: '0x89d24A6b4CcB1B6fAA2625fE562bDD9a23260359',
		decimals: 18,
		erc20: true,
		logo: 'sai.svg',
		name: 'Sai Stablecoin v1.0',
		symbol: 'SAI',
	},
];

const EMPTY_LEGACY_TRANSACTION_DATA = {
	transactionFeeFiat: '',
	transactionFee: '',
	transactionTotalAmount: '',
	transactionTotalAmountFiat: '',
};

const MODE_SELECT = 'select';
const MODE_AMOUNT = 'amount';

/**
 * View to generate a payment request link
 */
class PaymentRequest extends PureComponent {
	static propTypes = {
		/**
		 * Object that represents the navigator
		 */
		navigation: PropTypes.object,
		/**
		 * ETH-to-current currency conversion rate from CurrencyRateController
		 */
		conversionRate: PropTypes.number,
		/**
		 * Currency code for currently-selected currency from CurrencyRateController
		 */
		currentCurrency: PropTypes.string,
		/**
		 * Object containing token exchange rates in the format address => exchangeRate
		 */
		contractExchangeRates: PropTypes.object,
		/**
		 * Primary currency, either ETH or Fiat
		 */
		primaryCurrency: PropTypes.string,
		/**
		 * A string that represents the selected address
		 */
		selectedAddress: PropTypes.string,
		/**
		 * Array of ERC20 assets
		 */
		tokens: PropTypes.array,
		/**
		 * A string representing the chainId
		 */
		chainId: PropTypes.string,
		/**
		 * Current provider ticker
		 */
		ticker: PropTypes.string,
		/**
		 * List of tokens from TokenListController (Formatted into array)
		 */
		tokenList: PropTypes.array,
		/**
		 * Object that represents the current route info like params passed to it
		 */
		route: PropTypes.object,
	};

	amountInput = React.createRef();
	searchInput = React.createRef();

	state = {
		searchInputValue: '',
		results: [getEther(this.props.ticker), ...this.props.tokens],
		selectedAsset: this.props.route.params?.skipSelectAsset ? this.props.selectedAsset : undefined,
		mode: this.props.route.params?.skipSelectAsset ? MODE_AMOUNT : MODE_SELECT,
		internalPrimaryCurrency: '',
		cryptoAmount: undefined,
		amount: undefined,
		secondaryAmount: undefined,
		symbol: undefined,
		showError: false,
		inputWidth: { width: '99%' },
		toolTipVisible: false,
		isVisibleCustom: false,
		showCustomWarning: false,
		paymentDetail: false,
		tokenWarning: false,
		messageError: "Hãy nhập số lượng cần gửi",
		checkInfoSend: false,
		EIP1559TransactionData: {},
		LegacyTransactionData: {},
		advancedGasInserted: false,
		gasSelected: AppConstants.GAS_OPTIONS.MEDIUM,
		pollToken: null,
		transactionConfirmed: false,
		loading: false,
		selectGasFee: 1,
		stopUpdateGas: false,
		transactionGasFee: false,
		selectCustomGasFee: false,
		warningIncorrectPassword: undefined,
		ready: false,
		authTransaction: false,
		authUsePass: false,
		estimatedTotalGas: undefined,
		saveAddressModal: null,
		newAddressName: '',
		warningSelectedAsset: undefined
	};

	updateNavBar = () => {
		const { navigation, route } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		// navigation.setOptions(
		// 	getPaymentRequestOptionsTitle(strings('payment_request.title'), navigation, route, colors)
		// );
	};

	estimateGasLimit = async () => {
		const { transaction: { from }, transactionTo } = this.props.transactionState;
		const { gas } = await getGasLimit({
			from,
			to: transactionTo,
		});
		return gas;
	};

	GasFeeEstimates = async () => {
		const { GasFeeController } = Engine.context;
		const [gasEstimates, gas] = await Promise.all([
			GasFeeController.fetchGasFeeEstimates({ shouldUpdateState: false }),
			this.estimateGasLimit(),
		]);

		if (gasEstimates.gasEstimateType === GAS_ESTIMATE_TYPES.FEE_MARKET) {
			const gasFeeEstimates = gasEstimates.gasFeeEstimates[AppConstants.GAS_OPTIONS.MEDIUM];
			const estimatedBaseFeeHex = decGWEIToHexWEI(gasEstimates.gasFeeEstimates.estimatedBaseFee);
			const suggestedMaxPriorityFeePerGasHex = decGWEIToHexWEI(gasFeeEstimates.suggestedMaxPriorityFeePerGas);
			const suggestedMaxFeePerGasHex = decGWEIToHexWEI(gasFeeEstimates.suggestedMaxFeePerGas);
			const gasLimitHex = BNToHex(gas);
			const gasHexes = calculateEIP1559GasFeeHexes({
				gasLimitHex,
				estimatedBaseFeeHex,
				suggestedMaxFeePerGasHex,
				suggestedMaxPriorityFeePerGasHex,
			});
			this.setState({
				estimatedTotalGas: hexToBN(gasHexes.gasFeeMaxHex),
			});
		} else if (gasEstimates.gasEstimateType === GAS_ESTIMATE_TYPES.LEGACY) {
			const gasPrice = hexToBN(decGWEIToHexWEI(gasEstimates.gasFeeEstimates[AppConstants.GAS_OPTIONS.MEDIUM]));
			this.setState({ estimatedTotalGas: gas.mul(gasPrice) });
		} else {
			const gasPrice = hexToBN(decGWEIToHexWEI(gasEstimates.gasFeeEstimates.gasPrice));
			this.setState({ estimatedTotalGas: gas.mul(gasPrice) });
		}
	}

	getGasLimit = async (transaction) => {
		const {
			prepareTransaction,
		} = this.props;
		const estimation = await getGasLimit(transaction);
		prepareTransaction({ ...transaction, ...estimation });
	};
	/**
	 * Set chainId, internalPrimaryCurrency and receiveAssets, if there is an asset set to this payment request chose it automatically, to state
	 */
	componentDidMount = async () => {
		const { primaryCurrency, route, tokenList } = this.props;
		this.setState({
			internalPrimaryCurrency: primaryCurrency,
			inputWidth: { width: '100%' },
		});
		// this.updateNavBar()
		await this.GasFeeEstimates()

		const { GasFeeController } = Engine.context;
		const pollToken = await GasFeeController.getGasFeeEstimatesAndStartPolling(this.state.pollToken);
		this.setState({ pollToken });


		const receiveAsset = route.params?.skipSelectAsset // route?.params?.receiveAsset;
		if (receiveAsset) {
			this.goToAmountInput(this.props.selectedAsset);
		}
		// TODO: Fuse will only be updated once on mount. When we convert this component to hooks, we can utilize useEffect to update fuse.
		// Update fuse collection with token list
		fuse.setCollection(tokenList);

		// const biometryType = await SecureKeychain.getSupportedBiometryType();
		// const biometryChoice = await AsyncStorage.getItem(BIOMETRY_CHOICE);
		// console.log('biometryType : ', biometryChoice, biometryType)
		// if (isNull(biometryType) || isNull(biometryChoice) || !biometryChoice) {
		// 	this.setState({ authUsePass: true });
		// } else {
		// 	const credentials = await SecureKeychain.getGenericPassword();
		// 	this.privateKey = await this.tryExportPrivateKey(credentials.password);
		// 	if (credentials) {
		// 		this.setState({ authTransaction: false });
		// 		this.onSend()
		// 	} else {
		// 		this.setState({ authUsePass: true });
		// 	}
		// 	InteractionManager.runAfterInteractions(() => PreventScreenshot.forbid());
		// }
	};

	componentDidUpdate = (prevProps, prevState) => {
		try {
			const {
				transactionState: {
					transactionTo,
					transaction: { value, gas },
				},
				contractBalances,
			} = this.props;
			if (this.props.gasFeeEstimates && gas && (!shallowEqual(prevProps.gasFeeEstimates, this.props.gasFeeEstimates)
				|| gas !== prevProps?.transactionState?.transaction?.gas || this.state.mode !== prevState.mode)
			) {
				const gasEstimateTypeChanged = prevProps.gasEstimateType !== this.props.gasEstimateType;
				const gasSelected = gasEstimateTypeChanged ? AppConstants.GAS_OPTIONS.MEDIUM : this.state.gasSelected;

				if ((!this.state.stopUpdateGas && !this.state.advancedGasInserted) || gasEstimateTypeChanged) {
					if (this.props.gasEstimateType === GAS_ESTIMATE_TYPES.FEE_MARKET) {
						const suggestedGasLimit = fromWei(gas, 'wei');

						const EIP1559TransactionData = this.parseTransactionDataEIP1559({
							...this.props.gasFeeEstimates[gasSelected],
							suggestedGasLimit,
							selectedOption: gasSelected,
						});
						this.setState({
							gasEstimationReady: true,
							EIP1559TransactionData,
							LegacyTransactionData: EMPTY_LEGACY_TRANSACTION_DATA,
							gasSelected,
						});
					}
					else if (this.props.gasEstimateType !== GAS_ESTIMATE_TYPES.NONE) {
						const suggestedGasLimit = fromWei(gas, 'wei');
						const LegacyTransactionData = this.parseTransactionDataLegacy({
							suggestedGasPrice:
								this.props.gasEstimateType === GAS_ESTIMATE_TYPES.LEGACY
									? this.props.gasFeeEstimates[gasSelected]
									: this.props.gasFeeEstimates.gasPrice,
							suggestedGasLimit,
						});
						this.setState({
							gasEstimationReady: true,
							LegacyTransactionData,
							EIP1559TransactionData: {},
							gasSelected,
						});
					}
				}
			}
		} catch (error) {
			console.log('-----componentDidUpdate ------error-------- : ', error)
		}

		// this.updateNavBar();
		// InteractionManager.runAfterInteractions(() => {
		// 	this.amountInput.current && this.amountInput.current.focus();
		// });

	};

	componentWillUnmount = () => {
		const { GasFeeController } = Engine.context;
		GasFeeController.stopPolling(this.state.pollToken);
	};

	parseTransactionDataEIP1559 = (gasFee, options) => {
		const parsedTransactionEIP1559 = parseTransactionEIP1559(
			{
				...this.props,
				selectedGasFee: { ...gasFee, estimatedBaseFee: this.props.gasFeeEstimates.estimatedBaseFee },
			},
			options
		);
		// const { transaction } = this.props;
		// parsedTransactionEIP1559.error = this.validateAmount({
		// 	transaction,
		// 	total: parsedTransactionEIP1559.totalMaxHex,
		// });
		return parsedTransactionEIP1559;
	};

	parseTransactionDataLegacy = (gasFee, options) => {
		// console.log('parseTransactionDataLegacy gasFee : ', gasFee)
		const parsedTransactionLegacy = parseTransactionLegacy(
			{
				...this.props,
				selectedGasFee: gasFee,
			}, options
		);
		// const { transaction } = this.props;

		// parsedTransactionLegacy.error = this.validateAmount({
		// 	transaction,
		// 	total: parsedTransactionLegacy.totalHex,
		// });

		return parsedTransactionLegacy;
	};
	/**
	 * Go to asset selection view and modify navbar accordingly
	 */
	goToAssetSelection = () => {
		const { navigation } = this.props;
		navigation && navigation.setParams({ mode: MODE_SELECT, dispatch: undefined });
		this.setState({
			mode: MODE_SELECT,
			amount: undefined,
			cryptoAmount: undefined,
			secondaryAmount: undefined,
			symbol: undefined,
		});
	};

	prepareTransaction = async (value, selectedAsset) => {
		const {
			prepareTransaction,
			transactionState: { transaction, transactionTo },
		} = this.props;
		if (selectedAsset.isETH) {
			transaction.data = undefined;
			transaction.to = transactionTo;
			transaction.value = BNToHex(toWei(value));
		} else if (selectedAsset.tokenId) {
			const collectibleTransferTransactionProperties = this.getCollectibleTranferTransactionProperties();
			transaction.data = collectibleTransferTransactionProperties.data;
			transaction.to = collectibleTransferTransactionProperties.to;
			transaction.value = collectibleTransferTransactionProperties.value;
		} else {
			const tokenAmount = toTokenMinimalUnit(value, selectedAsset.decimals);
			transaction.data = generateTransferData('transfer', {
				toAddress: transactionTo,
				amount: BNToHex(tokenAmount),
			});
			transaction.to = selectedAsset.address;
			transaction.value = '0x0';
		}
		this.getGasLimit({ ...transaction, gas: undefined })
		prepareTransaction(transaction);
		return transaction

	};

	updateTransaction = (value = 0, selectedAsset) => {
		const {
			transactionState: { transaction, transactionTo },
			setTransactionObject,
			selectedAddress,
		} = this.props;

		const transactionObject = {
			...transaction,
			value: BNToHex(toWei(value)),
			selectedAsset,
			from: selectedAddress,
		};

		if (selectedAsset.tokenId) {
			const collectibleTransferTransactionProperties = this.getCollectibleTranferTransactionProperties();
			transactionObject.data = collectibleTransferTransactionProperties.data;
			transactionObject.to = collectibleTransferTransactionProperties.to;
			transactionObject.value = collectibleTransferTransactionProperties.value;
		} else if (!selectedAsset.isETH) {
			const tokenAmount = toTokenMinimalUnit(value, selectedAsset.decimals);
			transactionObject.data = generateTransferData('transfer', {
				toAddress: transactionTo,
				amount: BNToHex(tokenAmount),
			});
			transactionObject.value = '0x0';
		}

		if (selectedAsset.erc20) {
			transactionObject.readableValue = value;
		}
		this.getGasLimit({ ...transactionObject, gas: undefined })
		setTransactionObject(transactionObject);
	};
	/**
	 * Go to enter amount view, with selectedAsset and modify navbar accordingly
	 *
	 * @param {object} selectedAsset - Asset selected to build the payment request
	 */
	goToAmountInput = async (selectedAsset) => {
		const { navigation, transactionState: { transaction } } = this.props;
		// navigation.navigate('Swaps')
		// const { navigation } = this.props;
		// navigation && navigation.setParams({ mode: MODE_AMOUNT, dispatch: this.goToAssetSelection });
		this.setState({ selectedAsset, mode: MODE_AMOUNT, transactionGasFee: false, selectCustomGasFee: false });
		this.props.setSelectedAsset(selectedAsset)
		this.prepareTransaction(0, selectedAsset);
		// console.log('transaction : ', transaction)
		// if (transaction.value !== undefined) {
		// 	this.updateTransaction(0, selectedAsset);
		// } else {
		// 	await this.prepareTransaction(0, selectedAsset);
		// }
		this.updateAmount('0', selectedAsset); // Swaps
	};
	/**
	 * Handle search input result
	 *
	 * @param {string} searchInputValue - String containing assets query
	 */
	handleSearch = (searchInputValue) => {
		const { tokenList, tokens, ticker } = this.props;
		if (!searchInputValue) {
			// let results = [getEther(ticker), ...tokens];
			this.setState({ searchInputValue });
			return
		}
		if (typeof searchInputValue !== 'string') {
			searchInputValue = this.state.searchInputValue;
		}

		// const fuseSearchResult = fuse.search(searchInputValue);
		// const addressSearchResult = tokenList.filter((token) => toLowerCaseEquals(token.address, searchInputValue));
		// const results = [...addressSearchResult, ...fuseSearchResult];
		this.setState({ searchInputValue });
	};

	/** Clear search input and focus */
	clearSearchInput = () => {
		this.setState({ searchInputValue: '' });
		this.searchInput.current?.focus?.();
	};

	/**
	 * Renders a view that allows user to select assets to build the payment request
	 * Either top picks and user's assets are available to select
	 */
	renderSelectAssets = (results) => {
		// const { tokens, chainId, ticker, tokenList } = this.props;
		// const { accounts,
		// 	selectedAddress,
		// 	conversionRate,
		// 	currentCurrency,
		// 	contractBalances,
		// 	contractExchangeRates
		// } = this.props;

		const { inputWidth } = this.state;
		// let results = [getEther(ticker), ...tokens];
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		// const userTokens = tokens.map((token) => {
		// 	const contract = tokenList.find((contractToken) => contractToken.address === token.address);
		// 	if (contract) return contract;
		// 	return token;
		// });

		// if (chainId === '1') {
		// 	results = this.state.searchInputValue ? this.state.results : defaultAssets;
		// } else if (Object.values(NetworksChainId).find((value) => value === chainId)) {
		// 	results = [defaultEth];
		// } else {
		// 	results = [{ ...defaultEth, symbol: getTicker(ticker), name: '' }];
		// }

		// console.log('results : ', results, tokenList)

		return (
			<View style={baseStyles.flexGrow}>
				<View style={styles.searchWrapper}>
					<FeatherIcon name="search" size={22} color={'#FFFFFF'} style={styles.searchIcon} />
					<TextInput
						ref={this.searchInput}
						style={styles.searchInput}
						placeholder={'Tìm theo tên hoặc địa chỉ token'}
						placeholderTextColor={'#58586C'}
						value={this.state.searchInputValue}
						onChangeText={this.handleSearch}
						returnKeyType="search"
						onSubmitEditing={this.handleSearch}
					// blurOnSubmit
					// keyboardAppearance={themeAppearance}
					/>
					<TouchableOpacity onPress={this.pateAddress}>
						<Text style={{
							color: '#B1B5FF',
							fontSize: 16,
							fontWeight: "400",
							marginRight: 15
						}}>{'Dán'}</Text>
					</TouchableOpacity>
				</View>
				{results.length > 0 && (
					<View style={styles.assetsWrapper}>
						{/* <Text style={styles.assetsTitle}>{'Token đang nắm giữ'}</Text> */}
						<AssetList
							searchResults={results}
							handleSelectAsset={(selectedAsset) => {
								if (!selectedAsset?.verified) {
									// Hiển thị cảnh báo
									this.setState({ tokenWarning: true, warningSelectedAsset: selectedAsset })
								} else {
									this.goToAmountInput(selectedAsset)
								}
								// this.goToAmountInput(selectedAsset)
							}}
							selectedAsset={this.state.selectedAsset}
							searchQuery={this.state.searchInputValue}
						// accounts={accounts}
						// selectedAddress={selectedAddress}
						// conversionRate={conversionRate}
						// currentCurrency={currentCurrency}
						// contractBalances={contractBalances}
						// contractExchangeRates={contractExchangeRates}
						/>
					</View>
				)}
			</View>
		);
	};

	/**
	 * Handles payment request parameters for ETH as primaryCurrency
	 *
	 * @param {string} amount - String containing amount number from input, as token value
	 * @returns {object} - Object containing respective symbol, secondaryAmount and cryptoAmount according to amount and selectedAsset
	 */
	handleETHPrimaryCurrency = (amount, selectedAsset) => {
		const { conversionRate, currentCurrency, contractExchangeRates } = this.props;
		// const { selectedAsset } = this.state;
		let secondaryAmount;
		const symbol = selectedAsset?.symbol;
		const undefAmount = isDecimal(amount) && !ethersUtils.isHexString(amount) ? amount : 0;
		const cryptoAmount = amount;
		const exchangeRate = selectedAsset && selectedAsset?.address && contractExchangeRates[selectedAsset.address];
		if (!selectedAsset.isETH) {
			secondaryAmount = exchangeRate
				? balanceToFiat(undefAmount, conversionRate, exchangeRate, currentCurrency)
				: undefined;
		} else {
			secondaryAmount = weiToFiat(toWei(undefAmount), conversionRate, currentCurrency);
		}
		return { symbol, secondaryAmount, cryptoAmount };
	};

	/**
	 * Handles payment request parameters for Fiat as primaryCurrency
	 *
	 * @param {string} amount - String containing amount number from input, as fiat value
	 * @returns {object} - Object containing respective symbol, secondaryAmount and cryptoAmount according to amount and selectedAsset
	 */
	handleFiatPrimaryCurrency = (amount, selectedAsset) => {
		const { conversionRate, currentCurrency, contractExchangeRates } = this.props;
		// const { selectedAsset } = this.state;
		const symbol = currentCurrency;
		const exchangeRate = selectedAsset && selectedAsset.address && contractExchangeRates[selectedAsset.address];
		const undefAmount = (isDecimal(amount) && amount) || 0;
		let secondaryAmount, cryptoAmount;
		if (!selectedAsset.isETH && exchangeRate && exchangeRate !== 0) {
			const secondaryMinimalUnit = fiatNumberToTokenMinimalUnit(
				undefAmount,
				conversionRate,
				exchangeRate,
				selectedAsset?.decimals
			);
			secondaryAmount =
				renderFromTokenMinimalUnit(secondaryMinimalUnit, selectedAsset?.decimals)  // + ' ' + selectedAsset?.symbol;
			cryptoAmount = fromTokenMinimalUnit(secondaryMinimalUnit, selectedAsset?.decimals);
		} else {
			secondaryAmount = renderFromWei(fiatNumberToWei(undefAmount, conversionRate)) // + ' ' + strings('unit.eth');
			cryptoAmount = fromWei(fiatNumberToWei(undefAmount, conversionRate));
		}
		return { symbol, secondaryAmount, cryptoAmount };
	};

	useMax = () => {
		const { accounts, selectedAddress, contractBalances, selectedAsset, conversionRate, contractExchangeRates } =
			this.props;
		const { estimatedTotalGas } = this.state;
		try {
			let input;
			if (selectedAsset.isETH) {
				const balanceBN = hexToBN(accounts[selectedAddress].balance);
				const realMaxValue = balanceBN.sub(estimatedTotalGas);
				const maxValue = balanceBN.isZero() || realMaxValue.isNeg() ? new BN(0) : realMaxValue;
				input = fromWei(maxValue);
			} else {
				input = fromTokenMinimalUnitString(
					contractBalances[selectedAsset.address]?.toString(10),
					selectedAsset.decimals
				);
			}
			if (input === "0" || !input) {
				this.updateAmount(`${Number(input)}`, selectedAsset);
			} else {
				this.updateAmount(`${Number(input).toFixed(5)}`, selectedAsset);
			}
		} catch (error) {
			console.log('useMax error : ', error)
		}
		// this.updateAmount(`${Number(input).toFixed(10)}`, selectedAsset);
	};

	/**
	 * Handles amount update, setting amount related state parameters, it handles state according to internalPrimaryCurrency
	 *
	 * @param {string} amount - String containing amount number from input
	 */
	updateAmount = (amount, selectedAsset) => {
		const { internalPrimaryCurrency, estimatedTotalGas } = this.state;
		const { contractBalances, accounts, selectedAddress, conversionRate, contractExchangeRates, currentCurrency, transactionState: { transaction } } = this.props;
		// console.log('----- transaction -----: ', transaction)
		const currencySymbol = currencySymbols[currentCurrency];
		const exchangeRate = selectedAsset && selectedAsset?.address && contractExchangeRates[selectedAsset.address];
		// let res;
		// If primary currency is not crypo we need to know if there are conversion and exchange rates to handle0,
		// fiat conversion for the payment request
		// console.log('conversionRate : ', internalPrimaryCurrency, conversionRate, exchangeRate)
		// if (internalPrimaryCurrency !== 'ETH' && conversionRate && (exchangeRate || selectedAsset?.isETH)) {
		// 	res = this.handleFiatPrimaryCurrency(amount?.replace(',', '.'), selectedAsset);
		// } else {
		// 	res = this.handleETHPrimaryCurrency(amount?.replace(',', '.'), selectedAsset);
		// }
		const res = this.handleETHPrimaryCurrency(amount?.replace(',', '.'), selectedAsset);
		// console.log('res 1: ', res, currencySymbol, res.secondaryAmount[0])
		const { cryptoAmount, symbol } = res;
		// console.log('res : ', res)
		if (amount && amount[0] === currencySymbol) amount = amount.substr(1);
		if (res.secondaryAmount && res.secondaryAmount[0] === currencySymbol)
			res.secondaryAmount = res.secondaryAmount.substr(1);
		if (amount && amount === '0') amount = undefined;
		const secondaryAmount = (res.secondaryAmount && res.secondaryAmount !== '0' && res.secondaryAmount !== '0.00') ? res.secondaryAmount : ""
		this.setState({ amount: amount?.replace(',', '.'), cryptoAmount, secondaryAmount, showError: false, transactionGasFee: false, selectCustomGasFee: false });

		if (transaction.value !== undefined) {
			this.updateTransaction(parseFloat(amount?.replace(',', '.') || 0), selectedAsset);
		} else {
			this.prepareTransaction(parseFloat(amount?.replace(',', '.') || 0), selectedAsset);
		}
		let weiBalance, weiInput;
		if (cryptoAmount && cryptoAmount !== "0" && isDecimal(cryptoAmount)) {
			if (selectedAsset.isETH) {
				weiBalance = hexToBN(accounts[selectedAddress].balance);
				weiInput = toWei(cryptoAmount).add(estimatedTotalGas);
			} else {
				weiBalance = contractBalances[selectedAsset.address];
				weiInput = toTokenMinimalUnit(cryptoAmount, selectedAsset.decimals);
			}
			// console.log('weiBalance : ', cryptoAmount, selectedAsset.decimals, weiBalance, weiInput)
			// TODO: weiBalance is not always guaranteed to be type BN. Need to consolidate type.
			if (gte(weiBalance, weiInput)) {
				this.setState({ messageError: "Kiểm tra thông tin trước khi gửi", checkInfoSend: true })
			} else {
				this.setState({ messageError: "Không đủ số dư", checkInfoSend: false })
			}
		} else {
			this.setState({ messageError: "Hãy nhập số lượng cần gửi", checkInfoSend: false })
		}

		// if (cryptoAmount && cryptoAmount !== "0") {
		// 	if (selectedAsset.isETH) {
		// 		// Kiểm tra phí gas + amount

		// 	} else {
		// 		// Kiểm tra amount và gas native

		// 	}
		// 	// if (cryptoAmount && (parseFloat(cryptoAmount) > parseFloat(selectedAsset?.balance))) {
		// 	// 	this.setState({ messageError: "Không đủ số dư", checkInfoSend: false })
		// 	// } else {
		// 	// 	this.setState({ messageError: "Kiểm tra thông tin trước khi gửi", checkInfoSend: true })
		// 	// }
		// } else {
		// 	this.setState({ messageError: "Hãy nhập số lượng cần gửi", checkInfoSend: false })
		// }
	};

	updateSecondaryAmount = (amount, selectedAsset) => {
		const { internalPrimaryCurrency, estimatedTotalGas } = this.state;
		const { contractBalances, accounts, selectedAddress, conversionRate, contractExchangeRates, currentCurrency, transactionState: { transaction } } = this.props;
		const currencySymbol = currencySymbols[currentCurrency];
		const exchangeRate = selectedAsset && selectedAsset?.address && contractExchangeRates[selectedAsset.address];

		const res = this.handleFiatPrimaryCurrency(amount?.replace(',', '.'), selectedAsset);

		const { cryptoAmount, symbol } = res;
		// console.log('res 2: ', res, currencySymbol)
		if (amount && amount[0] === currencySymbol) amount = amount.substr(1);
		if (res.secondaryAmount && res.secondaryAmount[0] === currencySymbol)
			res.secondaryAmount = res.secondaryAmount.substr(1);
		if (amount && amount === '0') amount = undefined;
		const firstAmount = (res.secondaryAmount && res.secondaryAmount !== '0' && res.secondaryAmount !== '0.00') ? res.secondaryAmount : ""
		this.setState({ amount: firstAmount, cryptoAmount, secondaryAmount: amount?.replace(',', '.'), showError: false });

		if (transaction.value !== undefined) {
			this.updateTransaction(parseFloat(firstAmount?.replace(',', '.') || 0), selectedAsset);
		} else {
			this.prepareTransaction(parseFloat(firstAmount?.replace(',', '.') || 0), selectedAsset);
		}

		let weiBalance, weiInput;
		if (cryptoAmount && cryptoAmount !== "0" && isDecimal(cryptoAmount)) {
			if (selectedAsset.isETH) {
				weiBalance = hexToBN(accounts[selectedAddress].balance);
				weiInput = toWei(cryptoAmount).add(estimatedTotalGas);
			} else {
				weiBalance = contractBalances[selectedAsset.address];
				weiInput = toTokenMinimalUnit(cryptoAmount, selectedAsset.decimals);
			}
			// TODO: weiBalance is not always guaranteed to be type BN. Need to consolidate type.
			if (gte(weiBalance, weiInput)) {
				this.setState({ messageError: "Kiểm tra thông tin trước khi gửi", checkInfoSend: true })
			} else {
				this.setState({ messageError: "Không đủ số dư", checkInfoSend: false })
			}
		} else {
			this.setState({ messageError: "Hãy nhập số lượng cần gửi", checkInfoSend: false })
		}

		// if (cryptoAmount && cryptoAmount !== "0") {
		// 	if (cryptoAmount && (parseFloat(cryptoAmount) > parseFloat(selectedAsset?.balance))) {
		// 		this.setState({ messageError: "Không đủ số dư", checkInfoSend: false })
		// 	} else {
		// 		this.setState({ messageError: "Kiểm tra thông tin trước khi gửi", checkInfoSend: true })
		// 	}
		// } else {
		// 	this.setState({ messageError: "Hãy nhập số lượng cần gửi", checkInfoSend: false })
		// }
	}
	/**
	 * Updates internalPrimaryCurrency
	 */
	switchPrimaryCurrency = async () => {
		const { internalPrimaryCurrency, secondaryAmount, selectedAsset } = this.state;
		const primarycurrencies = {
			ETH: 'Fiat',
			Fiat: 'ETH',
		};
		this.setState({ internalPrimaryCurrency: primarycurrencies[internalPrimaryCurrency] });
		// await this.setState({ internalPrimaryCurrency: primarycurrencies[internalPrimaryCurrency] });
		this.updateAmount(secondaryAmount.split(' ')[0], selectedAsset);
	};
	/**
	 * Generates payment request link and redirects to PaymentRequestSuccess view with it
	 * If there is an error, an error message will be set to display on the view
	 */
	onNext = () => {
		const { selectedAddress, navigation, chainId } = this.props;
		const { cryptoAmount, selectedAsset } = this.state;

		try {
			if (cryptoAmount && cryptoAmount > '0') {
				let eth_link;
				if (selectedAsset.isETH) {
					const amount = toWei(cryptoAmount).toString();
					eth_link = generateETHLink(selectedAddress, amount, chainId);
				} else {
					const amount = toTokenMinimalUnit(cryptoAmount, selectedAsset.decimals).toString();
					eth_link = generateERC20Link(selectedAddress, selectedAsset.address, amount, chainId);
				}

				// Convert to universal link / app link
				const link = generateUniversalLinkRequest(eth_link);

				navigation &&
					navigation.replace('PaymentRequestSuccess', {
						link,
						qrLink: eth_link,
						amount: cryptoAmount,
						symbol: selectedAsset.symbol,
					});
			} else {
				this.setState({ showError: true });
			}
		} catch (e) {
			this.setState({ showError: true });
		}
	};

	prepareTransactionToSend = (transaction) => {
		const {
			// transactionState: { transaction },
			showCustomNonce,
			gasEstimateType,
		} = this.props;
		const fromSelectedAddress = transaction.from
		const { LegacyTransactionData, EIP1559TransactionData, selectCustomGasFee, transactionGasFee } = this.state;
		const { nonce } = this.props.transaction;
		const transactionToSend = { ...transaction };

		const gasFee = selectCustomGasFee ? transactionGasFee : EIP1559TransactionData

		if (gasEstimateType === GAS_ESTIMATE_TYPES.FEE_MARKET) {
			transactionToSend.gas = gasFee.gasLimitHex;
			transactionToSend.maxFeePerGas = addHexPrefix(gasFee.suggestedMaxFeePerGasHex); //'0x2540be400'
			transactionToSend.maxPriorityFeePerGas = addHexPrefix(
				gasFee.suggestedMaxPriorityFeePerGasHex
			); //'0x3b9aca00';
			transactionToSend.estimatedBaseFee = addHexPrefix(gasFee.estimatedBaseFeeHex);
			delete transactionToSend.gasPrice;
		} else {
			transactionToSend.gas = LegacyTransactionData.suggestedGasLimitHex;
			transactionToSend.gasPrice = addHexPrefix(LegacyTransactionData.suggestedGasPriceHex);
		}

		transactionToSend.from = fromSelectedAddress;
		if (showCustomNonce && nonce) transactionToSend.nonce = BNToHex(nonce);

		return transactionToSend;
	};

	checkRemoveCollectible = () => {
		const {
			transactionState: { selectedAsset, assetType, transaction },
			chainId,
		} = this.props;
		const fromSelectedAddress = transaction.from
		if (assetType === 'ERC721' && chainId !== NetworksChainId.mainnet) {
			const { CollectiblesController } = Engine.context;
			this.props.removeFavoriteCollectible(fromSelectedAddress, chainId, selectedAsset);
			CollectiblesController.removeCollectible(selectedAsset.address, selectedAsset.tokenId);
		}
	};

	getCollectibleTranferTransactionProperties() {
		const {
			selectedAsset,
			transactionState: { transaction, transactionTo },
		} = this.props;

		const collectibleTransferTransactionProperties = {};

		const collectibleTransferInformation = collectiblesTransferInformation[selectedAsset.address.toLowerCase()];
		if (
			!collectibleTransferInformation ||
			(collectibleTransferInformation.tradable && collectibleTransferInformation.method === 'transferFrom')
		) {
			collectibleTransferTransactionProperties.data = generateTransferData('transferFrom', {
				fromAddress: transaction.from,
				toAddress: transactionTo,
				tokenId: toHexadecimal(selectedAsset.tokenId),
			});
		} else if (collectibleTransferInformation.tradable && collectibleTransferInformation.method === 'transfer') {
			collectibleTransferTransactionProperties.data = generateTransferData('transfer', {
				toAddress: transactionTo,
				amount: selectedAsset.tokenId.toString(16),
			});
		}
		collectibleTransferTransactionProperties.to = selectedAsset.address;
		collectibleTransferTransactionProperties.value = '0x0';

		return collectibleTransferTransactionProperties;
	}

	onSend = async () => {
		const { TransactionController, KeyringController } = Engine.context;
		const {
			transactionState: { assetType },
			navigation,
			resetTransaction,
			gasEstimateType,
		} = this.props;

		const { amount, selectedAsset } = this.state
		// const { EIP1559TransactionData, LegacyTransactionData, transactionConfirmed } = this.state;
		// if (transactionConfirmed) return;
		// this.setState({ transactionConfirmed: true, stopUpdateGas: true });
		try {
			// Cập nhật trạng thái load
			this.setState({ loading: true })
			const result_transaction = await this.prepareTransaction(amount, selectedAsset)
			const transaction = this.prepareTransactionToSend(result_transaction);
			// console.log('--------transaction : ', transaction)
			// let error;
			// if (gasEstimateType === GAS_ESTIMATE_TYPES.FEE_MARKET) {
			// 	error = this.validateAmount({ transaction, total: EIP1559TransactionData.totalMaxHex });
			// } else {
			// 	error = this.validateAmount({ transaction, total: LegacyTransactionData.totalHex });
			// }
			// this.setError(error);
			// if (error) {
			// 	this.setState({ transactionConfirmed: false, stopUpdateGas: true });
			// 	return;
			// }

			const { result, transactionMeta } = await TransactionController.addTransaction(
				transaction,
				TransactionTypes.MMM,
				WalletDevice.MM_MOBILE
			);
			// console.log('transactionMeta : ', transactionMeta, result)
			await KeyringController.resetQRKeyringState();
			await TransactionController.approveTransaction(transactionMeta.id);

			await new Promise((resolve) => resolve(result));
			if (transactionMeta.error) {
				throw transactionMeta.error;
			}

			InteractionManager.runAfterInteractions(() => {
				// Cập nhật trạng thái load
				this.setState({ loading: false })
				// Đóng thông tin giao dịch
				this.setState({ paymentDetail: false })
				setTimeout(() => {
					// Hiển thị thông tin gửi thành công
					this.setState({ transactionConfirmed: true })
				}, 400)
				// NotificationManager.watchSubmittedTransaction({
				// 	...transactionMeta,
				// 	assetType,
				// });
			});
		} catch (error) {
			if (!error?.message.startsWith(KEYSTONE_TX_CANCELED)) {
				Alert.alert(strings('transactions.transaction_error'), error && error.message, [
					{
						text: strings('navigation.ok'), onPress: () => {
							this.setState({ loading: false })
							// Đóng thông tin giao dịch
							this.setState({ paymentDetail: false })
						}
					},
				]);
			}
			// console.log('--------error : ', error)
			// if (!error?.message.startsWith(KEYSTONE_TX_CANCELED)) {
			// 	Alert.alert(strings('transactions.transaction_error'), error && error.message, [{ text: 'OK' }]);
			// 	Logger.error(error, 'error while trying to send transaction (Confirm)');
			// } else {
			// 	AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.QR_HARDWARE_TRANSACTION_CANCELED);
			// }
		}
	};

	renderLogo = (asset, styles) => {
		const { address, isETH, iconUrl, thumbnails } = asset;
		if (isETH) {
			return <NetworkMainAssetLogo url={thumbnails || iconUrl} big style={styles.ethLogo} />;
		}
		if (!iconUrl && !thumbnails) {
			return <Identicon address={address} customStyle={styles.ethLogo} />;
		}
		return <AssetIcon logo={thumbnails || iconUrl} customStyle={styles.ethLogo} />;
	}

	renderLogoReview = (asset, styles) => {
		const { address, isETH, iconUrl, thumbnails } = asset;
		if (isETH) {
			return <NetworkMainAssetLogo url={thumbnails || iconUrl} big style={styles.ethLogoReview} />;
		}
		if (!iconUrl && !thumbnails) {
			return <Identicon address={address} customStyle={styles.ethLogoReview} />;
		}
		return <AssetIcon logo={thumbnails || iconUrl} customStyle={styles.ethLogoReview} />;
	}

	handleUnhandledTouches() {
		Keyboard.dismiss();
		return false;
	}

	calculateTempGasFee = (gas, selected) => {
		const {
			transactionState: { transaction },
			gasEstimateType
		} = this.props;
		if (selected && gas) {
			gas.suggestedGasLimit = fromWei(transaction.gas, 'wei');
		}
		if (gasEstimateType === GAS_ESTIMATE_TYPES.FEE_MARKET) {
			const data = this.parseTransactionDataEIP1559({ ...gas, selectedOption: selected })
			this.setState({ transactionGasFee: data });
		} else if (this.props.gasEstimateType !== GAS_ESTIMATE_TYPES.NONE) {
			const data = this.parseTransactionDataLegacy({ ...gas, selectedOption: selected })
			this.setState({ transactionGasFee: data });
		}
	};

	calculateOptionGasFee = (gas, option) => {
		try {
			const {
				transactionState: { transaction },
				gasEstimateType, gasFeeEstimates
			} = this.props;
			let data = []
			if (gasEstimateType === GAS_ESTIMATE_TYPES.FEE_MARKET) {
				for (let index = 0; index < option.length; index++) {
					const element = option[index];
					const gasFee = { ...gas[element] }
					if (element && gas[element]) {
						gasFee.suggestedGasLimit = fromWei(transaction.gas, 'wei');
						const result = this.parseTransactionDataEIP1559({ ...gasFee, selectedOption: element })
						data.push(result)
					}
				}
				// "gasFeeEstimates": {"high": "5", "low": "5", "medium": "5"} // GAS_ESTIMATE_TYPES.LEGACY
				// "gasFeeEstimates": {"gasPrice": "5"}
			} else if (this.props.gasEstimateType !== GAS_ESTIMATE_TYPES.NONE) {
				for (let index = 0; index < option.length; index++) {
					const element = option[index];
					const gasFee = gasEstimateType === GAS_ESTIMATE_TYPES.LEGACY
						? {
							suggestedGasPrice: gasFeeEstimates[element]
						}
						: {
							suggestedGasPrice: gasFeeEstimates.gasPrice
						}
					gasFee.suggestedGasLimit = fromWei(transaction.gas, 'wei');
					gasFee.selectedOption = element
					const result = this.parseTransactionDataLegacy(gasFee)
					data.push(result)
				}
			}
			return data
		} catch (error) {
			console.log('calculateOptionGasFee error : ', error)
			return []
		}
	};
	/**
	 * Renders a view that allows user to set payment request amount
	 */
	renderEnterAmount = (gasFee) => {
		const { network, conversionRate, contractExchangeRates, currentCurrency, ensRecipient, transactionTo, transactionToName, gasEstimateType, primaryCurrency, chainId } = this.props;
		const { estimatedTotalGas, transactionGasFee, selectCustomGasFee, amount, secondaryAmount, symbol, cryptoAmount, showError,
			selectedAsset, internalPrimaryCurrency, messageError, checkInfoSend, selectGasFee } = this.state;
		const currencySymbol = currencySymbols[currentCurrency];
		const exchangeRate = selectedAsset && selectedAsset.address && contractExchangeRates[selectedAsset.address];
		let switchable = true;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);

		let name = ""
		if (network.provider?.nickname) {
			name = network.provider.nickname;
		} else {
			name =
				(Networks[network.provider.type] && Networks[network.provider.type].name) ||
				{ ...Networks.rpc, color: null }.name;
		}

		if (!conversionRate) {
			switchable = false;
		} else if (selectedAsset?.symbol !== 'ETH' && !exchangeRate) {
			switchable = false;
		}


		const showFeeMarket =
			!gasEstimateType ||
			gasEstimateType === GAS_ESTIMATE_TYPES.FEE_MARKET ||
			gasEstimateType === GAS_ESTIMATE_TYPES.NONE;

		const isMainnet = isMainnetByChainId(chainId);

		const nativeCurrencySelected = primaryCurrency === 'ETH' || !isMainnet;

		// console.log('transactionState : ', transactionState)
		// 	"estimatedBaseFee":"0.075407109",
		//    "estimatedBaseFeeHex":"47e9f05",
		//    "gasFeeMaxConversion":"0.06",
		//    "gasFeeMaxNative":"0.000034",
		//    "gasFeeMinConversion":"0.06",
		//    "gasFeeMinNative":"0.000033",
		//    "gasLimitHex":"0x5208",
		//    "maxPriorityFeeConversion":"0.06",
		//    "maxPriorityFeeNative":"0.000031",
		//    "renderableGasFeeMaxConversion":"0,06 US$",
		//    "renderableGasFeeMaxNative":"0.000034 ETH",
		//    "renderableGasFeeMinConversion":"0,06 US$",
		//    "renderableGasFeeMinNative":"0.000033 ETH",
		//    "renderableMaxFeePerGasConversion":"0,06 US$",
		//    "renderableMaxFeePerGasNative":"0.000034 ETH",
		//    "renderableMaxPriorityFeeConversion":"0,06 US$",
		//    "renderableMaxPriorityFeeNative":"0.000031 ETH",
		//    "renderableTotalMaxConversion":"0,06 US$",
		//    "renderableTotalMaxNative":"0.000034 ETH",
		//    "renderableTotalMinConversion":"0,06 US$",
		//    "renderableTotalMinNative":"0.000033 ETH",
		//    "suggestedGasLimit":"21000",
		//    "suggestedMaxFeePerGas":"1.601799598",
		//    "suggestedMaxFeePerGasHex":"5f7985ae",
		//    "suggestedMaxPriorityFeePerGas":"1.5",
		//    "suggestedMaxPriorityFeePerGasHex":"59682f00",
		//    "timeEstimate":"30 giây",
		//    "timeEstimateColor":"green",
		//    "timeEstimateId":"likely",
		//    "totalMaxConversion":"0.06",
		//    "totalMaxHex":"1e97e89de970",
		//    "totalMaxNative":"0.000034",
		//    "totalMinConversion":"0.06",
		//    "totalMinHex":"1e16dd340a28",
		//    "totalMinNative":"0.000033"
		// const option = ['low', 'medium', 'high']
		const optionText = ['Chuẩn', 'Nhanh', 'Nhanh hơn']
		// const gasFee = this.calculateOptionGasFee(gasFeeEstimates, option)
		const fee = selectCustomGasFee ? transactionGasFee : gasFee[selectGasFee]

		let gasFeePrimary, renderableGasFeeMinNative, timeEstimate;

		if (nativeCurrencySelected) {
			if (showFeeMarket) {
				gasFeePrimary = fee?.gasFeeMinConversion;
				timeEstimate = fee?.timeEstimate;
				renderableGasFeeMinNative = fee?.renderableGasFeeMinNative;
			} else {
				gasFeePrimary = fee?.transactionFeeFiat;
				renderableGasFeeMinNative = fee?.transactionFee;
			}
		}
		return (
			<View style={styles.enterAmountWrapper} onStartShouldSetResponder={this.handleUnhandledTouches}>
				{/* <View style={styles.label}>
					<Text numberOfLines={1} style={styles.labelText}>tới:
					</Text>
					<TextInput
						autoCapitalize="none"
						autoCorrect={false}
						numberOfLines={1}
						onFocus={() => {
							this.props.navigation.replace('SendFlowView', {
								screen: 'SendTo',
								params: {
									skipSelectAsset: true,
									toSelectedAddress: transactionToName,
									// toSelectedAddressName: transactionToName,
									// toSelectedAddressReady: true,
									// toEnsName: ensRecipient
								},
							});
						}}
						autoFocus={false}
						style={styles.inputAddress}
						value={` ${transactionToName || renderShortAddress(transactionTo)}`} />
				</View> */}
				<View style={styles.amountWrapper}>
					<View style={styles.container}>
						<View style={styles.ethContainer}>
							<View style={styles.amounts}>
								<Text style={[styles.fee, { textAlign: 'center', marginTop: -6, fontSize: 13, fontWeight: '100' }]}>{`Mạng: ${name}`}</Text>
								<View style={styles.split}>
									<TextInput
										autoCapitalize="none"
										autoCorrect={false}
										keyboardType="numeric"
										numberOfLines={1}
										onChangeText={(value) => this.updateAmount(value, selectedAsset)}
										placeholder={`Nhập số ${symbol || selectedAsset.symbol}`}
										placeholderTextColor={'#58586C'}
										spellCheck={false}
										style={styles.input}
										value={amount}
										// onSubmitEditing={this.onNext}
										autoFocus
										// ref={this.amountInput}
										// testID={'request-amount-input'}
										keyboardAppearance={themeAppearance}
									/>
									<View style={{
										flexDirection: 'row',
										alignItems: 'center'
									}}>
										<Pressable disabled={!estimatedTotalGas}
											style={{
												padding: 5, backgroundColor: 'rgba(255, 255, 255, 0.1)',
												borderRadius: 30,
												marginRight: 10
											}} onPress={this.useMax}>
											<Text style={styles.max} numberOfLines={1}>
												{'Max'}
											</Text>
										</Pressable>
										<Pressable style={styles.assetContainer}
											onPress={() => this.setState({ mode: MODE_SELECT })}>
											{this.renderLogo(selectedAsset, styles)}
											<Text style={styles.textName} numberOfLines={1}>{selectedAsset.symbol}</Text>
											<Image source={require('../../../images/arrowRight.png')} resizeMode={"contain"} style={styles.iconArrow}></Image>
										</Pressable>
									</View>
								</View>
								<Text style={styles.textSymbol} numberOfLines={1}>{`Số dư : ${selectedAsset.balance} ${selectedAsset.symbol}`}</Text>
								<View style={styles.secondaryAmount}>
									{/* <Text style={styles.currencySymbolSmall}>{'~'}</Text> */}
									{/* <TextInput
										autoCapitalize="none"
										autoCorrect={false}
										keyboardType="numeric"
										numberOfLines={1}
										onChangeText={(value) => this.updateSecondaryAmount(value, selectedAsset)}
										placeholder={`Nhập số USD`}
										placeholderTextColor={'#58586C'}
										spellCheck={false}
										style={styles.input}
										value={secondaryAmount}
										// value={Number(secondaryAmount) || ""}
										// onSubmitEditing={this.onNext}
										// autoFocus
										// ref={this.amountInput}
										// testID={'request-amount-input'}
										keyboardAppearance={themeAppearance}
									/> */}
									{/* {secondaryAmount && (
										<Text style={styles.fiatValue} numberOfLines={1}>
											{secondaryAmount}
										</Text>
									)} */}
									<Text style={[styles.fiatValue, { color: "#58586C" }]} numberOfLines={1}>
										{`~ $${secondaryAmount || ""}`}
									</Text>
									{/* <Text style={styles.currencySymbolSmall}>{'USD'}</Text> */}
								</View>
							</View>
						</View>
					</View>
					{ // (gasFeePrimary && Number(gasFeePrimary) !== 0)
						gasFeePrimary !== undefined && <View style={styles.feeContainer}>
							{
								showFeeMarket ? <View>
									<Text style={styles.feeEstimate}>{'Phí gas ước tính'}</Text>
									<Text style={styles.fee}>
										{`${renderableGasFeeMinNative} - ${timeEstimate}`}
									</Text>
									<Text style={[styles.fee, { color: '#58586C' }]}>
										{`${(gasFeePrimary && Number(gasFeePrimary) !== 0) ? '~ $' + gasFeePrimary : '< $0.01'}`}
										{/* {`${'~ $' + gasFeePrimary}`} */}
									</Text>
								</View> : <View>
									<Text style={styles.feeEstimate}>{'Phí gas ước tính'}</Text>
									<Text style={styles.fee}>{`${renderableGasFeeMinNative}`}</Text>
									<Text style={[styles.fee, { color: '#58586C' }]}>
										{/* {`~ ${gasFeePrimary}`} */}
										{`${(gasFeePrimary && Number(gasFeePrimary) !== 0) ? `~ ${gasFeePrimary}` : '< $0.01'}`}
									</Text>
								</View>
							}
							<Tooltip
								isVisible={this.state.toolTipVisible}
								contentStyle={{
									padding: 0,
									borderRadius: 15,
								}}
								backgroundStyle={{
									backgroundColor: 'transparent',
								}}
								content={<View style={{
									padding: 15,
									paddingTop: 0,
									paddingRight: 50,
									backgroundColor: '#1B1B23',
									borderColor: 'rgba(255, 255, 255, 0.1)',
									borderWidth: 1,
									borderRadius: 15,
								}}>
									{
										gasFee.length !== 0 && gasFee.map((item, index) => {
											return <Pressable
												key={`ii${index}`}
												style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}
												onPress={() => this.setState({ selectGasFee: index, selectCustomGasFee: false })}
											>
												<Image source={require('../../../images/check.png')}
													resizeMode={"contain"}
													style={[styles.iconCheck, { tintColor: selectGasFee === index ? '#2BEB4A' : "#3D3D4B" }]}>
												</Image>
												<View>
													<Text style={styles.feeEstimate}>{optionText[index]}</Text>
													{
														showFeeMarket
															? <Text style={styles.fee}>
																{`${(gasFeePrimary && Number(gasFeePrimary) !== 0) ? '$' + gasFee[index].gasFeeMinConversion : gasFee[index].renderableGasFeeMinNative}`}
															</Text>
															: <Text style={styles.fee}>
																{`${(gasFeePrimary && Number(gasFeePrimary) !== 0) ? gasFee[index].transactionFeeFiat : gasFee[index].transactionFee}`}
															</Text>
													}
												</View>
											</Pressable>
										})
									}
									{/* <Pressable style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>
										<Image source={require('../../../images/check.png')} resizeMode={"contain"} style={styles.iconCheck}></Image>
										<View>
											<Text style={styles.feeEstimate}>{'Chuẩn'}</Text>
											<Text style={styles.fee}>{'$24,30'}</Text>
										</View>
									</Pressable>
									<Pressable style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>
										<Image source={require('../../../images/check.png')} resizeMode={"contain"} style={[styles.iconCheck, { tintColor: '#2BEB4A' }]}></Image>
										<View>
											<Text style={styles.feeEstimate}>{'Nhanh'}</Text>
											<Text style={styles.fee}>{'$40,30'}</Text>
										</View>
									</Pressable>
									<Pressable style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>
										<Image source={require('../../../images/check.png')} resizeMode={"contain"} style={styles.iconCheck}></Image>
										<View>
											<Text style={styles.feeEstimate}>{'Nhanh hơn'}</Text>
											<Text style={styles.fee}>{'$80,30'}</Text>
										</View>
									</Pressable> */}
									<View style={{ height: 1, backgroundColor: 'rgba(255, 255, 255, 0.1)', marginVertical: 15 }} />
									<Pressable style={{ flexDirection: 'row', alignItems: 'center' }}
										onPress={() => {
											this.setState({ toolTipVisible: false })
											this.setState({ isVisibleCustom: true })
										}}>
										<Image source={require('../../../images/setting2.png')} resizeMode={"contain"} style={styles.iconCheck}></Image>
										<Text style={[styles.fee, { marginTop: 0 }]}>{'Tuỳ chỉnh'}</Text>
									</Pressable>
								</View>}
								placement="top"
								onClose={() => this.setState({ toolTipVisible: false })}>
								<Pressable hitSlop={20} onPress={() => {
									// this.switchPrimaryCurrency()
									if (gasFee.length !== 0) {
										this.setState({ toolTipVisible: true })
									}
								}}>
									<Image source={require('../../../images/setting.png')} resizeMode={"contain"} style={styles.iconSetting}></Image>
								</Pressable>
							</Tooltip>
						</View>
					}

				</View>
				<KeyboardAvoidingView
					style={styles.buttonsWrapper}
					behavior={'padding'}
					keyboardVerticalOffset={KEYBOARD_OFFSET}
					enabled={Device.isIos()}
				>
					<View style={styles.buttonsContainer}>
						<TouchableOpacity
							disabled={!checkInfoSend}
							style={[styles.buttonStyle, { backgroundColor: checkInfoSend ? "#6A45FF" : '#3D3D4B' }]}
							onPress={() => {
								// Hiển thị thông tin chi tiết giao dịch
								this.setState({ paymentDetail: true })
							}}>
							<Text style={styles.content}>{messageError}</Text>
						</TouchableOpacity>
					</View>
				</KeyboardAvoidingView>
			</View>
		);
	};

	goToPage(pageId) {
		this.tabView.goToPage(pageId);
	}

	renderTabBar() {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<DefaultTabBar
				underlineStyle={styles.tabUnderlineStyle}
				activeTextColor={'#FFFFFF'}
				inactiveTextColor={'#FFFFFF'}
				backgroundColor={'#282832'}
				tabStyle={styles.tabStyle}
				textStyle={styles.textStyle}
				style={styles.tabBar}
			/>
		);
	}

	tryExportSeedPhrase = async (password) => {
		const { KeyringController } = Engine.context;
		const { selectedAddress } = this.props;
		let address = selectedAddress;

		let mnemonic = await KeyringController.exportSeedPhrase(password);
		try {
			const accKeyring = await KeyringController.getAccountKeyring(address);
			// Dont know why it has to be like that
			let privKey = accKeyring.hdWallet._hdkey;
			privKey = JSON.parse(JSON.stringify(privKey));
			mnemonic = await KeyringController.exportSeedPhraseByWallet(password, privKey.xpriv);
		} finally {
			// const seed = JSON.stringify(mnemonic).replace(/"/g, '').split(' ');
			// return seed;
		}
		const seed = JSON.stringify(mnemonic).replace(/"/g, '').split(' ');
		return seed;
	};

	tryExportPrivateKey = async (password) => {
		const { KeyringController } = Engine.context;
		const { selectedAddress } = this.props;
		let address = selectedAddress
		const privateKey = await KeyringController.exportAccount(password, address);
		return privateKey;
	};

	tryUnlockWithPassword = async (password) => {
		// this.setState({ ready: false });
		try {
			this.words = await this.tryExportSeedPhrase(password);
			this.privateKey = await this.tryExportPrivateKey(password);
			this.setState({ authTransaction: false });
			this.onSend()
		} catch (e) {
			let msg = strings('reveal_credential.warning_incorrect_password');
			if (e.toString().toLowerCase() !== WRONG_PASSWORD_ERROR.toLowerCase()) {
				msg = strings('reveal_credential.unknown_error');
			}
			this.setState({ warningIncorrectPassword: msg });
		}
	};

	tryUnlock = async (pin) => {
		//const { password } = this.state;
		const hasPin = await hasUserSetPinCode();
		if (hasPin) {
			this.tryUnlockWithPassword(pin);
		}
	};

	renderConfirmPassword() {
		const { warningIncorrectPassword } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);

		return (
			<View style={styles.authWrapper}>
				{/* <PINCode
					titleEnter={'Nhập mã PIN'}
					subtitleEnter={'Mã gồm 6 chữ số'}
					titleAttemptFailed={'Mã PIN không khớp'}
					subtitleError={'Vui lòng thử lại'}
					passwordLength={6}
					stylePinCodeCircle={styles.dots}
					disableLockScreen
					status={'enter'}
					touchIDDisabled
					finishProcess={(pin) => this.tryUnlock(pin)}
				/> */}
				<PINCode
					titleChoose={strings('pin_code.title_choose')}
					subtitleChoose={strings('pin_code.subtitle')}
					titleConfirm={strings('pin_code.title_confirm')}
					subtitleConfirm={strings('pin_code.subtitle_confirm')}
					titleConfirmFailed={strings('pin_code.title_confirm_failed')}
					subtitleError={strings('pin_code.subtitl_error')}
					buttonDeleteText={strings('pin_code.button_delete_text')}
					passwordLength={6}
					disableLockScreen
					status={'enter'}
					touchIDDisabled
					finishProcess={(pin) => this.tryUnlock(pin)}
					stylePinCodeTextTitle={styles.titlePinCode}
					stylePinCodeTextSubtitle={styles.subtitlePinCode}
					stylePinCodeCircle={styles.dots}
					stylePinCodeTextButtonCircle={styles.pinCodeTextButtonCircle}
					stylePinCodeButtonCircle={styles.pinCodeButtonCircle}
					stylePinCodeDeleteButtonText={styles.pinCodeDeleteButtonText}
				/>
			</View>
		);
	}

	pateAddress = async () => {
		const text = await Clipboard.getString();
		this.setState({ searchInputValue: text });
		this.searchInput?.current?.focus();
	}

	isAddressSaved = () => {
		const checksummedResolvedAddress = toChecksumAddress(this.props.transactionTo);
		const networkAddressBook = this.props.addressBook[this.props.network.network] || {};
		if (networkAddressBook[checksummedResolvedAddress] || this.props.identities[checksummedResolvedAddress]) {
			return true;
		}
		return false;
	};

	onSaveToAddressBookPress = () => {
		this.setState({
			saveAddressModal: {
				address: this.props.transactionTo  // this.state.toSelectedAddress,
			},
		});
	};

	onSaveToAddressBookCancel = () => {
		this.setState({
			saveAddressModal: null,
		});
	};

	onSaveToAddressBookSubmit = ({ address, newName }) => {
		this.setState({
			saveAddressModal: null,
		});
		const { network } = this.props;
		const { AddressBookController } = Engine.context;
		AddressBookController.set(toChecksumAddress(address), newName, network.network, '', '');
		this.setState({ newAddressName: newName })
		NotificationManager.showSimpleNotification({
			status: 'success',
			duration: 1500,
			title: strings('address_book.add_contact_success_title'),
			description: strings('address_book.add_contact_success_description').replace(
				'{name}',
				newName || renderShortAddress(address)
			),
		});
	};

	render() {
		const { warningSelectedAsset, newAddressName, saveAddressModal, searchInputValue, authTransaction, selectCustomGasFee, transactionGasFee, selectGasFee, paymentDetail, mode, tokenWarning, amount, selectedAsset, loading, transactionConfirmed } = this.state;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		const themeAppearance = this.context.themeAppearance || 'light';

		const { tokenList, currentCurrency, conversionRate, selectedAddress, accounts, tokens, ticker, gasFeeEstimates, primaryCurrency, network, transactionToName, transactionTo, chainId, navigation, gasEstimateType } = this.props;
		let name = ""
		if (network.provider?.nickname) {
			name = network.provider.nickname;
		} else {
			name =
				(Networks[network.provider.type] && Networks[network.provider.type].name) ||
				{ ...Networks.rpc, color: null }.name;
		}
		const isMainnet = isMainnetByChainId(chainId);
		const nativeCurrencySelected = primaryCurrency === 'ETH' || !isMainnet;

		const showFeeMarket =
			!gasEstimateType ||
			gasEstimateType === GAS_ESTIMATE_TYPES.FEE_MARKET ||
			gasEstimateType === GAS_ESTIMATE_TYPES.NONE;

		const option = ['low', 'medium', 'high']
		const gasFee = mode === MODE_AMOUNT ? this.calculateOptionGasFee(gasFeeEstimates, option) : []
		const fee = selectCustomGasFee ? transactionGasFee : gasFee[selectGasFee]
		let gasFeePrimary, renderableGasFeeMinNative;
		if (nativeCurrencySelected) {
			if (showFeeMarket) {
				gasFeePrimary = fee?.gasFeeMinNative;
				renderableGasFeeMinNative = fee?.renderableGasFeeMinNative;
			} else {
				gasFeePrimary = fee?.transactionFeeFiat;
				renderableGasFeeMinNative = fee?.transactionFee;
			}
		}
		/////////////////
		let balance = 0;
		let assets = tokens;
		let tokenMainnet = null;
		if (ticker) {
			tokenMainnet = getTokenByTicker(ticker);
		} else {
			tokenMainnet = getTokenByTicker('ETH');
		}
		if (accounts[selectedAddress]) {
			balance = renderFromWei(accounts[selectedAddress].balance);
			assets = [
				{
					name: tokenMainnet ? tokenMainnet.name : ticker,
					symbol: getTicker(ticker),
					isETH: true,
					balance,
					balanceFiat: weiToFiat(
						hexToBN(accounts[selectedAddress].balance),
						conversionRate,
						currentCurrency
					),
					logo: '',
					coingeckoId: tokenMainnet ? tokenMainnet.coinGeckoId : ticker,
					address: swapsUtils.getNativeSwapsToken(chainId).address,
					addressName: tokenMainnet ? tokenMainnet.coinGeckoId : ticker,
					iconUrl: tokenMainnet ? tokenMainnet.thumbnail : '',
					verified: true,
				},
				...(tokens || []),
			];
		} else {
			assets = tokens;
		}

		var listTokens = assets.map(t1 => ({
			...tokenList.find(t2 => t2.address.toLowerCase() === t1.address.toLowerCase()),
			...t1
		}));

		if (searchInputValue) {
			listTokens = listTokens
				.concat([...tokenList].filter((token) => !listTokens.map(item => item.address?.toLowerCase()).includes(token.address?.toLowerCase())))
		}

		const tokenFuse = new Fuse(listTokens, {
			shouldSort: true,
			threshold: 0.45,
			location: 0,
			distance: 100,
			maxPatternLength: 32,
			minMatchCharLength: 1,
			keys: ['symbol', 'address', 'name'],
		})

		const tokenSearchResults = searchInputValue.length > 0
			? tokenFuse.search(searchInputValue)
			: listTokens

		if (authTransaction) {
			return <PrivateCredential
				onSend={this.onSend}
				onBack={() => this.setState({ authTransaction: false })} />
		}

		return (
			<SafeAreaView style={styles.wrapper}>
				<View style={styles.titleWrapper}>
					<View style={styles.dragger} />
				</View>
				<View style={styles.titleWrapper}>
					<Text style={{
						color: '#FFFFFF',
						fontSize: 18,
						fontWeight: "500"
					}}>{'Gửi'}</Text>
				</View>
				<View style={styles.labelTextWrapper}>
					<View style={styles.label}>
						<Text numberOfLines={1} style={styles.labelText}>tới:
						</Text>
						<TextInput
							autoCapitalize="none"
							autoCorrect={false}
							numberOfLines={1}
							onFocus={() => {
								this.props.navigation.replace('SendFlowView', {
									screen: 'SendTo',
									params: {
										skipSelectAsset: mode === MODE_AMOUNT,
										toSelectedAddress: newAddressName || transactionToName,
										// toSelectedAddressName: transactionToName,
										// toSelectedAddressReady: true,
										// toEnsName: ensRecipient
									},
								});
							}}
							autoFocus={false}
							style={styles.inputAddress}
							value={` ${newAddressName || transactionToName || renderShortAddress(transactionTo)}`}
						/>
					</View>
					{!this.isAddressSaved() && (
						<TouchableOpacity
							style={styles.saveButton}
							onPress={this.onSaveToAddressBookPress}
						>
							<Image style={styles.saveButtonImage} source={saveAddressIcon} />
						</TouchableOpacity>
					)}
				</View>
				{
					mode === MODE_SELECT
						? <>
							<ScrollableTabView
								initialPage={0}
								renderTabBar={() => this.renderTabBar()}
								ref={(tabView) => {
									this.tabView = tabView;
								}}>
								<KeyboardAwareScrollView
									tabLabel={"Token"}
									showsVerticalScrollIndicator={false}
									style={styles.contentWrapper}
									contentContainerStyle={styles.scrollViewContainer}
									keyboardShouldPersistTaps="handled"
								>
									{this.renderSelectAssets(tokenSearchResults.slice(0, 8))}
								</KeyboardAwareScrollView>
								<KeyboardAwareScrollView
									tabLabel={"NFT"}
									showsVerticalScrollIndicator={false}
									style={styles.contentWrapper}
									contentContainerStyle={styles.scrollViewContainer}
									keyboardShouldPersistTaps="handled"
								>
								</KeyboardAwareScrollView>
							</ScrollableTabView>
						</>
						: this.renderEnterAmount(gasFee)
				}
				{
					(gasFee.length !== 0 && this.state.isVisibleCustom)
						? showFeeMarket ? <SheetActions
							transactionGasFee={this.state.transactionGasFee}
							gasOptions={gasFeeEstimates}
							gasFeeOptions={gasFee}
							isVisible={this.state.isVisibleCustom}
							onCalculateGasFee={(gasCustomFee) => {
								this.calculateTempGasFee(gasCustomFee, null)
							}}
							onSaveCustom={(feeGas) => {
								this.setState({ selectCustomGasFee: true, isVisibleCustom: false, transactionGasFee: feeGas })
							}}
							closeModal={() => this.setState({ isVisibleCustom: false })}
						/>
							: <SheetActionsLegacy
								transactionGasFee={this.state.transactionGasFee}
								gasOptions={gasFeeEstimates}
								gasFeeOptions={gasFee}
								isVisible={this.state.isVisibleCustom}
								onCalculateGasFee={(gasCustomFee) => {
									this.calculateTempGasFee(gasCustomFee, null)
								}}
								onSaveCustom={(feeGas) => {
									this.setState({ selectCustomGasFee: true, isVisibleCustom: false, transactionGasFee: feeGas })
								}}
								closeModal={() => this.setState({ isVisibleCustom: false })}
							/>
						: null
				}

				<PaymentDetail isVisible={mode === MODE_AMOUNT && paymentDetail}
					name={name}
					transactionTo={transactionTo}
					amount={amount}
					gasFeePrimary={gasFeePrimary}
					renderableGasFeeMinNative={renderableGasFeeMinNative}
					onSend={() => this.setState({ authTransaction: true })}
					loading={loading}
					symbol={selectedAsset?.symbol}
					logo={this.state.selectedAsset ? this.renderLogoReview(this.state.selectedAsset, styles) : null}
					closeModal={() => this.setState({ paymentDetail: false })}
				/>
				<OrderExecuted
					isVisible={mode === MODE_AMOUNT && transactionConfirmed}
					name={name}
					loading={loading}
					transactionTo={transactionTo}
					amount={amount}
					gasFeePrimary={gasFeePrimary}
					renderableGasFeeMinNative={renderableGasFeeMinNative}
					onExecuted={() => {
						this.checkRemoveCollectible();
						resetTransaction();
						this.setState({ transactionConfirmed: false })
						navigation && navigation.popToTop();
					}}
					symbol={selectedAsset?.symbol}
					logo={this.state.selectedAsset ? this.renderLogo(this.state.selectedAsset, styles) : null}
				/>
				<WalletModal
					warningModalVisible={tokenWarning}
					onCancelPress={() => this.setState({ tokenWarning: false })}
					displayConfirmButton={false}
					displayCancelButton={false}
					confirmText={strings('accounts.ok')}
					cancelText={strings('accounts.cancel')}>
					<View style={{
						flex: 1,
						padding: 20
					}}>
						<View style={{
							alignItems: 'center'
						}}>
							<Image source={require('../../../images/danger.png')} resizeMode={"contain"} style={styles.iconWarning}></Image>
						</View>
						<Text style={styles.titleWarning}>
							{'Cảnh báo: Token không đáng tin cậy'}</Text>
						<View style={{
							alignItems: 'center',
							marginTop: 20
						}}>
							<Image source={require('../../../images/ink.png')} resizeMode={"contain"} style={styles.warning}></Image>
						</View>
						<Text style={[styles.titleWarning, { fontWeight: '100', fontSize: 16 }]}>
							{'Việc giao dịch với token chưa được đánh giá có thể dẫn đến mất tài sản. Vui lòng kiểm tra kĩ địa chỉ token và thanh khoản trước khi giao dịch.'}</Text>
						<Pressable style={[styles.btnWarning, { backgroundColor: '#6A45FF' }]} onPress={() => {
							this.setState({ tokenWarning: false })
							this.goToAmountInput(warningSelectedAsset)
						}}>
							<Text style={styles.contentBtnWarning}>
								{'Tôi đã hiểu nguy cơ và đồng ý giao dịch'}
							</Text>
						</Pressable>
						<Pressable style={styles.btnWarning} onPress={() => this.setState({ tokenWarning: false })}>
							<Text style={styles.contentBtnWarning}>
								{'Huỷ'}
							</Text>
						</Pressable>
					</View>
				</WalletModal>
				<AddressSaveModal
					isVisible={!!saveAddressModal}
					address={saveAddressModal?.address}
					onAddressSaveCancel={this.onSaveToAddressBookCancel}
					onAddressSaveSubmit={this.onSaveToAddressBookSubmit}
				/>
			</SafeAreaView>
		);
	}
}

PaymentRequest.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	accounts: state.engine.backgroundState.AccountTrackerController.accounts,
	conversionRate: state.engine.backgroundState.CurrencyRateController.conversionRate,
	currentCurrency: state.engine.backgroundState.CurrencyRateController.currentCurrency,
	nativeCurrency: state.engine.backgroundState.CurrencyRateController.nativeCurrency,
	contractExchangeRates: state.engine.backgroundState.TokenRatesController.contractExchangeRates,
	contractBalances: state.engine.backgroundState.TokenBalancesController.contractBalances,
	searchEngine: state.settings.searchEngine,
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
	tokens: state.engine.backgroundState.TokensController.tokens,
	primaryCurrency: state.settings.primaryCurrency,
	ticker: state.engine.backgroundState.NetworkController.provider.ticker,
	chainId: state.engine.backgroundState.NetworkController.provider.chainId,
	tokenList: getTokenListArray(state),
	selectedAsset: state.transaction.selectedAsset,
	showCustomNonce: state.settings.showCustomNonce,
	// transaction
	transactionState: state.transaction,
	transaction: state.transaction.transaction,
	ensRecipient: state.transaction.ensRecipient,
	transactionTo: state.transaction.transactionTo,
	transactionToName: state.transaction.transactionToName,
	transactionFromName: state.transaction.transactionFromName,
	//
	gasFeeEstimates: state.engine.backgroundState.GasFeeController.gasFeeEstimates,
	gasEstimateType: state.engine.backgroundState.GasFeeController.gasEstimateType,
	network: state.engine.backgroundState.NetworkController,
	//
	selectedAsset: state.transaction.selectedAsset,
	swapsTokens: swapsTokensSelector(state),
	identities: state.engine.backgroundState.PreferencesController.identities,
	addressBook: state.engine.backgroundState.AddressBookController.addressBook


});

const mapDispatchToProps = (dispatch) => ({
	setTransactionObject: (transaction) => dispatch(setTransactionObject(transaction)),
	prepareTransaction: (transaction) => dispatch(prepareTransaction(transaction)),
	setSelectedAsset: (selectedAsset) => dispatch(setSelectedAsset(selectedAsset)),
	resetTransaction: () => dispatch(resetTransaction()),
	removeFavoriteCollectible: (selectedAddress, chainId, collectible) =>
		dispatch(removeFavoriteCollectible(selectedAddress, chainId, collectible)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentRequest);
