import React, { useState } from 'react';
import {
    Pressable, StyleSheet, Text, View, Dimensions,
    Image, TextInput, KeyboardAvoidingView, Keyboard, ActivityIndicator, TouchableOpacity
} from 'react-native';
import Modal from 'react-native-modal';
import { fontStyles, baseStyles } from '../../../../styles/common';
import Device from '../../../../util/device';
import WalletModal from '../../../UI/WalletModal';
import { strings } from '../../../../../locales/i18n';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const KEYBOARD_OFFSET = 120;

function PaymentDetail({ isVisible, closeModal, logo, name,
    transactionTo,
    amount,
    symbol,
    renderableGasFeeMinNative,
    onSend,
    loading
}) {

    const [customWarning, showCustomWarning] = useState(false)

    function handleUnhandledTouches() {
        Keyboard.dismiss();
        return false;
    }

    return (
        <Modal
            hideModalContentWhileAnimating={true}
            useNativeDriver={true}
            isVisible={isVisible}
            animationIn={'fadeInUp'}
            animationOut={'fadeOutDown'}
            animationInTiming={200}
            animationOutTiming={200}
            onBackdropPress={() => loading ? null : closeModal(false)}
            onBackButtonPress={() => loading ? null : closeModal(false)}
            backdropOpacity={0.5}
            statusBarTranslucent
            deviceHeight={screenHeight}
            style={{
                flex: 1,
                margin: 0,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
            <KeyboardAvoidingView
                behavior={'padding'}
                // keyboardVerticalOffset={KEYBOARD_OFFSET}
                enabled={Device.isIos()}>
                <View style={{ width: screenWidth }} onStartShouldSetResponder={handleUnhandledTouches}>
                    <View style={{
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15,
                        backgroundColor: '#1B1B23',
                        paddingBottom: 30,
                        alignItems: 'center',
                        paddingHorizontal: 10
                    }}>
                        <Pressable style={styles.view}>
                            <Text style={styles.title}>{'Chi tiết giao dịch'}</Text>
                        </Pressable>
                        <View style={{
                            marginVertical: 10,
                            marginTop: 15,
                        }}>
                            {logo}
                        </View>
                        <Text style={styles.addressTitle}>{'Địa chỉ ví nhận'}</Text>

                        <Text style={styles.address}>{transactionTo}</Text>

                        <Text style={styles.networkTitle}>{'Mạng'}
                            <Text style={styles.network}>{`  ${name}`}</Text>
                        </Text>

                        <Text style={styles.networkTitle}>{'Bạn sẽ gửi'}
                            <Text style={styles.network}>{`  ${amount} ${symbol}`}</Text>
                        </Text>
                        <Text style={styles.networkTitle}>{'Phí gas ước tính'}
                            <Text style={styles.network}>{`  ${renderableGasFeeMinNative}`}</Text>
                        </Text>

                        {/* <Pressable style={styles.saveStyle} delayLongPress={2000} onLongPress={onSend}>
                            <Image source={require('../../../../images/faceId.png')} resizeMode={"contain"} style={styles.iconfaceId}></Image>
                            <Text style={styles.save}>
                                {'Giữ 2 giây để gửi'}
                            </Text>
                        </Pressable> */}
                        <TouchableOpacity style={styles.saveStyle} delayLongPress={2000} onLongPress={onSend}>
                            {
                                loading ? <ActivityIndicator size={"small"} /> : <>
                                    <Image source={require('../../../../images/faceId.png')} resizeMode={"contain"} style={styles.iconfaceId}></Image>
                                    <Text style={styles.save}>
                                        {'Giữ 2 giây để gửi'}
                                    </Text>
                                </>
                            }
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </Modal>
    );
}

const styles = StyleSheet.create({
    view: {
        paddingVertical: 12,
        alignItems: 'center',
    },
    line: {
        height: 1,
        backgroundColor: 'rgba(255, 255, 255, 0.15)',
        margin: 20,
        marginBottom: 0
    },
    title: {
        ...fontStyles.normal,
        fontSize: 18,
        color: '#FFFFFF',
        marginTop: 5,
        fontWeight: '500'
    },
    content: {
        ...fontStyles.normal,
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: '500',
        padding: 10
    },
    btnStyle: {
        borderColor: 'rgba(255, 255, 255, 0.1)',
        borderWidth: 1,
        borderRadius: 15,
    },
    feeEstimate: {
        fontSize: 14,
        color: '#A0A0B1',
        marginTop: 5,
        fontWeight: '500'
    },
    fee: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500'
    },
    iconSetting: {
        width: 30,
        height: 30,
        tintColor: '#6A45FF' // #58586C
    },
    iconCheck: {
        marginRight: 15,
        width: 25,
        height: 25,
        tintColor: '#3D3D4B', // '#2BEB4A'
    },
    iconInfo: {
        width: 20,
        height: 20,
        marginTop: 2,
        marginLeft: 3 // #EDA045
    },
    iconAdd: {
        width: 25,
        height: 25,
    },
    iconMinus: {
        width: 25,
        height: 25,
    },
    input: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        width: 100,
        borderColor: 'rgba(255, 255, 255, 0.1)',
        borderWidth: 1,
        borderRadius: 10,
        textAlign: 'center',
        paddingVertical: 10,
        marginHorizontal: 10
    },
    saveStyle: {
        flexDirection: 'row',
        backgroundColor: '#4A369A',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        width: screenWidth - 40,
        margin: 20,
        marginBottom: 0,
        padding: 15
    },
    save: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        // fontWeight: '500'
    },
    titleWarning: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 15
    },
    btnWarning: {
        backgroundColor: '#3D3D4B',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        marginTop: 15
    },
    contentBtnWarning: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
    },
    iconWarning: {
        width: 30,
        height: 30,
        tintColor: '#EDA045'
    },
    iconfaceId: {
        width: 20,
        height: 20,
        marginRight: 8
    },
    addressTitle: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#A0A0B1',
        // fontWeight: '500',
        textAlign: 'center',
        marginTop: 15
    },
    address: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 10
    },
    networkTitle: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#A0A0B1',
        // fontWeight: '500',
        textAlign: 'center',
        marginTop: 20
    },
    network: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 20,
    }
});
export default PaymentDetail;
