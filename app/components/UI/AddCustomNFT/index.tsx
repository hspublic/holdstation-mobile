import React, { useState, useEffect } from 'react';
import { useSelector, connect } from 'react-redux';
import { Alert, Text, TextInput, View, StyleSheet, TouchableOpacity, Dimensions, Image } from 'react-native';
import { fontStyles } from '../../../styles/common';
import Engine from '../../../core/Engine';
import { strings } from '../../../../locales/i18n';
import { isValidAddress } from 'ethereumjs-util';
// import ActionView from '../ActionView';
import ActionViewNFTs from '../ActionViewNFTs';
import { isSmartContractAddress } from '../../../util/transactions';
import Device from '../../../util/device';
import AnalyticsV2 from '../../../util/analyticsV2';
import { useAppThemeFromContext, mockTheme } from '../../../util/theme';
import NotificationManager from '../../../core/NotificationManager';
import CollectibleMedia from '../CollectibleMedia';
import Clipboard from '@react-native-clipboard/clipboard';
import AsyncStorage from '@react-native-community/async-storage';
import { getCollectible } from '../../../actions/collectibles';

const screenWidth = Dimensions.get('window').width;

const createStyles = (colors: any) =>
	StyleSheet.create({
		wrapper: {
			flex: 1,
			backgroundColor: '#1B1B23',
			alignItems: 'center',
		},
		rowWrapperTop: {
			paddingHorizontal: 15,
			marginTop: 20,
		},
		rowWrapperBottom: {
			paddingHorizontal: 15,
			marginTop: 10,
		},
		rowTitleText: {
			paddingBottom: 3,
			...(fontStyles.normal as any),
			color: colors.text.default,
		},
		textInput: {
			// borderWidth: 1,
			// borderRadius: 4,
			// borderColor: colors.border.default,
			// padding: 16,
			// ...(fontStyles.normal as any),
			// color: colors.text.default,
			width: screenWidth - 80,
			//borderWidth: 1,
			//borderRadius: 4,
			//borderColor: colors.border.default,
			// padding: 16,
			// ...fontStyles.normal,
			color: '#FFF',
			paddingHorizontal: 12,
			paddingVertical: 22,
		},
		warningText: {
			// marginTop: 15,
			// color: colors.error.default,
			// ...(fontStyles.normal as any),
			fontSize: 10,
			fontWeight: '400',
			marginLeft: 12,
			marginTop: 4,
			color: '#EB5252',
		},
		searchSection: {
			marginBottom: 0,
			flex: 1,
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
			// borderWidth: 1,
			// borderRadius: 4,
			// borderColor: colors.border.default,
			// color: colors.text.default,
			borderWidth: 1,
			borderColor: 'rgba(255, 255, 255, 0.15)',
			borderRadius: 12,
		},
		sectionSearchFocus: {
			borderColor: '#FFF',
		},
		sectionSearchError: {
			borderColor: '#EB5252',
		},
		pasteTxt: { color: '#B1B5FF', marginRight: 20 },
		mainSection: {
			width: screenWidth - 30,
			marginHorizontal: 15,
			marginTop: 10,
			paddingHorizontal: 10,
			backgroundColor: '#282832',
			borderRadius: 12,
			borderColor: 'rgba(255,255,255,0.15)',
			borderWidth: 1,
			flexDirection: 'column',
			alignItems: 'center',
		},
		imagePreviewStyle: {
			marginTop: 12,
			width: screenWidth - 48,
			height: screenWidth - 48,
			borderRadius: 12,
		},
		collectibleContractPreview: {
			width: screenWidth - 48,
			height: screenWidth - 48,
		},
		iconImage: {
			height: 12,
			resizeMode: 'contain',
			marginTop: 8,
		},
	});

interface AddCustomNFTProps {
	navigation?: any;
	collectibleContract?: {
		address: string;
	};
	getCollectibleContracts?: any;
}

const AddCustomNFT = ({navigation, collectibleContract, getCollectibleContracts, chainId }: AddCustomNFTProps) => {
	const [mounted, setMounted] = useState<boolean>(true);
	const [address, setAddress] = useState<string>('');
	const [tokenId, setTokenId] = useState<string>('');
	const [warningAddress, setWarningAddress] = useState<string>('');
	const [warningTokenId, setWarningTokenId] = useState<string>('');
	const [NFTsDetail, setNFTsDetail] = useState<any>(false);
	const [warningNFT, setWarningNFT] = useState<string>('');
	const [inputWidth, setInputWidth] = useState<string | undefined>(Device.isAndroid() ? '99%' : undefined);
	const [addressInputFocus, setAddressInputFocus] = useState(false);
	const [idInputFocus, setIdInputFocus] = useState(false);
	const assetTokenIdInput = React.createRef() as any;
	const { colors, themeAppearance } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);

	const collectibleContracts = useSelector((state: any) => state.collectibles.collectibles);
	const chainName = useSelector((state: any) => {
		switch (state.engine.backgroundState.NetworkController.provider.type) {
			case 'mainnet':
				return 'eth';
			case 'ropsten':
			case 'kovan':
			case 'rinkeby':
			case 'goerli':
				return state.engine.backgroundState.NetworkController.provider.type;
			case 'rpc':
				if (!state.engine.backgroundState.NetworkController.provider.nickname) {
					return '';
				}
				return state.engine.backgroundState.NetworkController.provider.nickname
					.split(' ')
					.map((nicknamePart) => nicknamePart[0])
					.join('');
			default:
				return '';
		}
	});
	const selectedAddress = useSelector((state: any) => state.engine.backgroundState.PreferencesController.selectedAddress);

	// console.log('selectedAddress : ', selectedAddress);
	useEffect(() => {
		setMounted(true);
		// Workaround https://github.com/facebook/react-native/issues/9958
		inputWidth &&
			setTimeout(() => {
				mounted && setInputWidth('100%');
			}, 100);
		collectibleContract && setAddress(collectibleContract.address);
		return () => {
			setMounted(false);
		};
	}, [mounted, collectibleContract, inputWidth]);

	const getAnalyticsParams = () => {
		try {
			const { NetworkController } = Engine.context as any;
			const { type } = NetworkController?.state?.provider || {};
			return {
				network_name: type,
				chain_id: chainId,
			};
		} catch (error) {
			return {};
		}
	};

	const validateCustomCollectibleAddress = async (): Promise<boolean> => {
		let validated = true;
		const isValidEthAddress = isValidAddress(address);
		if (address.length === 0) {
			setWarningAddress(strings('add_asset.input_address_error'));
			validated = false;
		}
		//  else if (!isValidEthAddress) {
		// 	setWarningAddress('Hãy nhập thông tin địa chỉ NFT');
		// 	validated = false;
		// } else if (!(await isSmartContractAddress(address, chainId))) {
		// 	setWarningAddress('Hãy nhập thông tin địa chỉ NFT');
		// 	validated = false;
		// }
		else {
			setWarningAddress(``);
		}
		return validated;
	};

	const validateCustomCollectibleTokenId = (): boolean => {
		let validated = false;
		if (tokenId.length === 0) {
			setWarningTokenId(strings('add_asset.input_id_nft_error'));
		} else {
			setWarningTokenId(``);
			validated = true;
		}
		return validated;
	};

	const validateCustomCollectible = async (): Promise<boolean> => {
		const validatedAddress = await validateCustomCollectibleAddress();
		const validatedTokenId = validateCustomCollectibleTokenId();
		return validatedAddress && validatedTokenId;
	};

	/**
	 * Method to validate collectible ownership.
	 *
	 * @returns Promise that resolves ownershio as a boolean.
	 */
	const validateCollectibleOwnership = async (): Promise<boolean> => {
		try {
			const { CollectiblesController } = Engine.context as any;
			// console.log('selectedAddress, address, tokenId : ', selectedAddress, address, tokenId);
			const isOwner = await CollectiblesController.isCollectibleOwner(selectedAddress, address, tokenId);
			// console.log('isOwner : ', isOwner);
			if (!isOwner) {
				NotificationManager.showSimpleNotification({
					status: 'error',
					duration: 5000,
					title: strings('add_asset.collectibles.nft_not_owner'),
					description: strings('add_asset.collectibles.not_owner_description'),
				});
			}
			// Alert.alert(strings('collectible.not_owner_error_title'), strings('collectible.not_owner_error'));
			return isOwner;
		} catch {
			Alert.alert(
				strings('collectible.ownership_verification_error_title'),
				strings('collectible.ownership_verification_error')
			);
			return false;
		}
	};

	const addCollectible = async (): Promise<void> => {
		if (!(await validateCustomCollectible())) return;
		// if (!(await validateCollectibleOwnership())) return;
		// Kiểm tra xem NFTs có phải của selectedAddress hay không?
		if (NFTsDetail.owner_of.toLowerCase() !== selectedAddress.toLowerCase()) {
			NotificationManager.showSimpleNotification({
				status: 'error',
				duration: 5000,
				title: strings('add_asset.collectibles.nft_not_owner'),
				description: strings('add_asset.collectibles.not_owner_description'),
			});
			return;
		}
		var listCollectible = [...collectibleContracts];
		// Kiểm tra xem NFTs đã tồn tại trong collectible hay chưa?
		// Kiểm tra xem collectible đã bị xoá/ tồn tại hay không?
		let collectible = null;
		for (let index = 0; index < listCollectible.length; index++) {
			const element = listCollectible[index];
			if (element.token_address === NFTsDetail.token_address) {
				// Cập nhật để biết collectible với slug có tồn tại
				collectible = element;
				// console.log('vào đây 1');
				// Kiểm tra xem collectible bị xoá chưa?
				if (element.remove) {
					// console.log('vào đây 2');
					// Nếu đã bị xoá thì đổi lại trạng thái hiển thị, cập nhật lại NFTs
					listCollectible[index].remove = false;
					listCollectible[index].nfts = [NFTsDetail];
					// console.log('listCollectible[index] : ', listCollectible[index]);
				} else {
					// Nếu chưa bị xoá thì kiểm tra xem NFTsDetail đã có trong list nfts của collectible hay chưa?
					for (let a = 0; a < element.nfts.length; a++) {
						const e = element.nfts[a];
						if (e.id === NFTsDetail.id) {
							NotificationManager.showSimpleNotification({
								status: 'success',
								duration: 5000,
								title: strings('add_asset.collectibles.nft_exist_inlist'),
								description: `${NFTsDetail.name} ${strings(
									'add_asset.collectibles.nft_exist_inlist_description'
								)}`,
							});
							return;
						}
					}
					// Thêm NFTs vào list nfts của listCollectible[index]
					const nft_item = element.nfts.concat([NFTsDetail]);
					listCollectible[index].nfts = nft_item;
				}
				// console.log('vào đây 3');
				break;
			}
		}
		// Nếu chưa tồn tại collectible
		if (!collectible) {
			listCollectible = listCollectible.concat({
				token_address: NFTsDetail.token_address,
				name: NFTsDetail.name,
				image: NFTsDetail.metadata?.image || '',
				nfts: [NFTsDetail],
			});
		}
		// console.log(
		// 	'vào đây 4 : ',
		// 	listCollectible.map((q) => {
		// 		return {
		// 			name: q.name,
		// 			nfts: q.nfts.length,
		// 		};
		// 	})
		// );
		// console.log('listCollectible : ', listCollectible);
		getCollectibleContracts(listCollectible);
		const savedCollectibles = JSON.parse(await AsyncStorage.getItem(selectedAddress.toLowerCase()));
		savedCollectibles[chainName] = listCollectible;
		await AsyncStorage.setItem(selectedAddress.toLowerCase(), JSON.stringify(savedCollectibles));
		NotificationManager.showSimpleNotification({
			status: 'success',
			duration: 5000,
			title: strings('add_asset.collectibles.add_nft_success'),
			description: `${NFTsDetail.name} ${strings('add_asset.collectibles.add_ntf_success_description')}`,
		});
		AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.COLLECTIBLE_ADDED, getAnalyticsParams());
		navigation.goBack();
	};

	const fetchNFTDetail = async () => {

		if (!validateCustomCollectibleAddress() || !validateCustomCollectibleTokenId()) {
			return;
		}

		const { CollectibleDetectionController } = Engine.context as any;
		const collectible = await CollectibleDetectionController.getMoralisCollectibleDetails(chainName, address, tokenId);
		if (!collectible) {
			NotificationManager.showSimpleNotification({
				status: 'error',
				duration: 5000,
				title: strings('add_asset.collectibles.get_nft_error'),
				description: strings('add_asset.collectibles.nft_not_found'),
			});
		} else {
			setNFTsDetail(collectible);

			if (collectible.owner_of.toLowerCase() !== selectedAddress.toLowerCase()) {
				setWarningNFT('not_owner');
				NotificationManager.showSimpleNotification({
					status: 'error',
					duration: 5000,
					title: strings('add_asset.collectibles.nft_not_owner'),
					description: strings('add_asset.collectibles.not_owner_description'),
				});
				return;
			}

			var listCollectible = [...collectibleContracts];
			for (let index = 0; index < listCollectible.length; index++) {
				const _collectible = listCollectible[index];
				if (_collectible.token_address === collectible.token_address) {
					if (!_collectible.remove) {
						for (let a = 0; a < _collectible.nfts.length; a++) {
							const e = _collectible.nfts[a];
							if (e.id === collectible.id) {
								setWarningNFT('added');
								NotificationManager.showSimpleNotification({
									status: 'success',
									duration: 5000,
									title: strings('add_asset.collectibles.nft_exist_inlist'),
									description: `${collectible.name} ${strings(
										'add_asset.collectibles.nft_exist_inlist_description'
									)}`,
								});
								return;
							}
						}
					}
					break;
				}
			}
		}
	};

	const addNFTsToList = async () => {
		addCollectible();
	};

	const cancelAddCollectible = (): void => {
		navigation.goBack();
	};

	const onAddressChange = (newAddress: string): void => {
		setAddress(newAddress);
		setNFTsDetail(null);
		setWarningNFT('');
	};

	const onTokenIdChange = (newTokenId: string): void => {
		setTokenId(newTokenId);
		setNFTsDetail(null);
		setWarningNFT('');
	};

	const jumpToAssetTokenId = (): void => {
		assetTokenIdInput.current?.focus();
	};

	const fetchCopiedAddress = async () => {
		const text = await Clipboard.getString();
		setAddress(text);
		setNFTsDetail(null);
	};

	const fetchCopiedTokenId = async () => {
		const text = await Clipboard.getString();
		setTokenId(text);
		setNFTsDetail(null);
	};

	return (
		<View style={styles.wrapper} testID={'add-custom-token-screen'}>
			<ActionViewNFTs
				cancelTestID={'add-custom-asset-cancel-button'}
				confirmTestID={'add-custom-asset-confirm-button'}
				cancelText={strings('add_asset.collectibles.cancel_add_collectible')}
				confirmText={'Kiểm tra'}
				onCancelPress={cancelAddCollectible}
				onConfirmPress={fetchNFTDetail}
				confirmDisabled={warningAddress || warningTokenId || warningNFT}
				NFTsDetail={NFTsDetail}
				addNFTsToList={addNFTsToList}
			>
				<View>
					<View style={styles.rowWrapperTop}>
						{/* <TextInput
							style={[styles.textInput, inputWidth ? { width: inputWidth } : {}]}
							placeholder={'Nhập địa chỉ của NFT'}
							placeholderTextColor={colors.text.muted}
							value={address}
							onChangeText={onAddressChange}
							onBlur={validateCustomCollectibleAddress}
							testID={'input-collectible-address'}
							onSubmitEditing={jumpToAssetTokenId}
							keyboardAppearance={themeAppearance}
						/> */}
						<View
							style={[
								styles.searchSection,
								addressInputFocus ? styles.sectionSearchFocus : {},
								warningAddress ? styles.sectionSearchError : {},
							]}
						>
							<TextInput
								style={[styles.textInput, inputWidth ? { width: inputWidth } : {}]}
								placeholder={strings('add_asset.enter_address_nft')}
								placeholderTextColor={colors.text.muted}
								value={address}
								onChangeText={(text) => {
									onAddressChange(text);
									setTimeout(() => {
										validateCustomCollectibleAddress();
									}, 0);
								}}
								// onBlur={validateCustomCollectibleAddress}
								onFocus={() => setAddressInputFocus(true)}
								onBlur={() => setAddressInputFocus(false)}
								testID={'input-nft-address'}
								onSubmitEditing={jumpToAssetTokenId}
								keyboardAppearance={themeAppearance}
							/>
							<TouchableOpacity onPress={fetchCopiedAddress}>
								<Text style={styles.pasteTxt}>{strings("Paste")}</Text>
							</TouchableOpacity>
						</View>
						<Text style={styles.warningText} testID={'nft-address-warning'}>
							{warningAddress}
						</Text>
					</View>
					<View style={styles.rowWrapperBottom}>
						<View
							style={[
								styles.searchSection,
								idInputFocus ? styles.sectionSearchFocus : {},
								warningTokenId ? styles.sectionSearchError : {},
							]}
						>
							<TextInput
								style={[styles.textInput, inputWidth ? { width: inputWidth } : {}]}
								placeholder={strings('add_asset.id_nft')}
								placeholderTextColor={colors.text.muted}
								value={tokenId}
								onChangeText={(text) => {
									onTokenIdChange(text);
									setTimeout(() => {
										validateCustomCollectibleTokenId();
									}, 0);
								}}
								// onBlur={validateCustomCollectibleTokenId}
								onFocus={() => setIdInputFocus(true)}
								onBlur={() => setIdInputFocus(false)}
								testID={'input-nft-id'}
								onSubmitEditing={jumpToAssetTokenId}
								keyboardAppearance={themeAppearance}
							/>
							<TouchableOpacity onPress={fetchCopiedTokenId}>
								<Text style={styles.pasteTxt}>{strings("Paste")}</Text>
							</TouchableOpacity>
						</View>
						{/* <TextInput
							style={[styles.textInput, inputWidth ? { width: inputWidth } : {}]}
							value={tokenId}
							keyboardType="numeric"
							onChangeText={onTokenIdChange}
							onBlur={validateCustomCollectibleTokenId}
							testID={'input-token-decimals'}
							ref={assetTokenIdInput}
							onSubmitEditing={addCollectible}
							returnKeyType={'done'}
							placeholder={strings('collectible.id_placeholder')}
							placeholderTextColor={colors.text.muted}
							keyboardAppearance={themeAppearance}
						/> */}
						<Text style={styles.warningText} testID={'collectible-identifier-warning'}>
							{warningTokenId}
						</Text>
					</View>
					{NFTsDetail && (
						<View style={styles.mainSection}>
							<CollectibleMedia
								collectible={{
									name: strings('collectible.untitled_collection'),
									...NFTsDetail,
									image: NFTsDetail.metadata?.image,
								}}
								big
							/>
							<View
								style={{
									flexDirection: 'row',
									// marginHorizontal: 12,
									marginTop: 16,
									marginBottom: 12,
								}}
							>
								<View
									style={{
										flex: 1,
										flexDirection: 'column',
									}}
								>
									<Text
										style={{
											color: 'white',
											fontSize: 20,
											fontWeight: '700',
										}}
									>
										{NFTsDetail.metadata?.name || `${NFTsDetail.name} #${NFTsDetail.token_id}`}
									</Text>
									<View
										style={{
											flexDirection: 'row',
										}}
									>
										<Text
											style={{
												color: '#A0A0B1',
												fontSize: 14,
												fontWeight: '500',
												marginEnd: 5,
												marginTop: 5,
											}}
										>
											{NFTsDetail.name}
										</Text>
										<Image
											source={require('../../../images/blue-check.png')}
											style={styles.iconImage}
										/>
										<Image
											source={require('../../../images/arrow-right.png')}
											style={styles.iconImage}
										/>
									</View>
								</View>
							</View>
						</View>
					)}
				</View>
			</ActionViewNFTs>
		</View>
	);
};

const mapDispatchToProps = (dispatch: any) => ({
	getCollectibleContracts: (collectibles: any) => dispatch(getCollectible(collectibles)),
});

export default connect(null, mapDispatchToProps)(AddCustomNFT);
