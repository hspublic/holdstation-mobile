import React, { useState } from 'react';

import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

const CustomSwitch = ({ navigation, selectionMode, roundCorner, option1, option2, onSelectSwitch, selectionColor }) => {
	const [getSelectionMode, setSelectionMode] = useState(selectionMode);
	const [getRoundCorner, setRoundCorner] = useState(roundCorner);

	const updatedSwitchData = (val) => {
		setSelectionMode(val);
		onSelectSwitch(val);
	};

	return (
		<View>
			<View
				style={{
					height: 44,
					width: 215,
					backgroundColor: 'white',
					borderRadius: getRoundCorner ? 12 : 0,
					borderWidth: 1,
					borderColor: selectionColor,
					flexDirection: 'row',
					//justifyContent: 'center',
					padding: 2,
				}}
			>
				<TouchableOpacity
					activeOpacity={1}
					onPress={() => updatedSwitchData(option1.key)}
					style={{
						flex: 1,
						backgroundColor: getSelectionMode == option1.key ? selectionColor : 'white',
						borderRadius: getRoundCorner ? 12 : 0,
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<Text
						style={{
							color: getSelectionMode == option1.key ? 'white' : selectionColor,
						}}
					>
						{option1.label}
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					TouchableOpacity
					activeOpacity={1}
					onPress={() => updatedSwitchData(option2.key)}
					style={{
						flex: 1,
						backgroundColor: getSelectionMode == option2.key ? selectionColor : 'white',
						borderRadius: getRoundCorner ? 12 : 0,
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<Text
						style={{
							color: getSelectionMode == option2.key ? 'white' : selectionColor,
						}}
					>
						{option2.label}
					</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};
export default CustomSwitch;
