import React, { useState, useEffect, useCallback, } from 'react';
import PropTypes from 'prop-types';
import {
	TouchableOpacity,
	StyleSheet,
	View,
	InteractionManager,
	Image,
	Alert,
	ActivityIndicator,
	Dimensions
} from "react-native";
import { connect, useSelector } from 'react-redux';
import { fontStyles } from '../../../styles/common';
import { strings } from '../../../../locales/i18n';
import Engine from '../../../core/Engine';
import CollectibleContractElement from '../CollectibleContractElement';
import Analytics from '../../../core/Analytics';
import { ANALYTICS_EVENT_OPTS } from '../../../util/analytics';
import {
	collectibleContractsSelector,
	collectiblesSelector,
	favoritesCollectiblesSelector,
} from '../../../reducers/collectibles';
import { removeFavoriteCollectible, getCollectible, removeCollectible } from '../../../actions/collectibles';
import { setNftDetectionDismissed } from '../../../actions/user';
import Text from '../../Base/Text';
import AppConstants from '../../../core/AppConstants';
import { compareTokenIds } from '../../../util/tokens';
import { useAppThemeFromContext, mockTheme } from '../../../util/theme';
import { rgbaColor  } from 'react-native-reanimated/src/reanimated2/Colors';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from "react-native-modal";
import { toggleDetailNftModal } from '../../../actions/modals';


const win = Dimensions.get('window');

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			//backgroundColor: colors.background.default,
			backgroundColor: '#1B1B23',
			flex: 1,
			minHeight: 200,
			marginTop: 16,
		},
		info: {
			height: win.height/20,
			width: '100%',
			marginBottom: win.height/110
		},
		emptyView: {
			justifyContent: 'center',
			alignItems: 'flex-start',
			marginTop: 10,
			marginLeft: win.width / 20
		},
		add: {
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'center',
		},
		// addText: {
		// 	fontSize: 14,
		// 	color: colors.primary.default,
		// 	...fontStyles.normal,
		// },
		addText: {
			fontSize: 12,
			color: '#B1B5FF',
			...fontStyles.normal,
		},
		footer: {
			flex: 1,
			paddingBottom: 30,
			alignItems: 'center',
			marginTop: 24,
		},
		emptyContainer: {
			flex: 1,
			marginBottom: 18,
			justifyContent: 'center',
			alignItems: 'center',
			// backgroundColor: 'red'
		},
		loadingContainer: {
			flex: 1,
			marginBottom: 70,
			justifyContent: 'center',
			alignItems: 'center',
		},
		emptyImageContainer: {
			width: 76,
			height: 76,
			marginTop: 30,
			marginBottom: 12,
			tintColor: colors.icon.default,
		},
		emptyTitleText: {
			fontSize: 14,
			color: '#58586C',
			...fontStyles.normal,
			lineHeight: 18,
		},
		emptyText: {
			color: colors.text.alternative,
			marginBottom: 8,
			fontSize: 14,
		},
		tabLabel: {
			fontSize: 20,
			color: '#FFFFFF',
			...fontStyles.boldHight,
			lineHeight: 24,
		},
		hWrapper: {
			flex: 1,
			flexDirection: 'row',
			// backgroundColor: 'blue',
			height: win.height / 40,
		},
		iconSwipeLeft: {
			alignItems: 'flex-start',
			marginLeft: win.width / 20,
		},
		iconSwipe: {
			marginLeft: 'auto',
			marginRight: win.width / 60,
			marginTop: win.height / 400

		},
		iconSwipeRight: {
			alignItems: 'flex-end',
			marginRight: win.width / 20,
			marginTop: win.height / 400
		},
		iconImage: {
			height: 24,
			width: 24,
			resizeMode: 'contain',
		},
		collectibleItem: {
			width: '90%',
		},
		collapseText: {
			color: '#FFFFFF',
			marginBottom: 8,
			fontSize: 14,
			marginHorizontal: 15
		},
		tokenMenu: {
			backgroundColor: 'rgba(255, 255, 255, 0.15)',
			borderRadius: 60,
			height: 34,
		},
		marginRight10: {
			marginRight: 8,
		},
		doneBtn: {
			backgroundColor: '#6A45FF',
		},
		buttonText: {
			fontSize: 14,
			color: '#FFFFFF',
			...fontStyles.normal,
			paddingVertical: 6,
			paddingHorizontal: 12,
			marginTop: 2,
		},

		tokenDelete : {
			left: win.width / 20,
			marginRight: win.width / 15,
		},
		tokenPin : {
			marginRight: win.width / 60,
		},
		tokenDone: {
			position: 'absolute',
			right : win.width/ 20,
		},
		wrapperContainer: {
			flex: 1,
			marginTop: - win.height / 75
		},

		modal: {
			margin: 0,
			padding: 10,
			justifyContent: 'center',
		},
		modalContent: {
			backgroundColor: '#1B1B23',
			borderWidth: 1,
			borderColor: 'rgba(255, 255, 255, 0.15)',
			borderRadius: 12,
			padding: 20,
		},
		modalTitle: {
			fontSize: 16,
			fontWeight: '600',
			color: '#FFF',
			textAlign: 'center',
		},
		modalMessage: {
			fontSize: 16,
			fontWeight: '400',
			color: '#FFF',
			textAlign: 'center',
			marginTop: 20,
		},
		modalButton: {
			backgroundColor: '#6A45FF',
			padding: 12,
			borderRadius: 12,
			alignItems: 'center',
			marginTop: 20,
		},
		modalButtonText: {
			fontSize: 14,
			fontWeight: '500',
			color: '#FFF',
		},
		bottomModal: {
			justifyContent: 'flex-end',
			margin: 0,
		},
	});

const GroupCollectible = (nfts) => {
	const collectible = [];
	// Dùng vòng lặp kiểm tra từng nfts
	for (let index = 0; index < nfts.length; index++) {
		const e = nfts[index];
		if (!collectible.some((item) => item.token_address === e.token_address)) {
			collectible.push({
				token_address: e.token_address,
				name: e.name,
				image: e.metadata?.image || '',
				nfts: [{ ...e }],
			});
		} else {
			const x = collectible.findIndex((object) => object.token_address === e.token_address);
			collectible[x].nfts = [...collectible[x].nfts, { ...e }];
		}
	}

	return collectible;
}
/**
 * View that renders a list of CollectibleContract
 * ERC-721 and ERC-1155
 */
const CollectibleContracts = ({
	assetOwner,
	chainId,
	selectedAddress,
	navigation,
	favoriteCollectibles,
	removeFavoriteCollectible,
	useCollectibleDetection,
	setNftDetectionDismissed,
	nftDetectionDismissed,
	removeCollectibleContracts,
	getCollectibleContracts,
	hub,
	detailNFTModalVisible,
	toggleDetailNFT,
}) => {

	

	const { colors } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);
	const [isAddNFTEnabled, setIsAddNFTEnabled] = useState(true);
	const [updateData, setUpdateData] = useState(1);
	const [collectibles, setCollectibles] = useState([]);
	// const [listCollectibleContracts, setCollectibleContracts] = useState([]);

	const listCollectibleContracts = useSelector((state) => state.collectibles.collectibles);

	const collectibleContracts = [...listCollectibleContracts].sort((a, b) => {
		return a.pin ? -1 : 1;
	}); // filter(e => !e.remove)
	const [isLoaded, setIsLoaded] = useState(false)
	const [pageIndex, setPageIndex] = useState(0); // load limit 10 record, pageIndex
	const [isCollapsed, setIsCollapsed] = useState(collectibleContracts.length <= 3); // load all?

	const [showHiddenMenu, setShowHiddenMenu] = useState(false)
	const [checkedArrObject, setCheckedArrObject] = useState([])

	const [shouldShowPinButton, setShouldShowPinButton] = useState(false)
	const [shouldShowUnpinButton, setShouldShowUnPinButton] = useState(false)

	const [isLoadingCollectibles, setIsLoadingCollectibles] = useState(false);
	const chainName = useSelector((state: any) => {
		switch (state.engine.backgroundState.NetworkController.provider.type) {
			case 'mainnet':
				return 'eth';
			case 'ropsten':
			case 'kovan':
			case 'rinkeby':
			case 'goerli':
				return state.engine.backgroundState.NetworkController.provider.type;
			case 'rpc':
				if (!state.engine.backgroundState.NetworkController.provider.nickname) {
					return '';
				}
				switch (state.engine.backgroundState.NetworkController.provider.chainId) {
					case '56':
						return 'bsc';
					case '137':
						return 'polygon';
					default:
						return '';
				}
			default:
				return '';
		}
	});

	const [messageAlert, setMessageAlert] = useState(null);

	const [isLoading, setIsLoading] = useState(false);

	const onItemPress = useCallback(
		(collectible, contractName) => {
			toggleDetailNFT(collectible)

			// navigation.navigate('CollectiblesDetails', { collectible });
			// console.log('chay vao day roi', collectible)
		},
		[navigation]
	);

	useEffect(() => {
		setIsCollapsed(collectibleContracts.length <= 3)
	}, [collectibleContracts.length])

	useEffect(() => {
		// TO DO: Move this fix to the controllers layer
		const { CollectibleDetectionController } = Engine.context;
		const fetchCollectiblesContract = async () => {
			// Lấy các collectible đã lưu từ local
			let collectibles = await AsyncStorage.getItem(assetOwner.toLowerCase());
			if (collectibles) {
				collectibles = JSON.parse(collectibles);
				if (collectibles[chainName]) {
					getCollectibleContracts(collectibles[chainName]);
					return;
				}
				collectibles[chainName] = [];
				setIsLoadingCollectibles(true);
			} else {
				collectibles = { [chainName]: [] };
				setIsLoadingCollectibles(true);
			}

			const response = await CollectibleDetectionController.getMoralisOwnerCollectibles(chainName, assetOwner);
			const result = GroupCollectible(response);
			if (result.length > 0) {
				for (let index = 0; index < result.length; index++) {
					const element = result[index];
					const indexCollectible = collectibles[chainName].findIndex(e => e.token_address === element.token_address)
					if (indexCollectible !== -1) {
						result[index].remove = collectibles[chainName][indexCollectible].remove || false;
						result[index].pin = collectibles[chainName][indexCollectible].pin || false;
						result[index].nfts = collectibles[chainName][indexCollectible].nfts;
					}
				}
				collectibles[chainName] = result;
			}
			getCollectibleContracts(collectibles[chainName]);
			await AsyncStorage.setItem(assetOwner.toLowerCase(), JSON.stringify(collectibles));
			setIsLoadingCollectibles(false);
		};

		fetchCollectiblesContract().catch(console.error);

	}, [assetOwner, chainName]); // contractCollectibles

	const onEndReach = useCallback(() => {
		if (!pageIndex || isCollapsed || isLoading) {
			return;
		}
		setIsLoading(true);
		setTimeout(() => {
			setIsLoading(false);
			viewMore();
		}, 2000);
	}, [pageIndex, isCollapsed, isLoading]);
	useEffect(() => {
		hub.on('wallet_view::onEndReach', onEndReach);
		return () => {
			hub.removeListener('wallet_view::onEndReach', onEndReach);
		};
	}, [hub, onEndReach]);

	const goToAddCollectible = () => {
		setIsAddNFTEnabled(false);
		navigation.push('AddAsset', { assetType: 'collectible' });
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_ADD_COLLECTIBLES);
			setIsAddNFTEnabled(true);
		});
	};

	const goToAddNFT = () => {
		navigation.push('AddNFT', { assetType: 'collectible' });
		// InteractionManager.runAfterInteractions(() => {
		// 	Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_ADD_TOKENS);
		// 	this.setState({ isAddTokenEnabled: true });
		// });
	};

	const hideExtraMenu = () => {
		setShowHiddenMenu(false)
		setCheckedArrObject([])
	};

	const pinsCollectibles = async () => {
		if (checkedArrObject.length === 0) {
			Alert.alert('No Item Selected');
		} else {
			const collectibles = collectibleContracts.map((e) => {
				if (checkedArrObject.some(x => x.token_address === e.token_address && !x.pin)) {
					// pinCollectible.push(e.slug)
					return {
						...e, pin: true
					}
				}
				return e
			})
			// setCollectibleContracts(collectibles)
			getCollectibleContracts(collectibles)
			// Lưu các collectible đã pin xuống local
			const savedCollectibles = JSON.parse(await AsyncStorage.getItem(assetOwner.toLowerCase()));
			savedCollectibles[chainName] = collectibles;
			await AsyncStorage.setItem(assetOwner.toLowerCase(), JSON.stringify(savedCollectibles));
			hideExtraMenu();
		}
	};

	const checkToUpdateUnpinButton = (checkedArr) => {
		setShouldShowPinButton(false)
		setShouldShowUnPinButton(false)
		let isFoundPined = false;
		let isFoundUnPined = false;
		for (let index = 0; index < checkedArr.length; index++) {
			const element = checkedArr[index];
			if (element.pin) {
				isFoundUnPined = true
			}
			if (!element.pin) {
				isFoundPined = true
			}
		}
		if (isFoundPined) {
			setShouldShowPinButton(true)
		}
		if (isFoundUnPined) {
			setShouldShowUnPinButton(true)
		}
	};

	const unPinCollectibles = async () => {
		if (checkedArrObject.length === 0) {
			Alert.alert('No Item Selected');
		} else {
			const collectibles = collectibleContracts.map((e) => {
				if (checkedArrObject.some(x => x.token_address === e.token_address && x.pin)) {
					return {
						...e,
						pin: false,
					};
				}
				return e;
			})
			getCollectibleContracts(collectibles);

			// Lưu các collectible đã unpin xuống local
			const savedCollectibles = JSON.parse(await AsyncStorage.getItem(assetOwner.toLowerCase()));
			savedCollectibles[chainName] = collectibles;
			await AsyncStorage.setItem(assetOwner.toLowerCase(), JSON.stringify(savedCollectibles));
			hideExtraMenu();
		}
	};

	const removeCollectibles = async () => {
		if (checkedArrObject.length === 0) {
			// Alert.alert('No Item Selected');
			setMessageAlert({
				title: strings('collectibles.no_item_selected'),
				message: strings('collectibles.no_item_selected_message'),
				button: strings('collectibles.ok'),
			});
		} else {
			// Duyệt lần lượt collectible
			const listSlugCollectible = checkedArrObject.map(e => e.token_address)
			// Cập nhật lại list Collectible
			const collectibles = []
			collectibleContracts.forEach(element => {
				if (listSlugCollectible.includes(element.token_address)) {
					collectibles.push({
						...element, remove: true, pin: false // Thêm biến xác định collectible đã bị xoá
					})
					// collectibles.push(element)
				} else {
					collectibles.push(element)
				}
			});
			getCollectibleContracts(collectibles)
			// Lưu danh sách xuống biến local
			const savedCollectibles = JSON.parse(await AsyncStorage.getItem(assetOwner.toLowerCase()));
			savedCollectibles[chainName] = collectibles;
			await AsyncStorage.setItem(assetOwner.toLowerCase(), JSON.stringify(savedCollectibles));
			setMessageAlert({
				title: strings('collectibles.remove_success'),
				message: strings('collectibles.remove_success_message'),
				button: strings('collectibles.ok'),
			});
		}
		hideExtraMenu();
	};

	const renderFooter = () => (
		<View style={styles.footer} key={'collectible-contracts-footer'}>
			<Text style={styles.emptyText}>{strings('wallet.no_collectibles')}</Text>
			<TouchableOpacity
				style={styles.add}
				onPress={goToAddCollectible}
				disabled={!isAddNFTEnabled}
				testID={'add-collectible-button'}
			>
				<Text style={styles.addText}>{strings('wallet.add_collectibles')}</Text>
			</TouchableOpacity>
		</View>
	);

	const renderCollectibleContract =
		(item, index) => {
			return (
				<CollectibleContractElement
					showHiddenMenu={showHiddenMenu}
					onShowHiddenMenu={() => {
						setShowHiddenMenu(true)
						setCheckedArrObject([...checkedArrObject, item])
						checkToUpdateUnpinButton([...checkedArrObject, item])
					}}
					onPress={onItemPress}
					asset={item}
					key={`iii_${index}`}
					contractCollectibles={item.nfts}
					// collectiblesVisible={index === 0}
					collectiblesVisible={false}
					checkedArrObject={checkedArrObject}
					onCheckedArrObject={() => {
						setCheckedArrObject([...checkedArrObject, item])
						checkToUpdateUnpinButton([...checkedArrObject, item])
					}}
					onUnCheckedArrObject={() => {
						const collectibles = checkedArrObject.filter(obj => obj.token_address !== item.token_address);
						setCheckedArrObject(collectibles)
						checkToUpdateUnpinButton(collectibles)
					}}
				/>
			);
		}

	const renderFavoriteCollectibles = useCallback(() => {
		const filteredCollectibles = favoriteCollectibles.map((collectible) =>
			collectibles.find(
				({ tokenId, address }) =>
					compareTokenIds(collectible.tokenId, tokenId) && collectible.address === address
			)
		);
		return (
			null
			// Boolean(filteredCollectibles.length) && (
			// 	<CollectibleContractElement
			// 		onPress={onItemPress}
			// 		asset={{ name: 'Favorites', favorites: true }}
			// 		key={'Favorites'}
			// 		contractCollectibles={filteredCollectibles}
			// 		collectiblesVisible
			// 	/>
			// )
		);
	}, [favoriteCollectibles, collectibles, onItemPress]);

	const renderHeader = () => {
		return(
		<View key={'nft-footer'} style={styles.info}>
		{!showHiddenMenu ? (
			<View key={'tokens-header'} style={styles.hWrapper}>
				<View style={[styles.iconSwipeLeft, styles.marginTop20]}>
					<Text style={styles.tabLabel}>NFT</Text>
				</View>
				<View style={[styles.iconSwipe]}>
					<TouchableOpacity
						onPress={() => {setShowHiddenMenu(true)}}
						testID={'edit-NFT-button'}
						style={{
							alignItems: 'center',
							flexDirection: 'row'
						}}>
						<Image
							source={require('../../../images/edit.png')}
							style={styles.iconImage}
							resizeMethod={'auto'}
						/>
					</TouchableOpacity>
				</View>
				<View style={[styles.iconSwipeRight]}>
					<TouchableOpacity
						onPress={goToAddNFT}
						testID={'add-token-button'}
						style={{
							alignItems: 'center',
							flexDirection: 'row'
						}}>
						<Image
							source={require('../../../images/add-circle.png')}
							style={styles.iconImage}
							resizeMethod={'auto'}
						/>
						{/* <Text style={styles.addText}>{strings('wallet.add_tokens')}</Text> */}
					</TouchableOpacity>
				</View>
			</View>
		) : (
			<View style={styles.hWrapper}>
				<TouchableOpacity
					onPress={removeCollectibles}
					style={[styles.tokenMenu, styles.marginRight10, styles.tokenDelete]}>
						<Text style={styles.buttonText}>{strings('token.remove')}</Text>
				</TouchableOpacity>
				{
					shouldShowPinButton && <TouchableOpacity
						onPress={pinsCollectibles}
						style={[styles.tokenMenu, styles.marginRight10, styles.tokenPin]}
					>
						<Text style={styles.buttonText}>{strings('token.pin')}</Text>
					</TouchableOpacity>
				}
				{
					shouldShowUnpinButton && <TouchableOpacity
						onPress={unPinCollectibles}
						style={[styles.tokenMenu, styles.tokenPin]}>
							<Text style={styles.buttonText}>{strings('token.unPin')}</Text>
					</TouchableOpacity>
				}
				<TouchableOpacity
					onPress={hideExtraMenu}
					style={[styles.tokenMenu, styles.doneBtn, styles.tokenDone]}>
						<Text style={styles.buttonText}>{strings('token.done')}</Text>
				</TouchableOpacity>
			</View>
		)}
	</View>)
	};

	const viewMore = () => {
		if (isCollapsed) {
			setPageIndex(0)
			setIsCollapsed(false)
		} else {
			setPageIndex((pageIndex => pageIndex + 1))
			if (collectibleContracts.length <= (pageIndex + 1) * 10 + 3) {
				setIsCollapsed(true)
			}
		}

	};

	const renderLoader = () => {
		const styles = createStyles(colors);

		return (
			<View style={styles.loadingContainer}>
				<ActivityIndicator style={styles.loader} size="small" />
			</View>
		);
	};

	const renderList = (listCollectiblesContracts) => {
		if (listCollectiblesContracts.length <= 3) {
			return <View>
				{renderFavoriteCollectibles()}
				<View>{listCollectiblesContracts?.map((item, index) => renderCollectibleContract(item, index))}</View>
			</View>
		}
		// Nếu đã tải hết dữ liệu
		if (isCollapsed) {
			return (
				<View >
					{renderFavoriteCollectibles()}
					<View>{listCollectiblesContracts?.map((item, index) => renderCollectibleContract(item, index))}</View>
					<TouchableOpacity
						onPress={viewMore}
					>
						<View
							style={{
								width: 145,
								height: 31,
								alignItems: 'center',
								justifyContent: 'center',
								backgroundColor: rgbaColor(255, 255, 255, 0.15),
								borderRadius: 12,
								flexDirection: 'row',
								marginLeft: win.width/20
							}}
						>
							<Text
								style={{
									color: '#FFFFFF',
									fontSize: 12,
									...fontStyles.semiBold,
									marginRight: 2
								}}
							>
								{strings('wallet.collapse_tokens')}
							</Text>
							<Image source={require('../../../images-new/arrow-up.svg')} style= {{
								width: 12,
								height: 12,
								resizeMode: 'cover'
							}} />
						</View>
					</TouchableOpacity>
				</View>
			);
		}
		let collectibleDisplay = listCollectiblesContracts.slice(0, pageIndex * 10 + 3);
		return (
			<View>
				{renderFavoriteCollectibles()}
				<View>{collectibleDisplay?.map((item, index) => renderCollectibleContract(item, index))}</View>
				{!pageIndex && (
					<TouchableOpacity
						style={{
							marginRight: 5,
						}}
						onPress={viewMore}
					>
						<View
							style={{
								width: 113,
								height: 31,
								alignItems: 'center',
								justifyContent: 'center',
								backgroundColor: rgbaColor(255, 255, 255, 0.15),
								borderRadius: 12,
								flexDirection: 'row',
								marginLeft: win.width/20
							}}
						>
							<Text
								style={{
									color: '#FFFFFF',
									fontSize: 12,
									...fontStyles.semiBold,
									marginRight: 5
								}}
							>
								{listCollectiblesContracts.length - collectibleDisplay.length} {strings('wallet.remain_tokens')}
							</Text>
							<Image source={require('../../../images/icon-go-ahead.png')} />
						</View>
					</TouchableOpacity>
				)}
			</View>
		);
	}

	const logData = (message) => {
		console.log(message);
	}

	const goToLearnMore = () =>
		navigation.navigate('Webview', { screen: 'SimpleWebview', params: { url: AppConstants.URLS.NFT } });

	const dismissNftInfo = async () => {
		setNftDetectionDismissed(true);
	};

	const renderEmpty = () => (
		<View style={styles.emptyView}>
			<View style={styles.emptyContainer}>
				<Text style={styles.emptyTitleText}>
					{strings('wallet.no_nfts_yet')}
				</Text>
			</View>
		</View>
	);
	// Lọc các collectible chưa bị xoá
	const listCollectiblesContracts = [...collectibleContracts].filter(e => !e.remove);
	return (
		<View style={styles.wrapper} testID={'collectible-contracts'}>
			{renderHeader()}
			{/*</View>*/}
			<View style={styles.wrapperContainer}>
				{isLoadingCollectibles ? renderLoader() : listCollectiblesContracts.length > 0 ? renderList(listCollectiblesContracts) : renderEmpty()}
				{isLoading && renderLoader()}
			</View>
			{/*{renderFooter()}*/}
			<Modal
				isVisible={!!messageAlert}
				style={styles.modal}
				onBackdropPress={() => setMessageAlert(null)}
				onBackButtonPress={() => setMessageAlert(null)}
				onSwipeComplete={() => setMessageAlert(null)}
				swipeDirection={'down'}
				propagateSwipe={false}
				backdropColor={'#000'}
				backdropOpacity={0.9}
				avoidKeyboard
			>
				<View style={styles.modalContent}>
					<Text style={styles.modalTitle}>{messageAlert?.title}</Text>
					<Text style={styles.modalMessage}>{messageAlert?.message}</Text>
					<TouchableOpacity
						style={styles.modalButton}
						onPress={() => setMessageAlert(null)}
					>
						<Text style={styles.modalButtonText}>{messageAlert?.button}</Text>
					</TouchableOpacity>
				</View>
			</Modal>

			
		</View>
	);
};

CollectibleContracts.propTypes = {
	/**
	 * Chain id
	 */
	chainId: PropTypes.string,
	/**
	 * Selected address
	 */
	selectedAddress: PropTypes.string,
	/**
	 * Array of collectibleContract objects
	 */
	collectibleContracts: PropTypes.array,
	/**
	 * Array of collectibles objects
	 */
	collectibles: PropTypes.array,
	/**
	 * Navigation object required to push
	 * the Asset detail view
	 */
	navigation: PropTypes.object,
	/**
	 * Object of collectibles
	 */
	favoriteCollectibles: PropTypes.array,
	/**
	 * Dispatch remove collectible from favorites action
	 */
	removeFavoriteCollectible: PropTypes.func,
	/**
	 * Boolean to show if NFT detection is enabled
	 */
	useCollectibleDetection: PropTypes.bool,
	/**
	 * Setter for NFT detection state
	 */
	setNftDetectionDismissed: PropTypes.func,
	/**
	 * State to manage display of modal
	 */
	nftDetectionDismissed: PropTypes.bool,
	hub: PropTypes.any,
};

function mapStateToProps (state) {
	const props = {
		chainId: state.engine.backgroundState.NetworkController.provider.chainId,
		selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
		useCollectibleDetection: state.engine.backgroundState.PreferencesController.useCollectibleDetection,
		nftDetectionDismissed: state.user.nftDetectionDismissed,
		collectibleContracts: collectibleContractsSelector(state),
		collectibles: collectiblesSelector(state),
		favoriteCollectibles: favoritesCollectiblesSelector(state),
		detailNFTModalVisible: state.modals.detailNFTModalVisible,

	}
	return props;
};

function mapDispatchToProps (dispatch) {
	const props = {
	removeFavoriteCollectible: (selectedAddress, chainId, collectible) =>
		dispatch(removeFavoriteCollectible(selectedAddress, chainId, collectible)),
	getCollectibleContracts: (collectibles) => dispatch(getCollectible(collectibles)),
	removeCollectibleContracts: (collectible) => dispatch(removeCollectible(collectible)),
	setNftDetectionDismissed: () => dispatch(setNftDetectionDismissed()),
	toggleDetailNFT: (collectibles) => dispatch(toggleDetailNftModal(collectibles)),

	}
	return props
};

export default connect(mapStateToProps, mapDispatchToProps)(CollectibleContracts);
