import React, { Component } from 'react';
import {
  Alert, DeviceEventEmitter,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from "react-native";
import PropTypes from 'prop-types';

const styles = StyleSheet.create(
  {
    CheckboxContainer: {
      flex: 1,
      padding: 22,
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: (Platform.OS === 'ios') ? 25 : 0
    },
    showSelectedButton: {
      padding: 20,
      marginTop: 25,
      alignSelf: 'stretch',
      backgroundColor: '#5D52FF'
    },
    buttonText: {
      fontSize: 20,
      color: '#ffffff',
      textAlign: 'center',
      alignSelf: 'stretch'
    },
    selectedUI: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    checkboxLabel: {
      fontSize: 18,
      paddingLeft: 15
    }
  });


class Checkbox extends Component {
  constructor() {
    super();
    this.state = {
      checked: null
    }
  }
  componentDidMount() {
    this.eventListener = DeviceEventEmitter.addListener('refreshCheckbox', this.handleEvent)
    if (this.props.checked) {
      this.setState({ checked: true }, () => {
        this.props.checkedObjArr.addItem({
          'key': this.props.keyValue,
          'value': this.props.value,
          'label': this.props.label
        });
      });
    } else {
      this.setState({
        checked: false
      });
    }
  }

  componentWillUnmount() {
    this.eventListener.remove();
  }

  handleEvent = (event) => {
    //Do something with event object
    // console.log('event.symbol : ', event.symbol, this.props.keyValue)
    if (event.symbol === this.props.keyValue) {
      this.stateSwitcher(event.symbol, this.props.label, event.symbol);
    }
  }

  stateSwitcher(key, label, value) {
    // console.log('key : ', key, label)
    this.setState({ checked: !this.state.checked }, () => {
      if (this.state.checked) {
        this.props.checkedObjArr.addItem({
          'key': key,
          'value': value,
          'label': label
        });
      } else {
        this.props.checkedObjArr.fetchArray().splice(
          this.props.checkedObjArr.fetchArray().findIndex(y => y.key == key), 1
        );
      }
      this.props.handleShowUnpinButton();
    });
  }


  render() {
    const {pin, value} = this.props;
    // console.log('pin', pin, value)
    return (
      <TouchableHighlight
        onPress={this.stateSwitcher.bind(this, this.props.keyValue, this.props.label, this.props.value)}
        underlayColor="transparent"
        style={{  marginTop: 9}}>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center'
        }}>
          <View style={{
            marginRight: 10,
          }}>
            { 
              (!this.state.checked)? 
                pin ? (<View style={styles.selectedUI}>
                  <Image source={require('../../../images-new/pin-token.png')} style={{
                    width: this.props.size,
                    height: this.props.size,
                  }} />
                </View>)
                :
                (<Image source={require('../../../images/unchecked.png')} style={{
                  width: this.props.size,
                  height: this.props.size
                }} />)
                :
                (<View style={styles.selectedUI}>
                  <Image source={require('../../../images/checked.png')} style={{
                    width: this.props.size,
                    height: this.props.size
                  }} />
                </View>)

                
            }
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

Checkbox.propTypes = {
  keyValue: PropTypes.string.isRequired,
  size: PropTypes.number,
  color: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  checked: PropTypes.bool,
  labelColor: PropTypes.string,
  checkedObjArr: PropTypes.object.isRequired,
  pin: PropTypes.bool,
}

Checkbox.defaultProps = {
  size: 32,
  checked: false,
  value: 'Default',
  label: 'Default',
  color: '#cecece',
  labelColor: '000000',
}

export default Checkbox;
