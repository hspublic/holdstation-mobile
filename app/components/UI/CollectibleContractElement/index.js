import React, { useEffect, useState, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TouchableOpacity, Alert, Image, Dimensions } from "react-native";
import { connect } from 'react-redux';
import { colors as importedColors, fontStyles } from '../../../styles/common';
import CollectibleMedia from '../CollectibleMedia';
import Icon from 'react-native-vector-icons/Ionicons';
import Device from '../../../util/device';
import AntIcons from 'react-native-vector-icons/AntDesign';
import Text from '../../Base/Text';
import ActionSheet from 'react-native-actionsheet';
import { strings } from '../../../../locales/i18n';
import Engine from '../../../core/Engine';
import { removeFavoriteCollectible } from '../../../actions/collectibles';
import { collectibleContractsSelector } from '../../../reducers/collectibles';
import { useAppThemeFromContext, mockTheme } from '../../../util/theme';
import Checkbox from '../CheckCollectible';
import {GLOBAL_ITEM_FONT_SIZE} from '../../../styles/common'

const DEVICE_WIDTH = Device.getDeviceWidth();
const COLLECTIBLE_WIDTH = (DEVICE_WIDTH - 60) / 2;


const win = Dimensions.get('window')

const createStyles = (colors) =>
	StyleSheet.create({
		itemWrapper: {
			flex: 1,
			justifyContent: 'space-between',
			marginBottom: win.height/150,
		},
		collectibleContractIcon: { width: 32, height: 32 },
		collectibleContractIconContainer: {
			height: 45,
			marginTop: 4,
			marginLeft: - win.width/ 400
		},
		collectibleContractIconContainerHidden: {
			height: 45,
			marginTop: 4,
			marginLeft: win.width/ 14.8
		},
		shadowImage: {
			shadowColor: '#FFF',
			shadowOffset: { width: 0, height: 0 },
			shadowOpacity: 0.2,
			shadowRadius: 3,
			flex: 1
		},

		verticalAlignedContainer: {
			marginTop: win.height / 100,
			marginLeft: win.width / 50,
		},
		verticalAlignedContainerArrow: {
			marginTop: win.height/100,
			flex: 1,
			flexDirection: 'row',
			position: 'absolute',
			right: win.width / 30,
		},
		titleText: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			color: '#FFFFFF',
			...fontStyles.normal,
			// textTransform: 'uppercase',
			fontWeight: '600',
			lineHeight: 17,
		},
		titleTextLength: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			color: '#FFFFFF',
			...fontStyles.normal,
			fontWeight: '400',
			lineHeight: 18,
			marginRight: 6,
			marginTop: -1
		},
		collectibleIcon: {
			width: COLLECTIBLE_WIDTH,
			height: COLLECTIBLE_WIDTH,
		},
		collectibleInTheMiddle: {
			marginLeft: win.width / 20
		},
		collectiblesRowContainer: {
			flex: 1,
			flexDirection: 'row',
			// paddingVertical: win.height/160,
			// paddingHorizontal: win.width/17,
		},
		collectibleBox: {
			flex: 1,
			flexDirection: 'row',
		},
		favoritesLogoWrapper: {
			flex: 1,
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
			// This yellow doesn't change colors between themes
			backgroundColor: importedColors.yellow,
			width: 32,
			height: 32,
			borderRadius: 16,
		},
		iconPin: {
			marginTop: win.height / 70,
			height: 14,
			width: 14,
			marginRight: win.width / 40,
			marginLeft: win.width / 40,
			resizeMode: 'contain',
		},
		iconPinHidden: {
			marginTop: win.height / 70,
			height: 14,
			width: 14,
			marginRight: win.width / 40,
			marginLeft: -win.width / 40,
			resizeMode: 'contain',
		},
		arrowIcon: {
			marginRight: win.width / 37,
			width: 16,
			height: 16,
			// width
		},
		checkBox: {
			marginLeft: win.width / 19,
		},
		nftContainer: {
			flex: 1,
			flexDirection: 'row',
		},
		zoomInItem: {
			transform: [{scale: 0.97}]
		}
	});

const splitIntoSubArrays = (array, count) => {
	const newArray = [];
	while (array.length > 0) {
		newArray.push(array.splice(0, count));
	}
	return newArray;
};

/**
 * Customizable view to render assets in lists
 */
function CollectibleContractElement({
	asset,
	contractCollectibles,
	collectiblesVisible: propsCollectiblesVisible,
	onPress,
	collectibleContracts,
	chainId,
	selectedAddress,
	removeFavoriteCollectible,
	// propsCallAPIGetCollectible,
	showHiddenMenu,
	onShowHiddenMenu,
	// onCheckToUpdateUnpinButton,
	checkedArrObject,
	onCheckedArrObject,
	onUnCheckedArrObject
}) {
	const [collectiblesGrid, setCollectiblesGrid] = useState([]);
	const [collectiblesVisible, setCollectiblesVisible] = useState(propsCollectiblesVisible);
	const [isPressItem, setIsPressItem] = useState(false);
	const [currentIndexPress, setCurrentIndexPress] = useState(-1)
	const [isPressCollectible, setIsPressCollectible] = useState(false);
	const actionSheetRef = useRef();
	const longPressedCollectible = useRef(null);
	const { colors, themeAppearance } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);

	const check = checkedArrObject.some(e => e.token_address === asset.token_address)

	const toggleCollectibles = useCallback(() => {
		// const addressToFetch = asset.primary_asset_contracts[0]?.address;
		// if (addressToFetch !== undefined && addressToFetch !== null) {
		// 	propsCallAPIGetCollectible(addressToFetch);
		// 	setCollectiblesVisible(!collectiblesVisible);
		// } else {
		// 	Alert.alert('Địa chỉ Collectible Contract không hợp lệ');
		// }
		setCollectiblesVisible(!collectiblesVisible);

	}, [collectiblesVisible, setCollectiblesVisible]);

	const onPressCollectible = useCallback(
		(collectible) => {
			const contractName = collectibleContracts.find(({ token_address }) => token_address === collectible.token_address)?.name;
			onPress(collectible, contractName || collectible.name);
		},
		[collectibleContracts, onPress]
	);

	const onLongPressCollectible = useCallback((collectible) => {
		actionSheetRef.current.show();
		longPressedCollectible.current = collectible;
	}, []);

	const removeCollectible = () => {
		const { CollectiblesController } = Engine.context;
		removeFavoriteCollectible(selectedAddress, chainId, longPressedCollectible.current);
		CollectiblesController.removeAndIgnoreCollectible(
			longPressedCollectible.current.address,
			longPressedCollectible.current.tokenId
		);
		Alert.alert(strings('wallet.collectible_removed_title'), strings('wallet.collectible_removed_desc'));
	};

	const refreshMetadata = () => {
		const { CollectiblesController } = Engine.context;

		CollectiblesController.addCollectible(
			longPressedCollectible.current.address,
			longPressedCollectible.current.tokenId
		);
	};

	const handleMenuAction = (index) => {
		if (index === 1) {
			removeCollectible();
		} else if (index === 0) {
			refreshMetadata();
		}
	};

	const renderCollectible = useCallback(
		(collectible, index) => {
			if (!collectible) return null;
			const name =
				collectible.name || collectibleContracts.find(({ token_address }) => token_address === collectible.token_address)?.name;
			const onPress = () => collectiblesVisible && onPressCollectible({ ...collectible, name });
			const onLongPress = () => (!asset.favorites ? onLongPressCollectible({ ...collectible, name }) : null);
			return (
				<View key={`collectible_${index}`} styles={styles.collectibleBox}>
					<TouchableOpacity onPress={onPress}
									onLongPress={onLongPress}
									onPressIn={() => {
										setIsPressCollectible(true);
										setCurrentIndexPress(index);
									}}
									onPressOut={() => {
										setIsPressCollectible(false);
										setCurrentIndexPress(-1);
									}}
									style={isPressCollectible && index === currentIndexPress && styles.zoomInItem}
									activeOpacity={1}
					>
						<View style={[index === 1 ? styles.collectibleInTheMiddle : {marginLeft: win.width / 15}]}>
							<CollectibleMedia
								style={styles.collectibleIcon}
								collectible={{
									...collectible,
									name,
									image: collectible.metadata?.image,
								}}
								// small
							/>
						</View>
					</TouchableOpacity>
				</View>
			);
		},
		[asset.favorites, collectibleContracts, onPressCollectible, onLongPressCollectible, styles]
	);

	useEffect(() => {
		const temp = splitIntoSubArrays([...contractCollectibles], 2);
		setCollectiblesGrid(temp);
	}, [contractCollectibles, setCollectiblesGrid]);
	return (
		<View style={styles.itemWrapper}>
			<TouchableOpacity onPress={() =>
			{	if (!showHiddenMenu) toggleCollectibles()
				else{
					if (check) {
						onUnCheckedArrObject()
					} else {
						onCheckedArrObject()
					}
				}
			}}
			// onLongPress={onShowHiddenMenu}
			onPressIn={() => setIsPressItem(true)}
			onPressOut={() => setIsPressItem(false)}
			style={isPressItem && styles.zoomInItem}
			activeOpacity={1}
			>
				<View style={styles.nftContainer}>
					<View style={{
						flex: 1,
						flexDirection: 'row',
						marginHorizontal: showHiddenMenu ? 0 : 0
					}}>
						{showHiddenMenu && (
							<View style={styles.checkBox}>
								<Checkbox
									size={20}
									keyValue={asset.token_address}
									value={asset.token_address}
									checked={check}
									onPress={() => {
										if (check) {
											onUnCheckedArrObject()
										} else {
											onCheckedArrObject()
										}
									}}
									pin={asset.pin}
									// checkedObjArr={checkedArrObject}
									// handleShowUnpinButton={onCheckToUpdateUnpinButton}
								/>
							</View>

						)}
						{/* {asset.pin ? (
							<Image
								source={require('../../../images-new/pin.png')}
								style={!showHiddenMenu ? styles.iconPin: styles.iconPinHidden}
								resizeMethod={'auto'}
							/>
						) : <View style={!showHiddenMenu ? styles.iconPin: styles.iconPinHidden}></View>} */}
						<View style={!showHiddenMenu?  styles.collectibleContractIconContainerHidden :styles.collectibleContractIconContainer}>
							{!asset.favorites ? (
								<View style={styles.shadowImage}>
									<CollectibleMedia
										iconStyle={styles.collectibleContractIcon}
										collectible={{
											name: strings('collectible.untitled_collection'),
											...asset,
											image: asset.image,
										}}
										tiny
										group
									/>
								</View>
							) : (
								<View style={[styles.favoritesLogoWrapper, styles.shadowImage]}>
									<AntIcons color={importedColors.white} name={'star'} size={32} />
								</View>
							)}
						</View>
						<View style={styles.verticalAlignedContainer}>
							<Text numberOfLines={1} style={styles.titleText}>
								{asset?.name || strings('collectible.untitled_collection')}
							</Text>
						</View>
					</View>
					<View style={styles.verticalAlignedContainerArrow}>
						<Text style={styles.titleTextLength}>{`${asset.nfts.length}`}</Text>
						{/* <Icon
							name={`ios-arrow-${collectiblesVisible ? 'down' : 'forward'}`}
							size={16}
							color={'white'}
							style={styles.arrowIcon}
						/> */}
						{!collectiblesVisible ? <Image source={require('../../../images-new/arrow-right.png')} style={styles.arrowIcon}></Image>
						: <Image source={require('../../../images-new/arrow-down.png')} style={styles.arrowIcon}></Image>}
					</View>
				</View>

			</TouchableOpacity>
			{/* {collectiblesVisible &&  */}

				{collectiblesGrid.map((row, i) => (
					<View key={i} style={[styles.collectiblesRowContainer, !collectiblesVisible && {height: 0, width: 0, opacity: 0},
						collectiblesVisible && {marginBottom: win.height/50}
					]}>
						{row.map((collectible, index) => renderCollectible(collectible, index))}
					</View>
				))}

			<ActionSheet
				ref={actionSheetRef}
				title={strings('wallet.collectible_action_title')}
				options={[strings('wallet.refresh_metadata'), strings('wallet.remove'), strings('wallet.cancel')]}
				cancelButtonIndex={2}
				destructiveButtonIndex={1}
				onPress={handleMenuAction}
				theme={themeAppearance}
			/>
		</View>
	);
}

CollectibleContractElement.propTypes = {
	/**
	 * Object being rendered
	 */
	asset: PropTypes.object,
	/**
	 * Array of collectibles
	 */
	contractCollectibles: PropTypes.array,
	/**
	 * Whether the collectibles are visible or not
	 */
	collectiblesVisible: PropTypes.bool,
	/**
	 * Called when the collectible is pressed
	 */
	onPress: PropTypes.func,
	collectibleContracts: PropTypes.array,
	/**
	 * Selected address
	 */
	selectedAddress: PropTypes.string,
	/**
	 * Chain id
	 */
	chainId: PropTypes.string,
	/**
	 * Dispatch remove collectible from favorites action
	 */
	removeFavoriteCollectible: PropTypes.func,
};

const mapStateToProps = (state) => ({
	collectibleContracts: collectibleContractsSelector(state),
	chainId: state.engine.backgroundState.NetworkController.provider.chainId,
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
});

const mapDispatchToProps = (dispatch) => ({
	removeFavoriteCollectible: (selectedAddress, chainId, collectible) =>
		dispatch(removeFavoriteCollectible(selectedAddress, chainId, collectible)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CollectibleContractElement);
