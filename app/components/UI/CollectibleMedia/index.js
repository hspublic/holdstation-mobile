import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Dimensions, StyleSheet, View, ViewPropTypes } from "react-native";
import RemoteImage from '../../Base/RemoteImage';
import MediaPlayer from '../../Views/MediaPlayer';
import scaling from '../../../util/scaling';
import Text from '../../Base/Text';
import Device from '../../../util/device';
import { useAppThemeFromContext, mockTheme } from '../../../util/theme';

const MEDIA_WIDTH_MARGIN = Device.isMediumDevice() ? 32 : 0;
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

const createStyles = (colors) =>
	StyleSheet.create({
		container(backgroundColor) {
			return {
				flex: 0,
				borderRadius: 16,
				backgroundColor: `#${backgroundColor}`,
			};
		},
		tinyImage: {
			width: 32,
			height: 32,
		},
		resizeModeCover: {
			resizeMode: 'cover',
		},
		smallImage: {
			width: 40,
			height: 40,
		},
		bigImage: {
			// height: 260,
			// width: 260,
			width: screenWidth - 48,
			height: screenWidth - 48,
			// marginTop: 12,
		},
		cover: {
			height: scaling.scale(Device.getDeviceWidth() - MEDIA_WIDTH_MARGIN, { baseModel: 2 }),
		},
		image: {
			borderRadius: 16,
		},
		textContainer: {
			alignItems: 'center',
			justifyContent: 'center',
			// backgroundColor: colors.background.alternative,
			backgroundColor: '#FFF',
			borderRadius: 8,
		},
		textWrapper: {
			textAlign: 'center',
			color: '#000',
		},
		textWrapperIcon: {
			textAlign: 'center',
			fontSize: 18,
			color: '#000',
		},
		mediaPlayer: {
			minHeight: 10,
		},
		groupImage: {
			borderRadius: 16,
			
		},
		marginImage: {
			marginLeft: - screenWidth / 80,
			marginRight: screenWidth / 70,
		}
	});

/**
 * View that renders an ERC-721 Token image
 */
export default function CollectibleMedia({ collectible, renderAnimation, style, group, tiny, small, big, cover, onClose }) {
	const [sourceUri, setSourceUri] = useState(null);
	const { colors } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);
	const fallback = () => setSourceUri(null);

	useEffect(() => {
		const { image, imagePreview, address } = collectible;
		if (address) {
			if (small && imagePreview && imagePreview !== '') setSourceUri(imagePreview);
			else setSourceUri(image);
		} else if (image !== null){
			setSourceUri(image);
		}
	}, [collectible, small, big, setSourceUri]);

	const renderMedia = useCallback(() => {
		if (renderAnimation && collectible.animation && collectible.animation.includes('.mp4')) {
			return (
				<View style={styles.marginImage}>
					<MediaPlayer
						onClose={onClose}
						uri={collectible.animation}
						style={[styles.mediaPlayer, group && styles.groupImage, cover && styles.cover, style, styles.resizeModeCover]}
					/>
				</View>
			);
		} else if (sourceUri) {
			/*
			 * the tiny boolean is used to indicate when the image is the NFT source icon
			 */
			return (
				<View style={styles.marginImage}>
					{big && <View style={[{ height: 32 }]} />}
					<RemoteImage
						fadeIn
						resizeMode={'cover'}
						source={{ uri: sourceUri }}
						style={[
							styles.image,
							group && styles.groupImage,
							tiny && styles.tinyImage,
							small && styles.smallImage,
							big && styles.bigImage,
							cover && styles.cover,
							style,
						]}
						onError={fallback}
					/>
				</View>
			);
		}
		return (
			<>
				{big && <View style={{ height: 32 }} />}
				<View
					style={[
						styles.textContainer,
						style,
						group && styles.groupImage,
						tiny && styles.tinyImage,
						small && styles.smallImage,
						big && styles.bigImage,
						cover && styles.cover,
						{overflow: 'hidden'},
						styles.resizeModeCover,
						styles.marginImage
					]}
				>
					<Text big={big} small={tiny || small} style={[tiny ? styles.textWrapperIcon : styles.textWrapper]}>
						{tiny ? collectible.name?.[0] || 'C' : `${collectible.name || ''} ${collectible.token_id ? `#${collectible.token_id}` : ''}`}
					</Text>
				</View>
			</>
		);
	}, [collectible, sourceUri, onClose, renderAnimation, style, tiny, small, big, cover, styles]);

	return <View style={styles.container(collectible.backgroundColor)}>{renderMedia()}</View>;
}

CollectibleMedia.propTypes = {
	/**
	 * Collectible object (in this case ERC721 token)
	 */
	collectible: PropTypes.object,
	/**
	 * Whether is tiny size or not
	 */
	group: PropTypes.bool,
	/**
	 * Whether is group or not
	 */
	tiny: PropTypes.bool,
	/**
	 * Whether is small size or not
	 */
	small: PropTypes.bool,
	/**
	 * Whether is big size or not
	 */
	big: PropTypes.bool,
	/**
	 * Whether render animation or not, if any
	 */
	renderAnimation: PropTypes.bool,
	/**
	 * Whether the media should cover the whole screen width
	 */
	cover: PropTypes.bool,
	/**
	 * Custom style object
	 */
	style: ViewPropTypes.style,
	/**
	 * On close callback
	 */
	onClose: PropTypes.func,
};
