import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import StyledButton from '../../StyledButton';
import { strings } from '../../../../../locales/i18n';
import { useAppThemeFromContext, mockTheme } from '../../../../util/theme';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { fontStyles } from '../../../../styles/common';
import LinearGradient from 'react-native-linear-gradient';
import { Image } from 'react-native-animatable';


const {width, height} = Dimensions.get('window')

const createStyles = (colors) =>
	StyleSheet.create({
		cancelWrapper:{
			alignItems: 'center', 
			justifyContent: 'center',
			marginTop: width / 40, 
			marginBottom: width / 20, 
		},
		viewWrapper: {
			flexDirection: 'column',
			justifyContent: 'center',
			alignItems: 'center',
			marginHorizontal: 20,
		},
		viewContainer: {
			width: '100%',
			backgroundColor: colors.background.default,
			borderRadius: 10,
		},
		actionHorizontalContainer: {
			flexDirection: 'row',
			padding: 16,
			borderTopWidth: 1,
			borderTopColor: colors.border.muted,
		},
		actionVerticalContainer: {
			flexDirection: 'column',
			paddingHorizontal: 16,
			paddingVertical: 8,
		},
		childrenContainer: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		button: {
			margin: 8,
		},
		buttonHorizontal: {
			flex: 1,
		},
		okButton:{
			height: width / 10,
			borderRadius: 12,
			width : width * 0.788,
			alignItems : 'center',
			justifyContent: 'center',
			marginTop: width / 40,
			flexDirection:'row'
		},
		disableButtonColor: {
			backgroundColor: '#3D3D4B'
		},
		zoominItem: {
			transform:[{scale: 0.97}]
		},
	});

/**
 * View that renders the content of an action modal
 * The objective of this component is to reuse it in other places and not
 * only on ActionModal component
 */
export default function ActionContent({
	cancelTestID,
	confirmTestID,
	cancelText,
	children,
	confirmText,
	confirmDisabled,
	cancelButtonMode,
	cancelButtonDisabled,
	confirmButtonMode,
	displayCancelButton,
	displayConfirmButton,
	onCancelPress,
	onConfirmPress,
	viewWrapperStyle,
	viewContainerStyle,
	actionContainerStyle,
	childrenContainerStyle,
	verticalButtons,
	type,
}) {
	const { colors } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);
	const [isPressOk, setIsPressOk] = useState(false);
	const [isPressCancel, setIsPressCancel] = useState(false);
	return (
		<View style={[styles.viewWrapper, viewWrapperStyle]}>
			<View style={[styles.viewContainer, viewContainerStyle]}>
				<View style={[styles.childrenContainer, childrenContainerStyle]}>{children}</View>
				<View
					style={[
						verticalButtons ? styles.actionVerticalContainer : styles.actionHorizontalContainer,
						actionContainerStyle,
					]}
				>
					{displayCancelButton && (
						<TouchableOpacity
							disabled={cancelButtonDisabled}
							testID={cancelTestID}
							onPress={onCancelPress}
							style={[styles.button, !verticalButtons && styles.buttonHorizontal, isPressOk && styles.zoominItem]}
							onPressIn={() => setIsPressOk(true)}
							onPressOut={() => setIsPressOk(false)} 
							activeOpacity={1}
						>
							{type === 'confirm_delete' ? 
							(<View
								style={[{borderRadius: 12}, styles.okButton]}
							>
								{type === 'confirm_delete' && 
									<Image source={require('../../../../images-new/button-delete.png')} style={{width: 20, height: 20}}/>
								}
								<Text style={{...fontStyles.semiBold, fontSize: 16, color:'#EB5252', marginLeft: width / 40}}>
										{confirmText}
								</Text>	
							</View>)
							:
							!cancelButtonDisabled? 
							(<LinearGradient
										colors={['#5E44FF', '#7785FF']}
										style={[{borderRadius: 12}, styles.okButton]}
										start={{x: 0, y: 0}}
										end={{x: 1, y: 0}}
								>
									<Text style={{...fontStyles.semiBold, fontSize: 16, color:'white'}}>
										{confirmText}
									</Text>
							</LinearGradient>)
							: 
							(<View
								style={[{borderRadius: 12}, styles.okButton, styles.disableButtonColor]}
							>
								<Text style={{...fontStyles.semiBold, fontSize: 16, color:'white'}}>
										{confirmText}
								</Text>	
							</View>)
							}		
							
						</TouchableOpacity>
						// <StyledButton
						// 	disabled={cancelButtonDisabled}
						// 	testID={cancelTestID}
						// 	type={cancelButtonMode}
						// 	onPress={onCancelPress}
						// 	containerStyle={[styles.button, !verticalButtons && styles.buttonHorizontal]}
						// >
						// 	{confirmText}
						// </StyledButton>
					)}
					{displayConfirmButton && (
						<TouchableOpacity
								testID={confirmTestID}
								onPress={onConfirmPress}
								style={[!verticalButtons && styles.buttonHorizontal, styles.cancelWrapper, isPressCancel && {transform: [{scale: 0.9}]}
							]}
								disabled={confirmDisabled}
								onPressIn={() => setIsPressCancel(true)}
								onPressOut={() => setIsPressCancel(false)} 
								activeOpacity={1}
						>
							
							<Text style={{...fontStyles.semiBold, fontSize: 16, color:'white'}}>
										{cancelText}
							</Text>
						</TouchableOpacity>
						// <StyledButton
						// 	testID={confirmTestID}
						// 	type={confirmButtonMode}
						// 	onPress={onConfirmPress}
						// 	containerStyle={[styles.button, !verticalButtons && styles.buttonHorizontal]}
						// 	disabled={confirmDisabled}
						// >
						// 	{cancelText}
						// </StyledButton>
					)}
				</View>
			</View>
		</View>
	);
}

ActionContent.defaultProps = {
	cancelButtonMode: 'neutral',
	cancelButtonDisabled: false,
	confirmButtonMode: 'warning',
	confirmTestID: '',
	cancelTestID: '',
	cancelText: strings('action_view.cancel'),
	confirmText: strings('action_view.confirm'),
	confirmDisabled: false,
	displayCancelButton: true,
	displayConfirmButton: true,
	viewWrapperStyle: null,
	viewContainerStyle: null,
	childrenContainerStyle: null,
};

ActionContent.propTypes = {
	cancelButtonDisabled: PropTypes.bool,
	/**
	 * TestID for the cancel button
	 */
	cancelTestID: PropTypes.string,
	/**
	 * TestID for the confirm button
	 */
	confirmTestID: PropTypes.string,
	/**
	 * Text to show in the cancel button
	 */
	cancelText: PropTypes.string,
	/**
	 * Content to display above the action buttons
	 */
	children: PropTypes.node,
	/**
	 * Type of button to show as the cancel button
	 */
	cancelButtonMode: PropTypes.string,
	/**
	 * Type of button to show as the confirm button
	 */
	confirmButtonMode: PropTypes.string,
	/**
	 * Whether confirm button is disabled
	 */
	confirmDisabled: PropTypes.bool,
	/**
	 * Text to show in the confirm button
	 */
	confirmText: PropTypes.string,
	/**
	 * Whether cancel button should be displayed
	 */
	displayCancelButton: PropTypes.bool,
	/**
	 * Whether confirm button should be displayed
	 */
	displayConfirmButton: PropTypes.bool,
	/**
	 * Called when the cancel button is clicked
	 */
	onCancelPress: PropTypes.func,
	/**
	 * Called when the confirm button is clicked
	 */
	onConfirmPress: PropTypes.func,
	/**
	 * View wrapper style
	 */
	viewWrapperStyle: PropTypes.object,
	/**
	 * View container style
	 */
	viewContainerStyle: PropTypes.object,
	/**
	 * Action container style
	 */
	actionContainerStyle: PropTypes.object,
	/**
	 * Whether buttons are rendered vertically
	 */
	verticalButtons: PropTypes.bool,
	/**
	 * Children container style
	 */
	childrenContainerStyle: PropTypes.object,
};
