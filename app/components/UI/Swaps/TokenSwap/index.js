import React, { useCallback, useMemo, useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet,
    TextInput,
    TouchableOpacity,
    View,
    TouchableWithoutFeedback,
    ActivityIndicator,
    InteractionManager,
    Pressable,
    Keyboard, Image, KeyboardAvoidingView, Alert
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context'

import { FlatList } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/Ionicons';
import FAIcon from 'react-native-vector-icons/FontAwesome5';
import Fuse from 'fuse.js';
import { connect } from 'react-redux';
import { isValidAddress } from 'ethereumjs-util';

import Device from '../../../../util/device'
import {
    balanceToFiat, hexToBN, renderFromTokenMinimalUnit,
    renderFromWei, weiToFiat, toTokenMinimalUnit, fromTokenMinimalUnitString,
    safeNumberToBN, toHexadecimal, fiatNumberToTokenMinimalUnit, isDecimal,
    fiatNumberToWei, fromWei, toWei, addHexPrefix, fromTokenMinimalUnit, weiToFiatNumber
} from '../../../../util/number';

import { safeToChecksumAddress } from '../../../../util/address';
import { strings } from '../../../../../locales/i18n';
import { fontStyles } from '../../../../styles/common';

import Text from '../../../Base/Text';
import ListItem from '../../../Base/ListItem';
import ModalDragger from '../../../Base/ModalDragger';
import TokenIcon from '../components/TokenIcon';
// import Alert from '../../../Base/Alert';
import useBlockExplorer from '../utils/useBlockExplorer';
import useFetchTokenMetadata from '../utils/useFetchTokenMetadata';
import useModalHandler from '../../../Base/hooks/useModalHandler';
import TokenImportModal from '../components/TokenImportModal';
import Analytics from '../../../../core/Analytics';
import { ANALYTICS_EVENT_OPTS } from '../../../../util/analytics';
import { useAppThemeFromContext, mockTheme } from '../../../../util/theme';
import {
    setSwapsHasOnboarded,
    setSwapsLiveness,
    swapsControllerTokens,
    swapsHasOnboardedSelector,
    swapsTokensSelector,
    swapsTokensWithBalanceSelector,
    swapsTopAssetsSelector,
} from '../../../../reducers/swaps';
import FeatherIcon from 'react-native-vector-icons/Feather';
import Logger from '../../../../util/Logger';
import Engine from '../../../../core/Engine';
import Clipboard from '@react-native-clipboard/clipboard';
import Networks from '../../../../util/networks';
import Tooltip from '../../PaymentRequest/Tooltip';
import { getErrorMessage, getFetchParams, getQuotesNavigationsParams, isSwapsNativeAsset, estimateGas } from '../utils';
import BigNumber from 'bignumber.js';
import { swapsUtils } from '@metamask/swaps-controller';
import ApproveTransaction from '../ApproveTransaction'
import { decodeApproveData, getTicker } from '../../../../util/transactions';
import TransactionDetail from '../TransactionDetail'
import PrivateCredential from '../PrivateCredential'
import OrderExecuted from '../OrderExecuted'
import SlippageCustom from '../SlippageCustom'
import { SWAP_ADDRESS_FEE_RECEIVER, SWAP_FEE_AMOUNT, SWAP_CHARGE_FEE, SWAP_CLIENT_DATA, USE_MAX_PERCENT } from '../../../../constants/transaction'
const KEYBOARD_OFFSET = 120;

const gasFeeLevel = [{
    saveGas: '0',
    title: 'Chuẩn'
},
{
    saveGas: '1',
    title: 'Tiết kiệm'
}]

const slippageOption = [{
    slippage: '0.1',
    title: '0.1%'
},
{
    slippage: '0.5',
    title: '0.5%'
},
{
    slippage: '1',
    title: '1%'
}]

const createStyles = (colors) =>
    StyleSheet.create({
        wrapper: {
            flex: 1,
            backgroundColor: '#1B1B23',
            paddingHorizontal: 20
        },
        authWrapper: {
            backgroundColor: '#1B1B23',
            flex: 1,
        },
        contentWrapper: {
            paddingTop: 24,
        },
        title: {
            ...fontStyles.normal,
            fontSize: 16,
            color: colors.text.default,
        },
        amountWrapper: {
            flex: 1,
            // backgroundColor: 'blue'
        },
        searchWrapper: {
            marginVertical: 15,
            borderColor: colors.border.default,
            borderWidth: 1,
            borderRadius: 8,
            flexDirection: 'row',
        },
        searchInput: {
            paddingTop: Device.isAndroid() ? 12 : 0,
            paddingLeft: 8,
            fontSize: 16,
            height: 60,
            flex: 1,
            color: '#FFFFFF', // colors.text.default,
            ...fontStyles.normal,
        },
        searchIcon: {
            textAlignVertical: 'center',
            marginLeft: 12,
            alignSelf: 'center',
        },
        clearButton: { paddingHorizontal: 12, justifyContent: 'center' },

        input: {
            ...fontStyles.normal,
            // backgroundColor: colors.background.default,
            // borderWidth: 0,
            fontSize: 16,
            // paddingBottom: 0,
            // paddingRight: 0,
            // paddingLeft: 0,
            // paddingTop: 0,
            color: '#FFFFFF',  // colors.text.default,
            maxWidth: '45%'
        },
        eth: {
            ...fontStyles.normal,
            fontSize: 16,
            paddingTop: Device.isAndroid() ? 3 : 0,
            paddingLeft: 10,
            textTransform: 'uppercase',
            color: '#FFFFFF' // colors.text.default,
        },
        max: {
            ...fontStyles.normal,
            fontSize: 12,
            color: '#FFFFFF' // colors.text.default,
        },
        fiatValue: {
            ...fontStyles.normal,
            fontSize: 16,
            color: '#FFFFFF', // colors.text.default,
            maxWidth: '45%'
        },
        firstAmount: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 15,
            alignItems: 'center'
        },
        ethContainer: {
            flex: 1,
            flexDirection: 'row',
            paddingLeft: 6,
            paddingRight: 10,
        },
        container: {
            // flex: 1,
            flexDirection: 'row',
            // paddingRight: 10,
            paddingVertical: 20,
            paddingHorizontal: 10,
            // paddingLeft: 14,
            // position: 'relative',
            borderColor: 'rgba(255, 255, 255, 0.2)', // colors.border.default,
            borderWidth: 1,
            borderRadius: 15,
            shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
        },
        amounts: {
            width: '100%',
        },
        switchContainer: {
            flex: 1,
            flexDirection: 'column',
            alignSelf: 'center',
            right: 0,
        },
        switchTouchable: {
            flexDirection: 'row',
            alignSelf: 'flex-end',
            right: 0,
        },
        enterAmountWrapper: {
            flex: 1,
            flexDirection: 'column',
        },
        button: {
            marginBottom: 16,
        },
        buttonsWrapper: {
            flexDirection: 'row',
            alignSelf: 'center',
        },
        buttonsContainer: {
            flex: 1,
            flexDirection: 'column',
            alignSelf: 'flex-end',
        },
        scrollViewContainer: {
            flexGrow: 1,
        },
        errorWrapper: {
            // backgroundColor: colors.error.muted,
            borderRadius: 4,
            marginTop: 8,
        },
        errorText: {
            color: colors.text.default,
            alignSelf: 'center',
        },
        assetsWrapper: {
            // marginTop: 16,
        },
        assetsTitle: {
            ...fontStyles.normal,
            fontSize: 16,
            marginBottom: 12,
            color: "#FFFFFF",
            fontWeight: '400'
        },
        secondaryAmount: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
        currencySymbol: {
            ...fontStyles.normal,
            fontSize: 24,
            color: colors.text.default,
        },
        currencySymbolSmall: {
            ...fontStyles.normal,
            fontSize: 16,
            color: '#FFFFFF' // colors.text.default,
        },
        titleWrapper: {
            width: '100%',
            // height: 33,
            alignItems: 'center',
            justifyContent: 'center',
        },
        dragger: {
            width: 48,
            height: 5,
            borderRadius: 4,
            backgroundColor: colors.border.default,
            opacity: 0.6,
            marginTop: 10
        },
        tabUnderlineStyle: {
            height: '100%',
            zIndex: -1,
            backgroundColor: '#3D3D4B',
            borderRadius: 10,
            alignItems: 'center'
            // marginHorizontal: 10
        },
        tabStyle: {
            padding: 0,
        },
        textStyle: {
            fontSize: 16,
            letterSpacing: 0.5,
            ...fontStyles.bold,
            fontWeight: '400',
            marginTop: 10
        },
        tabBar: {
            borderColor: 'transparent',
            marginTop: 10,
            borderRadius: 10,
        },
        label: {
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 10
        },
        labelText: {
            ...fontStyles.normal,
            color: '#FFFFFF',
            fontSize: 16,
            width: '60%'
        },
        ethLogo: {
            width: 28,
            height: 28,
        },
        assetContainer: {
            // backgroundColor: '#282832',
            borderColor: 'rgba(255, 255, 255, 0.2)', // colors.border.default,
            borderWidth: 1,
            borderRadius: 15,
            padding: 6,
            paddingHorizontal: 8,
            flexDirection: 'row',
            alignItems: 'center'
        },
        assetInfo: {
            alignSelf: 'center',
            justifyContent: 'center',
            marginLeft: 10
        },
        textName: {
            ...fontStyles.normal,
            fontSize: 16,
            color: '#FFFFFF',
            fontWeight: "500",
            marginHorizontal: 8
        },
        textSymbol: {
            ...fontStyles.normal,
            fontSize: 14,
            color: '#A0A0B1',
            marginTop: 8,
            textAlign: 'right'
        },
        count: {
            ...fontStyles.normal,
            fontSize: 15,
            color: '#FFFFFF',
            fontWeight: "500"
        },
        feeContainer: {
            justifyContent: 'space-between',
            flexDirection: 'row',
            // paddingHorizontal: 20,
            marginTop: 15,
        },
        feeEstimate: {
            // ...fontStyles.normal,
            fontSize: 14,
            color: '#A0A0B1',
            marginTop: 5,
            fontWeight: '500'
        },
        fee: {
            ...fontStyles.normal,
            fontSize: 16,
            color: '#FFFFFF',
            marginTop: 5,
            fontWeight: '500'
        },
        iconSetting: {
            width: 30,
            height: 30,
            tintColor: '#58586C'
        },
        iconCheck: {
            marginRight: 15,
            width: 25,
            height: 25,
            tintColor: '#3D3D4B', // '#2BEB4A' // #3D3D4B
        },
        iconSwap: {
            width: 40,
            height: 40,
        },
        buttonStyle: {
            height: 45,
            backgroundColor: '#6A45FF',
            // marginHorizontal: 15,
            borderRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
            // marginBottom: 10
        },
        content: {
            color: "#FFFFFF",
            fontSize: 16,
        },
        titleWarning: {
            ...fontStyles.normal,
            fontSize: 18,
            color: '#FFFFFF',
            fontWeight: '500',
            textAlign: 'center',
            marginTop: 15
        },
        btnWarning: {
            backgroundColor: '#3D3D4B',
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
            padding: 15,
            marginTop: 15
        },
        contentBtnWarning: {
            ...fontStyles.normal,
            fontSize: 16,
            color: '#FFFFFF',
            textAlign: 'center'
        },
        iconWarning: {
            width: 30,
            height: 30,
            tintColor: '#B22828'
        },
        warning: {
            width: 50,
            height: 50,
        },
        iconArrow: {
            width: 16,
            height: 16,
        },
    });

function TokenSwap({
    navigation, route,
    network,
    selectedAddress, chainId,
    conversionRate, contractExchangeRates, currentCurrency, swapsTokens, accounts, identities,
    nativeCurrency
}) {

    const accountInfo = { address: selectedAddress, ...identities[selectedAddress], ...accounts[selectedAddress] };

    const nativeToken = route.params?.nativeToken
    const [sourceToken, setSourceToken] = useState(route.params?.sourceToken)
    const [destinationToken, setDestinationToken] = useState(route.params?.destinationToken)
    const [resultSwaps, setResultSwaps] = useState(false)
    const [slippage, setSlippage] = useState('0.5') // Mức trượt giá [0, 2000], 10 means 0.1% (default value is 50/10000 ~ 0.5%)
    const [slippageIndex, setSlippageIndex] = useState(false) // Hiển thi thay đổi trượt giá
    const [toolTipVisible, setToolTipVisible] = useState(false) // Hiển thi thay đổi phí gas
    const [amount, setAmount] = useState(0) // Số lương token hoán đổi
    const [outAmount, setOutAmount] = useState(0)
    const inputAmount = toTokenMinimalUnit(amount, sourceToken?.decimals).toString(10)

    const [countDown, setCountdown] = useState(15) // Số lương token hoán đổi
    const [saveGas, changeSaveGas] = useState('0')
    const [messageError, setMessageError] = useState("Hãy nhập số lượng cần hoán đổi")
    const [checkInfoSwap, setCheckInfoSwap] = useState(false)
    const [transactionDetail, showTransactionDetail] = useState(false)
    const [authTransaction, setAuthTransaction] = useState(false)
    const [loading, setLoading] = useState(false)
    const [transactionConfirmed, setTransactionConfirmed] = useState(false)
    const [approvalRequire, setApprovalRequire] = useState(false)
    const [approvalTransaction, setApprovalTransaction] = useState(false)
    const [customSlippage, showCustomSlippage] = useState(false)
    const [stopQuery, setStopQuery] = useState(false)


    async function handleSwapsToken(sourceToken, destinationToken, amountIn, saveGas) {
        // Gọi API của kyber để lấy data encode
        const { SwapsTokenController } = Engine.context;
        const fetchParams = {
            // chainId: chainId, // 1 - mainnet , chainId :  137
            amountIn: amountIn,
            to: selectedAddress,
            tokenIn: sourceToken.address,
            tokenOut: destinationToken.address,
            saveGas: saveGas, //  1
            slippageTolerance: `${Number(slippage) * 100}`, // 50 -> 0.5%
            isInBps: true,
            feeAmount: SWAP_FEE_AMOUNT,
            chargeFeeBy: SWAP_CHARGE_FEE,
            feeReceiver: SWAP_ADDRESS_FEE_RECEIVER,
            clientData: SWAP_CLIENT_DATA
        }
        const result = await SwapsTokenController.retrieveInfoSwapTokens(fetchParams);
        if (result) {
            const outAmount = renderFromTokenMinimalUnit(result.outputAmount, destinationToken.decimals)
            setOutAmount(outAmount)
        } else {
            return
        }
        // Nếu không phải native asset thì kiểm tra hạn mức cấp quyền
        if (!isSwapsNativeAsset(sourceToken)) {
            // Kiểm tra đầu vào nhập có lớn hơn số dư hiện có hay không?
            if (!result?.routerAddress || Number(amount) > Number(sourceToken.balance)) {
                setMessageError("Không đủ số dư")
                setApprovalRequire(false)
                setCheckInfoSwap(false)
                setResultSwaps(result)
                return
            }
            const amountApproval = await SwapsTokenController.getERC20Allowance(selectedAddress, result.routerAddress, sourceToken.address)
            // Hiển thị trạng thái cấp quyền nếu amount approval < input amount
            if (amountApproval?.code === 200
                && amountApproval?.result
                && Number(amountApproval.result) < Number(amountIn)) {
                setMessageError("Cấp quyền")
                setApprovalRequire(true)
                setCheckInfoSwap(false)
                setResultSwaps(result)
                return
            }
        }
        // Lấy gas estimate
        const gasFeeWei = await SwapsTokenController.estimateGasTransaction({
            from: selectedAddress,
            to: result.routerAddress,
            data: result.encodedSwapData,
            value: isSwapsNativeAsset(sourceToken) ? amountIn : 0
        })
        if (!gasFeeWei) {
            setMessageError("Trượt giá thấp")
            setApprovalRequire(false)
            setCheckInfoSwap(false)
        } else {
            result.estimateGasNative = renderFromWei(gasFeeWei.gasFee)
            result.estimateGasUsd = weiToFiat(toWei(result.estimateGasNative), conversionRate, currentCurrency)
            result.gasLimit = gasFeeWei.gasLimit
            result.gasPrice = gasFeeWei.gasPrice
            // Kiểm tra số dư
            if (sourceToken.isETH) {
                if (result.estimateGasNative && result.estimateGasNative !== "0") {
                    if (parseFloat(amount) + parseFloat(result.estimateGasNative) > parseFloat(sourceToken.balance)) {
                        setMessageError("Không đủ số dư")
                        setApprovalRequire(false)
                        setCheckInfoSwap(false)
                    } else {
                        setMessageError("Kiểm tra thông tin trước khi hoán đổi")
                        setApprovalRequire(false)
                        setCheckInfoSwap(true)
                    }
                } else {
                    setMessageError("Hãy nhập số lượng cần hoán đổi")
                    setApprovalRequire(false)
                    setCheckInfoSwap(false)
                }
            } else {
                if (result.estimateGasNative && result.estimateGasNative !== "0") {
                    if (nativeToken) {
                        nativeToken.balance = renderFromWei(accounts[selectedAddress] && accounts[selectedAddress].balance);
                    }
                    if (!nativeToken
                        || parseFloat(result.estimateGasNative) > parseFloat(nativeToken.balance)
                        || parseFloat(amount) > parseFloat(sourceToken.balance)) {
                        setMessageError("Không đủ số dư")
                        setCheckInfoSwap(false)
                        setApprovalRequire(false)
                    } else {
                        setMessageError("Kiểm tra thông tin trước khi hoán đổi")
                        setCheckInfoSwap(true)
                        setApprovalRequire(false)
                    }
                } else {
                    setMessageError("Hãy nhập số lượng cần hoán đổi")
                    setApprovalRequire(false)
                    setCheckInfoSwap(false)
                }
            }
        }
        setResultSwaps(result)
    }

    async function startSwapsToken() {
        if (transactionConfirmed || authTransaction) {
            return
        }
        handleSwapsToken(sourceToken, destinationToken, inputAmount, saveGas)
    }

    async function swapInputToken() {
        if (transactionConfirmed || authTransaction) {
            return
        }
        setCountdown(15)
        setResultSwaps(false)
        setSourceToken(destinationToken)
        setDestinationToken(sourceToken)
        setCheckInfoSwap(false)
        setMessageError("Hãy nhập số lượng cần hoán đổi")

        if (amount && amount !== "0") {
            handleSwapsToken(destinationToken, sourceToken, toTokenMinimalUnit(amount, destinationToken?.decimals).toString(10), saveGas)
        }

        // const { SwapsTokenController } = Engine.context;
        // const fetchParams = {
        //     amountIn: toTokenMinimalUnit(amount, destinationToken?.decimals).toString(10),
        //     to: selectedAddress,
        //     tokenIn: destinationToken.address,
        //     tokenOut: sourceToken.address,
        //     saveGas: saveGas,
        //     slippageTolerance: `${Number(slippage) * 100}`,
        //     isInBps: true,
        //     feeAmount: SWAP_FEE_AMOUNT,
        //     chargeFeeBy: SWAP_CHARGE_FEE,
        //     feeReceiver: SWAP_ADDRESS_FEE_RECEIVER,
        //     clientData: SWAP_CLIENT_DATA
        // }
        // const result = await SwapsTokenController.retrieveInfoSwapTokens(fetchParams);
        // if (result) {
        //     const outAmount = renderFromTokenMinimalUnit(result.outputAmount, sourceToken.decimals)
        //     setOutAmount(outAmount)
        // } else {
        //     return
        // }
        // // console.log('swapInputToken result: ', result.gasUsd, result.gasPriceGwei)
        // const gasFeeWei = await SwapsTokenController.estimateGasTransaction({
        //     from: selectedAddress,
        //     to: result.routerAddress,
        //     data: result.encodedSwapData,
        //     value: isSwapsNativeAsset(destinationToken) ? toTokenMinimalUnit(amount, destinationToken?.decimals).toString(10) : 0,
        // })
        // // console.log('swapInputToken gasFeeWei : ', gasFeeWei, conversionRate, renderFromWei(gasFeeWei.gasFee))
        // if (!gasFeeWei) {
        //     setMessageError("Không đủ số dư")
        //     setCheckInfoSwap(false)
        // } else {
        //     result.estimateGasNative = renderFromWei(gasFeeWei.gasFee) // renderFromWei(fiatNumberToWei(result.estimateGasUsd, conversionRate));
        //     result.estimateGasUsd = weiToFiat(toWei(result.estimateGasNative), conversionRate, currentCurrency) // weiToFiatNumber(result.estimateGasNative) // estimateGas(result.gasUsd)
        //     result.gasLimit = gasFeeWei.gasLimit
        //     result.gasPrice = gasFeeWei.gasPrice
        //     // Kiểm tra số dư
        //     if (result.estimateGasNative && result.estimateGasNative !== "0") {
        //         if (parseFloat(amount) + parseFloat(result.estimateGasNative) > parseFloat(sourceToken.balance)) {
        //             setMessageError("Không đủ số dư")
        //             setCheckInfoSwap(false)
        //         } else {
        //             setMessageError("Kiểm tra thông tin trước khi hoán đổi")
        //             setCheckInfoSwap(true)
        //         }
        //     } else {
        //         setMessageError("Hãy nhập số lượng cần hoán đổi")
        //         setCheckInfoSwap(false)
        //     }
        // }
        // setResultSwaps(result)
    }

    async function swapToken(privateKey) {
        // setLoading(true)
        const { SwapsTokenController } = Engine.context;
        const transactionObject = {
            from: selectedAddress,
            to: resultSwaps.routerAddress,
            data: resultSwaps.encodedSwapData,
            value: isSwapsNativeAsset(sourceToken) ? inputAmount : 0,
            privateKey: privateKey,
            gasLimit: resultSwaps.gasLimit,
            gasPrice: resultSwaps.gasPrice
        }
        showTransactionDetail(false)
        setTimeout(() => {
            // setLoading(false)
            // Hiển thị lệnh gửi đang đc thực hiện
            setTransactionConfirmed(true)
        }, 400)
        await SwapsTokenController.swapTokens(transactionObject,
            async () => {
                const { TokensController } = Engine.context;
                const { AssetsContractController } = Engine.context;
                const decimals = await AssetsContractController.getERC20TokenDecimals(destinationToken.address);
                const symbol = await AssetsContractController.getERC721AssetSymbol(destinationToken.address);
                await TokensController.addToken(destinationToken.address, symbol, decimals);
            }, (message) => {
                Alert.alert(strings('transactions.transaction_error'), message || "", [
                    {
                        text: strings('navigation.ok'), onPress: () => {
                        }
                    },
                ]);
            })
    }

    async function approvalLimitTransaction(privateKey, amountApprove, gasLimit, gasPrice) {
        setLoading(true)
        const { SwapsTokenController } = Engine.context;
        const approveObject = {
            contractAddress: sourceToken.address,
            from: selectedAddress,
            to: resultSwaps.routerAddress,
            gasLimit: gasLimit,
            gasPrice: gasPrice,
            value: amountApprove,
            privateKey: privateKey
        }
        await SwapsTokenController.approveERC20(approveObject)
        setLoading(false)
        setApprovalTransaction(false)
        startSwapsToken()
    }

    async function useMax() {
        const { SwapsTokenController } = Engine.context;
        const fetchParams = {
            amountIn: toTokenMinimalUnit(sourceToken.balance, sourceToken?.decimals).toString(10),
            to: selectedAddress,
            tokenIn: sourceToken.address,
            tokenOut: destinationToken.address,
            saveGas: saveGas, //  1
            slippageTolerance: `${Number(slippage) * 100}`, // 50 -> 0.5%
            isInBps: true,
            feeAmount: SWAP_FEE_AMOUNT,
            chargeFeeBy: SWAP_CHARGE_FEE,
            feeReceiver: SWAP_ADDRESS_FEE_RECEIVER,
            clientData: SWAP_CLIENT_DATA
        }
        const result = await SwapsTokenController.retrieveInfoSwapTokens(fetchParams);
        if (!result) {
            setAmount('0')
            return
        }
        const gasFeeWei = await SwapsTokenController.estimateGasTransaction({
            from: selectedAddress,
            to: result.routerAddress,
            data: result.encodedSwapData,
            value: toTokenMinimalUnit(Number(sourceToken.balance), sourceToken?.decimals).toString(10)
        })
        if (gasFeeWei) {
            result.estimateGasNative = renderFromWei(gasFeeWei.gasFee)
            if (Number(sourceToken.balance) - result.estimateGasNative > 0) {
                setAmount(`${Number(sourceToken.balance) - result.estimateGasNative}`)
            } else {
                setAmount('0')
            }
        } else {
            setAmount('0')
        }
    }

    useEffect(() => {
        setCountdown(15)
        if (!transactionConfirmed && !authTransaction) {
            if (inputAmount && inputAmount !== "0") {
                startSwapsToken()
            } else {
                setResultSwaps(false)
            }
        }

    }, [slippage, inputAmount, chainId, saveGas, transactionConfirmed, authTransaction]);

    useEffect(() => {
        const timer = setInterval(() => {
            if (inputAmount && inputAmount !== "0" && !transactionConfirmed && !authTransaction) {
                if (countDown <= 1) {
                    setCountdown(15)
                    startSwapsToken()
                } else {
                    setCountdown(countDown - 1)
                }
            }
        }, 1000);
        if (!checkInfoSwap) {
            clearInterval(timer)
        }
        return () => clearInterval(timer)
    }, [countDown, inputAmount, transactionConfirmed, authTransaction, checkInfoSwap])

    const { colors, themeAppearance } = useAppThemeFromContext() || mockTheme;
    const styles = createStyles(colors);

    const symbol = ""

    let name = ""
    if (network.provider?.nickname) {
        name = network.provider.nickname;
    } else {
        name =
            (Networks[network.provider.type] && Networks[network.provider.type].name) ||
            { ...Networks.rpc, color: null }.name;
    }

    function handleUnhandledTouches() {
        Keyboard.dismiss();
        return false;
    }

    if (authTransaction) {
        return <PrivateCredential
            onSend={swapToken}
            onBack={() => setAuthTransaction(false)} />
    }

    return (
        <SafeAreaView style={styles.wrapper} onStartShouldSetResponder={handleUnhandledTouches}>
            <View style={styles.titleWrapper}>
                <View style={styles.dragger} />
            </View>
            <View style={styles.titleWrapper}>
                <Text style={{
                    color: '#FFFFFF',
                    fontSize: 18, paddingTop: 12,
                    fontWeight: "500"
                }}>{'Hoán đổi'}
                </Text>
            </View>
            <View style={styles.amountWrapper}>
                <View style={{
                    flex: 1,
                    marginTop: 15,
                }}>
                    <View style={styles.container}>
                        <View style={styles.ethContainer}>
                            <View style={styles.amounts}>
                                <Text style={[styles.fee, { textAlign: 'center', marginTop: -10 }]}>{`Mạng: ${name}`}</Text>
                                <View style={styles.firstAmount}>
                                    {/* {internalPrimaryCurrency !== 'ETH' && (
                                    <Text style={styles.currencySymbol}>{currencySymbol}</Text>
                                )} */}
                                    <TextInput
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        keyboardType="numeric"
                                        numberOfLines={1}
                                        onChangeText={(value) => {
                                            setCountdown(15)
                                            setResultSwaps(false)
                                            setCheckInfoSwap(false)
                                            setApprovalRequire(false)
                                            setAmount(parseFloat(value.replace(',', '.')) || 0)
                                        }}
                                        placeholder={`Nhập số ${symbol || sourceToken.symbol}`}
                                        placeholderTextColor={'#58586C'}
                                        spellCheck={false}
                                        style={styles.input}
                                        value={amount}
                                        // onSubmitEditing={this.onNext}
                                        autoFocus
                                        // ref={this.amountInput}
                                        // testID={'request-amount-input'}
                                        keyboardAppearance={themeAppearance}
                                    />
                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'center'
                                    }}>
                                        <Pressable style={{
                                            padding: 5, backgroundColor: 'rgba(255, 255, 255, 0.1)',
                                            borderRadius: 30,
                                            marginRight: 10
                                        }}
                                            onPress={() => {
                                                if (isSwapsNativeAsset(sourceToken)) {
                                                    useMax()
                                                } else {
                                                    // setAmount(`${USE_MAX_PERCENT * Number(sourceToken.balance)}`)
                                                    setAmount(sourceToken.balance)
                                                }
                                            }}
                                        >
                                            <Text style={styles.max} numberOfLines={1}>
                                                {'Max'}
                                            </Text>
                                        </Pressable>
                                        {/* <Text style={styles.eth} numberOfLines={1}>
                                        {symbol}
                                    </Text> */}
                                        <View style={styles.assetContainer}>
                                            <TokenIcon medium icon={sourceToken.iconUrl} symbol={sourceToken.symbol} />
                                            <Text style={styles.textName} numberOfLines={1}>{sourceToken.symbol}</Text>
                                            <Image source={require('../../../../images/arrowRight.png')}
                                                resizeMode={"contain"}
                                                style={styles.iconArrow}>
                                            </Image>
                                        </View>
                                    </View>
                                </View>
                                <Text style={styles.textSymbol} numberOfLines={1}>{`Số dư : ${sourceToken.balance} ${sourceToken.symbol}`}</Text>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    marginVertical: 20
                                }}>
                                    <View style={{
                                        height: 40, width: 40,
                                        borderColor: 'rgba(255, 255, 255, 0.2)',
                                        borderRadius: 20, borderWidth: 2.5,
                                        justifyContent: 'center', alignItems: 'center'
                                    }}>
                                        <Text style={styles.count}>{`${countDown}`}</Text>
                                    </View>
                                    <View style={{
                                        flex: 1,
                                        height: 1,
                                        backgroundColor: 'rgba(255, 255, 255, 0.2)', marginHorizontal: 10
                                    }}></View>
                                    <Pressable onPress={swapInputToken}>
                                        <Image source={require('../../../../images/Swap.png')}
                                            resizeMode={"contain"}
                                            style={styles.iconSwap}></Image>
                                    </Pressable>
                                </View>
                                <View style={styles.secondaryAmount}>
                                    {/* <Text style={styles.currencySymbolSmall}>{'~'}</Text> */}
                                    {/* {secondaryAmount && (
                                        <Text style={styles.fiatValue} numberOfLines={1}>
                                            {secondaryAmount}
                                        </Text>
                                    )}
                                    {secondaryAmount && internalPrimaryCurrency === 'ETH' && (
                                        <Text style={styles.currencySymbolSmall}>{'USD'}</Text>
                                    )} */}
                                    {/* {secondaryAmount && (
                                        <Text style={styles.fiatValue} numberOfLines={1}>
                                            {secondaryAmount}
                                        </Text>
                                    )} */}
                                    {/* {
                                        resultSwaps && resultSwaps.outputAmount ? <Text numberOfLines={1} style={styles.fiatValue}>
                                            {`${renderFromTokenMinimalUnit(resultSwaps.outputAmount, destinationToken.decimals)}`}
                                        </Text> : amount ? <ActivityIndicator size={"small"} /> :
                                            <Text numberOfLines={1} style={styles.fiatValue}>{"0"}</Text>
                                    } */}
                                    {
                                        amount && amount !== "0" && outAmount ? <Text numberOfLines={1} style={styles.fiatValue}>
                                            {`${outAmount}`}
                                        </Text> : <Text numberOfLines={1} style={styles.fiatValue}>{"0"}</Text>
                                    }
                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'center'
                                    }}>
                                        {/* <Text style={styles.eth} numberOfLines={1}>
                                        {symbol}
                                    </Text> */}
                                        <View style={styles.assetContainer}>
                                            <TokenIcon medium icon={destinationToken.iconUrl} symbol={destinationToken.symbol} />
                                            <Text style={styles.textName} numberOfLines={1}>{destinationToken.symbol}</Text>
                                            <Image source={require('../../../../images/arrowRight.png')}
                                                resizeMode={"contain"}
                                                style={styles.iconArrow}>
                                            </Image>
                                        </View>
                                    </View>
                                </View>
                                <Text style={styles.textSymbol} numberOfLines={1}>{`Số dư : ${destinationToken.balance} ${destinationToken.symbol}`}</Text>
                            </View>

                            {/* {switchable && (
                            <View style={styles.switchContainer}>
                                <TouchableOpacity
                                    onPress={this.switchPrimaryCurrency}
                                    style={styles.switchTouchable}
                                >
                                    <FontAwesome
                                        name="exchange"
                                        size={18}
                                        color={colors.icon.default}
                                        style={{ transform: [{ rotate: '270deg' }] }}
                                    />
                                </TouchableOpacity>
                            </View>
                        )} */}

                        </View>
                    </View>
                    <View style={styles.feeContainer}>
                        {
                            resultSwaps.estimateGasUsd ?
                                <View style={{ flex: 1.2, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 15 }}>
                                    <View>
                                        <Text style={styles.feeEstimate}>{'Phí gas ước tính'}</Text>
                                        <Text style={styles.fee}>{`${resultSwaps?.estimateGasUsd || 0}`}</Text>
                                    </View>
                                    <Tooltip
                                        isVisible={toolTipVisible}
                                        contentStyle={{
                                            padding: 0,
                                            borderRadius: 15,
                                        }}
                                        backgroundStyle={{
                                            backgroundColor: 'transparent',
                                        }}
                                        content={<View style={{
                                            padding: 15,
                                            paddingTop: 0,
                                            paddingRight: 50,
                                            backgroundColor: '#1B1B23',
                                            borderColor: 'rgba(255, 255, 255, 0.1)',
                                            borderWidth: 1,
                                            borderRadius: 15,
                                        }}>
                                            {
                                                gasFeeLevel.map((item, index) => {
                                                    return <Pressable
                                                        key={`ii${index}`}
                                                        style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}
                                                        onPress={() => {
                                                            // setToolTipVisible(false)
                                                            changeSaveGas(item.saveGas)
                                                        }}>
                                                        <Image source={require('../../../../images/check.png')}
                                                            resizeMode={"contain"}
                                                            style={[styles.iconCheck, { tintColor: saveGas === String(index) ? '#2BEB4A' : "#3D3D4B" }]}>
                                                        </Image>
                                                        <View>
                                                            <Text style={styles.fee}>{item.title}</Text>
                                                            {/* <Text style={styles.fee}>{gasFee[index].gasFeeMinConversion}</Text> */}
                                                        </View>
                                                    </Pressable>
                                                })
                                            }
                                            {/* <View style={{ height: 1, backgroundColor: 'rgba(255, 255, 255, 0.1)', marginVertical: 15 }} />
                                    <Pressable style={{ flexDirection: 'row', alignItems: 'center' }}
                                        onPress={() => {
                                            setToolTipVisible(true)
                                        }}>
                                        <Image source={require('../../../images/setting2.png')} resizeMode={"contain"} style={styles.iconCheck}></Image>
                                        <Text style={[styles.fee, { marginTop: 0 }]}>{'Tuỳ chỉnh'}</Text>
                                    </Pressable> */}
                                        </View>}
                                        placement="top"
                                        onClose={() => setToolTipVisible(false)}>
                                        <Pressable hitSlop={20} onPress={() => {
                                            setToolTipVisible(true)
                                            // if (gasFee.length !== 0) {
                                            //     this.setState({ toolTipVisible: true })
                                            // }
                                        }}>
                                            <Image source={require('../../../../images/setting.png')} resizeMode={"contain"} style={styles.iconSetting}></Image>
                                        </Pressable>
                                    </Tooltip>
                                </View>
                                : <View style={{ flex: 1.2, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 15 }}>
                                </View>
                        }

                        <View style={{
                            width: 1,
                            backgroundColor: '#A0A0B1',
                            marginVertical: 10
                        }}></View>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 15 }}>
                            <View>
                                <Text style={styles.feeEstimate}>{'Trượt giá'}</Text>
                                <Text style={styles.fee}>{`${slippage}%`}</Text>
                            </View>
                            <Tooltip
                                isVisible={slippageIndex}
                                contentStyle={{
                                    padding: 0,
                                    borderRadius: 15,
                                }}
                                backgroundStyle={{
                                    backgroundColor: 'transparent',
                                }}
                                content={<View style={{
                                    padding: 15,
                                    paddingTop: 0,
                                    paddingRight: 25,
                                    backgroundColor: '#1B1B23',
                                    borderColor: 'rgba(255, 255, 255, 0.1)',
                                    borderWidth: 1,
                                    borderRadius: 15,
                                }}>
                                    {
                                        slippageOption.map((item, index) => {
                                            return <Pressable
                                                key={`ii${index}`}
                                                style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}
                                                onPress={() => {
                                                    setSlippage(slippageOption[index].slippage)
                                                    setSlippageIndex(false)
                                                }}
                                            >
                                                <Image source={require('../../../../images/check.png')}
                                                    resizeMode={"contain"}
                                                    style={[styles.iconCheck, { tintColor: slippage === slippageOption[index].slippage ? '#2BEB4A' : "#3D3D4B" }]}>
                                                </Image>
                                                <View>
                                                    <Text style={styles.fee}>{slippageOption[index].title}</Text>
                                                    {/* <Text style={styles.fee}>{slippageOption[index].gasFeeMinConversion}</Text> */}
                                                </View>
                                            </Pressable>
                                        })
                                    }
                                    <View style={{ height: 1, backgroundColor: 'rgba(255, 255, 255, 0.1)', marginVertical: 15 }} />
                                    <Pressable style={{ flexDirection: 'row', alignItems: 'center' }}
                                        onPress={() => {
                                            setSlippageIndex(false)
                                            showCustomSlippage(true)
                                        }}>
                                        <Image source={require('../../../../images/setting2.png')} resizeMode={"contain"} style={styles.iconCheck}></Image>
                                        <Text style={[styles.fee, { marginTop: 0 }]}>{'Tuỳ chỉnh'}</Text>
                                    </Pressable>
                                </View>}
                                placement="top"
                                onClose={() => setSlippageIndex(false)}>
                                <Pressable hitSlop={20} onPress={() => {
                                    setSlippageIndex(true)
                                    // if (gasFee.length !== 0) {
                                    //     this.setState({ toolTipVisible: true })
                                    // }
                                }}>
                                    <Image source={require('../../../../images/setting.png')} resizeMode={"contain"} style={styles.iconSetting}></Image>
                                </Pressable>
                            </Tooltip>
                        </View>
                    </View>
                </View>
                <KeyboardAvoidingView
                    style={styles.buttonsWrapper}
                    behavior={'padding'}
                    keyboardVerticalOffset={KEYBOARD_OFFSET}
                    enabled={Device.isIos()} >
                    <View style={styles.buttonsContainer}>
                        <TouchableOpacity
                            disabled={!checkInfoSwap && !approvalRequire}
                            // disabled={true}
                            style={[styles.buttonStyle, { backgroundColor: checkInfoSwap || approvalRequire ? "#6A45FF" : '#3D3D4B' }]}
                            // onPress={onEditQuoteTransactionsApproveAmount}
                            // onPress={() => setApproveTransaction(true)}
                            // onPress={sawpToken}
                            onPress={() => {
                                if (approvalRequire) {
                                    setApprovalTransaction(true)
                                } else {
                                    showTransactionDetail(true)
                                }
                                // onStopRefresh(true)
                            }}
                        >
                            <Text style={styles.content}>{messageError}</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </View>
            <ApproveTransaction
                loading={loading}
                isVisible={approvalTransaction}
                resultSwaps={resultSwaps}
                amountApproval={amount}
                gasFeeLevel={gasFeeLevel}
                sourceToken={sourceToken}
                selectedAddress={selectedAddress}
                conversionRate={conversionRate}
                currentCurrency={currentCurrency}
                nativeCurrency={nativeCurrency}
                account={accountInfo}
                closeModal={() => setApprovalTransaction(false)}
                approvalLimitTransaction={approvalLimitTransaction}
            />

            <TransactionDetail
                loading={loading}
                isVisible={transactionDetail && checkInfoSwap}
                sourceToken={sourceToken}
                destinationToken={destinationToken}
                resultSwaps={resultSwaps}
                slippage={slippage}
                amount={amount}
                swapValue={outAmount}
                nativeCurrency={nativeCurrency}
                onSend={() => setAuthTransaction(true)}
                closeModal={() => showTransactionDetail(false)} />

            <OrderExecuted
                nativeCurrency={nativeCurrency}
                isVisible={transactionConfirmed}
                sourceToken={sourceToken}
                destinationToken={destinationToken}
                resultSwaps={resultSwaps}
                slippage={slippage}
                onExecuted={() => {
                    setTransactionConfirmed(false)
                    navigation && navigation.popToTop();
                }}
                amount={amount}
                swapValue={outAmount} />

            <SlippageCustom
                value={slippage}
                isVisible={customSlippage}
                onSaveCustom={(slippage) => {
                    setSlippage(slippage)
                    showCustomSlippage(false)
                }}
                closeModal={() => showCustomSlippage(false)} />
        </SafeAreaView>
    );
}

const mapStateToProps = (state) => ({
    accounts: state.engine.backgroundState.AccountTrackerController.accounts,
    conversionRate: state.engine.backgroundState.CurrencyRateController.conversionRate,
    currentCurrency: state.engine.backgroundState.CurrencyRateController.currentCurrency,
    selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
    balances: state.engine.backgroundState.TokenBalancesController.contractBalances,
    tokenExchangeRates: state.engine.backgroundState.TokenRatesController.contractExchangeRates,
    contractExchangeRates: state.engine.backgroundState.TokenRatesController.contractExchangeRates,
    chainId: state.engine.backgroundState.NetworkController.provider.chainId,
    provider: state.engine.backgroundState.NetworkController.provider,
    frequentRpcList: state.engine.backgroundState.PreferencesController.frequentRpcList,
    //
    swapsTokens: swapsTokensSelector(state),
    tokensWithBalance: swapsTokensWithBalanceSelector(state),
    //
    network: state.engine.backgroundState.NetworkController,
    approvalTransaction: state.engine.backgroundState.SwapsController.approvalTransaction,
    gasEstimateType: state.engine.backgroundState.GasFeeController.gasEstimateType,
    gasFeeEstimates: state.engine.backgroundState.GasFeeController.gasFeeEstimates,
    nativeCurrency: state.engine.backgroundState.CurrencyRateController.nativeCurrency,
    identities: state.engine.backgroundState.PreferencesController.identities,
});

export default connect(mapStateToProps)(TokenSwap);