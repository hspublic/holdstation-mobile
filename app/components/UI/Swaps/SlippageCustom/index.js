import React, { useState } from 'react';
import {
    Pressable, StyleSheet, Text, View, Dimensions,
    Image, TextInput, KeyboardAvoidingView, Keyboard
} from 'react-native';
import Modal from 'react-native-modal';
import { fontStyles, baseStyles } from '../../../../styles/common';
import Device from '../../../../util/device';
import WalletModal from '../../../UI/WalletModal';
import { strings } from '../../../../../locales/i18n';
import SlippageInput from '../SlippageInput'
import BigNumber from 'bignumber.js';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const KEYBOARD_OFFSET = 120;


const SLIPPAGE_MAX = new BigNumber(20);
const SLIPPAGE_MIN = new BigNumber(0);

function SlippageCustom({ value, isVisible, closeModal, onSaveCustom }) {
    const [slippageCustom, setSlippageCustom] = useState(value)
    const [showSlippageInfo, setShowSlippageInfo] = useState(false)

    function handleUnhandledTouches() {
        Keyboard.dismiss();
        return false;
    }
    return (
        <Modal
            hideModalContentWhileAnimating={true}
            useNativeDriver={true}
            isVisible={isVisible}
            animationIn={'fadeInUp'}
            animationOut={'fadeOutDown'}
            animationInTiming={500}
            animationOutTiming={500}
            // onBackdropPress={() => closeModal(false)}
            onBackButtonPress={() => closeModal(false)}
            backdropOpacity={0.5}
            statusBarTranslucent
            deviceHeight={screenHeight}
            style={{
                flex: 1,
                margin: 0,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
            <KeyboardAvoidingView
                behavior={'padding'}
                // keyboardVerticalOffset={KEYBOARD_OFFSET}
                enabled={Device.isIos()}>
                <View style={{ width: screenWidth }} onStartShouldSetResponder={handleUnhandledTouches}>
                    <View style={{
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15,
                        backgroundColor: '#1B1B23',
                        paddingBottom: 30
                    }}>
                        <View style={styles.view}>
                            <Text style={styles.title}>{'Tuỳ chỉnh trượt giá'}</Text>
                        </View>
                        {/* <View style={styles.line} /> */}
                        <SlippageInput
                            setShowSlippageInfo={() => {
                                setShowSlippageInfo(true)
                            }}
                            style={[styles.input, { textAlign: 'center', marginRight: 0 }]}
                            value={`${slippageCustom}`}
                            name={'% trượt giá tối đa'}
                            nameError={'Trượt giá'}
                            changeLimit={true}
                            max={SLIPPAGE_MAX}
                            min={SLIPPAGE_MIN}
                            increment={0.1}
                            error={false}
                            onChangeValue={(value) => setSlippageCustom(value)}
                            color={'#D94343'}
                        />
                        <Pressable
                            disabled={!slippageCustom || Number(slippageCustom) > Number(SLIPPAGE_MAX)}
                            style={[styles.saveStyle, { backgroundColor: slippageCustom && (Number(slippageCustom) <= Number(SLIPPAGE_MAX)) ? '#6A45FF' : "#3D3D4B" }]}
                            onPress={() => onSaveCustom(slippageCustom)}>
                            <Text style={styles.save}>
                                {'Xong'}
                            </Text>
                        </Pressable>
                    </View>
                </View>
            </KeyboardAvoidingView>
            {/* <WalletModal
                warningModalVisible={customWarning}
                onCancelPress={() => showCustomWarning(false)}
                displayConfirmButton={false}
                displayCancelButton={false}
                confirmText={strings('accounts.ok')}
                cancelText={strings('accounts.cancel')}>
                <View style={{
                    flex: 1,
                    padding: 25
                }}>
                    <View style={{
                        alignItems: 'center'
                    }}>
                        <Image source={require('../../../../images/info.png')} resizeMode={"contain"} style={styles.iconWarning}></Image>
                    </View>
                    <Text style={styles.titleWarning}>
                        {'Phí slippage quá thấp, giao dịch của bạn có thể bị chậm'}</Text>
                    <Text style={[styles.titleWarning, { fontWeight: '100', fontSize: 16 }]}>
                        {'Hãy điều chỉnh phí slippage cao hơn để tránh lỗi xảy ra'}</Text>
                    <Pressable style={[styles.btnWarning, { backgroundColor: '#6A45FF' }]} onPress={() => showCustomWarning(false)}>
                        <Text style={styles.contentBtnWarning}>
                            {'Điều chỉnh phí slippage'}
                        </Text>
                    </Pressable>
                    <Pressable style={styles.btnWarning} onPress={() => showCustomWarning(false)}>
                        <Text style={styles.contentBtnWarning}>
                            {'Tôi đã hiểu và vẫn muốn thực hiện'}
                        </Text>
                    </Pressable>
                </View>
            </WalletModal> */}

            <WalletModal
                warningModalVisible={showSlippageInfo}
                onCancelPress={() => setShowSlippageInfo(false)}
                displayConfirmButton={false}
                displayCancelButton={false}
                confirmText={strings('accounts.ok')}
                cancelText={strings('accounts.cancel')}>
                <View style={{
                    flex: 1,
                    padding: 25
                }}>
                    <Text style={styles.titleWarning}>
                        {'Trượt giá'}</Text>
                    <Text style={[styles.titleWarning, { fontWeight: '100', fontSize: 16 }]}>
                        {'Trượt giá là khái niệm chỉ sự khác biệt giữa mức giá dự kiến của một lệnh và mức giá mà tại đó lệnh được thực hiện.'}</Text>
                    <Pressable style={[styles.btnWarning, { backgroundColor: '#6A45FF' }]} onPress={() => setShowSlippageInfo(0)}>
                        <Text style={styles.contentBtnWarning}>
                            {'Ok'}
                        </Text>
                    </Pressable>
                </View>
            </WalletModal>
        </Modal>
    );
}

const styles = StyleSheet.create({
    view: {
        paddingVertical: 12,
        paddingBottom: 5,
        alignItems: 'center',
    },
    line: {
        height: 1,
        backgroundColor: 'rgba(255, 255, 255, 0.15)',
        margin: 20,
        marginBottom: 0
    },
    title: {
        ...fontStyles.normal,
        fontSize: 18,
        color: '#FFFFFF',
        marginTop: 5,
        fontWeight: '500'
    },
    content: {
        ...fontStyles.normal,
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: '500',
        padding: 10
    },
    btnStyle: {
        borderColor: 'rgba(255, 255, 255, 0.1)',
        borderWidth: 1,
        borderRadius: 16,
    },
    feeEstimate: {
        fontSize: 14,
        color: '#A0A0B1',
        marginTop: 5,
        fontWeight: '500'
    },
    fee: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500'
    },
    iconSetting: {
        width: 30,
        height: 30,
        tintColor: '#6A45FF' // #58586C
    },
    iconCheck: {
        marginRight: 15,
        width: 25,
        height: 25,
        tintColor: '#3D3D4B', // '#2BEB4A'
    },
    iconInfo: {
        width: 20,
        height: 20,
        marginTop: 2,
        marginLeft: 3 // #EDA045
    },
    iconAdd: {
        width: 25,
        height: 25,
    },
    iconMinus: {
        width: 25,
        height: 25,
    },
    input: {
        ...fontStyles.normal,
        fontSize: 15,
        color: '#FFFFFF',
        paddingVertical: 10,
        marginRight: 5,
        maxWidth: 60,
        textAlign: 'right'
    },
    unit: {
        ...fontStyles.normal,
        fontSize: 15,
        color: '#FFFFFF',
        textAlign: 'center',
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'rgba(255, 255, 255, 0.1)',
        borderWidth: 1,
        borderRadius: 10,
        marginHorizontal: 5,
        paddingHorizontal: 10,
        width: 120,
    },
    saveStyle: {
        backgroundColor: '#3D3D4B',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
        marginBottom: 0,
        padding: 15
    },
    save: {
        ...fontStyles.normal,
        fontSize: 18,
        color: '#FFFFFF',
    },
    titleWarning: {
        ...fontStyles.normal,
        fontSize: 18,
        color: '#FFFFFF',
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 15
    },
    btnWarning: {
        backgroundColor: '#3D3D4B',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        marginTop: 15
    },
    contentBtnWarning: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
    },
    iconWarning: {
        width: 30,
        height: 30,
        tintColor: '#EDA045'
    },
});
export default SlippageCustom;
