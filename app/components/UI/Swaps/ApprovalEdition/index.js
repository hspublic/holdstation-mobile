import React, { useState, useCallback } from 'react';
import {
    Pressable, StyleSheet, Text, View, Dimensions,
    Image, TextInput, KeyboardAvoidingView, Keyboard, ActivityIndicator, TouchableOpacity
} from 'react-native';
import Modal from 'react-native-modal';
import { fontStyles, baseStyles } from '../../../../styles/common';
import Device from '../../../../util/device';
import WalletModal from '../../WalletModal';
import { strings } from '../../../../../locales/i18n';
import TokenIcon from '../components/TokenIcon';
import { useAppThemeFromContext, mockTheme } from '../../../../util/theme';
import { renderShortAddress } from '../../../../util/address';
import Engine from '../../../../core/Engine';
import { renderFiat } from '../../../../util/number';
import { BACKGROUND_COLOR } from '../../../../constants/backgroundColor'
import Avatar from '../../../../util/avatar';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const KEYBOARD_OFFSET = 120;
const win = Dimensions.get('window');

function ApprovalEdition({
    // isVisible,
    // closeModal,
    // loading,
    // sourceToken,
    // minimumSpendLimit,
    // spendLimitUnlimitedSelected,
    // spendLimitCustomValue,
    // originalApproveAmount,
    // onSetApprovalAmount: setApprovalAmount,
    // onSpendLimitCustomValueChange,
    // onPressSpendLimitUnlimitedSelected,
    // onPressSpendLimitCustomSelected,
    // toggleEditPermission,
    //
    isVisible,
    // selectCustomLimit,
    sourceToken,
    resultSwaps,
    maxApprove,
    originalAmountApproval,
    tokenSymbol,
    closeModal,
    account,
    currentCurrency,
    handleCustomLimit,
    setApprovalTransactionAmount
}) {

    const { colors, themeAppearance } = useAppThemeFromContext() || mockTheme;

    const [customWarning, showCustomWarning] = useState(false)

    function handleUnhandledTouches() {
        Keyboard.dismiss();
        return false;
    }
    const [amountApproval, onSetApprovalAmount] = useState(`${originalAmountApproval}`)
    const [selectCustomLimit, onSelectCustomLimit] = useState(false)

    const fiatBalance = `${renderFiat(Engine.getTotalFiatAccountBalance(), currentCurrency)}`;

    return (
        <Modal
            hideModalContentWhileAnimating={true}
            useNativeDriver={true}
            isVisible={isVisible}
            animationIn={'fadeInUp'}
            animationOut={'fadeOutDown'}
            animationInTiming={200}
            animationOutTiming={200}
            onBackdropPress={closeModal}
            onBackButtonPress={closeModal}
            backdropOpacity={0.5}
            statusBarTranslucent
            deviceHeight={screenHeight}
            style={{
                flex: 1,
                margin: 0,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
            <KeyboardAvoidingView
                behavior={'padding'}
                // keyboardVerticalOffset={KEYBOARD_OFFSET}
                enabled={Device.isIos()}>
                <View style={{ width: screenWidth }} onStartShouldSetResponder={handleUnhandledTouches}>
                    <View style={{
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15,
                        backgroundColor: '#1B1B23',
                        paddingBottom: 30,
                        alignItems: 'center',
                        paddingHorizontal: 10
                    }}>
                        <View style={styles.view}>
                            <Text style={styles.title}>{'Chỉnh sửa hạn mức'}</Text>
                        </View>

                        <View style={styles.addressContainer}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                {/* <TokenIcon medium icon={sourceToken.iconUrl} symbol={sourceToken.symbol} /> */}
                                {/* <View style={{
                                    shadowColor: BACKGROUND_COLOR[
                                        (account.address.charCodeAt(account.address.length - 3) * 100 + account.address.charCodeAt(account.address.length - 2) * 10
                                            + account.address.charCodeAt(account.address.length - 1)) % 20
                                    ],
                                    shadowOffset: { width: 0, height: 0 },
                                    shadowOpacity: 0.3,
                                    shadowRadius: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Image source={require('../../../../images-new/hexagon.svg')} style={{
                                        tintColor: BACKGROUND_COLOR[
                                            (account.address.charCodeAt(account.address.length - 3) * 100 + account.address.charCodeAt(account.address.length - 2) * 10
                                                + account.address.charCodeAt(account.address.length - 1)) % 20
                                        ],
                                        width: win.width / 10, // 8.625,
                                        height: win.height / 20,// 17.23,
                                        zIndex: -1,
                                    }}>
                                    </Image>
                                    <Text style={[styles.textWrapperIcon]}>
                                        {account.address.toUpperCase().slice(account.address.length - 3)}
                                    </Text>
                                </View> */}
                                <Avatar address={account.address}/>
                                <View style={{
                                    marginLeft: 8
                                }}>
                                    <Text style={styles.textName} numberOfLines={1}>{account.name}</Text>
                                    <Text style={styles.textAddress} numberOfLines={1}>{renderShortAddress(account.address)}</Text>
                                </View>
                            </View>
                            <View>
                                {/* <Text style={styles.textName} numberOfLines={1}>{`${fiatBalance}`}</Text> */}
                                <Text style={styles.textName} numberOfLines={1}>{`${sourceToken.balance} ${tokenSymbol}`}</Text>
                                <Text style={styles.textAddress} numberOfLines={1}>{'Số dư hiện có'}</Text>
                            </View>
                        </View>

                        <View style={styles.option}>
                            <TouchableOpacity onPress={() => onSelectCustomLimit(false)}>
                                {!selectCustomLimit ? (
                                    <View style={styles.outSelectedCircle}>
                                        <View style={styles.selectedCircle} />
                                    </View>
                                ) : (
                                    <View style={styles.circle} />
                                )}
                            </TouchableOpacity>
                            <View style={styles.spendLimitContent}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}>
                                    <Text style={styles.sectionExplanationText}>
                                        {"Số lượng mặc định"}
                                    </Text>
                                    <Pressable hitSlop={20}>
                                        <Image source={require('../../../../images/info_2.png')}
                                            resizeMode={"contain"}
                                            style={styles.iconInfo}></Image>
                                    </Pressable>
                                </View>
                                <Text
                                    style={[styles.optionText, styles.textBlack]}
                                >{`${maxApprove} ${tokenSymbol}`}</Text>
                            </View>
                        </View>

                        <View style={styles.option}>
                            <TouchableOpacity onPress={() => onSelectCustomLimit(true)}>
                                {selectCustomLimit ? (
                                    <View style={styles.outSelectedCircle}>
                                        <View style={styles.selectedCircle} />
                                    </View>
                                ) : (
                                    <View style={styles.circle} />
                                )}
                            </TouchableOpacity>
                            <View style={styles.spendLimitContent}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}>
                                    <Text style={styles.sectionExplanationText}>
                                        {'Số lượng tuỳ chỉnh'}
                                    </Text>
                                    <Pressable hitSlop={20}>
                                        <Image source={require('../../../../images/info_2.png')}
                                            resizeMode={"contain"}
                                            style={styles.iconInfo}></Image>
                                    </Pressable>
                                </View>
                                <TextInput
                                    // autoCapitalize="none"
                                    // keyboardType="numeric"
                                    // autoCorrect={false}
                                    // onChangeText={(value) => {
                                    //     const amount = parseFloat(value.replace(',', '.')) || 0
                                    //     onSetApprovalAmount(`${amount}`)
                                    // }}
                                    // placeholder={`${tokenSymbol}`}
                                    // placeholderTextColor={'#58586C'}
                                    // spellCheck={false}
                                    // style={styles.input}
                                    // value={amountApproval}
                                    // numberOfLines={1}
                                    // autoFocus
                                    // // onFocus={() => console.log('onFocus')}
                                    // returnKeyType={'done'}
                                    // keyboardAppearance={themeAppearance}
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    keyboardType="numeric"
                                    numberOfLines={1}
                                    onChangeText={(value) => {
                                        onSetApprovalAmount(parseFloat(value.replace(',', '.')) || 0)
                                    }}
                                    placeholder={tokenSymbol}
                                    placeholderTextColor={'#58586C'}
                                    spellCheck={false}
                                    style={styles.input}
                                    value={amountApproval}
                                    // onSubmitEditing={this.onNext}
                                    autoFocus
                                    // ref={this.amountInput}
                                    // testID={'request-amount-input'}
                                    keyboardAppearance={themeAppearance}
                                />
                                {/* {displayErrorMessage && (
							<View style={styles.errorMessageWrapper}>
								<ErrorMessage
									errorMessage={strings('spend_limit_edition.must_be_at_least', {
										allowance: minimumSpendLimit,
									})}
								/>
							</View>
						)} */}
                            </View>
                        </View>
                        <TouchableOpacity style={styles.saveStyle} onPress={() => {
                            if (selectCustomLimit) {
                                setApprovalTransactionAmount(amountApproval)
                            }
                            handleCustomLimit(selectCustomLimit)
                            closeModal()
                        }}>
                            <Text style={styles.save}>
                                {'Lưu'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </Modal>
    );
}

const styles = StyleSheet.create({
    view: {
        paddingVertical: 12,
        alignItems: 'center',
    },
    title: {
        ...fontStyles.normal,
        fontSize: 18,
        color: '#FFFFFF',
        marginTop: 5,
        fontWeight: '500'
    },
    content: {
        ...fontStyles.normal,
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: '500',
        padding: 10
    },
    iconInfo: {
        width: 18,
        height: 18,
        marginLeft: 3
    },
    saveStyle: {
        flexDirection: 'row',
        backgroundColor: '#4A369A',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        width: screenWidth - 40,
        margin: 20,
        marginBottom: 0,
        padding: 15
    },
    save: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
    },
    address: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 10
    },
    networkTitle: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#A0A0B1',
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 20
    },
    network: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 20,
    },
    addressContainer: {
        borderColor: 'rgba(255, 255, 255, 0.15)',
        borderWidth: 1,
        borderRadius: 12,
        paddingVertical: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        width: screenWidth - 40,
        justifyContent: 'space-between'
    },
    textName: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: "500",
    },
    textAddress: {
        ...fontStyles.normal,
        fontSize: 14,
        color: '#A0A0B1',
        marginTop: 5
    },
    selectedCircle: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: '#B1B5FF',
    },
    outSelectedCircle: {
        width: 22,
        height: 22,
        borderRadius: 11,
        borderWidth: 2,
        borderColor: '#B1B5FF',
        justifyContent: 'center',
        alignItems: 'center'
    },
    circle: {
        width: 22,
        height: 22,
        borderRadius: 11,
        opacity: 1,
        borderWidth: 2,
        borderColor: '#B1B5FF',
    },
    spendLimitContent: {
        marginLeft: 18,
        flex: 1,
    },
    spendLimitTitle: {
        ...fontStyles.bold,
        // color: colors.text.default,
        fontSize: 14,
        lineHeight: 20,
        marginBottom: 8,
    },
    optionText: {
        ...fontStyles.normal,
        fontSize: 18,
        color: '#FFFFFF',
        marginTop: 6,
        fontWeight: "500",
    },
    textBlack: {
        color: '#FFFFFF',
    },
    option: {
        flexDirection: 'row',
        marginTop: 20,
        width: screenWidth - 40,
        alignItems: 'center'
    },
    sectionExplanationText: {
        ...fontStyles.normal,
        fontSize: 14,
        color: '#A0A0B1'
    },
    touchableOption: {
        flexDirection: 'row',
    },
    input: {
        padding: 12,
        borderColor: 'rgba(255, 255, 255, 0.15)',
        borderWidth: 1,
        borderRadius: 10,
        color: '#FFFFFF',
        marginTop: 8,
        fontSize: 18,
        fontWeight: "500",
    },
    textWrapperIcon: {
        position: 'absolute',
        textAlign: 'center',
        fontSize: 14,
        color: '#FFF',
        ...fontStyles.boldHight
    },
});
export default ApprovalEdition;
