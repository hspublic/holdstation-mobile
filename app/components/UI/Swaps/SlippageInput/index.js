import React, { useCallback, useRef, useEffect, useState } from 'react';
import { View, TextInput, StyleSheet, Pressable, Image, Text } from 'react-native';
// import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
// import Text from './Text';
// import PropTypes from 'prop-types';
import BigNumber from 'bignumber.js';
import { useAppThemeFromContext, mockTheme } from '../../../../util//theme';
import { fontStyles, baseStyles } from '../../../../styles/common';

const createStyles = (colors) =>
    StyleSheet.create({
        inputContainer: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            borderColor: 'rgba(255, 255, 255, 0.1)',
            borderWidth: 1,
            borderRadius: 10,
            marginHorizontal: 5,
            paddingHorizontal: 10,
            width: 120,
        },
        iconAdd: {
            width: 25,
            height: 25,
        },
        iconMinus: {
            width: 25,
            height: 25,
        },
        fee: {
            ...fontStyles.normal,
            fontSize: 15,
            color: '#FFFFFF',
            fontWeight: '500'
        },
        iconInfo: {
            width: 20,
            height: 20,
            marginTop: 2,
            marginLeft: 3 // #EDA045
        },
        unit: {
            ...fontStyles.normal,
            fontSize: 15,
            color: '#FFFFFF',
            textAlign: 'center',
        },
    });

const SlippageInput = ({
    value,
    increment,
    onChangeValue,
    min,
    max,
    name,
    setShowSlippageInfo,
    error,
    style,
    color,
    nameError,
    changeLimit
}) => {
    const textInput = useRef(null);
    const [errorState, setErrorState] = useState('');
    const { colors, themeAppearance } = useAppThemeFromContext() || mockTheme;
    const styles = createStyles(colors);

    const handleClickUnit = useCallback(() => {
        textInput?.current?.focus?.();
    }, []);

    const changeValue = useCallback(
        (newValue, dontEmptyError) => {
            if (!dontEmptyError) setErrorState('');
            const cleanValue = newValue?.replace?.(',', '.');
            if (cleanValue && new BigNumber(cleanValue).isNaN()) {
                setErrorState(`${nameError} phải là một số`);
                return;
            }
            onChangeValue?.(cleanValue);
        },
        [nameError, onChangeValue]
    );

    const increaseNumber = useCallback(() => {
        const newValue = new BigNumber(value).plus(new BigNumber(increment));
        if (!new BigNumber(max).isNaN() && newValue.gt(max)) return;
        changeValue(newValue.toString());
    }, [changeValue, increment, max, value]);

    const decreaseNumber = useCallback(() => {
        const newValue = new BigNumber(value).minus(new BigNumber(increment));
        if (!new BigNumber(min).isNaN() && newValue.lt(min)) return;
        changeValue(newValue.toString());
    }, [changeValue, increment, min, value]);

    const checkLimits = useCallback(() => {
        if (new BigNumber(value || 0).lt(min)) {
            setErrorState(`${nameError} phải lớn hơn ${min}%`);
            return changeValue(min.toString(), true);
        }
        if (new BigNumber(value || 0).gt(max)) {
            setErrorState(`${nameError} giới hạn ${max}%`);
            return changeValue(max.toString());
        }
    }, [changeValue, max, min, nameError, value]);

    useEffect(() => {
        if (textInput?.current?.isFocused?.()) return;
        checkLimits();
    }, [checkLimits]);

    const hasError = Boolean(error) || Boolean(errorState);
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 20 }}>
            <View style={{ justifyContent: 'center' }}>
                <Pressable style={{ flexDirection: 'row', alignItems: 'center' }}
                    onPress={setShowSlippageInfo}
                >
                    <Text style={styles.fee}>{name}</Text>
                    <Image source={require('../../../../images/info.png')} resizeMode={"contain"} style={styles.iconInfo}></Image>
                </Pressable>
                {
                    hasError && <Text style={[styles.fee, { color: color, width: 150 }]}>{error || errorState}</Text>
                }
            </View>
            <View style={{
                flexDirection: 'row',
                alignItems: 'center'
            }}>
                <Pressable hitSlop={10} onPress={decreaseNumber}>
                    <Image source={require('../../../../images/minus.png')} resizeMode={"contain"} style={styles.iconMinus}></Image>
                </Pressable>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={style}
                        onChangeText={changeValue}
                        onBlur={checkLimits}
                        value={value}
                        keyboardType="numeric"
                        ref={textInput}
                        keyboardAppearance={themeAppearance}
                    />
                    {/* {
                        !changeLimit && <Text onPress={handleClickUnit} style={styles.unit}>
                        </Text>
                    } */}
                </View>
                <Pressable hitSlop={10} onPress={increaseNumber}>
                    <Image source={require('../../../../images/add.png')} resizeMode={"contain"} style={styles.iconAdd}></Image>
                </Pressable>
            </View>
        </View>
    );
};

SlippageInput.defaultProps = {
    increment: new BigNumber(1),
};

export default SlippageInput;
