import React, { useState, useCallback, useEffect, useMemo } from 'react';
import {
    Pressable, StyleSheet, Text, View, Dimensions,
    Image, TextInput, KeyboardAvoidingView, Keyboard, ActivityIndicator, TouchableOpacity
} from 'react-native';
import Modal from 'react-native-modal';
import { fontStyles, baseStyles } from '../../../../styles/common';
import Device from '../../../../util/device';
import WalletModal from '../../../UI/WalletModal';
import { strings } from '../../../../../locales/i18n';
import TokenIcon from '../components/TokenIcon';
import Tooltip from '../../PaymentRequest/Tooltip';
import ApprovalEdition from '../ApprovalEdition'
import { useAppThemeFromContext, mockTheme } from '../../../../util/theme';
import { fromTokenMinimalUnitString, hexToBN, toTokenMinimalUnit, fromTokenMinimalUnit, renderFromWei, weiToFiat, toWei } from '../../../../util/number';
import { decodeApproveData, generateApproveData } from '../../../../util/transactions';
import { swapsUtils } from '@metamask/swaps-controller';
import BigNumber from 'bignumber.js';
import Engine from '../../../../core/Engine';
import PrivateCredential from '../PrivateCredential'

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const KEYBOARD_OFFSET = 120;
const MAX_APPROVEL = new BigNumber(2).pow(256).minus(1);
function ApproveTransaction({
    loading,
    resultSwaps,
    isVisible,
    closeModal,
    sourceToken,
    amountApproval,
    selectedAddress,
    conversionRate,
    currentCurrency,
    nativeCurrency,
    account,
    approvalLimitTransaction
}) {
    const [toolTipVisible, setToolTipVisible] = useState(false) // Hiển thi thay đổi phí gas
    const [showDetail, setShowDetail] = useState(false)
    const [gasResult, setGasResult] = useState(false)

    function handleUnhandledTouches() {
        Keyboard.dismiss();
        return false;
    }
    const [approvalTransactionAmount, setApprovalTransactionAmount] = useState(amountApproval);
    const [selectCustomLimit, onSelectCustomLimit] = useState(false);
    const [customLimit, onCustomLimit] = useState(false);
    const [authTransaction, setAuthTransaction] = useState(false)

    async function estimateGas() {
        const { SwapsTokenController } = Engine.context;
        if (resultSwaps) {
            const approveObject = {
                contractAddress: sourceToken.address,
                from: selectedAddress,
                to: resultSwaps.routerAddress,
                value: toTokenMinimalUnit(approvalTransactionAmount, sourceToken?.decimals).toString(10),
            }
            const gasFeeWei = await SwapsTokenController.estimateGasApprove(approveObject)
            if (gasFeeWei) {
                gasFeeWei.estimateGasNative = renderFromWei(gasFeeWei.gasFee)
                gasFeeWei.estimateGasUsd = weiToFiat(toWei(gasFeeWei.estimateGasNative), conversionRate, currentCurrency)
                setGasResult(gasFeeWei)
            }
        }
    }

    useEffect(() => {
        estimateGas()
        const timer = setInterval(async () => {
            estimateGas()
        }, 5000);
        return () => clearInterval(timer)
    }, [approvalTransactionAmount, sourceToken?.address, sourceToken?.decimals, resultSwaps?.routerAddress])

    if (authTransaction) {
        return <View style={{ flex: 1, position: 'absolute', width: screenWidth, height: screenHeight }}>
            <PrivateCredential
                onSend={(privateKey) => {
                    if (selectCustomLimit) {
                        approvalLimitTransaction(privateKey, toTokenMinimalUnit(approvalTransactionAmount, sourceToken?.decimals).toString(10), gasResult.gasLimit, gasResult.gasPrice)
                    } else {
                        approvalLimitTransaction(privateKey, MAX_APPROVEL, gasResult.gasLimit, gasResult.gasPrice)
                    }
                }}
                onBack={() => setAuthTransaction(false)} />
        </View>
    }

    return (
        <Modal
            hideModalContentWhileAnimating={true}
            useNativeDriver={true}
            isVisible={isVisible}
            animationIn={'fadeInUp'}
            animationOut={'fadeOutDown'}
            animationInTiming={200}
            animationOutTiming={200}
            onBackdropPress={() => loading ? null : closeModal()}
            onBackButtonPress={() => loading ? null : closeModal()}
            onSwipeComplete={() => loading ? null : closeModal()}
            backdropOpacity={0.5}
            statusBarTranslucent
            deviceHeight={screenHeight}
            style={{
                flex: 1,
                margin: 0,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
            <KeyboardAvoidingView
                behavior={'padding'}
                // keyboardVerticalOffset={KEYBOARD_OFFSET}
                enabled={Device.isIos()}>
                <View style={{ width: screenWidth }} onStartShouldSetResponder={handleUnhandledTouches}>
                    <View style={{
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15,
                        backgroundColor: '#1B1B23',
                        paddingBottom: 30,
                        alignItems: 'center',
                        paddingHorizontal: 10
                    }}>
                        <View style={styles.assetContainer}>
                            <TokenIcon medium icon={sourceToken.iconUrl} symbol={sourceToken.symbol} />
                            <Text style={styles.textName} numberOfLines={1}>{sourceToken.symbol}</Text>
                        </View>
                        <View style={styles.view}>
                            <Text style={styles.title}>{'Cấp quyền sử dụng hạn mức'}</Text>
                        </View>
                        <View style={styles.containWarning}>
                            <Text style={styles.titleWarning}>
                                {`Khi nhấn đồng ý, bạn sẽ cho phép hợp đồng thông minh sử dụng số dư ${sourceToken.symbol} của bạn theo hạn mức sau:`}
                            </Text>
                        </View>
                        <View style={{
                            width: screenWidth - 40,
                            marginTop: 20
                        }}>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}>
                                <Text style={styles.maxTitle}>{`Số lượng ${sourceToken.symbol} tối đa cho phép sử dụng`}</Text>
                                <Pressable hitSlop={20}>
                                    <Image source={require('../../../../images/info_2.png')}
                                        resizeMode={"contain"}
                                        style={styles.iconInfo}></Image>
                                </Pressable>
                            </View>
                            <Text style={styles.valueMax}>
                                {`${selectCustomLimit ? approvalTransactionAmount : MAX_APPROVEL} ${sourceToken.symbol}`}
                            </Text>
                        </View>
                        <TouchableOpacity style={styles.cancelStyle} onPress={() => onCustomLimit(true)}>
                            <Text style={styles.btnContent}>
                                {'Chỉnh sửa hạn mức'}
                            </Text>
                        </TouchableOpacity>
                        {
                            showDetail && <View style={{
                                width: screenWidth - 40,
                                marginTop: 20
                            }}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}>
                                    <Text style={styles.maxTitle}>{'Hợp đồng được cấp phép'}</Text>
                                    <Pressable hitSlop={20}>
                                        <Image source={require('../../../../images/info_2.png')}
                                            resizeMode={"contain"}
                                            style={styles.iconInfo}></Image>
                                    </Pressable>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                }}>
                                    <Text style={[styles.valueMax, { width: screenWidth - 100 }]}>{resultSwaps.routerAddress}</Text>
                                    <Image source={require('../../../../images/copy.png')}
                                        resizeMode={"contain"}
                                        style={styles.iconCopy}>
                                    </Image>
                                </View>
                            </View>
                        }
                        {
                            !showDetail ? <TouchableOpacity onPress={() => setShowDetail(true)} style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginVertical: 30
                            }}>
                                <Text style={styles.btnContent}>{'Xem chi tiết'}
                                </Text>
                                <Image source={require('../../../../images/arrowRight.png')}
                                    resizeMode={"contain"}
                                    style={styles.iconArrow}>
                                </Image>
                            </TouchableOpacity> : <TouchableOpacity onPress={() => setShowDetail(false)} style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginVertical: 30
                            }}>
                                <Text style={styles.btnContent}>{'Thu gọn'}
                                </Text>
                                <Image source={require('../../../../images/arrow-up.png')}
                                    resizeMode={"contain"}
                                    style={styles.iconArrow}>
                                </Image>
                            </TouchableOpacity>
                        }
                        <View style={styles.line} />
                        <View style={{
                            width: screenWidth - 40,
                            flexDirection: 'row',
                            alignItems: 'center', justifyContent: 'space-between',
                            paddingHorizontal: 15,
                            marginVertical: 18
                        }}>
                            {
                                gasResult && <View>
                                    <Text style={styles.feeEstimate}>{'Phí gas ước tính'}</Text>
                                    <Text style={styles.fee}>{`${gasResult.estimateGasUsd} ~ ${gasResult.estimateGasNative} ${nativeCurrency}`}</Text>
                                </View>
                            }
                            {/* <Tooltip
                            isVisible={toolTipVisible}
                            contentStyle={{
                                padding: 0,
                                borderRadius: 15,
                            }}
                            backgroundStyle={{
                                backgroundColor: 'transparent',
                            }}
                            content={<View style={{
                                padding: 15,
                                paddingTop: 0,
                                paddingRight: 50,
                                backgroundColor: '#1B1B23',
                                borderColor: 'rgba(255, 255, 255, 0.1)',
                                borderWidth: 1,
                                borderRadius: 15,
                            }}>
                                {
                                    gasFeeLevel.map((item, index) => {
                                        return <Pressable
                                            key={`ii${index}`}
                                            style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}
                                            onPress={() => {
                                            }}>
                                            <Image source={require('../../../../images/check.png')}
                                                resizeMode={"contain"}
                                                style={[styles.iconCheck, { tintColor: saveGas === String(index) ? '#2BEB4A' : "#3D3D4B" }]}>
                                            </Image>
                                            <View>
                                                <Text style={styles.fee}>{item.title}</Text>
                                            </View>
                                        </Pressable>
                                    })
                                }
                            </View>}
                            placement="top"
                            onClose={() => setToolTipVisible(false)}>
                            <Pressable hitSlop={20} onPress={() => {
                                setToolTipVisible(true)
                                // if (gasFee.length !== 0) {
                                //     this.setState({ toolTipVisible: true })
                                // }
                            }}>
                                <Image source={require('../../../../images/setting.png')}
                                    resizeMode={"contain"}
                                    style={styles.iconSetting}></Image>
                            </Pressable>
                        </Tooltip> */}
                        </View>
                        <View style={styles.line} />
                        <TouchableOpacity
                            disabled={loading || !gasResult}
                            onPress={() => setAuthTransaction(true)}
                            style={styles.saveStyle}>
                            {
                                loading ? <ActivityIndicator size={"small"} /> :
                                    <>
                                        <Image source={require('../../../../images/faceId.png')} resizeMode={"contain"} style={styles.iconfaceId}></Image>
                                        <Text style={styles.btnContent}>
                                            {'Đồng ý cấp quyền'}
                                        </Text>
                                    </>
                            }
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.cancelStyle}
                            onPress={closeModal}>
                            <Text style={styles.btnContent}>
                                {'Huỷ'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
            <ApprovalEdition
                isVisible={customLimit}
                sourceToken={sourceToken}
                resultSwaps={resultSwaps}
                maxApprove={MAX_APPROVEL}
                originalAmountApproval={amountApproval}
                tokenSymbol={sourceToken.symbol}
                selectedAddress={selectedAddress}
                account={account}
                currentCurrency={currentCurrency}
                handleCustomLimit={(value) => onSelectCustomLimit(value)}
                closeModal={() => onCustomLimit(false)}
                setApprovalTransactionAmount={(value) => setApprovalTransactionAmount(value)}
            // originalApproveAmount={approvalTransactionAmount}
            // minimumSpendLimit={minimumSpendLimit}
            // spendLimitUnlimitedSelected={spendLimitUnlimitedSelected}
            // spendLimitCustomValue={approvalCustomValue}
            // onSetApprovalAmount={onSetApprovalAmount}
            // onSpendLimitCustomValueChange={onSpendLimitCustomValueChange}
            // onPressSpendLimitUnlimitedSelected={onPressSpendLimitUnlimitedSelected}
            // onPressSpendLimitCustomSelected={onPressSpendLimitCustomSelected}
            // toggleEditPermission={closeModal}

            />
        </Modal>
    );
}

const styles = StyleSheet.create({
    view: {
        marginVertical: 20,
        alignItems: 'center',
    },
    line: {
        height: 1,
        backgroundColor: 'rgba(255, 255, 255, 0.15)',
        width: screenWidth - 40,
    },
    title: {
        ...fontStyles.normal,
        fontSize: 18,
        color: '#FFFFFF',
        fontWeight: '500'
    },
    content: {
        ...fontStyles.normal,
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: '500',
        padding: 10
    },
    btnStyle: {
        borderColor: 'rgba(255, 255, 255, 0.1)',
        borderWidth: 1,
        borderRadius: 15,
    },
    feeEstimate: {
        fontSize: 14,
        color: '#A0A0B1',
        marginTop: 5,
        fontWeight: '500'
    },
    fee: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500',
        marginTop: 4
    },
    iconSetting: {
        width: 30,
        height: 30,
    },
    iconCheck: {
        marginRight: 15,
        width: 25,
        height: 25,
        tintColor: '#3D3D4B', // '#2BEB4A'
    },
    iconInfo: {
        width: 18,
        height: 18,
        marginLeft: 3
    },
    iconAdd: {
        width: 25,
        height: 25,
    },
    iconMinus: {
        width: 25,
        height: 25,
    },
    input: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        width: 100,
        borderColor: 'rgba(255, 255, 255, 0.1)',
        borderWidth: 1,
        borderRadius: 10,
        textAlign: 'center',
        paddingVertical: 10,
        marginHorizontal: 10
    },
    saveStyle: {
        flexDirection: 'row',
        backgroundColor: '#4A369A',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        width: screenWidth - 40,
        margin: 20,
        marginBottom: 0,
        padding: 15
    },
    btnContent: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
    },
    titleWarning: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFBC39',
        padding: 12
    },
    containWarning: {
        borderColor: '#FFBC39',
        borderWidth: 1,
        borderRadius: 15,
        width: screenWidth - 40,
        backgroundColor: '#3D2514'
    },
    contentBtnWarning: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        padding: 8,
        paddingHorizontal: 10,
    },
    iconWarning: {
        width: 30,
        height: 30,
        tintColor: '#EDA045'
    },
    iconfaceId: {
        width: 20,
        height: 20,
        marginRight: 8
    },
    maxTitle: {
        ...fontStyles.normal,
        fontSize: 15,
        color: '#A0A0B1',
    },
    address: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 10
    },
    valueMax: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500',
        marginTop: 8,
    },
    network: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 20,
    },
    cancelStyle: {
        flexDirection: 'row',
        backgroundColor: '#282832',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        width: screenWidth - 40,
        margin: 20,
        marginBottom: 0,
        padding: 15,
        borderColor: 'rgba(255, 255, 255, 0.15)',
        borderWidth: 1,
    },
    assetContainer: {
        borderColor: 'rgba(255, 255, 255, 0.15)',
        borderWidth: 1,
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20
    },
    textName: {
        ...fontStyles.normal,
        fontSize: 16,
        color: '#FFFFFF',
        fontWeight: "500",
        marginLeft: 8
    },
    iconArrow: {
        width: 18,
        height: 18,
        marginLeft: 2
    },
    iconCopy: {
        width: 26,
        height: 26,
        marginTop: 7,
    },
});
export default ApproveTransaction;
