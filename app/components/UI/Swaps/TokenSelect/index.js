import React, { useCallback, useMemo, useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet,
    TextInput,
    TouchableOpacity,
    View,
    TouchableWithoutFeedback,
    ActivityIndicator,
    InteractionManager,
    Pressable, Image
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context'

import { FlatList } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/Ionicons';
import FAIcon from 'react-native-vector-icons/FontAwesome5';
import Fuse from 'fuse.js';
import { connect } from 'react-redux';
import { isValidAddress } from 'ethereumjs-util';

import Device from '../../../../util/device';
import { balanceToFiat, hexToBN, renderFromTokenMinimalUnit, renderFromWei, weiToFiat } from '../../../../util/number';
import { safeToChecksumAddress } from '../../../../util/address';
import { isSwapsNativeAsset } from '../utils';
import { strings } from '../../../../../locales/i18n';
import { fontStyles } from '../../../../styles/common';

import Text from '../../../Base/Text';
import ListItem from '../../../Base/ListItem';
import ModalDragger from '../../../Base/ModalDragger';
import TokenIcon from '../components/TokenIcon';
import Alert from '../../../Base/Alert';
import useBlockExplorer from '../utils/useBlockExplorer';
import useFetchTokenMetadata from '../utils/useFetchTokenMetadata';
import useModalHandler from '../../../Base/hooks/useModalHandler';
import TokenImportModal from '../components/TokenImportModal';
import Analytics from '../../../../core/Analytics';
import { ANALYTICS_EVENT_OPTS } from '../../../../util/analytics';
import { useAppThemeFromContext, mockTheme } from '../../../../util/theme';
import {
    setSwapsHasOnboarded,
    setSwapsLiveness,
    swapsControllerTokens,
    swapsHasOnboardedSelector,
    swapsTokensSelector,
    swapsTokensWithBalanceSelector,
    swapsTopAssetsSelector,
} from '../../../../reducers/swaps';
import FeatherIcon from 'react-native-vector-icons/Feather';
import Logger from '../../../../util/Logger';
import Engine from '../../../../core/Engine';
import Clipboard from '@react-native-clipboard/clipboard';
import { swapsUtils } from '@metamask/swaps-controller';
import { getTicker, getTokenByTicker } from '../../../../util/transactions';

const createStyles = (colors) =>
    StyleSheet.create({
        modal: {
            margin: 0,
            justifyContent: 'flex-end',
        },
        modalView: {
            flex: 1,
            backgroundColor: '#1B1B23',
            paddingHorizontal: 20,
            // backgroundColor: colors.background.default,
            // borderTopLeftRadius: 10,
            // borderTopRightRadius: 10,
        },
        inputWrapper: {
            flexDirection: 'row',
            alignItems: 'center',
            marginHorizontal: 30,
            marginVertical: 10,
            paddingVertical: Device.isAndroid() ? 0 : 10,
            paddingHorizontal: 5,
            borderRadius: 5,
            borderWidth: 1,
            borderColor: colors.border.default,
        },
        // searchIcon: {
        //     marginHorizontal: 8,
        //     color: colors.icon.default,
        // },
        searchIcon: {
            marginHorizontal: 10,
            // color: colors.icon.default,
        },
        searchWrapper: {
            marginVertical: 15,
            borderColor: colors.border.default,
            borderWidth: 1,
            borderRadius: 12,
            flexDirection: 'row',
            alignItems: 'center',
        },
        searchInput: {
            paddingTop: Device.isAndroid() ? 12 : 0,
            fontSize: 16,
            height: 60,
            flex: 1,
            color: '#FFFFFF', // colors.text.default,
            ...fontStyles.normal,
            paddingRight: 15
        },
        input: {
            ...fontStyles.normal,
            flex: 1,
            color: colors.text.default,
        },
        modalTitle: {
            marginTop: Device.isIphone5() ? 10 : 15,
            marginBottom: Device.isIphone5() ? 5 : 5,
        },
        resultsView: {
            height: Device.isSmallDevice() ? 200 : 280,
            marginTop: 10,
        },
        resultRow: {
            borderTopWidth: StyleSheet.hairlineWidth,
            borderColor: colors.border.muted,
        },
        emptyList: {
            marginVertical: 10,
        },
        emptyTitle: {
            ...fontStyles.normal,
            color: '#FFFFFF', // colors.text.default,
            fontSize: 16,
            textAlign: 'center'
        },
        importButton: {
            paddingVertical: 6,
            paddingHorizontal: 10,
            backgroundColor: colors.primary.default,
            borderRadius: 100,
        },
        importButtonText: {
            color: colors.primary.inverse,
        },
        loadingIndicator: {
            margin: 10,
        },
        loadingTokenView: {
            // flex: 1,
            marginTop: 30,
            justifyContent: 'center',
            alignItems: 'center',
        },
        footer: {
            padding: 30,
        },
        footerIcon: {
            paddingTop: 4,
            paddingRight: 8,
        },
        titleWrapper: {
            width: '100%',
            // height: 33,
            alignItems: 'center',
            justifyContent: 'center',
        },
        dragger: {
            width: 48,
            height: 5,
            borderRadius: 4,
            backgroundColor: colors.border.default,
            opacity: 0.6,
            marginTop: 10
        },
        wrapper: {
            flex: 1,
            backgroundColor: '#1B1B23',
            paddingHorizontal: 20
        },
        assetListElement: {
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
            alignItems: 'center',
        },
        assetNameElement: {
            flexDirection: 'row',
            marginVertical: 4
        },
        text: {
            ...(fontStyles.normal),
            color: '#828282',
            marginLeft: 5,
            marginTop: 3
        },
        textSymbol: {
            ...fontStyles.normal,
            marginLeft: 5,
            fontSize: 16,
            color: '#FFFFFF',
            fontWeight: "500",
            // width: 150
        },
        assetInfo: {
            // flex: 1,
            alignSelf: 'center',
            justifyContent: 'center',
        },
        assetIcon: {
            marginRight: 12,
        },
        ethLogo: {
            width: 35,
            height: 35,
        },
        checkedIcon: {
            width: 15,
            height: 15,
            marginTop: 0,
            marginLeft: 6
        },
    });

const MAX_TOKENS_RESULTS = 10;

function TokenSelect({
    navigation,
    // dismiss,
    // title,
    tokens,
    ticker,
    // initialTokens,
    // onItemPress,
    accounts,
    selectedAddress,
    currentCurrency,
    conversionRate,
    tokenExchangeRates,
    chainId,
    provider,
    frequentRpcList,
    balances,
    //
    swapsTokens,
    tokensWithBalance
}) {
    // const navigation = useNavigation();
    const searchInput = useRef(null);
    const list = useRef();
    const [searchString, setSearchString] = useState('');
    const [loadingTokens, setLoadingTokens] = useState(false);

    const [sourceToken, setSourceToken] = useState(null);
    const [destinationToken, setDestinationToken] = useState(null);

    const explorer = useBlockExplorer(provider, frequentRpcList);
    const [isTokenImportVisible, , showTokenImportModal, hideTokenImportModal] = useModalHandler(false);
    const { colors, themeAppearance } = useAppThemeFromContext() || mockTheme;
    const styles = createStyles(colors);

    const excludeAddresses = sourceToken?.address ? [sourceToken?.address] : []

    const excludedAddresses = useMemo(
        () => excludeAddresses.filter(Boolean).map((address) => address.toLowerCase()),
        [excludeAddresses]
    );

    let balance = 0;
    let assets = tokens;
    let tokenMainnet = null;
    if (ticker) {
        tokenMainnet = getTokenByTicker(ticker);
    } else {
        tokenMainnet = getTokenByTicker('ETH');
    }
    if (accounts[selectedAddress]) {
        balance = renderFromWei(accounts[selectedAddress].balance);
        assets = [
            {
                name: tokenMainnet ? tokenMainnet.name : ticker,
                symbol: getTicker(ticker),
                isETH: true,
                balance,
                balanceFiat: weiToFiat(
                    hexToBN(accounts[selectedAddress].balance),
                    conversionRate,
                    currentCurrency
                ),
                logo: '',
                coingeckoId: tokenMainnet ? tokenMainnet.coinGeckoId : ticker,
                address: swapsUtils.getNativeSwapsToken(chainId).address,
                addressName: tokenMainnet ? tokenMainnet.coinGeckoId : ticker,
                iconUrl: tokenMainnet ? tokenMainnet.thumbnail : '',
                verified: true,
            },
            ...(tokens || []),
        ];
    } else {
        assets = tokens;
    }

    var listTokens = assets.map(t1 => ({
        ...swapsTokens.find(t2 => t2.address.toLowerCase() === t1.address.toLowerCase()),
        ...t1
    }));

    if (sourceToken) {
        listTokens = listTokens
            .concat([...swapsTokens].filter((token) => !listTokens.map(item => item.address?.toLowerCase()).includes(token.address?.toLowerCase())))
    }
    // const listTokens = [swapsUtils.getNativeSwapsToken(chainId)].concat([...swapsTokens
    //     .slice(0, MAX_TOKENS_RESULTS)
    //     .filter((asset) => asset.address !== swapsUtils.getNativeSwapsToken(chainId).address),
    // ])

    const filteredTokens = useMemo(
        () => listTokens?.filter((token) => !excludedAddresses.includes(token.address?.toLowerCase())),
        [listTokens, excludedAddresses]
    );

    const filteredInitialTokens = filteredTokens
    //  useMemo(
    //     () =>
    //         tokensWithBalance?.length > 0
    //             ? tokensWithBalance.filter((token) => !excludedAddresses.includes(token.address?.toLowerCase()))
    //             : filteredTokens,
    //     [excludedAddresses, filteredTokens, tokensWithBalance]
    // );

    useEffect(() => {
        (async () => {
            const { SwapsController } = Engine.context;
            try {
                setLoadingTokens(true);
                await SwapsController.fetchTokenWithCache();
                setLoadingTokens(false);
            } catch (error) {
                Logger.error(error, 'Swaps: Error while fetching tokens in amount view');
            } finally {
                setLoadingTokens(false);
            }
        })();
    }, []);

    const tokenFuse = useMemo(
        () =>
            new Fuse(filteredTokens, {
                shouldSort: true,
                threshold: 0.45,
                location: 0,
                distance: 100,
                maxPatternLength: 32,
                minMatchCharLength: 1,
                keys: ['symbol', 'address', 'name'],
            }),
        [filteredTokens]
    );

    const tokenSearchResults = useMemo(
        () =>
            searchString.length > 0
                ? tokenFuse.search(searchString) // ?.slice(0, MAX_TOKENS_RESULTS)
                : filteredInitialTokens,
        [searchString, tokenFuse, filteredInitialTokens]
    );

    const shouldFetchToken = useMemo(
        () =>
            tokenSearchResults.length === 0 &&
            isValidAddress(searchString) &&
            !excludedAddresses.includes(searchString?.toLowerCase()),
        [excludedAddresses, searchString, tokenSearchResults.length]
    );

    // const [loadingTokenMetadata, tokenMetadata] = useFetchTokenMetadata(
    //     shouldFetchToken ? searchString : null,
    //     chainId
    // );

    const handleTokenPress = (item) => {
        // console.log('item : ', item)
        if (!sourceToken) {
            setSourceToken(item);
            handleSearchTextChange("")
        } else {
            setDestinationToken(item);
            navigation.navigate('TokenSwapView', {
                sourceToken,
                destinationToken: item,
                nativeToken: assets.find(({ isETH }) => isETH === true)
            });
        }
    }

    const handleSourceTokenPress = useCallback(
        (item) => {
            // toggleSourceModal();
            setSourceToken(item);
            // setSlippageAfterTokenPress(item.address, destinationToken?.address);
        }, []
    );

    const handleDestinationTokenPress = useCallback(
        (item) => {
            // toggleDestinationModal();
            setDestinationToken(item);
            // setSlippageAfterTokenPress(sourceToken?.address, item.address);
        },
        []
    );

    const RenderBalance = ({ secondaryBalance, balanceError }) => {
        const styles = StyleSheet.create({
            balanceFiat: {
                fontSize: 16,
                color: '#FFFFFF',
                ...fontStyles.normal,
                textTransform: 'uppercase',
            },
            balanceFiatTokenError: {
                textTransform: 'capitalize',
            },
        })
        return useMemo(() =>
            secondaryBalance ? <Text style={[styles.balanceFiat, balanceError && styles.balanceFiatTokenError]}>
                {secondaryBalance}
            </Text> : <Text style={[styles.balanceFiat, balanceError && styles.balanceFiatTokenError]}>
                {'$0.00'}
            </Text>
            , [secondaryBalance, balanceError])
    }

    const renderItem = useCallback(
        ({ item }) => {
            const itemAddress = safeToChecksumAddress(item.address);
            // let balance = item.balance, balanceFiat = item.balanceFiat;
            let balance, balanceFiat;
            if (isSwapsNativeAsset(item)) {
                balance = renderFromWei(accounts[selectedAddress] && accounts[selectedAddress].balance);
                balanceFiat = weiToFiat(hexToBN(accounts[selectedAddress].balance), conversionRate, currentCurrency);
            } else {
                const exchangeRate = itemAddress in tokenExchangeRates ? tokenExchangeRates[itemAddress] : undefined;
                balance =
                    itemAddress in balances ? renderFromTokenMinimalUnit(balances[itemAddress], item.decimals) : 0;
                balanceFiat = balanceToFiat(balance, conversionRate, exchangeRate, currentCurrency);
            }
            return (
                <Pressable onPress={() => handleTokenPress({
                    ...item,
                    balance
                })} style={styles.assetListElement}>
                    <View style={styles.assetNameElement}>
                        <View style={styles.assetIcon}>
                            <TokenIcon medium icon={item.iconUrl} symbol={item.symbol} />
                        </View>
                        <View style={styles.assetInfo}>
                            {/* <View>
                            <Text numberOfLines={1} style={styles.textSymbol}>{item.name}</Text>

                            </View> */}
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={styles.textSymbol}>{item.name}</Text>
                                {item.verified && <Image source={require('../../../../images-new/checked-icon.png')} style={styles.checkedIcon}></Image>}
                            </View>
                            <Text style={styles.text}>{`${balance} ${item.symbol}`}</Text>
                        </View>
                    </View>
                    <RenderBalance secondaryBalance={balanceFiat} balanceError={item?.balanceError} />
                </Pressable>
            )
            //     <TouchableOpacity style={styles.resultRow} onPress={() => handleTokenPress(item)}>
            //         <ListItem style={{}}>
            //             <ListItem.Content>
            //                 <ListItem.Icon>
            //                     <TokenIcon medium icon={item.iconUrl} symbol={item.symbol} />
            //                 </ListItem.Icon>
            //                 <ListItem.Body>
            //                     <ListItem.Title>{item.symbol}</ListItem.Title>
            //                     {item.name && <Text>{item.name}</Text>}
            //                 </ListItem.Body>
            //                 <ListItem.Amounts>
            //                     <ListItem.Amount>{balance}</ListItem.Amount>
            //                     {balanceFiat && <ListItem.FiatAmount>{balanceFiat}</ListItem.FiatAmount>}
            //                 </ListItem.Amounts>
            //             </ListItem.Content>
            //         </ListItem>
            //     </TouchableOpacity>
            // );
        },
        [balances, accounts, selectedAddress, conversionRate, currentCurrency, tokenExchangeRates, styles]
    );

    const handleSearchPress = () => searchInput?.current?.focus();

    const handleShowImportToken = useCallback(() => {
        searchInput?.current?.blur();
        showTokenImportModal();
    }, [showTokenImportModal]);

    const handlePressImportToken = useCallback(
        (item) => {
            const { address, symbol } = item;
            InteractionManager.runAfterInteractions(() => {
                Analytics.trackEventWithParameters(
                    ANALYTICS_EVENT_OPTS.CUSTOM_TOKEN_IMPORTED,
                    { address, symbol, chain_id: chainId },
                    true
                );
            });
            hideTokenImportModal();
            handleTokenPress(item);
        },
        [chainId, hideTokenImportModal]
    );

    const handleBlockExplorerPress = useCallback(() => {
        navigation.navigate('Webview', {
            screen: 'SimpleWebview',
            params: {
                url: shouldFetchToken ? explorer.token(searchString) : explorer.token('').replace('token/', 'tokens/'),
                title: strings(shouldFetchToken ? 'swaps.verify' : 'swaps.find_token_address'),
            },
        });
        // dismiss();
    }, [explorer, navigation, searchString, shouldFetchToken]);

    // const renderFooter = useMemo(
    //     () => (
    //         <TouchableWithoutFeedback>
    //             <Alert
    //                 renderIcon={() => (
    //                     <FAIcon name="info-circle" style={styles.footerIcon} color={colors.primary.default} size={15} />
    //                 )}
    //             >
    //                 {(textStyle) => (
    //                     <Text style={textStyle}>
    //                         <Text reset bold>
    //                             {strings('swaps.cant_find_token')}
    //                         </Text>
    //                         {` ${strings('swaps.manually_pasting')}`}
    //                         {explorer.isValid && (
    //                             <Text reset>
    //                                 {` ${strings('swaps.token_address_can_be_found')} `}
    //                                 <Text reset link underline onPress={handleBlockExplorerPress}>
    //                                     {explorer.name}
    //                                 </Text>
    //                                 .
    //                             </Text>
    //                         )}
    //                     </Text>
    //                 )}
    //             </Alert>
    //         </TouchableWithoutFeedback>
    //     ),
    //     [explorer.isValid, explorer.name, handleBlockExplorerPress, styles, colors]
    // );

    const renderEmptyList = useMemo(
        () => (
            <View style={styles.emptyList}>
                <Text style={styles.emptyTitle}>{'Không tìm thấy token'}</Text>
            </View>
        ),
        [searchString, styles]
    );

    const handleSearchTextChange = useCallback((text) => {
        setSearchString(text);
    }, []);

    const handleClearSearch = useCallback(() => {
        setSearchString('');
        searchInput?.current?.focus();
    }, [setSearchString]);

    const pateAddress = async () => {
        const text = await Clipboard.getString();
        handleSearchTextChange(text)
        searchInput?.current?.focus();
    }

    return (
        // <Modal
        //     isVisible={isVisible}
        //     onBackdropPress={dismiss}
        //     onBackButtonPress={dismiss}
        //     onSwipeComplete={dismiss}
        //     swipeDirection="down"
        //     propagateSwipe
        //     avoidKeyboard
        //     onModalHide={() => setSearchString('')}
        //     style={styles.modal}
        //     backdropColor={colors.overlay.default}
        //     backdropOpacity={1}
        // >
        <SafeAreaView style={styles.wrapper}>
            {/* <ModalDragger /> */}
            {/* <Text style={styles.modalTitle}>
                {"Chọn token"}
            </Text> */}
            <View style={styles.titleWrapper}>
                <View style={styles.dragger} />
            </View>
            <View style={styles.titleWrapper}>
                <Text style={{
                    color: '#FFFFFF',
                    fontSize: 18, paddingTop: 12,
                    fontWeight: "500"
                }}>{'Chọn token'}</Text>
            </View>
            {/* <TouchableWithoutFeedback onPress={handleSearchPress}> */}
            <View style={styles.searchWrapper}>
                {/* <Icon name="ios-search" size={20} style={styles.searchIcon} /> */}
                <FeatherIcon name="search" size={22} color={'#FFFFFF'} style={styles.searchIcon} />
                <TextInput
                    ref={searchInput}
                    style={styles.searchInput}
                    placeholder={'Tìm theo tên hoặc địa chỉ token'}
                    placeholderTextColor={'#58586C'}
                    value={searchString}
                    onChangeText={handleSearchTextChange}
                    keyboardAppearance={themeAppearance}
                />
                <TouchableOpacity onPress={pateAddress}>
                    <Text style={{
                        color: '#B1B5FF',
                        fontSize: 16,
                        fontWeight: "400",
                        marginRight: 15
                    }}>{'Dán'}</Text>
                </TouchableOpacity>
                {/* {searchString.length > 0 && (
                    <TouchableOpacity onPress={handleClearSearch}>
                        <Icon name="ios-close-circle" size={20} style={styles.searchIcon} />
                    </TouchableOpacity>
                )} */}
            </View>
            {/* </TouchableWithoutFeedback> */}
            {/* {shouldFetchToken ? (
                <View style={styles.resultsView}>
                    {loadingTokenMetadata ? (
                        <View style={styles.loadingTokenView}>
                            <ActivityIndicator style={styles.loadingIndicator} />
                            <Text>{strings('swaps.gathering_token_details')}</Text>
                        </View>
                    ) : tokenMetadata.error ? (
                        <View style={styles.emptyList}>
                            <Text>{strings('swaps.error_gathering_token_details')}</Text>
                        </View>
                    ) : tokenMetadata.valid ? (
                        <View style={styles.resultRow}>
                            <ListItem>
                                <ListItem.Content>
                                    <ListItem.Icon>
                                        <TokenIcon
                                            medium
                                            icon={tokenMetadata.metadata.iconUrl}
                                            symbol={tokenMetadata.metadata.symbol}
                                        />
                                    </ListItem.Icon>
                                    <ListItem.Body>
                                        <ListItem.Title>{tokenMetadata.metadata.symbol}</ListItem.Title>
                                        {tokenMetadata.metadata.name && <Text>{tokenMetadata.metadata.name}</Text>}
                                    </ListItem.Body>
                                    <ListItem.Amounts>
                                        <TouchableOpacity
                                            style={styles.importButton}
                                            onPress={handleShowImportToken}
                                        >
                                            <Text small style={styles.importButtonText}>
                                                {strings('swaps.Import')}
                                            </Text>
                                        </TouchableOpacity>
                                    </ListItem.Amounts>
                                </ListItem.Content>
                            </ListItem>
                            <TokenImportModal
                                isVisible={isTokenImportVisible}
                                dismiss={hideTokenImportModal}
                                token={tokenMetadata.metadata}
                                onPressImport={() => handlePressImportToken(tokenMetadata.metadata)}
                            />
                        </View>
                    ) : (
                        <View style={styles.emptyList}>
                            <Text>
                                {strings('swaps.invalid_token_contract_address')}
                                {explorer.isValid && (
                                    <Text reset>
                                        {` ${strings('swaps.please_verify_on_explorer')} `}
                                        <Text reset link underline onPress={handleBlockExplorerPress}>
                                            {explorer.name}
                                        </Text>
                                        .
                                    </Text>
                                )}
                            </Text>
                        </View>
                    )}
                </View>
            ) : (
                <FlatList
                    ref={list}
                    showsVerticalScrollIndicator={false}
                    style={styles.resultsView}
                    keyboardDismissMode="none"
                    keyboardShouldPersistTaps="always"
                    data={tokenSearchResults}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.address}
                // ListEmptyComponent={renderEmptyList}
                // ListFooterComponent={renderFooter}
                // ListFooterComponentStyle={[styles.resultRow, styles.footer]}
                />
            )} */}
            {
                loadingTokens ? <View style={styles.loadingTokenView}>
                    <ActivityIndicator size={"small"} />
                    {/* <Text>{strings('swaps.gathering_token_details')}</Text> */}
                </View> : <FlatList
                    showsVerticalScrollIndicator={false}
                    ref={list}
                    style={styles.resultsView}
                    keyboardDismissMode="none"
                    keyboardShouldPersistTaps="always"
                    data={tokenSearchResults}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.address}
                    ListEmptyComponent={renderEmptyList}
                // ListFooterComponent={renderFooter}
                // ListFooterComponentStyle={[styles.resultRow, styles.footer]}
                />
            }

        </SafeAreaView>
        // </Modal>
    );
}

TokenSelect.propTypes = {
    /**
     * ETH to current currency conversion rate
     */
    conversionRate: PropTypes.number,
    /**
     * Map of accounts to information objects including balances
     */
    accounts: PropTypes.object,
    /**
     * Currency code of the currently-active currency
     */
    currentCurrency: PropTypes.string,
    /**
     * A string that represents the selected address
     */
    selectedAddress: PropTypes.string,
    /**
     * An object containing token balances for current account and network in the format address => balance
     */
    balances: PropTypes.object,
    /**
     * An object containing token exchange rates in the format address => exchangeRate
     */
    tokenExchangeRates: PropTypes.object,
    /**
     * Chain Id
     */
    chainId: PropTypes.string,
    /**
     * Current Network provider
     */
    provider: PropTypes.object,
    /**
     * Frequent RPC list from PreferencesController
     */
    frequentRpcList: PropTypes.array,
};

const mapStateToProps = (state) => ({
    accounts: state.engine.backgroundState.AccountTrackerController.accounts,
    conversionRate: state.engine.backgroundState.CurrencyRateController.conversionRate,
    currentCurrency: state.engine.backgroundState.CurrencyRateController.currentCurrency,
    selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
    balances: state.engine.backgroundState.TokenBalancesController.contractBalances,
    tokenExchangeRates: state.engine.backgroundState.TokenRatesController.contractExchangeRates,
    chainId: state.engine.backgroundState.NetworkController.provider.chainId,
    provider: state.engine.backgroundState.NetworkController.provider,
    frequentRpcList: state.engine.backgroundState.PreferencesController.frequentRpcList,
    //
    swapsTokens: swapsTokensSelector(state),
    tokensWithBalance: swapsTokensWithBalanceSelector(state),
    tokens: state.engine.backgroundState.TokensController.tokens,
    ticker: state.engine.backgroundState.NetworkController.provider.ticker
});

export default connect(mapStateToProps)(TokenSelect);
