import React, { PureComponent } from "react";
import { Text, TextInput, View, StyleSheet, InteractionManager, TouchableOpacity, Image, Dimensions } from "react-native";
import { fontStyles, GLOBAL_ITEM_FONT_SIZE } from '../../../styles/common';
import Engine from '../../../core/Engine';
import PropTypes from 'prop-types';
import { strings } from '../../../../locales/i18n';
import { isValidAddress } from 'ethereumjs-util';
import ActionView from '../ActionView';
import { isSmartContractAddress } from '../../../util/transactions';
import AnalyticsV2 from '../../../util/analyticsV2';
import WarningMessage from '../../Views/SendFlow/WarningMessage';
import AppConstants from '../../../core/AppConstants';
import { ThemeContext, mockTheme } from '../../../util/theme';
import Clipboard from '@react-native-clipboard/clipboard';
import NotificationManager from '../../../core/NotificationManager';
import StyledButton from "../StyledButton";
import { TokenImage } from "../TokenImage";
import Identicon from "../Identicon";
import { getTokenList } from "../../../reducers/tokens";
import { connect, useSelector } from "react-redux";

const win = Dimensions.get('window')

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			backgroundColor: '#1B1B23',
			flex: 1,
		},
		rowWrapper: {
			// padding: 20,
			marginTop: win.width / 20,
		},
		searchSection: {
			//margin: 20,
			flex: 1,
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
			borderWidth: 1,
			borderRadius: 12,
			borderColor: '#FFFFFF26',
			color: '#FFFFFF',
			height: win.height / 17,
			// marginBottom : win.height / 30
		},
		textInput: {
			width: '80%',
			//borderWidth: 1,
			//borderRadius: 4,
			//borderColor: colors.border.default,
			padding: 16,
			...fontStyles.normal,
			color: '#FFFFFF',
			fontSize: 16,
			lineHeight: 20
		},
		inputLabel: {
			...fontStyles.normal,
			color: '#FFFFFF',
		},
		warningText: {
			marginTop: 15,
			color: colors.error.default,
			...fontStyles.normal,
		},
		warningContainer: { marginHorizontal: 20, marginTop: 20, paddingRight: 0 },
		warningLink: { color: colors.primary.default },
		pasteTxt: { 
			color: '#FFFFFF', 
			marginRight: win.width / 50,
			...fontStyles.boldHight,
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			// lineHeight: 17,
			textAlign: 'center',
		},
		item: {
			// borderWidth: 2,
			// borderColor: '#FFFFFF26',
			borderRadius: 12,
			height: win.height / 14
		},
		assetListElement: {
			flex: 1,
			flexDirection: 'row',
			alignItems: 'center',
			marginLeft : win.width / 40
		},
		text: {
			color: '#FFFFFF',
			marginLeft: win.width / 40,
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			...fontStyles.bold
		},
		symbolText: {
			color: '#828282',
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			marginLeft: win.width / 40,
			marginRight: win.width/ 120,
			lineHeight: 20,
		},
		importedWrapper: {
			borderRadius: 20,
			borderWidth: 1,
			borderColor: '#58586C',
			backgroundColor: '#58586C',
			alignSelf: 'center',
			alignItems: 'center',
			justifyContent: 'center',
			// height: 28,
		},
		importedText: {
			paddingLeft: 5,
			paddingRight: 5,
			color: '#FFFFFF',
			fontSize: 12,
			...fontStyles.normal
		},
		logo: {
			width: 32,
			height: 32,
			borderRadius: 16,
		},
		searchWrapper: {
			flex: 1,
			flexDirection: 'row',
			width: '100%',
			marginTop: win.height / 80
		},
		checkedIcon: {
			marginLeft: 3.5,
			marginTop: 1.5,
			width:12,
			height:12,
		},
	});

/**
 * Copmonent that provides ability to add custom tokens.
 */
class AddCustomToken extends PureComponent {
	state = {
		address: '',
		symbol: '',
		decimals: '',
		name: '',
		warningAddress: '',
		warningSymbol: '',
		warningDecimals: '',
		validated: false,
		isImported: false,
		image: '',
		verified: null,
		isPressToken: false,
		isSelected: false,
		isPressPaste: false,
	};

	static propTypes = {
		/**
		/* navigation object required to push new views
		*/
		navigation: PropTypes.object,
		/**
		 * List of tokens from TokenListController
		 */
		tokenList: PropTypes.object,
	};

	getAnalyticsParams = () => {
		try {
			const { NetworkController } = Engine.context;
			const { chainId, type } = NetworkController?.state?.provider || {};
			const { address, symbol } = this.state;
			return {
				token_address: address,
				token_symbol: symbol,
				network_name: type,
				chain_id: chainId,
				source: 'Custom token',
			};
		} catch (error) {
			return {};
		}
	};

	onPastePress = async (address) => {
		// get clipboard contents

		const clipboardContents = await Clipboard.getString();
		if (!clipboardContents) return;
		address = clipboardContents;
		const validated = address.length !== 0 && address.length === 42 && isValidAddress(address);
		if (validated) {
			try {
				const myTokens = Engine.context.TokensController.state.tokens;
				const isImported = myTokens.some((token) => token.address.toLowerCase() === address.trim().toLowerCase());
				const token = isImported
					? myTokens.filter((token) => token.address.toLowerCase() === address.trim().toLowerCase())[0]
					: null;
				this.setState({ address, validated, isImported });
				const { AssetsContractController } = Engine.context;
				
				const decimals = isImported ? token.decimals : await AssetsContractController.getERC20TokenDecimals(address);
				const symbol = isImported ? token.symbol : await AssetsContractController.getERC721AssetSymbol(address);
				const name = await AssetsContractController.getERC721AssetName(address);
				const {tokenList} = this.props;
				const thumbnails = address in tokenList? tokenList[address].thumbnails :
					address.toLowerCase() in tokenList? tokenList[address.toLowerCase()].thumbnails: undefined;

				const verifiedToken = tokenList.hasOwnProperty(address) ? tokenList[address].verified :
					tokenList.hasOwnProperty(address.toLowerCase())? tokenList[address.toLowerCase()].verified: undefined;
				this.setState({ decimals: String(decimals), symbol, name, image: thumbnails, verified: verifiedToken});
				await this.showWarningContract(address, validated);
			} catch (e){
				// console.log('ee', e)
				this.setState({
					address: address,
					symbol: '',
					decimals: '',
					name: '',
					warningAddress: strings('token.address_must_be_valid'),
					warningSymbol: '',
					warningDecimals: '',
					validated: false,
					isImported: false,
					image: '',
					verified: null,
				})
				await this.showWarningContract(address, false);
			}
		}
	};

	addToken = async () => {
		if (!(await this.validateCustomToken())) return;
		try {
			const { TokensController } = Engine.context;
		const { address } = this.state;
		const { AssetsContractController } = Engine.context;
		const decimals = await AssetsContractController.getERC20TokenDecimals(address);
		const symbol = await AssetsContractController.getERC721AssetSymbol(address);
		await TokensController.addToken(address.trim(), symbol, decimals);
		AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.TOKEN_ADDED, this.getAnalyticsParams())
		// Clear state before closing
		this.setState(
			{
				address: '',
				symbol: '',
				name: '',
				decimals: '',
				warningAddress: '',
				warningSymbol: '',
				warningDecimals: '',
			},
			() => {
				InteractionManager.runAfterInteractions(() => {
					this.props.navigation.goBack();
				});
				NotificationManager.showSimpleNotification({
					status: 'success',
					duration: 5000,
					title: 'Thêm token thành công',
					description: 'Token ' + symbol + ' đã được thêm vào danh sách',
				});
			}
		);
		}catch (e) {
			this.setState(
				{
					address: '',
					symbol: '',
					name: '',
					decimals: '',
					warningAddress: strings('token.address_must_be_valid'),
					warningSymbol: '',
					warningDecimals: '',
				},
			);
		}
		
	};

	cancelAddToken = () => {
		this.props.navigation.goBack();
	};

	onAddressChange = async (address) => {
		const validated = address.length !== 0 && address.length === 42 && isValidAddress(address);
		const myTokens = Engine.context.TokensController.state.tokens;
		const isImported = myTokens.some((token) => token.address === address);
		await this.showWarningContract(address, validated);
		this.setState({ address, validated, isImported });
	};

	showWarningContract = async (address, validated) => {
		if (address.length === 0) {
			this.setState({ warningAddress: strings('token.address_cant_be_empty') });
		} else if (address.length !== 42) {
			this.setState({ warningAddress: strings('token.address_must_be_valid') });
		}
		if (validated) {
			this.setState({ warningAddress: '' });
		}
	};

	onSymbolChange = (symbol) => {
		this.setState({ symbol });
	};

	onDecimalsChange = (decimals) => {
		this.setState({ decimals });
	};

	onAddressBlur = async () => {
		const validated = await this.validateCustomTokenAddress();
		if (validated) {
			try {
				const address = this.state.address;
				const { AssetsContractController } = Engine.context;
				const decimals = await AssetsContractController.getERC20TokenDecimals(address);
				const symbol = await AssetsContractController.getERC721AssetSymbol(address);
				this.setState({ decimals: String(decimals), symbol, validated });
				await this.showWarningContract(address, validated);
			} catch (e) {
				this.setState({
					address: address,
					symbol: '',
					decimals: '',
					name: '',
					warningAddress: strings('token.address_must_be_valid'),
					warningSymbol: '',
					warningDecimals: '',
					validated: false,
					isImported: false,
					image: '',
					verified: null,
				})
				await this.showWarningContract(address, false);
			}
		}
	};

	validateCustomTokenAddress = async () => {
		let validated = true;
		const address = this.state.address.trim();
		const isValidTokenAddress = isValidAddress(address);
		const { NetworkController } = Engine.context;
		const { chainId } = NetworkController?.state?.provider || {};
		const toSmartContract = isValidTokenAddress && isSmartContractAddress(address, chainId);
		if (address.length === 0) {
			this.setState({ warningAddress: strings('token.address_cant_be_empty') });
			validated = false;
		} else if (!isValidTokenAddress) {
			this.setState({ warningAddress: strings('token.address_must_be_valid') });
			validated = false;
		} else if (!toSmartContract) {
			this.setState({ warningAddress: strings('token.address_must_be_smart_contract') });
			validated = false;
		} else {
			this.setState({ warningAddress: `` });
		}
		return validated;
	};

	validateCustomTokenSymbol = () => {
		let validated = true;
		const symbol = this.state.symbol;
		if (symbol.length === 0) {
			this.setState({ warningSymbol: strings('token.symbol_cant_be_empty') });
			validated = false;
		} else {
			this.setState({ warningSymbol: `` });
		}
		return validated;
	};

	validateCustomTokenDecimals = () => {
		let validated = true;
		const decimals = this.state.decimals;
		if (decimals.length === 0) {
			this.setState({ warningDecimals: strings('token.decimals_cant_be_empty') });
			validated = false;
		} else {
			this.setState({ warningDecimals: `` });
		}
		return validated;
	};

	validateCustomToken = async () => {
		const validatedAddress = await this.validateCustomTokenAddress();
		// const validatedSymbol = this.validateCustomTokenSymbol();
		// const validatedDecimals = this.validateCustomTokenDecimals();
		// return validatedAddress && validatedSymbol && validatedDecimals;
		return validatedAddress;
	};

	assetSymbolInput = React.createRef();
	assetPrecisionInput = React.createRef();

	jumpToAssetSymbol = () => {
		const { current } = this.assetSymbolInput;
		current && current.focus();
	};

	jumpToAssetPrecision = () => {
		const { current } = this.assetPrecisionInput;
		current && current.focus();
	};

	renderWarning = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<WarningMessage
				style={styles.warningContainer}
				warningMessage={
					<>
						{strings('add_asset.warning_body_description')}
						<Text
							suppressHighlighting
							onPress={() => {
								// TODO: This functionality exists in a bunch of other places. We need to unify this into a utils function
								this.props.navigation.navigate('Webview', {
									screen: 'SimpleWebview',
									params: {
										url: AppConstants.URLS.SECURITY,
										title: strings('add_asset.security_tips'),
									},
								});
							}}
							style={styles.warningLink}
							testID={'add-asset-warning-message'}
						>
							{strings('add_asset.warning_link')}
						</Text>
					</>
				}
			/>
		);
	};
	render = () => {
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);
		return (
			<View style={styles.wrapper} testID={'add-custom-token-screen'}>
				<ActionView
					cancelTestID={'add-custom-asset-cancel-button'}
					confirmTestID={'add-custom-asset-confirm-button'}
					cancelText={strings('add_asset.tokens.cancel_add_token')}
					confirmText={strings('add_asset.tokens.add_token')}
					onCancelPress={this.cancelAddToken}
					testID={'add-asset-cancel-button'}
					onConfirmPress={this.addToken}
					confirmDisabled={!this.state.validated || this.state.isImported || this.state.symbol === ''}
					showCancelButton={false}
				>
					<View>
						<View style={styles.rowWrapper}>
							<View style={styles.searchSection} testID={'add-searched-token-screen'}>
								<TextInput
									style={styles.textInput}
									placeholder={'0x...'}
									placeholderTextColor={colors.text.muted}
									value={this.state.address}
									onChangeText={this.onAddressChange}
									onBlur={this.onAddressBlur}
									testID={'input-token-address'}
									onSubmitEditing={this.onAddressBlur}
									returnKeyType={'next'}
									keyboardAppearance={themeAppearance}
								/>
								<TouchableOpacity
									style={[{flex: 1, flexDirection: 'row', justifyContent:'center'}, this.state.isPressPaste && {transform: [{scale: 0.95}]}]}
									onPress={() => this.onPastePress(this.state.address)}
									onPressIn= {() => this.setState({isPressPaste: true})}
									onPressOut= {() => this.setState({isPressPaste: false})}
									activeOpacity={1}
								>
									<Image source={require('../../../images-new/copy-address.png')} style={{height:20, width: 20, marginRight: win.width / 100}}/>
									<Text style={styles.pasteTxt}>{strings("Paste")}</Text>
								</TouchableOpacity>
							</View>

							<Text style={styles.warningText} testID={'token-address-warning'}>
								{this.state.warningAddress}
							</Text>
						</View>

						{ this.state.validated && (this.state.name !== '') && (
							<View>
								<TouchableOpacity
									type={'selectedAlt'}
									style={[styles.item, this.state.isImported ? {opacity: 0.4}: {opacity: 1}, 
											this.state.isPressToken && {transform: [{scale: 0.97}]}, 
											this.state.address && ! this.state.isImported && {borderWidth: 2, borderColor: '#6A45FF'}]}
									testID={'searched-token-result'}
									disabled={this.state.isImported}
									onPressIn={() => {
										this.onAddressBlur();
										this.setState({isPressToken: true})
									}}
									onPressOut={() => {
									}}
									activeOpacity={1}
									onPress={() => {
										this.addToken();
										this.setState({isSelected:false})
									}}
								>
									<View style={styles.assetListElement}>
										{this.state.image ?
											<Image source={{ uri: this.state.image}} style={styles.logo}></Image>:
											<Identicon address={this.state.address} customStyle={{width: 32, height: 32}}/>}
										<View
											style={{ flexDirection: 'column' }}>
											<View style={styles.searchWrapper}>
												<Text style={styles.text}>
													{this.state.name}
												</Text>
												<View>
													{this.state.verified && <Image source={require('../../../images-new/checked-icon.png')}
																				   style={styles.checkedIcon}></Image>}
												</View>
											</View>
											<View style={{ flexDirection: 'row' , marginBottom: win.height / 150}}>
												<Text style={styles.symbolText}>
													{this.state.symbol}
												</Text>
												{this.state.isImported && (
													<View style={styles.importedWrapper}>
														<Text style={styles.importedText}>Imported</Text>
													</View>
												)}
											</View>
										</View>
									</View>
								</TouchableOpacity>
							</View>
						)}
					</View>
				</ActionView>
			</View>
		);
	};
}

AddCustomToken.contextType = ThemeContext;

function mapStateToProps(state) {
	const props = {
		tokenList: getTokenList(state)
	};
	return props;
}
export default connect(mapStateToProps)(AddCustomToken);
