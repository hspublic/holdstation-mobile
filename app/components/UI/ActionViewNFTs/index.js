import React from 'react';
import StyledButton from '../StyledButton';
import PropTypes from 'prop-types';
import { Keyboard, StyleSheet, View, ActivityIndicator, TouchableWithoutFeedback, TouchableOpacity, Text } from 'react-native';
import { baseStyles } from '../../../styles/common';
import { strings } from '../../../../locales/i18n';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { mockTheme, useAppThemeFromContext } from '../../../util/theme';

const styles = StyleSheet.create({
    actionContainer: {
        flex: 0,
        flexDirection: 'row',
        paddingVertical: 16,
        paddingHorizontal: 15,
    },
    button: {
        flex: 1,
    },
    cancel: {
        marginRight: 0,
    },
    confirm: {
        marginLeft: 0,
    },
	buttonBlock: {
		backgroundColor: '#6A45FF',
		padding: 12,
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
		borderRadius: 12,
	},
	buttonBlockText: {
		fontSize: 16,
		fontWeight: '500',
		color: '#FFF',
	},
	buttonBlockDisabled: {
		backgroundColor: 'rgba(255, 255, 255, 0.15)',
	},
	buttonBlockTextDisabled: {
		color: '#58586C',
	},
});

/**
 * PureComponent that renders scrollable content above configurable buttons
 */
export default function ActionViewNFTs({
    cancelTestID,
    confirmTestID,
    cancelText,
    children,
    confirmText,
    confirmButtonMode,
    onCancelPress,
    onConfirmPress,
    onTouchablePress,
    showCancelButton,
    showConfirmButton,
    confirmed,
    confirmDisabled,
    keyboardShouldPersistTaps = 'never',
    style = undefined,
    NFTsDetail,
    addNFTsToList
}) {
    const { colors } = useAppThemeFromContext() || mockTheme;

    return (
        <View style={baseStyles.flexGrow}>
            <KeyboardAwareScrollView
                style={[baseStyles.flexGrow, style]}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'space-between',
                }}
                resetScrollToCoords={{ x: 0, y: 0 }}
                keyboardShouldPersistTaps={keyboardShouldPersistTaps}
            >
                <TouchableWithoutFeedback
                    style={{
                        flex: 1
                    }}
                    // eslint-disable-next-line react/jsx-no-bind
                    onPress={() => {
                        if (keyboardShouldPersistTaps === 'handled') {
                            Keyboard.dismiss();
                        }
                        onTouchablePress && onTouchablePress();
                    }}
                >

                    {children}
                </TouchableWithoutFeedback>

                <View style={styles.actionContainer}>
                    {/* {showCancelButton && (
                        <StyledButton
                            testID={cancelTestID}
                            type={confirmButtonMode === 'sign' ? 'signingCancel' : 'cancel'}
                            onPress={onCancelPress}
                            containerStyle={[styles.button, styles.cancel]}
                            disabled={confirmed}
                        >
                            {cancelText}
                        </StyledButton>
                    )} */}
                    {/*{showConfirmButton && !NFTsDetail ? (*/}
                    {/*    <StyledButton*/}
                    {/*        testID={confirmTestID}*/}
                    {/*        type={confirmButtonMode}*/}
                    {/*        onPress={onConfirmPress}*/}
                    {/*        containerStyle={[styles.button, styles.confirm]}*/}
                    {/*        disabled={confirmed || confirmDisabled}*/}
                    {/*    >*/}
                    {/*        {confirmed ? (*/}
                    {/*            <ActivityIndicator size="small" color={colors.primary.inverse} />*/}
                    {/*        ) : (*/}
                    {/*            confirmText*/}
                    {/*        )}*/}
                    {/*    </StyledButton>*/}
                    {/*) : <StyledButton*/}
                    {/*    testID={confirmTestID}*/}
                    {/*    type={confirmButtonMode}*/}
                    {/*    onPress={addNFTsToList}*/}
                    {/*    containerStyle={[styles.button, styles.confirm]}*/}
                    {/*    disabled={confirmed || confirmDisabled}*/}
                    {/*>*/}
                    {/*    {confirmed ? (*/}
                    {/*        <ActivityIndicator size="small" color={colors.primary.inverse} />*/}
                    {/*    ) : (*/}
                    {/*        strings('add_asset.add_nft_tolist')*/}
                    {/*    )}*/}
                    {/*</StyledButton>}*/}
					{showConfirmButton && !NFTsDetail ? (
						<TouchableOpacity
							testID={confirmTestID}
							type={confirmButtonMode}
							onPress={onConfirmPress}
							style={[
								styles.buttonBlock,
								confirmed || confirmDisabled ? styles.buttonBlockDisabled : {},
							]}
							disabled={confirmed || confirmDisabled}
						>
							{confirmed ? (
								<ActivityIndicator size="small" color={colors.primary.inverse} />
							) : (
								<Text
									style={[
										styles.buttonBlockText,
										confirmed || confirmDisabled ? styles.buttonBlockTextDisabled : {},
									]}
								>
									{confirmText}
								</Text>
							)}
						</TouchableOpacity>
					) : <TouchableOpacity
						testID={confirmTestID}
						type={confirmButtonMode}
						onPress={addNFTsToList}
						style={[
							styles.buttonBlock,
							confirmed || confirmDisabled ? styles.buttonBlockDisabled : {},
						]}
						disabled={confirmed || confirmDisabled}
					>
						{confirmed ? (
							<ActivityIndicator size="small" color={colors.primary.inverse} />
						) : (
							<Text
								style={[
									styles.buttonBlockText,
									confirmed || confirmDisabled ? styles.buttonBlockTextDisabled : {},
								]}
							>
								{strings('add_asset.add_nft_tolist')}
							</Text>
						)}
					</TouchableOpacity>}
                </View>
            </KeyboardAwareScrollView>
        </View>
    );
}

ActionViewNFTs.defaultProps = {
    cancelText: strings('action_view.cancel'),
    confirmButtonMode: 'normal',
    confirmText: strings('action_view.confirm'),
    confirmTestID: '',
    confirmed: false,
    cancelTestID: '',
    showCancelButton: true,
    showConfirmButton: true,
    NFTsDetail: false
};

ActionViewNFTs.propTypes = {
    /**
     * TestID for the cancel button
     */
    cancelTestID: PropTypes.string,
    /**
     * TestID for the confirm button
     */
    confirmTestID: PropTypes.string,
    /**
     * Text to show in the cancel button
     */
    cancelText: PropTypes.string,
    /**
     * Content to display above the action buttons
     */
    children: PropTypes.node,
    /**
     * Type of button to show as the confirm button
     */
    confirmButtonMode: PropTypes.oneOf(['normal', 'confirm', 'sign']),
    /**
     * Text to show in the confirm button
     */
    confirmText: PropTypes.string,
    /**
     * Whether action view was confirmed in order to block any other interaction
     */
    confirmed: PropTypes.bool,
    /**
     * Whether action view confirm button should be disabled
     */
    confirmDisabled: PropTypes.bool,
    /**
     * Called when the cancel button is clicked
     */
    onCancelPress: PropTypes.func,
    /**
     * Called when the confirm button is clicked
     */
    onConfirmPress: PropTypes.func,
    /**
     * Called when the touchable without feedback is clicked
     */
    onTouchablePress: PropTypes.func,

    /**
     * Whether cancel button is shown
     */
    showCancelButton: PropTypes.bool,
    /**
     * Whether confirm button is shown
     */
    showConfirmButton: PropTypes.bool,
    /**
     * Determines if the keyboard should stay visible after a tap
     */
    keyboardShouldPersistTaps: PropTypes.string,
    /**
     * Optional View styles. Applies to scroll view
     */
    style: PropTypes.object,
    NFTsDetail: PropTypes.object
};
