import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text } from 'react-native';
import { strings } from '../../../../locales/i18n';
import { fontStyles } from '../../../styles/common';
import ActionModal from '../ActionModal';
import { useAppThemeFromContext, mockTheme } from '../../../util/theme';

const createStyles = (colors) =>
	StyleSheet.create({
		warningModalView: {
			margin: 24,
		},
		warningModalTitle: {
			...fontStyles.bold,
			color: colors.error.default,
			textAlign: 'center',
			fontSize: 20,
			marginBottom: 16,
		},
		warningModalText: {
			...fontStyles.normal,
			color: colors.text.default,
			textAlign: 'center',
			fontSize: 14,
			lineHeight: 18,
		},
		warningModalTextBold: {
			...fontStyles.bold,
			color: colors.text.default,
		},
	});

/**
 * View that renders a warning for existing user in a modal
 */
export default function WalletModal({
	modalStyle,
	warningModalVisible,
	onCancelPress,
	cancelButtonDisabled,
	displayConfirmButton,
	displayCancelButton,
	onRequestClose,
	onConfirmPress,
	children,
	cancelText,
	confirmText,
	actionContainerStyle,
	type
}) {
	return (
		<ActionModal
			modalVisible={warningModalVisible}
			cancelText={cancelText}
			confirmText={confirmText}
			onCancelPress={onCancelPress}
			cancelButtonDisabled={cancelButtonDisabled}
			onRequestClose={onRequestClose}
			onConfirmPress={onConfirmPress}
			cancelButtonMode={'warning'}
			confirmButtonMode={'neutral'}
			displayConfirmButton={displayConfirmButton}
			displayCancelButton={displayCancelButton !== null ? displayCancelButton : true}
			verticalButtons
			modalStyle={modalStyle}
			actionContainerStyle={actionContainerStyle}
			type={type}
		>
			{(children && children) || <Default />}
		</ActionModal>
	);
}

WalletModal.propTypes = {
	cancelText: PropTypes.string,
	cancelButtonDisabled: PropTypes.bool,
	displayConfirmButton: PropTypes.bool,
	confirmText: PropTypes.string,
	children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
	/**
	 * Whether the modal is visible
	 */
	warningModalVisible: PropTypes.bool,
	/**
	 * Cancel callback
	 */
	onCancelPress: PropTypes.func,
	/**
	 * Close callback
	 */
	onRequestClose: PropTypes.func,
	/**
	 * Confirm callback
	 */
	onConfirmPress: PropTypes.func,
};
