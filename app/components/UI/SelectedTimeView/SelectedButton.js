import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

function SelectedButton(props) {
	const {onPress, title, isSelected, time} = props
	return <TouchableOpacity
		style={{
			backgroundColor: isSelected == true ? '#4A369A' : null,
			width: 31,
			height: 24,
			borderRadius: 12,
			justifyContent: 'center',
			alignItems: 'center',
			marginHorizontal: 12
		}}
		onPress={onPress}
	>
		<Text
			style={{
				fontSize: 12,
				fontWeight: '500',
				lineHeight: 16,
				color: isSelected == true ? 'white' : '#A0A0B1'
			}}
		>
			{title}
		</Text>
	</TouchableOpacity>
}

export { SelectedButton};
