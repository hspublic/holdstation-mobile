import React, { useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { SelectedButton } from '../SelectedTimeView/SelectedButton'



function SelectedTimeView(props) {
	// const {setTimeRangeForChart} = this.props;
	const [timeTypes, setTimeTypes] = useState([
		{
			name: '1H',
			isSelected: false,
			time: 1 / 24,
		},
		{
			name: '1D',
			isSelected: true,
			time: 1,
		},
		{
			name: '1W',
			isSelected: false,
			time: 7,
		},
		{
			name: '1M',
			isSelected: false,
			time: 30,
		},
		{
			name: '1Y',
			isSelected: false,
			time: 365,
		},
	]);

	return <View style={{
		backgroundColor: '#1B1B23',
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'row',
		marginTop: 12,
		paddingBottom: 24
	}}>
		{timeTypes.map((timeType, index) =>
			<SelectedButton 
				key={`ii${index}`} 
				onPress={() => {
				// setTimeRangeForChart(365);
				let newTimeTypes = timeTypes.map(eachTimeType => {
					if (eachTimeType.name == timeType.name) {
						if (props.disableAction === false) {
							props.handleSetTimeChart(timeType.time);
						}
						return { ...eachTimeType, isSelected: true }
					} else {
						return { ...eachTimeType, isSelected: false }
					}

					// return {
					// 	...eachTimeType, isSelected: eachTimeType.name == timeType.name
					// }
				})
				setTimeTypes(newTimeTypes)
				}}
				isSelected={timeType.isSelected}
				title={timeType.name}
			/>)}
	</View>
}

export { SelectedTimeView };

