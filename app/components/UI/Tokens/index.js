import React, { PureComponent, useMemo } from 'react';
import PropTypes from 'prop-types';
import {
	Alert,
	TouchableOpacity,
	StyleSheet,
	Text,
	View,
	InteractionManager,
	Image,
	DevSettings,
	DeviceEventEmitter,
	Dimensions
} from "react-native";
import TokenImage from '../TokenImage';
import { fontStyles } from '../../../styles/common';
import { strings } from '../../../../locales/i18n';
import ActionSheet from 'react-native-actionsheet';
import { renderFromTokenMinimalUnit, balanceToFiat } from '../../../util/number';
import Engine from '../../../core/Engine';
import Logger from '../../../util/Logger';
import AssetElement from '../AssetElement';
import { connect } from 'react-redux';
import { safeToChecksumAddress } from '../../../util/address';
import Analytics from '../../../core/Analytics';
import AnalyticsV2 from '../../../util/analyticsV2';
import { ANALYTICS_EVENT_OPTS } from '../../../util/analytics';
import NetworkMainAssetLogo from '../NetworkMainAssetLogo';
import { getTokenList } from '../../../reducers/tokens';
import { isZero } from '../../../util/lodash';
import { ThemeContext, mockTheme } from '../../../util/theme';
import Device from '../../../util/device';
import Checkbox from '../Checkbox';
import StyledButton from '../../UI/StyledButton';
import WalletModal from '../../UI/WalletModal';
import { rgbaColor } from 'react-native-reanimated/src/reanimated2/Colors';
import Jazzicon from 'react-native-jazzicon';
import { toDataUrl } from '../../../util/blockies';
import { GLOBAL_ITEM_FONT_SIZE } from '../../../styles/common';
import {useDebounce} from '../../../actions/timeout/index';
import { toggleDetailTokenModal } from '../../../actions/modals';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';



const win = Dimensions.get('window');

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			flex: 1,
			//minHeight: 350,
			alignItems: 'center',
			backgroundColor: '#1B1B23',
			//justifyContent: 'center',
		},
		borderBlock: {
			width: '90%',
			height: 1,
			backgroundColor: '#ffffff4d',
			opacity: Device.isAndroid() ? 0.6 : 0.5,
		},
		tokenItem: {
			width: '90%',
			//height: 200,
		},
		emptyView: {
			backgroundColor: '#1B1B23',
			// justifyContent: 'center',
			// alignItems: 'center',
			marginTop: 24,
			marginHorizontal: 20,
			marginBottom: 30,
		},
		text: {
			// ...fontStyles.normal,
			width: '100%',
			marginLeft: 20,
			marginTop: 24,
			fontSize: 14,
			fontWeight: '400',
			color: '#58586C',
		},
		add: {
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'center',
		},
		addText: {
			fontSize: 12,
			color: '#B1B5FF',
			...fontStyles.normal,
		},

		tabLabel: {
			fontSize: 20,
			color: '#FFFFFF',
			...fontStyles.boldHight,
			lineHeight: 24,
		},
		info: {
			flex: 1,
			justifyContent: 'center',
			alignItems: 'center',
			textAlign: 'center',
			marginTop: 24,
		},
		hWrapper: {
			flex: 1,
			flexDirection: 'row',
			height: 45,
			width: win.width,
			marginBottom: -win.height / 70
		},
		iconSwipe: {
			justifyContent: 'center',
			marginTop: - win.height / 100
		},
		iconSwipeLeft: {
			alignItems: 'flex-start',
			marginLeft: win.width / 21,
		},
		iconSwipeRight: {
			alignItems: 'flex-end',
			marginRight: win.width / 20,
			marginTop: - win.height/ 50
		},
		iconEdit: {
			marginLeft: 'auto',
			marginRight: - win.width / 200,
			marginTop: - win.height/ 50
		},
		tokenTable: {
			flex: 1,
			flexDirection: 'row',
			width: win.width,
		},
		iconImage: {
			height: 24,
			width: 24,
			marginLeft: win.width / 40,
		},
		iconPin: {
			marginTop: 10,
			height: 14,
			width: 14,
			marginRight: 6,
			marginLeft: -10,
			resizeMode: 'contain',
		},
		viewBalance: {
			marginLeft: - win.width / 50,
			marginTop: -win.height / 400
			// justifyContent: 'flex-end
		},
		viewSecondBalance:{
			marginLeft: 'auto'
		},
		viewMainBlance: {
			alignSelf: 'stretch',
		},
		nameBalance: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			color: '#FFFFFF',
			...fontStyles.bold,
			// textTransform: 'uppercase',
			lineHeight: 17,
		},
		balance: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			color: '#828282',
			textTransform: 'uppercase',
			...fontStyles.normal,
			fontWeight: '400',
			lineHeight: 14,
			marginLeft: win.width / 9.2,
			marginTop: - win.width / 15
		},
		balanceHidden: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			color: '#828282',
			textTransform: 'uppercase',
			...fontStyles.normal,
			fontWeight: '400',
			lineHeight: 14,
			marginLeft: win.width / 5.6,
			marginTop: - win.width / 15
		},
		balanceFiat: {
			fontSize: 12,
			// color: colors.text.alternative,
			color: '#FFFFFF',
			...fontStyles.normal,
			textTransform: 'uppercase',
		},
		checkedIcon: {
			width: 12,
			height: 12,
			marginTop: 0,
			marginLeft: 6
		},
		balanceFiatTokenError: {
			textTransform: 'capitalize',
		},
		ethLogo: {
			width: 32,
			height: 32,
			borderRadius: 16,
			overflow: 'hidden',
			marginRight: 20,

		},
		shadowImage: {
			shadowColor: '#FFF',
			shadowOffset: { width: 0, height: 0 },
			shadowOpacity: 0.2,
			shadowRadius: 3,
		},
		emptyText: {
			color: '#FFFFFF',
			marginBottom: 8,
			fontSize: 14,
		},
		tokenMenu: {
			backgroundColor: 'rgba(255, 255, 255, 0.15)',
			borderRadius: 60,
			height: 34,
		},
		tokenDone: {
			// justifyContent: 'flex-end'
			position: 'absolute',
			right: win.width / 20
		},
		tokenDelete : {
			left: win.width / 20,
			marginRight: win.width / 15,
		},
		tokenPin : {
			marginRight: win.width / 60,
		},
		marginRight10: {
			marginRight: 8,
		},
		doneBtn: {
			backgroundColor: '#6A45FF',
		},
		buttonText: {
			fontSize: 14,
			color: '#FFFFFF',
			...fontStyles.normal,
			paddingVertical: 6,
			paddingHorizontal: 12,
			marginTop: 2
		},
		circle: {
			width: 12,
			height: 12,
			borderRadius: 12 / 2,
			opacity: 1,
			margin: 2,
			borderWidth: 2,
			borderColor: colors.border.default,
			marginRight: 6,
		},
		option: {
			flex: 1,
		},
		touchableOption: {
			flexDirection: 'row',
			marginTop: 20,
		},
		optionText: {
			...fontStyles.normal,
			color: colors.text.default,
		},
		selectedCircle: {
			height: 16,
			resizeMode: 'contain',
			marginRight: 5,
		},
		mainWrapper: {
			backgroundColor: '#1B1B23',
			flex: 1,
			justifyContent: 'center',
			alignItems: 'center',
		},
		containerWelcome: {
			justifyContent: 'center',
			alignItems: 'center',
		},
		container: {
			marginTop: win.height / 100
		},
		walletTitle: {
			fontSize: 16,
			...fontStyles.bold,
			justifyContent: 'center',
			textAlign: 'center',
			color: 'white',
			margin: 10,
			lineHeight: 20
		},
		warningMessageText: {
			flexDirection: 'row',
			fontSize: 16,
			color: 'white',
			...fontStyles.normal,
			textAlign: 'left',
			padding: 30,
		},
		addTokenBtn: {
			width: '100%',
			flexDirection: 'row',
			justifyContent: 'center',
			alignSelf: 'center',
			textAlign: 'center',
			paddingHorizontal: 20,
			paddingVertical: 20,
		},
		addTokenImg: {
			height: 24,
			width: 24,
			flexDirection: 'row',
		},
		addTokenText: {
			marginLeft: 30,
			marginTop: -20,
			fontSize: 14,
			color: '#B1B5FF',
			...fontStyles.normal,
		},
		assetWrapper: {
			flex: 1,
			flexDirection: 'row',
			paddingVertical: 20,
		},
		bottomModal: {
			justifyContent: 'flex-end',
			margin: 0,
		},
	});

class SelectedCheckboxes {
	constructor() {
		this.selectedCheckboxes = [];
	}

	addItem(option) {
		this.selectedCheckboxes.push(option);
	}

	fetchArray() {
		return this.selectedCheckboxes;
	}
}

const RenderBalance = ({ secondaryBalance, balanceError }) => {
	const styles = StyleSheet.create({
		balanceFiat: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			color: '#FFFFFF',
			...fontStyles.normal,
			textTransform: 'uppercase',
			fontWeight: '400',
			lineHeight: 17,
			marginTop: win.height / 100,
			marginRight: win.width / 55
		},
		balanceFiatTokenError: {
			textTransform: 'capitalize',
		},
	})
	return useMemo(() =>
			secondaryBalance ? <Text style={[styles.balanceFiat, balanceError && styles.balanceFiatTokenError]}>
				{secondaryBalance}
			</Text> : <Text style={[styles.balanceFiat, balanceError && styles.balanceFiatTokenError]}>
				{'$0.00'}
			</Text>
		, [secondaryBalance, balanceError])
}

/**
 * View that renders a list of ERC-20 Tokens
 */
class Tokens extends PureComponent {
	static propTypes = {
		/**
		 * Navigation object required to push
		 * the Asset detail view
		 */
		navigation: PropTypes.object,
		/**
		 * Array of assets (in this case ERC20 tokens)
		 */
		tokens: PropTypes.array,
		/**
		 * ETH to current currency conversion rate
		 */
		conversionRate: PropTypes.number,
		/**
		 * Currency code of the currently-active currency
		 */
		currentCurrency: PropTypes.string,
		/**
		 * Object containing token balances in the format address => balance
		 */
		tokenBalances: PropTypes.object,
		/**
		 * Object containing token exchange rates in the format address => exchangeRate
		 */
		tokenExchangeRates: PropTypes.object,
		/**
		 * Array of transactions
		 */
		transactions: PropTypes.array,
		/**
		 * Primary currency, either ETH or Fiat
		 */
		primaryCurrency: PropTypes.string,
		/**
		 * A bool that represents if the user wants to hide zero balance token
		 */
		hideZeroBalanceTokens: PropTypes.bool,
		/**
		 * List of tokens from TokenListController
		 */
		tokenList: PropTypes.object,
		/**
		 * Action that toggles the token detail modal
		 */
		 toggleDetailToken: PropTypes.func,
		 address: PropTypes.string,
		 chainId: PropTypes.number,
	};

	actionSheet = null;

	tokenToRemove = null;

	state = {
		isAddTokenEnabled: true,
		pageIndex: 0,
		totalPages: 0,
		isCollapsed: false,
		showRemovedTokensWarning: false,
		shouldShowUnpinButton: false,
		shouldShowPinButton: false,
		isPressItem: false,
		currentIndex: -1,
		messageAlert: null,
		// disabledPressToken: false,
	};

	constructor() {
		super();
		this.CheckedArrObject = new SelectedCheckboxes();
	}

	getListStorage = async (address, chainId) => {
		const result = JSON.parse(await AsyncStorage.getItem(`${address?.toLowerCase()}.${chainId}`))
		console.log(result)
		return result;
	}

	componentWillReceiveProps  = async () => {
		// console.log('render will prop');
		// const {address, chainId} = this.props
		// try{
		// 	const listStorage = JSON.parse(await this.getListStorage(address, chainId));
		// 	console.log('listStorage', listStorage?.length)
		// } catch (e){
		// 	console.log('error', e)
		// }
	}

	componentDidMount() {
		// const {debounce} = useDebounce();
		const { tokens } = this.props;
		if (tokens.length <= 4) {
			this.setState({ totalPages: 0 });
		} else {
			this.setState({ totalPages: Math.floor((tokens.length - 4) / 10) + 1 });
		}
	}

	renderEmpty = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<View style={styles.emptyView}>
				{this.renderHeader()}
				<Text style={styles.text}>{strings('wallet.no_tokens')}</Text>
			</View>
		);
	};

	onItemPress = async (token) => {
		this.setState({disabledPressToken: true})
		const timeoutPressToken = setTimeout(()=>{
			this.setState({
				disabledPressToken: false,
		   });
		 }, 2000)
		this.props.toggleDetailToken(token)

		// this.props.navigation.push('Asset', { ...token, transactions: this.props.transactions});	


		// clearTimeout(timeoutPressToken)	
		// this.props.navigation.push('AddAsset', { assetType: 'token' });
	};

	findCheckedToken = (asset) => {
		const findIndex = this.CheckedArrObject.fetchArray()?.findIndex((y) => y.key == asset.symbol);
		return findIndex >= 0 ? true : false;
	};

	renderHeader = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		return (
			<View key={'tokens-footer'} style={styles.info}>
				{/*<Text style={styles.emptyText}>{strings('wallet.no_available_tokens')}</Text>*/}
				{!this.state.showHiddenMenu ? (
					<View style={styles.hWrapper}>
						<View style={[styles.iconSwipe, styles.iconSwipeLeft]}>
							<Text style={styles.tabLabel}>Token</Text>
						</View>
						<View style={[styles.iconSwipe, styles.iconEdit]}>
							<TouchableOpacity
								onPress={this.showRemoveMenu}
								testID={'edit-token-button'}
								style={{
									alignItems: 'center',
									flexDirection: 'row'
								}}>
								<Image
									source={require('../../../images/edit.png')}
									style={styles.iconImage}
									resizeMethod={'auto'}
								/>
								{/* <Text style={styles.addText}>{strings('wallet.add_tokens')}</Text> */}
							</TouchableOpacity>
						</View>
						<View style={[styles.iconSwipe, styles.iconSwipeRight]}>
							<TouchableOpacity
								onPress={this.goToAddToken}
								disabled={!this.state.isAddTokenEnabled}
								testID={'add-token-button'}
								style={{
									alignItems: 'center',
									flexDirection: 'row'
								}}>
								<Image
									source={require('../../../images/add-circle.png')}
									style={styles.iconImage}
									resizeMethod={'auto'}
								/>
								{/* <Text style={styles.addText}>{strings('wallet.add_tokens')}</Text> */}
							</TouchableOpacity>
						</View>
					</View>
				) : (
					<View style={styles.hWrapper}>
						<TouchableOpacity onPress={this.removeTokens} style={[styles.tokenMenu, styles.marginRight10, styles.tokenDelete]}>
							<Text style={styles.buttonText}>{strings('token.remove')}</Text>
						</TouchableOpacity>
						{this.shouldShowPinButton && (
							<TouchableOpacity onPress={this.pinTokens} style={[styles.tokenMenu, styles.marginRight10, styles.tokenPin]}>
								<Text style={styles.buttonText}>{strings('token.pin')}</Text>
							</TouchableOpacity>
						)}

						{this.shouldShowUnpinButton && (
							<TouchableOpacity onPress={this.unPinTokens} style={[styles.tokenMenu, styles.tokenPin]}>
								<Text style={styles.buttonText}>{strings('token.unPin')}</Text>
							</TouchableOpacity>
						)}

						<TouchableOpacity onPress={this.hideExtraMenu} style={[styles.tokenMenu, styles.doneBtn, styles.tokenDone]}>
							<Text style={styles.buttonText}>{strings('token.done')}</Text>
						</TouchableOpacity>
					</View>
				)}
			</View>
		);
	};

	renderItem = (asset, index) => {
		const { conversionRate, currentCurrency, tokenBalances, tokenExchangeRates, primaryCurrency, tokenList } =
			this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		const itemAddress = safeToChecksumAddress(asset.address);

		const logo = itemAddress ? tokenList?.[itemAddress]?.iconUrl : null;
		let name = !asset.isETH? itemAddress? itemAddress in tokenList ?
			tokenList[itemAddress].name : itemAddress.toLowerCase() in tokenList?
				tokenList[itemAddress.toLowerCase()].name : null : null : asset.name;
		name = String(name).length > 20 ? String(name).substring(0, 18) + '...' : name;

		const verified = asset.verified ? asset.verified : null;
		const thumbnails = !asset.isETH? itemAddress ? itemAddress in tokenList ?
			tokenList[itemAddress].thumbnails : itemAddress.toLowerCase() in tokenList?
				tokenList[itemAddress.toLowerCase()].thumbnails : null : null: asset.thumbnails;
		const exchangeRate = itemAddress in tokenExchangeRates ? tokenExchangeRates[itemAddress] : undefined;
		const balance =
			asset.balance ||
			(itemAddress in tokenBalances ? renderFromTokenMinimalUnit(tokenBalances[itemAddress], asset.decimals) : 0);
		const balanceFiat = asset.balanceFiat || balanceToFiat(balance, conversionRate, exchangeRate, currentCurrency);
		const balanceValue = `${balance} ${asset.symbol}`;
		asset.fiatValue = (itemAddress in tokenBalances ? renderFromTokenMinimalUnit(tokenBalances[itemAddress], asset.decimals) : 0);
		// render balances according to primary currency
		let mainBalance, secondaryBalance;
		if (primaryCurrency === 'ETH') {
			mainBalance = balanceValue;
			secondaryBalance = balanceFiat;
		} else {
			mainBalance = !balanceFiat ? balanceValue : balanceFiat;
			secondaryBalance = !balanceFiat ? balanceFiat : balanceValue;
		}

		if (asset?.balanceError) {
			mainBalance = asset.symbol;
			secondaryBalance = strings('wallet.unable_to_load');
		}

		asset = { verified, thumbnails, logo, name, ...asset, balance, balanceFiat };
		return (
			<View key={index} style={{marginBottom: - win.height / 60}}>
				<AssetElement
					key={itemAddress || '0x'}
					testID={'asset'}
					onPress={
						this.state.showHiddenMenu
							? () => {
									DeviceEventEmitter.emit('refreshCheckbox', asset);
							  }
							//   : this.onItemPress
							:  !this.state.disabledPressToken ? this.onItemPress: null
					}
					// onLongPress={this.showRemoveMenu}
					asset={asset}
					onPressIn={() => this.setState({isPressItem : true, currentIndex:index})}
					onPressOut={() => this.setState({isPressItem: false, currentIndex: -1})}
					index={index}
					isPressItem={this.state.isPressItem}
					currentIndex={this.state.currentIndex}
					activeOpacity={1}
				>
					<View style={styles.assetWrapper}>
						{this.state.showHiddenMenu && (
							!asset?.isETH?
							<Checkbox
								size={20}
								keyValue={asset.symbol}
								value={asset.symbol}
								checked={false}
								checkedObjArr={this.CheckedArrObject}
								handleShowUnpinButton={this.checkToUpdateUnpinButton}
								pin={asset.pin}
							/> :
							<View style={{marginLeft: win.width / 14}}/>
						)}
						{/* {asset.pin && this.state.showHiddenMenu && (
							<Image
								source={require('../../../images-new/pin.png')}
								style={styles.iconPin}
								resizeMethod={'auto'}
							/>
						)} */}

						{asset.isETH ? (
							asset.symbol === 'ETH' || asset.symbol === 'BNB' ?
								<View style={styles.shadowImage}>
									<NetworkMainAssetLogo big style={styles.ethLogo} testID={'eth-logo'} />
								</View>:
								<View style={styles.shadowImage}>
									<Image source={{ uri: asset.thumbnails}} style={styles.ethLogo}></Image>
								</View>
						) : (
							asset.logo ?
								<View style={styles.shadowImage}>
									<TokenImage asset={asset} containerStyle={styles.ethLogo} />
								</View>:
							asset.thumbnails?
								<View style={styles.shadowImage}>
									<Image source={{ uri: asset.thumbnails}} style={styles.ethLogo}></Image>
								</View>:
								<View style={styles.shadowImage}>
									<Image source={{ uri: toDataUrl(String(asset.address)) }} style={styles.ethLogo} />
								</View>
							)}
						<View testID={'balance'} style={styles.tokenTable}>
							<View style={styles.viewBalance}>
								<Text style={styles.nameBalance}>{name}</Text>
							</View>
							<View>
								{ asset.verified && <Image source={require('../../../images-new/checked-icon.png')} style={styles.checkedIcon}></Image>}
							</View>
							<View style={styles.viewSecondBalance}>
								<RenderBalance secondaryBalance={secondaryBalance} balanceError={asset?.balanceError} />
							</View>

							{/* {
								secondaryBalance && (
									<Text style={[styles.balanceFiat, asset?.balanceError && styles.balanceFiatTokenError]}>
										{secondaryBalance}
									</Text>
								)
							} */}
						</View>
					</View>
					<View style={styles.viewMainBlance}>
						<Text style={this.state.showHiddenMenu ? styles.balanceHidden : styles.balance}>{mainBalance}</Text>
					</View>
				</AssetElement>
			</View>

	);
	};

	viewMore = () => {
		if (this.state.isCollapsed) {
			this.setState({ pageIndex: 0 });
		} else {
			this.setState({ pageIndex: this.state.pageIndex + 1 });
		}
	};

	goToBuy = () => {
		this.props.navigation.navigate('FiatOnRamp');
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_BUY_ETH);
			AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.ONRAMP_OPENED, {
				button_location: 'Home Screen',
				button_copy: 'Buy ETH',
			});
		});
	};

	renderList() {
		const { tokens, hideZeroBalanceTokens, tokenBalances } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		let tokensToDisplay = hideZeroBalanceTokens
			? tokens.filter((token) => {
				return !isZero(tokenBalances[address]) || isETH;
				// eslint-disable-next-line no-mixed-spaces-and-tabs
			})
			: tokens;
		if (this.state.pageIndex === 0 && this.state.totalPages === 0) {
			this.setState({ isCollapsed: false });
			tokensToDisplay = tokensToDisplay.slice(0, 4);
		} else if (tokens.length < this.state.pageIndex * 10 + 4) {
			this.setState({ isCollapsed: true });
			tokensToDisplay = tokensToDisplay.slice(0, tokens.length);
		} else {
			this.setState({ isCollapsed: false });
			tokensToDisplay = tokensToDisplay.slice(0, this.state.pageIndex * 10 + 4);
		}

		if (tokens.length <= 4) {
			return (
				<View style={styles.tokenItem}>
					{this.renderHeader()}
					{tokensToDisplay.map((item, index) => this.renderItem(item, index))}
				</View>
			);
		}
		if (this.state.isCollapsed) {
			return (
				<View style={styles.tokenItem}>
					{this.renderHeader()}
					{tokensToDisplay.map((item, index) => this.renderItem(item, index))}
					<TouchableOpacity onPress={this.viewMore}>
					<View
							style={{
								width: 145,
								height: 31,
								alignItems: 'center',
								justifyContent: 'center',
								backgroundColor: rgbaColor(255, 255, 255, 0.15),
								borderRadius: 12,
								flexDirection: 'row',
								marginTop: 18,
							}}
						>
							<Text
								style={{
									color: '#FFFFFF',
									fontSize: 12,
									...fontStyles.semiBold,
									marginRight: 2
								}}
							>
								{strings('wallet.collapse_tokens')}
							</Text>
							<Image source={require('../../../images-new/arrow-up.svg')} style= {{
								width: 12,
								height: 12,
								resizeMode: 'cover'
							}} />
						</View>
					</TouchableOpacity>
				</View>
			);
		}
		return (
			<View style={styles.tokenItem}>
				{this.renderHeader()}
				{tokensToDisplay.map((item, index) => this.renderItem(item, index))}

					<TouchableOpacity
						style={{
							marginRight: 5,
						}}
						onPress={this.viewMore}
					>
						<View
							style={{
								width: 100,
								height: 31,
								alignItems: 'center',
								justifyContent: 'center',
								backgroundColor: rgbaColor(255, 255, 255, 0.15),
								borderRadius: 12,
								flexDirection: 'row',
								marginTop: 18,
							}}
						>
							<Text
								style={{
									color: '#FFFFFF',
									fontSize: 12,
									...fontStyles.semiBold,
									marginRight: 5
								}}
							>
								{/* {tokens.length - tokensToDisplay.length}  */}
								{strings('wallet.view_more')}
							</Text>
							<Image source={require('../../../images/icon-go-ahead.png')} />
						</View>
					</TouchableOpacity>
			</View>
		);
	}

	goToAddToken = () => {
		this.setState({ showRemovedTokensWarning: false });
		this.setState({ isAddTokenEnabled: false });
		this.props.navigation.push('AddAsset', { assetType: 'token' });
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_ADD_TOKENS);
			this.setState({ isAddTokenEnabled: true });
		});
	};

	showRemoveMenu = (token) => {
		// this.state.showHiddenMenu = false;
		this.setState({ showHiddenMenu: true })
		DeviceEventEmitter.emit('refreshCheckbox', token);
		this.setState({ pageIndex: this.state.pageIndex + 1 });
		// setTimeout(() => {
		// 	DeviceEventEmitter.emit('refreshCheckbox', token);
		// 	this.setState({ pageIndex: this.state.pageIndex + 1 });
		// }, 500);
	};

	hideExtraMenu = () => {
		this.setState({ showHiddenMenu: false })
		// this.state.showHiddenMenu = false;
		this.CheckedArrObject = new SelectedCheckboxes();
		this.setState({ pageIndex: 0 });
	};

	pinTokens = () => {
		if (this.CheckedArrObject.fetchArray().length === 0) {
			this.setState({showRemovedTokensWarning: true,
				messageAlert: {
					title : strings('token.no_item_selected'),
					desc: strings('token.no_item_selected_message'),
				}
		})
		} else {
			const pinTokens = this.CheckedArrObject.fetchArray().flatMap((o) => (o.value ? [o.value] : []));
			this.props.tokens.forEach((token) => {
				token.pin = pinTokens.includes(token.symbol) ? true : token.pin;
			});
		}
		this.hideExtraMenu();
		this.CheckedArrObject = new SelectedCheckboxes();
	};

	checkToUpdateUnpinButton = () => {
		if (this.CheckedArrObject.fetchArray().length === 0) {
			this.shouldShowUnpinButton = false;
			this.shouldShowPinButton = false;
		} else {
			const pinTokens = this.CheckedArrObject.fetchArray().flatMap((o) => (o.value ? [o.value] : []));
			const pinedTokens = this.props.tokens.filter((item) => item.pin === true);
			const unPinedTokens = this.props.tokens.filter((item) => item.pin !== true);
			let isFoundPined = false;
			let isFoundUnPined = false;
			pinTokens.forEach((tokenSymbol) => {
				pinedTokens.some((token) => {
					if (token.symbol === tokenSymbol) {
						isFoundPined = true;
					} else {

					}
				})

				unPinedTokens.some((token1) => {
					if (token1.symbol === tokenSymbol) {
						isFoundUnPined = true;
					} else {

					}
				})
			});
			this.shouldShowUnpinButton = isFoundPined;
			if (isFoundPined === true) {
				if (isFoundUnPined === false) {
					this.shouldShowPinButton = false;
				} else {
					this.shouldShowPinButton = true;
				}
			} else {
				this.shouldShowPinButton = isFoundUnPined;
			}
		}
	};

	unPinTokens = () => {
		if (this.CheckedArrObject.fetchArray().length === 0) {
			this.setState({showRemovedTokensWarning: true,
				messageAlert: {
					title : strings('token.no_item_selected'),
					desc: strings('token.no_item_selected_message'),
				}
			})
		} else {
			const pinTokens = this.CheckedArrObject.fetchArray().flatMap((o) => (o.value ? [o.value] : []));

			this.props.tokens.forEach((token) => {
				token.pin = pinTokens.includes(token.symbol) ? false : token.pin;
			});
		}
		this.hideExtraMenu();
		this.CheckedArrObject = new SelectedCheckboxes();
	};

	removeTokens = async () => {
		if (this.CheckedArrObject.fetchArray().length === 0) {
			this.setState({showRemovedTokensWarning: true,
				messageAlert: {
					title : strings('token.no_item_selected'),
					desc: strings('token.no_item_selected_message'),
				}
			})
		} else {
			const pinTokens = this.CheckedArrObject.fetchArray().flatMap((o) => (o.value ? [o.value] : []));
			// try{
				
			// } catch (error){
			// }
			this.props.tokens.forEach((token) => {
				if (pinTokens.includes(token.symbol) && (!token?.isETH)) {
					this.removeToken(token);
				}
			});
			this.setState({ showRemovedTokensWarning: true,
				messageAlert: {
					title : strings('token.remove_success'),
					desc: strings('token.remove_success_message'),
				} 
			});
		}
		this.hideExtraMenu();
		this.CheckedArrObject = new SelectedCheckboxes();
	};

	removeToken = (token) => {
		const { TokensController } = Engine.context;
		const tokenAddress = token.address;
		try {
			TokensController.removeAndIgnoreToken(tokenAddress);
		} catch (error) {
			Logger.log('Error while trying to remove token', error, tokenAddress);
		}
	};

	toggleRemovedTokensWarning = () => {
		this.setState({ showRemovedTokensWarning: false });
	};

	render = () => {
		// console.log('render tokens')
		const { tokens } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance;
		const styles = createStyles(colors);
		if (tokens.length <= 4) {
			this.setState({ totalPages: 0 });
		} else {
			this.setState({ totalPages: Math.floor((tokens.length - 4) / 10) + 1 });
		}
		return (
			<View style={styles.wrapper} testID={'tokens'}>
				<View style={styles.borderBlock} />
				{tokens && tokens.length ? this.renderList() : this.renderEmpty()}
				<WalletModal
					warningModalVisible={this.state.showRemovedTokensWarning}
					onCancelPress={this.toggleRemovedTokensWarning}
					displayConfirmButton={false}
					confirmText={strings('accounts.ok')}
					cancelText={strings('accounts.cancel')}
				>
					<View style={styles.wrapperModal}>
						<View style={styles.container}>
							<Text style={styles.walletTitle}>{this.state.messageAlert?.title}</Text>
						</View>
						<View style={(styles.walletInfo, styles.warningWrapper)}>
							<View style={styles.accountInfo}>
								<Text style={styles.warningMessageText}>{this.state.messageAlert?.desc}</Text>
							</View>
						</View>
						{/* <View style={styles.addTokenBtn}>
							<TouchableOpacity
								onPress={this.goToAddToken}
								disabled={!this.state.isAddTokenEnabled}
								testID={'add-token-button'}
							>
								<Image
									source={require('../../../images/add-circle.png')}
									style={styles.addTokenImg}
									resizeMethod={'auto'}
								/>
								<View style={styles.accountInfo}>
									<Text style={styles.addTokenText}>{strings('wallet.add_tokens')}</Text>
								</View>
							</TouchableOpacity>
						</View> */}
					</View>
				</WalletModal>

				{/* Modal Token Detail */}
				

			</View>
		);
	};
}

function tokenSort(a ,b){
	const aPin = a.hasOwnProperty('pin') ? a.pin : false;
	const bPin = b.hasOwnProperty('pin') ? b.pin : false;
	const aVerified = a.verified !== null ? a.verified ? 1 : 0 : 0;
	const bVerified = b.verified !== null ? b.verified ? 1 : 0 : 0;

	const aSecondaryBalance = a.secondaryBalanceSort !== strings('wallet.unable_to_load') &&
		a.secondaryBalanceSort !== undefined ? a.secondaryBalanceSort : 0;
	const bSecondaryBalance = b.secondaryBalanceSort !== strings('wallet.unable_to_load') &&
		b.secondaryBalanceSort !== undefined?  b.secondaryBalanceSort : 0;
	return aPin !== bPin? aPin < bPin ? 1 : -1 : aVerified !== bVerified ?
		aVerified < bVerified ? 1 : -1 : aSecondaryBalance !== bSecondaryBalance ?
			aSecondaryBalance < bSecondaryBalance ? 1: -1 :
				a.symbol.toLowerCase() > b.symbol.toLowerCase() ? 1 : -1;
}

function mapStateToProps(state) {
	const props = {
		currentCurrency: state.engine.backgroundState.CurrencyRateController.currentCurrency,
		conversionRate: state.engine.backgroundState.CurrencyRateController.conversionRate,
		primaryCurrency: state.settings.primaryCurrency,
		tokenBalances: state.engine.backgroundState.TokenBalancesController.contractBalances,
		tokenExchangeRates: state.engine.backgroundState.TokenRatesController.contractExchangeRates,
		hideZeroBalanceTokens: state.settings.hideZeroBalanceTokens,
		tokenList: getTokenList(state),
		detailTokenModalVisible: state.modals.detailTokenModalVisible,
	};
	state.engine.backgroundState.TokensController.tokens.sort(tokenSort);
	return props;
}

const mapDispatchToProps = (dispatch) => ({
	toggleDetailToken: (token) => dispatch(toggleDetailTokenModal(token)),
});

Tokens.contextType = ThemeContext;

export default connect(mapStateToProps, mapDispatchToProps)(Tokens);
