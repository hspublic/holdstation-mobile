import React, { PureComponent, useState } from "react";
import {
	Alert,
	InteractionManager,
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	Image,
	Dimensions,
	SafeAreaView,
	ActivityIndicator, Linking, ScrollView
} from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import PropTypes, { any } from "prop-types";
import { swapsUtils } from "@metamask/swaps-controller";
import AssetIcon from "../AssetIcon";
import Identicon from "../Identicon";
import AssetActionButton from "../AssetActionButton";
import AppConstants from "../../../core/AppConstants";
import { fontStyles, GLOBAL_ITEM_FONT_SIZE } from "../../../styles/common";
import { strings } from "../../../../locales/i18n";
import { toggleReceiveModal } from "../../../actions/modals";
import { connect } from "react-redux";
import { renderFromTokenMinimalUnit, balanceToFiat, renderFromWei, weiToFiat, hexToBN } from "../../../util/number";
import { safeToChecksumAddress } from "../../../util/address";
import { getEther } from "../../../util/transactions";
import { newAssetTransaction } from "../../../actions/transaction";
import { isSwapsAllowed } from "../Swaps/utils";
import { swapsLivenessSelector, swapsTokensObjectSelector } from "../../../reducers/swaps";
import { getTokenList } from "../../../reducers/tokens";
import Engine from "../../../core/Engine";
import Logger from "../../../util/Logger";
import Analytics from "../../../core/Analytics";
import AnalyticsV2 from "../../../util/analyticsV2";
import { ANALYTICS_EVENT_OPTS } from "../../../util/analytics";
import { allowedToBuy } from "../FiatOrders";
import AssetSwapButton from "../Swaps/components/AssetSwapButton";
import NetworkMainAssetLogo from "../NetworkMainAssetLogo";
import { ThemeContext, mockTheme } from "../../../util/theme";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { LineChart } from "react-native-chart-kit";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Entypo from "react-native-vector-icons/Entypo";
import Modal from "react-native-modal";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import { CANCEL_BUTTON_ID } from "../../../constants/test-ids";
import StyledButton from "../StyledButton/index";
import { rgbaColor } from "react-native-reanimated/src/reanimated2/Colors";
import { SelectedTimeView } from "../SelectedTimeView/SelectedTimeView";
import { SelectedButton } from "../SelectedTimeView/SelectedButton";
import {
	getTokenDetail,
	saveTokenDetail,
} from '../../../util/localstorage/tokenDetail';
import RenderHTML from "react-native-render-html";


const win = Dimensions.get('window');
var show = 1;
var timeRangeChart = 1;

const createStyles = (colors) =>
	StyleSheet.create({
		// eslint-disable-next-line react-native/no-color-literals
		wrapper: {
			// top: 100,
			// position: 'absolute',
			minHeight: '85%',
			// height: '50%',
			backgroundColor: "#1B1B23",
			borderTopLeftRadius: 20,
			borderTopRightRadius: 20,
			// backgroundColor: 'transparent'
		},
		scrollView: {
			flex: 1,
			marginBottom: 0,
		},
		assetLogo: {
			marginTop: 15,
			alignItems: "center",
			justifyContent: "center",
			borderRadius: 10,
			marginBottom: 10
		},
		ethLogo: {
			width: 70,
			height: 70
		},
		balance: {
			alignItems: "center",
			marginTop: 10,
			marginBottom: 20
		},
		amount: {
			fontSize: 16,
			color: "#B1B5FF",
			...fontStyles.normal,
			textTransform: "uppercase",
			fontWeight: "600",
			paddingTop: 8,
			lineHeight: 19
		},
		amountFiat: {
			fontSize: 16,
			color: "white",
			...fontStyles.bold,
			textTransform: "uppercase",
			paddingTop: 8,
			lineHeight: 19
		},
		actions: {
			flex: 1,
			justifyContent: "center",
			alignItems: "flex-start",
			flexDirection: "row"
		},
		warning: {
			borderRadius: 8,
			color: "white",
			...fontStyles.normal,
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			lineHeight: 20,
			borderWidth: 1,
			borderColor: colors.warning.default,
			backgroundColor: colors.warning.muted,
			padding: 20
		},
		warningLinks: {
			color: colors.primary.default
		},
		title: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			color: "#FFFFFF",
			justifyContent: "center",
			textAlign: "center",
			width: "100%",
			alignItems: "center",
			paddingBottom: 48,
			borderBottomColor: '#282832',
			borderBottomWidth: 1
		},
		titleOption: {
			color: '#FFFFFF',
			...fontStyles.bold,
			fontSize: 16,
			lineHeight: 19,
		},
		btnAction: {
			flex: 1,
			backgroundColor: "#627EEA",
			borderRadius: 12,
			height: 44,
			justifyContent: "center",
			alignItems: "center",
			flexDirection: "row"
		},
		btnActionDisable: {
			flex: 1,
			backgroundColor: "#627EEA",
			borderRadius: 12,
			height: 44,
			justifyContent: "center",
			alignItems: "center",
			flexDirection: "row",
			opacity: 0.1
		},
		textBtn: {
			fontSize: 16,
			color: "white",
			fontWeight: "500",
			marginLeft: 10,
		},
		textTitle: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			lineHeight: 18,
			color: "#A0A0B1",
			fontWeight: "400",
			alignSelf: "flex-start"
		},
		valueText: {
			fontSize: 16,
			...fontStyles.bold,
			lineHeight: 19,
			color: "#D0D0D8",
			alignSelf: "flex-start",
			paddingTop: 5,
		},
		tokenName: {
			color: "white",
			fontWeight: "500",
			fontSize: 16,
			lineHeight: 20
		},
		tokenNoData: {
			color: "white",
			fontWeight: "500",
			fontSize: 16,
			alignSelf: "center",
			marginTop: win.height / 6
		},
		tokenInformation: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			fontWeight: "400",
			color: "white",
			padding: 10,
		},
		roundButton1: {
			width: 91,
			height: 34,
			justifyContent: "center",
			alignItems: "center",
			flexDirection: "row",
			// padding: 10,
			marginRight: 4,
			borderRadius: 15,
			backgroundColor: rgbaColor(255, 255, 255, 0.15)
		},
		roundButton1Disable: {
			width: 91,
			height: 34,
			justifyContent: "center",
			alignItems: "center",
			flexDirection: "row",
			// padding: 10,
			borderRadius: 15,
			backgroundColor: rgbaColor(255, 255, 255, 0.15),
			opacity: 0.1
		},
		roundButton2: {
			width: 34,
			height: 34,
			justifyContent: "center",
			alignItems: "center",
			// padding: 10,
			marginHorizontal: 4,
			borderRadius: 100,
			backgroundColor: rgbaColor(255, 255, 255, 0.15),
			opacity: 1
		},
		container: {
			flex: 1,
			justifyContent: "center"
		},
		horizontal: {
			flexDirection: "row",
			justifyContent: "space-around",
			padding: 10
		},
		dragger: {
			width: 40,
			height: 5,
			borderRadius: 4,
			backgroundColor: colors.border.default,
			opacity: 0.6,
			position: 'absolute',
			top: -9
		},
		titleWrapper: {
			width: "100%",
			height: 33,
			alignItems: "center",
			justifyContent: "center",
			position: 'relative'
			//borderBottomWidth: StyleSheet.hairlineWidth,
			//borderColor: colors.border.muted,
		},

		titlePin: {
			color: '#FFFFFF',
			...fontStyles.bold,
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			lineHeight: 18,
			marginLeft: 8,
		},
		iconPin: {
			width: 24,
			height: 24,
			paddingVertical: 4
		},
		headerLogo: {
			flex: 1,
			flexDirection: "row",
			alignItems: "center",
			justifyContent: "flex-start",
			// backgroundColor: 'green'
		},
		bottomModal: {
			justifyContent: 'flex-end',
			margin: 0,
		},
		wrapperModalView: {
			backgroundColor: '#1B1B23',
			borderTopLeftRadius: 15,
			borderTopRightRadius: 15,
			minHeight: 275,
		},
		titleWrapperView: {
			width: '100%',
			height: 18,
			alignItems: 'center',
			justifyContent: 'center',
			//borderBottomWidth: StyleSheet.hairlineWidth,
			//borderColor: colors.border.muted,
		},
		flexRow: {
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'center',
			width: '100%',
			paddingVertical: 12,
			borderBottomColor: '#282832',
			borderBottomWidth: 1
		},
		checkedIcon: {
			width: 12,
			height: 12,
			marginLeft: 4

		},
		headerLeft: {
			flexDirection: 'row',
			alignItems: 'center'
		},
		textPrimary: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			lineHeight: 18,
			color: "#A0A0B1",
		},
		priceToken: {
			fontSize: 24,
			lineHeight: 29,
			color: '#FFFFFF',
			...fontStyles.boldHight,
		},
		pricePercentage: {
			fontSize: 16,
			lineHeight: 19,
			...fontStyles.bold
		},
		zoominItem: {
			transform: [{ scale: 0.97 }]
		},
		shadowImage: {
			width: 54,
			height: 54
		}
	});

/**
 * View that displays the information of a specific asset (Token or ETH)
 * including the overview (Amount, Balance, Symbol, Logo)
 */
class AssetOverview extends PureComponent {
	static propTypes = {
		/**
		 * Map of accounts to information objects including balances
		 */
		accounts: PropTypes.object,
		/**
		/* navigation object required to access the props
		/* passed by the parent component
		 */
		navigation: PropTypes.object,
		/**
		 * Object that represents the asset to be displayed
		 */
		asset: PropTypes.object,
		/**
		 * ETH to current currency conversion rate
		 */
		conversionRate: PropTypes.number,
		/**
		 * Currency code of the currently-active currency
		 */
		currentCurrency: PropTypes.string,
		/**
		 * A string that represents the selected address
		 */
		selectedAddress: PropTypes.string,
		/**
		 * Start transaction with asset
		 */
		newAssetTransaction: PropTypes.func,
		/**
		 * An object containing token balances for current account and network in the format address => balance
		 */
		tokenBalances: PropTypes.object,
		/**
		 * An object containing token exchange rates in the format address => exchangeRate
		 */
		tokenExchangeRates: PropTypes.object,
		/**
		 * Action that toggles the receive modal
		 */
		toggleReceiveModal: PropTypes.func,
		/**
		 * Primary currency, either ETH or Fiat
		 */
		primaryCurrency: PropTypes.string,
		/**
		 * Chain id
		 */
		chainId: PropTypes.string,
		/**
		 * Wether Swaps feature is live or not
		 */
		swapsIsLive: PropTypes.bool,
		/**
		 * Object that contains swaps tokens addresses as key
		 */
		swapsTokens: PropTypes.object,
		/**
		 * Network ticker
		 */
		ticker: PropTypes.string,
		/**
		 * Object that contains tokens by token addresses as key
		 */
		tokenList: PropTypes.object,

		removeToken: PropTypes.func,
		/**
		 * Hides the modal that contains the component
		 */
		hideModal: PropTypes.func,
	};

	state = {
		details: {},
		chartData: {},
		favorite: false,
		pin: false,
		hide: false,
		selectOptionModalVisible: false,
		isLoaded: false,
		textShown: false,
		lengthMore: false,
		dayPercentage: any,
		htmlDescription: '',
		dayTime: 'one_day',
		isPressSwap: false,
		isPressSend: false,
	};

	onReceive = () => {
		const { asset } = this.props;
		this.props.toggleReceiveModal(asset);
	};

	onBuy = () => {
		this.props.navigation.navigate("FiatOnRamp");
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_BUY_ETH);
			AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.ONRAMP_OPENED, {
				button_location: "Token Screen",
				button_copy: "Buy"
			});
		});
	};

	onSend = async () => {
		const { asset, ticker } = this.props;
		this.props.hideModal()
		this.props.newAssetTransaction(asset);
		this.props.navigation.navigate('SendFlowView', {
			screen: 'SendTo',
			params: { skipSelectAsset: true },
		});
		// if (asset.isETH) {
		// 	// this.props.newAssetTransaction(getEther(ticker));
		// 	this.props.newAssetTransaction(asset);
		// 	this.props.navigation.navigate('SendFlowView', {
		// 		screen: 'SendTo',
		// 		params: { skipSelectAsset: true },
		// 	});
		// 	// this.props.navigation.navigate("SendFlowView", { skipSelectAsset: true });
		// } else {
		// 	this.props.newAssetTransaction(asset);
		// 	this.props.navigation.navigate('SendFlowView', {
		// 		screen: 'SendTo',
		// 		params: { skipSelectAsset: true },
		// 	});
		// 	// this.props.navigation.navigate("SendFlowView", { skipSelectAsset: true });
		// }
	};

	onFavorite = () => {
		const { asset } = this.props;
	};

	// goToSwaps = () => {
	// 	this.props.navigation.navigate("Swaps", {
	// 		screen: "SwapsAmountView",
	// 		params: {
	// 			sourceToken: this.props.asset.isETH ? swapsUtils.NATIVE_SWAPS_TOKEN_ADDRESS : this.props.asset.address
	// 		}
	// 	});
	// };
	goToSwaps = () => {
		this.props.hideModal()

		this.props.navigation.navigate('Swaps', {
			screen: 'TokenSelectView',
			params: {
			},
		});
	};

	goToBrowserUrl(url) {
		// this.props.navigation.navigate('BrowserTabHome', {
		// 	screen: 'BrowserView',
		// 	params: {
		// 		newTabUrl: url,
		// 		timestamp: Date.now(),
		// 	},
		// });
		Linking.openURL(url).catch((err) => console.error("Error", err));
	}

	formatBalance = (value) => {
		// console.log(value)
		if (value >= 1000000000) {
			let k = value / 1000000000;
			return k.toFixed(2) + "B";
		}
		if (value < 1000000) {
			return value;
		} else {
			let k = value / 1000000;
			return k.toFixed(2) + "M";
		}
	};

	renderLogo = () => {
		const {
			tokenList,
			asset: { address, isETH }
		} = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (isETH) {
			return <NetworkMainAssetLogo biggest style={styles.ethLogo} />;
		}
		if (this.state.details.image?.thumb) {
			return <Image style={{ width: 42, height: 42, borderRadius: 21 }} source={this.state.details.image?.thumb} />;
		}
		const iconUrl = tokenList[address]?.iconUrl || tokenList[address?.toLowerCase()]?.iconUrl || "";
		return iconUrl ? <AssetIcon logo={iconUrl} /> : <Identicon address={address} />;
	};

	componentDidMount = async () => {
		const { tokenList, asset } = this.props;

		const { SwapsController, TokenListController } = Engine.context;
		const coinGeckoId = asset.address ? asset.address in tokenList ? tokenList[asset.address].coingeckoId :
			asset.address.toLowerCase() in tokenList ? tokenList[asset.address.toLowerCase()].coingeckoId : asset.coingeckoId :
			null;
		// const date = new Date()
		// const dateAdd = new Date(date.getTime() + 30 * 60000)
		if (coinGeckoId === null) {
			const isLoaded = true;
			this.setState({ isLoaded });
		} else {
			let details = await getTokenDetail(asset.address);
			if (details == null || (details.hasOwnProperty('status') && details.status.hasOwnProperty('error_code') &&
				details.status.error_code === 429)) {
				details = await TokenListController.fetchTokenDetail(asset.address);
				await saveTokenDetail(asset.address, details, new Date());
			}

			await this.fetchTokenMarketChart();
			const isLoaded = true;
			this.setState({ isLoaded });
			this.setState({ details });
			this.setState({ htmlDescription: details.description?.en.slice(0, 290) })
			try {
				await SwapsController.fetchTokenWithCache();
			} catch (error) {
				Logger.error(error, "Swaps: error while fetching tokens with cache in AssetOverview");
			}
		}

	};


	fetchTokenMarketChart = async () => {
		const { asset } = this.props;
		const { TokenListController } = Engine.context;

		let options = {
			vs_currency: "usd",
			days: timeRangeChart,
			interval: 'hourly'
		};

		if (Number(timeRangeChart) > 1) {
			options.interval = 'daily'
		} else {
			options.interval = 'hourly'
		}

		let data = await TokenListController.fetchTokenMarketChart(asset.address, options);
		let chartData;
		if (Number(timeRangeChart) === 365) {
			let data365 = []
			for (i = 0; i < data.prices.length; i++) {
				if (i % 15 === 0) {
					data365.push(data.prices[i])
				}
			}
			chartData = {
				datasets: [{ data: data365.map((p) => p[1]) }]
			}
		} else {
			chartData = data ? {
				datasets: [
					{
						data: data.hasOwnProperty('prices') ? data.prices.map((p) => p[1]) : [null]
					}
				],
				// legend: [""] // optional
			} : {}
		}

		this.setState({ chartData });
	};


	onMoreAction() {

		this.toggleSelectOptionModal();
	}

	onPin() {

		this.setState({ pin: !this.state.pin });
		this.toggleSelectOptionModal();
	}


	pinTokens = (asset) => {
		this.props.tokens.forEach((token) => {
			token.pin = asset.symbol.toLowerCase() === token.symbol.toLowerCase() ? true : token.pin;
		})
		asset.pin = true
		this.toggleSelectOptionModal()
	};

	unPinTokens = (asset) => {
		this.props.tokens.forEach((token) => {
			token.pin = asset.symbol.toLowerCase() === token.symbol.toLowerCase() ? false : token.pin;
		})
		asset.pin = false
		this.toggleSelectOptionModal()

	};

	removeToken = (token) => {
		const { TokensController } = Engine.context;
		const tokenAddress = token.address;
		try {
			TokensController.removeAndIgnoreToken(tokenAddress);
		} catch (error) {
			Logger.log('Error while trying to remove token', error, tokenAddress);
		}
	};

	removeTokens = (asset) => {
		this.props.tokens.forEach((token) => {
			if (asset.symbol.includes(token.symbol) && (!token?.isETH)) {
				this.removeToken(token);
			}
		});
		this.toggleSelectOptionModal()
		this.props.navigation.navigate('Home')

	};

	onHide() {
		this.setState({ hide: !this.state.hide });
		this.toggleSelectOptionModal();
	}

	toggleSelectOptionModal = () => {
		this.setState((state) => ({ selectOptionModalVisible: !state.selectOptionModalVisible }));
	};

	renderWarning = () => {
		const {
			asset: { symbol }
		} = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		const supportArticleUrl =
			"https://metamask.zendesk.com/hc/en-us/articles/360028059272-What-to-do-when-your-balance-of-ETH-and-or-ERC20-tokens-is-incorrect-inaccurate";
		return (
			<TouchableOpacity onPress={() => this.goToBrowserUrl(supportArticleUrl)}>
				<Text style={styles.warning}>
					{strings("asset_overview.were_unable")} {symbol} {strings("asset_overview.balance")}{" "}
					<Text style={styles.warningLinks}>{strings("asset_overview.troubleshooting_missing")}</Text>{" "}
					{strings("asset_overview.for_help")}
				</Text>
			</TouchableOpacity>
		);
	};

	callbackSetTimeForChart = async (timeRange) => {
		timeRangeChart = timeRange;
		await this.fetchTokenMarketChart();
	};

	toggleInformation = () => { //To toggle the show text or hide it
		if (this.state.textShown) {
			this.setState({ htmlDescription: this.state.details.description?.en.slice(0, 290) })
			this.setState({ textShown: !this.state.textShown });
		} else {
			this.setState({ textShown: !this.state.textShown });
			this.setState({ htmlDescription: this.state.details.description?.en })
		}
	}

	render() {

		const {
			accounts,
			asset: { address, isETH = undefined, decimals, symbol, balanceError = null, details },
			primaryCurrency,
			selectedAddress,
			tokenExchangeRates,
			tokenBalances,
			conversionRate,
			currentCurrency,
			// chainId,
			// swapsIsLive,
			// swapsTokens
		} = this.props;
		if (show < 5) {
			show = show + 1;
		}
		let links = this.state.details.links;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		let mainBalance, secondaryBalance;
		const itemAddress = safeToChecksumAddress(address);
		let balance, balanceFiat;
		if (isETH) {
			balance = renderFromWei(accounts[selectedAddress] && accounts[selectedAddress].balance);
			balanceFiat = weiToFiat(hexToBN(accounts[selectedAddress].balance), conversionRate, currentCurrency);
		} else {
			const exchangeRate = itemAddress in tokenExchangeRates ? tokenExchangeRates[itemAddress] : undefined;
			balance =
				itemAddress in tokenBalances ? renderFromTokenMinimalUnit(tokenBalances[itemAddress], decimals) : 0;
			balanceFiat = balanceToFiat(balance, conversionRate, exchangeRate, currentCurrency);
		}
		// choose balances depending on 'primaryCurrency'
		if (primaryCurrency === "ETH") {
			mainBalance = `${balance} ${symbol}`;
			secondaryBalance = balanceFiat;
		} else {
			mainBalance = !balanceFiat ? `${balance} ${symbol}` : balanceFiat;
			secondaryBalance = !balanceFiat ? balanceFiat : `${balance} ${symbol}`;
		}
		const chartConfig = {
			backgroundGradientFrom: "#1B1B23",
			backgroundGradientTo: "#1B1B23",
			backgroundGradientFromOpacity: 0,
			fillShadowGradientFrom: Number(this.state.details.market_data?.price_change_percentage_24h?.toFixed(2)) > 0 ? '#2BFF80' : '#FF2B2B',
			fillShadowGradientTo: '#1B1B23',
			// fillShadowGradientFromOpacity: 0.1,
			// fillShadowGradientToOpacity: 0.9,
			backgroundGradientToOpacity: 0.5,
			// color: (opacity = 1) => `rgba(98, 126, 234, ${opacity})`,
			color: () => Number(this.state.details.market_data?.price_change_percentage_24h?.toFixed(2)) > 0 ? '#2BFF80' : '#FF2B2B',
			strokeWidth: 2, // optional, default 3
			// barPercentage: 0.5,
			useShadowColorFromDataset: false, // optional
			borderWidth: 2,
			// borderColor: (opacity = 1) => `rgba(98, 126, 234, ${opacity})`,

		};
		const data = {
			labels: ["January", "February", "March", "April", "May", "June"],
			datasets: [
				{
					data: [10, 10, 10, 10, 10, 10, 10, 10],
					color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
					strokeWidth: 2 // optional
				}
			],
			legend: ["No data"] // optional
		};

		let homepagelink = this.state.details.links?.homepage[0];
		let fblink = this.state.details.links?.facebook_username;
		let twlink = this.state.details.links?.twitter_screen_name;
		let telegramlink = this.state.details.links?.telegram_channel_identifier;
		let discordlink = this.state.details.links?.chat_url;

		let errorData = typeof this.state.details === "undefined" || this.state.details === null;
		if (typeof this.state.details.name === "undefined") {
			errorData = true;
		}
		const screenWidth = Dimensions.get("window").width;
		const screenHeight = Dimensions.get("window").height;
		const {
			tokenList,
			asset
		} = this.props;
		if (this.state.isLoaded === false) {
			return (
				<SafeAreaView style={styles.wrapper}>
					<View style={styles.titleWrapper}>
						<View style={styles.dragger} testID={"addasset-dragger"} />
					</View>
					<View style={[styles.container, styles.horizontal]}>
						<ActivityIndicator size="large" color="white" />
					</View>
				</SafeAreaView>
			);
		}
		return (
			<SafeAreaView
				style={styles.wrapper}
				onScrollToTop={() => this.props.hideModal()}
			>
				<View style={styles.titleWrapper}>
					<View style={styles.dragger} testID={"addasset-dragger"} />
				</View>
				<View style={styles.wrapper} testID={"token-asset-overview"}>

					<ScrollView style={{ flex: 1 }}>
						{/*------------ Logo & cac button chuc nang --------------*/}
						<View style={{ marginHorizontal: 20, flexDirection: "row", marginBottom: 20 }}>
							<View style={styles.headerLogo}>
								{errorData ? (
									this.renderLogo()
								) : (
									<View style={styles.headerLeft}>
										{
											asset.symbol === 'ETH' || asset.symbol === 'BNB' ?
												<View style={styles.shadowImage}>
													<NetworkMainAssetLogo big style={styles.shadowImage} testID={'eth-logo'} />
												</View>
												:
												<Image
													style={{ height: 54, width: 54, borderRadius: 100, }}
													source={{ uri: this.state.details.image?.small, }}
												/>
										}
										<View style={{ marginLeft: 8 }}>
											<View style={[styles.headerLeft]}>
												<Text
													style={styles.tokenName}>{errorData === true ? asset.symbol : this.state.details.name}
												</Text>
												{asset.verified &&
													<Image source={require('../../../images-new/checked-icon.png')}
														style={styles.checkedIcon}>
													</Image>
												}
											</View>
											<Text style={[styles.textPrimary, { marginTop: 3 }]}>{asset.symbol}</Text>
										</View>

									</View>
								)}

							</View>

							<View style={{
								flex: 1,
								flexDirection: "row",
								alignItems: "center",
								justifyContent: "flex-end",
								alignSelf: "center"
							}}>
								{/* <TouchableOpacity
									onPress={() => this.onFavorite()}
									style={styles.roundButton2}
								>
									<FontAwesome name={"heart"} size={20} color={"white"} />
								</TouchableOpacity> */}

								{/* <TouchableOpacity
									onPress={() => this.onFavorite()}
									style={styles.roundButton2}
								>
									<FontAwesome name={"bell"} size={20} color={"white"} />
								</TouchableOpacity> */}

								{
									asset?.isETH ? <Text></Text>
										:
										<TouchableOpacity
											onPress={() => this.onMoreAction()}
											style={styles.roundButton2}
										>
											<FontAwesome name={"ellipsis-h"} size={20} color={"white"} />
										</TouchableOpacity>
								}

							</View>
						</View>

						{/*============================================================*/}
						{/*------------ Asset name & Gia thay doi hom nay --------------*/}

						{/*{!balanceError && (*/}
						<View style={{ width: screenWidth }}>
							<View style={{
								flexDirection: "row",
								justifyContent: "space-between",
								paddingHorizontal: 20
							}}>
								<View style={{
									flexDirection: "column",
									flex: 1,
									paddingBottom: 25
								}}>
									<Text style={styles.priceToken}>
										{errorData === true ? "$--" : "$" + this.state.details.market_data?.current_price?.usd}
									</Text>
									{!errorData &&
										<Text style={[styles.pricePercentage, { color: Number(this.state.details.market_data?.price_change_percentage_24h?.toFixed(2)) > 0 ? "#22D07D" : "red", paddingTop: 4 },]}>
											{Number(this.state.details.market_data?.price_change_percentage_24h?.toFixed(2)) > 0 ?
												'+' + Number(this.state.details.market_data?.price_change_percentage_24h?.toFixed(2))
												: Number(this.state.details.market_data?.price_change_percentage_24h?.toFixed(2))} %
										</Text>
									}
								</View>

							</View>

							{/*============================================================*/}
							{/*------------ Chart View --------------*/}
							{!errorData && <View style={{
								backgroundColor: styles.wrapper.backgroundColor
							}}>
								{this.state.chartData.datasets ?
									<LineChart
										data={this.state.chartData.datasets ? this.state.chartData : data}
										getDotColor={"blue"}
										width={(screenWidth + screenWidth / (this.state.chartData.datasets[0].data.length - 1)) || screenWidth}
										height={170}
										// verticalLabelRotation={30}
										chartConfig={chartConfig}
										withDots={false}
										withVerticalLines={false}
										withHorizontalLines={false}
										withVerticalLabels={false}
										withHorizontalLabels={false}
										bezier
										style={{
											// borderBottomRightRadius: 15,
											paddingRight: 0,
											// backgroundColor: 'red'
										}}

									/>
									: <Text></Text>
								}


							</View>}

							{this.state.chartData.datasets && !errorData && <SelectedTimeView disableAction={errorData === true}
								handleSetTimeChart={this.callbackSetTimeForChart} />}
							{/*============================================================*/}
							{/*------------ So du token & gia tri token --------------*/}
							<View style={{
								backgroundColor: styles.wrapper.backgroundColor,
								flexDirection: "row",
								justifyContent: 'space-between',
								paddingHorizontal: 20
							}}>
								<View style={{
									flex: 1,
									justifyContent: "center",
									marginRight: 8
								}}>
									<Text style={styles.textTitle}>{strings("transaction.balance")}</Text>
									<Text
										style={styles.amount}
										testID={"token-amount"}
									>
										{mainBalance}
									</Text>
								</View>
								<View style={{
									flex: 1,
									justifyContent: "center",
									marginLeft: 8
								}}>
									<Text style={styles.textTitle}>{strings("transactions.value")}</Text>
									{<Text style={styles.amountFiat}>{secondaryBalance}</Text>}
								</View>
							</View>

							{/*============================================================*/}
							{/*------------ Button Swap & Button Send --------------*/}


							<View
								style={{
									flexDirection: "row",
									backgroundColor: styles.wrapper.backgroundColor,
									paddingTop: 19,
									paddingHorizontal: 20
								}}>

								<TouchableOpacity
									disabled={errorData === true}
									onPress={this.goToSwaps}
									style={[{ flex: 1, }, this.state.isPressSwap && styles.zoominItem]}
									onPressIn={() => this.setState({ isPressSwap: true })}
									onPressOut={() => this.setState({ isPressSwap: false })}
									activeOpacity={1}
								>
									<LinearGradient
										colors={['#481FEB', '#8958D9', '#FF7676']}
										start={{ x: 0, y: 0 }}
										end={{ x: 1, y: 0 }}
										style={[styles.btnAction, { marginRight: 8 }]}

									>
										<Image
											source={require('../../../images-new/Swap.png')}
											style={{
												width: 18,
												height: 18
											}}
										/>
										<Text style={styles.textBtn}>{strings("asset_overview.swap")}</Text>
									</LinearGradient>

								</TouchableOpacity>

								<TouchableOpacity
									disabled={errorData === true}
									onPress={this.onSend}
									style={[{ flex: 1, }, this.state.isPressSend && styles.zoominItem]}
									onPressIn={() => this.setState({ isPressSend: true })}
									onPressOut={() => this.setState({ isPressSend: false })}
									activeOpacity={1}
								>
									<LinearGradient
										colors={['#FFB7D5', '#677AAA']}
										start={{ x: 1.0, y: 1 }}
										end={{ x: 0, y: 0.0 }}
										locations={[0.0734, 0.6864]}
										style={[styles.btnAction, { marginLeft: 8 }]}
									>
										<Image
											source={require('../../../images-new/send-2.png')}
											style={{
												width: 18,
												height: 18
											}}
										/>
										<Text style={styles.textBtn}>{strings("transaction.send")}</Text>
									</LinearGradient>

								</TouchableOpacity>



								{/*<AssetActionButton*/}
								{/*	testID={'token-send-button'}*/}
								{/*	icon="send"*/}
								{/*	onPress={this.onSend}*/}
								{/*	label={strings('asset_overview.send_button')}*/}
								{/*/>*/}
								{/*{AppConstants.SWAPS.ACTIVE && (*/}
								{/*	<AssetSwapButton*/}
								{/*		isFeatureLive={swapsIsLive}*/}
								{/*		isNetworkAllowed={isSwapsAllowed(chainId)}*/}
								{/*		isAssetAllowed={isETH || address?.toLowerCase() in swapsTokens}*/}
								{/*		onPress={this.goToSwaps}*/}
								{/*	/>*/}
								{/*)}*/}
							</View>
							{/*============================================================*/}
							{/*------------ 24hVol Thanh khoan & Market Cap --------------*/}
							{!errorData && <View style={{
								flexDirection: "row",
								backgroundColor: styles.wrapper.backgroundColor,
								paddingTop: 20,
								paddingHorizontal: 20
							}}>
								<View style={{
									flex: 1,
									justifyContent: "center",
									alignItems: "flex-start",
									marginRight: 8,
								}}>
									<Text style={styles.textTitle}>{strings("token.24h_vol")}</Text>
									<Text
										style={styles.valueText}>{errorData === true ? "$--" : "$" + this.formatBalance(this.state.details.market_data?.total_volume?.usd)}</Text>
								</View>
								<View style={{
									flex: 1,
									justifyContent: "center",
									alignItems: "center",
									marginLeft: 8
								}}>
									<Text style={styles.textTitle}>{strings("token.market_cap")}</Text>

									<Text
										style={styles.valueText}>{errorData === true ? "$--" : "$" + this.formatBalance(this.state.details.market_data?.market_cap?.usd)}
									</Text>
								</View>

							</View>}

							{/*============================================================*/}
							{/*---------- Thong tin lien quan ----------*/}
							{!errorData ? <View style={{
								backgroundColor: styles.wrapper.backgroundColor,
								paddingTop: 24,
								paddingHorizontal: 20
							}}>
								<Text style={styles.tokenName} >{strings("token.introduce")} {this.state.details.name}
								</Text>
								<TouchableOpacity
									onPress={this.toggleInformation}
									activeOpacity={1}
								>

									<View
										onPress={this.toggleInformation}
										style={{ marginTop: 12 }}>

										<RenderHTML
											contentWidth={win.width}
											source={{ html: this.state.textShown ? this.state.htmlDescription : this.state.htmlDescription + '...' }}
											tagsStyles={{
												a: { color: '#D0D0D8', fontStyle: 'normal', textDecorationLine: 'none' },
											}}
											baseStyle={{ color: '#D0D0D8', lineHeight: 24 }}
										/>
										{
											this.state.textShown ?
												<Text
													style={{ color: '#58586C', marginTop: 12 }}
													onPress={this.toggleInformation}
												>{strings("token.see_less")}</Text> :
												<Text
													style={{ color: '#58586C', marginTop: 12 }}
													onPress={this.toggleInformation}
												>{strings("token.see_more")}</Text>
										}
									</View>
								</TouchableOpacity>
							</View> :
								<View style={{
									backgroundColor: styles.wrapper.backgroundColor,
									paddingTop: 20,
								}}>
									<Text style={styles.tokenNoData}>{strings("token.no_data")}</Text>
								</View>
							}

						</View>
						{/*============================================================*/}
						{/*---------- Button chuc nang khac ----------*/}
						<View style={{
							flexDirection: "row",
							backgroundColor: styles.wrapper.backgroundColor,
							paddingTop: 20,
							paddingBottom: 20,
							paddingHorizontal: 20
						}}>

							{errorData === true && (
								<View style={{
									height: 60
								}}></View>
							)}

							{homepagelink?.length !== 0 && errorData === false && (
								<TouchableOpacity
									onPress={() => this.goToBrowserUrl(homepagelink)}
									style={styles.roundButton1}
								>
									<Image source={require('../../../images-new/link-circle.png')} style={{
										width: 16,
										height: 16
									}}
									/>

									<Text style={{
										fontSize: GLOBAL_ITEM_FONT_SIZE,
										fontWeight: "500",
										color: "white",
										paddingLeft: 4
									}}>Website</Text>
								</TouchableOpacity>
							)}

							{ discordlink!=null &&  discordlink?.length !== 0 && errorData === false && (
								<TouchableOpacity
									onPress={() => this.goToBrowserUrl(discordlink)}
									style={styles.roundButton2}
								>
									{/* <MaterialCommunityIcons name={"discord"} size={20} color={"white"} /> */}
									<Image source={require('../../../images-new/icon-discord.png')} style={{
										width: 34,
										height: 34
									}}
									/>

								</TouchableOpacity>
							)}


							{twlink?.length !== 0 && errorData === false && (
								<TouchableOpacity

									onPress={() => this.goToBrowserUrl("https://twitter.com/" + this.state.details.links.twitter_screen_name)}
									style={styles.roundButton2}
								>
									{/* <Entypo name={"twitter"} size={20} color={"white"} /> */}
									<Image source={require('../../../images-new/icon-twitter.png')} style={{
										width: 34,
										height: 34
									}}
									/>
								</TouchableOpacity>
							)}


							{telegramlink?.length !== 0 && errorData === false && (
								<TouchableOpacity
									onPress={() => this.goToBrowserUrl("https://t.me/" + this.state.details.links.telegram_channel_identifier)}
									style={styles.roundButton2}
								>
									{/* <FontAwesome name={"telegram"} size={20} color={"white"} /> */}
									<Image source={require('../../../images-new/icon-telegram.png')} style={{
										width: 34,
										height: 34
									}}
									/>
								</TouchableOpacity>
							)}
						</View>
					</ScrollView>



					<Modal
						isVisible={this.state.selectOptionModalVisible}
						style={styles.bottomModal}
						onBackdropPress={this.toggleSelectOptionModal}
						onBackButtonPress={this.toggleSelectOptionModal}
						onSwipeComplete={this.toggleSelectOptionModal}
						swipeDirection={"down"}
						propagateSwipe
						backdropColor={colors.overlay.default}
						backdropOpacity={1}
					>
						<SafeAreaView style={styles.wrapperModalView}>
							<View style={styles.titleWrapperView}>
								<View style={styles.dragger} />
							</View>
							<View style={styles.title}>
								<Text style={styles.titleOption}>{strings("token.option")}</Text>
							</View>
							<View style={{ textAlign: 'center', alignItems: 'center' }}>
								<TouchableOpacity
									// onPress={() => this.onPin()}
									style={[styles.flexRow]}
									onPress={asset.pin ? () => this.unPinTokens(asset) : () => this.pinTokens(asset)}
								>
									{
										asset.pin ? <>
											<Image source={require('../../../images-new/unpin.png')} style={{
												width: 24,
												height: 24
											}}
											/>
											<Text style={styles.titlePin}>
												{strings("token.unPin")}
											</Text>
										</> : <>
											<Image source={require('../../../images-new/pin.png')} style={{
												width: 24,
												height: 24
											}}
											/>
											<Text style={styles.titlePin}>
												{strings("token.pin")}
											</Text>
										</>
									}
								</TouchableOpacity>
							</View>
							<View style={{ textAlign: 'center', alignItems: 'center' }}>
								<TouchableOpacity onPress={() => this.removeTokens(asset)} style={[styles.flexRow]}>
									{!this.state.hide ? (
										<>
											{/* <MaterialCommunityIcons name={"eye"} size={38} /> */}
											<Image
												source={require('../../../images/icon-trash.png')}
												style={styles.iconPin}
											/>
											<Text style={styles.titlePin}>
												{strings("token.remove")}
											</Text>
										</>
									) : (
										<>
											{/* <MaterialCommunityIcons name={"eye-off"} size={38} /> */}
											<Text style={styles.titlePin}>
												{strings("token.remove")}
											</Text>
										</>
									)}
								</TouchableOpacity>
							</View>
						</SafeAreaView>
					</Modal>

					{/*<AssetActionButton*/}
					{/*	icon="receive"*/}
					{/*	onPress={this.onReceive}*/}
					{/*	label={strings('asset_overview.receive_button')}*/}
					{/*/>*/}
					{/*{isETH && allowedToBuy(chainId) && (*/}
					{/*	<AssetActionButton*/}
					{/*		icon="buy"*/}
					{/*		onPress={this.onBuy}*/}
					{/*		label={strings('asset_overview.buy_button')}*/}
					{/*	/>*/}
					{/*)}*/}
					{/* </View> */}
					{/*)}*/}
				</View>
				{/* </ScrollView> */}
			</SafeAreaView>
		);
	}
}

const mapStateToProps = (state) => ({
	accounts: state.engine.backgroundState.AccountTrackerController.accounts,
	conversionRate: state.engine.backgroundState.CurrencyRateController.conversionRate,
	currentCurrency: state.engine.backgroundState.CurrencyRateController.currentCurrency,
	primaryCurrency: state.settings.primaryCurrency,
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
	tokenBalances: state.engine.backgroundState.TokenBalancesController.contractBalances,
	tokenExchangeRates: state.engine.backgroundState.TokenRatesController.contractExchangeRates,
	chainId: state.engine.backgroundState.NetworkController.provider.chainId,
	ticker: state.engine.backgroundState.NetworkController.provider.ticker,
	swapsIsLive: swapsLivenessSelector(state),
	swapsTokens: swapsTokensObjectSelector(state),
	tokenList: getTokenList(state),
	tokens: state.engine.backgroundState.TokensController.tokens,
	asset: state.modals.tokenDetail,

});

const mapDispatchToProps = (dispatch) => ({
	toggleReceiveModal: (asset) => dispatch(toggleReceiveModal(asset)),
	newAssetTransaction: (selectedAsset) => dispatch(newAssetTransaction(selectedAsset)),
});

AssetOverview.contextType = ThemeContext;

export default connect(mapStateToProps, mapDispatchToProps)(AssetOverview);
