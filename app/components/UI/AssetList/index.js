import React, { PureComponent, useState } from "react";
import { View, StyleSheet, TouchableHighlight, Image, Dimensions } from "react-native";
import PropTypes from 'prop-types';
import { strings } from '../../../../locales/i18n';
import StyledButton from '../StyledButton';
import AssetIcon from '../AssetIcon';
import { fontStyles, GLOBAL_ITEM_FONT_SIZE } from '../../../styles/common';
import Text from '../../Base/Text';
import { getTokenList } from "../../../reducers/tokens";
import { TokenListMap } from "@holdstation/controllers";
import { useSelector } from "react-redux";
import { TouchableOpacity } from "react-native-gesture-handler";

const win = Dimensions.get('window')

const styles = StyleSheet.create({
	rowWrapper: {
		// padding: 15,
	},
	item: {
		marginBottom: win.height / 60,
		// borderWidth: 2,
		// borderColor: '#FFFFFF26',
		borderRadius: 12,
		height: win.height / 14
		// backgroundColor: 'red'
	},
	imported: {
		opacity: 0.4,
	},
	nonImported: {
		opacity: 1,
	},
	assetListElement: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
	},
	wrapper: {
		//flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		width: '100%',
	},
	textOne: {
		height: 33,
		color: '#FFFFFF',
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: GLOBAL_ITEM_FONT_SIZE,
		...fontStyles.normal,
		lineHeight: 18
	},
	textTwo: {
		height: 33,
		color: '#B1B5FF',
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: GLOBAL_ITEM_FONT_SIZE,
		...fontStyles.semiBold,
	},
	text: {
		color: '#FFFFFF',
		marginLeft: win.width / 25,
		fontSize: GLOBAL_ITEM_FONT_SIZE,
		...fontStyles.bold,
	},
	symbolText: {
		color: '#828282',
		fontSize: GLOBAL_ITEM_FONT_SIZE,
		marginLeft: win.width / 25,
		// marginRight: 10,
		// marginTop: 10,
		lineHeight: 20,
	},
	normalText: {
		color: '#FFFFFF',
		...fontStyles.normal,
	},
	rowWrapperCenter: {
		paddingTop: 15,
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center',
	},
	importedWrapper: {
		borderRadius: 10,
		borderWidth: 1,
		borderColor: '#58586C',
		backgroundColor: '#58586C',
		alignSelf: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		// height: 28,
		// marginTop: win.height / 100,
		marginLeft : win.width / 50
	},
	importedText: {
		paddingLeft: 5,
		paddingRight: 5,
		paddingVertical: 0,
		color: '#FFFFFF',
		fontSize: 12,
	},
	searchWrapper: {
		flex: 1,
		flexDirection: 'row',
		width: '100%',
		marginTop: win.height / 130
	},
	checkedIcon: {
		marginLeft: 4,
		marginTop: 4,
		width: 12,
		height: 12,
	},
});

/**
 * PureComponent that provides ability to search assets.
 */
export default class AssetList extends PureComponent {
	static propTypes = {
		/**
		 * Array of assets objects returned from the search
		 */
		searchResults: PropTypes.array,
		/**
		 * Array of assets objects returned from the search
		 */
		tokenList: PropTypes.array,
		/**
		 * Callback triggered when a token is selected
		 */
		handleSelectAsset: PropTypes.func,
		/**
		 * Object of the currently-selected token
		 */
		selectedAsset: PropTypes.object,
		/**
		 * Search query that generated "searchResults"
		 */
		searchQuery: PropTypes.string,

		onPress: PropTypes.func,

		tokenListMeta: PropTypes.object,

		addToken: PropTypes.func,
	};
	state = {
		isPressToken: false,
		currentTokenIndex: -1,
	};

	render = () => {
		const { searchResults = [], handleSelectAsset, selectedAsset, tokenList, tokenListMeta } = this.props;
		return (
			<View style={styles.rowWrapper} testID={'add-searched-token-screen'}>
				{searchResults.length === 0 && this.props.searchQuery.length ? (
					<>
						<View style={styles.rowWrapperCenter}>
							<View style={styles.wrapper}>
								<Text style={styles.textOne}>{strings('token.no_tokens_found')}</Text>
							</View>
							<View style={styles.wrapper}>
								<Text style={styles.textOne}>{strings('token.try')}</Text>
							</View>
							<View style={styles.wrapper}>
								<TouchableHighlight onPress={() => this.props.onPress && this.props.onPress()}>
									<Text style={styles.textTwo}>{strings('add_asset.custom_token')}</Text>
								</TouchableHighlight>
							</View>
						</View>

					</>
				) : null}
				{searchResults.slice(0, 5).map((_, i) => {
					const { symbol, name, address, iconUrl } = searchResults[i] || {};
					const verified = tokenListMeta.hasOwnProperty(address) ? tokenListMeta[address]
						.hasOwnProperty('verified') ? tokenListMeta[address].verified : null : null;
					const isSelected = selectedAsset && selectedAsset.address === address;
					const isImported = tokenList.some((token) => token.address === address);

					return (
						<TouchableOpacity
							// type={isSelected ? 'selectedAlt' : 'transparentAlt'}
							style={[styles.item, isImported ? styles.imported : styles.nonImported, 
								this.state.isPressToken && this.state.currentTokenIndex === i && {transform:[{scale:0.97}]},
								isSelected && {borderWidth: 2, borderColor: '#6A45FF'}]}
							onPressIn={() => {
								this.setState({isPressToken: true, currentTokenIndex: i});
								handleSelectAsset(searchResults[i]);
							}}
							activeOpacity={1}
							onPressOut={() => this.setState({isPressToken: false, currentTokenIndex: -1})}
							onPress={() => {
								this.props.addToken();
							}} // eslint-disable-line
							key={i}
							disabled={isImported}
							testID={'searched-token-result'}
						>
							<View style={[styles.assetListElement, isImported ? styles.imported : styles.nonImported]}>
								<AssetIcon logo={iconUrl} customStyle={{marginLeft: win.width / 40}}/>
								<View style={{flexDirection: 'column'}}>
									<View style={styles.searchWrapper}>
										<Text style={styles.text}>
											{name}
										</Text>
										<View>
											{ verified && <Image source={require('../../../images-new/checked-icon.png')} style={styles.checkedIcon}></Image>}
										</View>
									</View>

									<View style={{ flexDirection: 'row', marginBottom: win.height / 150}}>
										<Text style={styles.symbolText}>
											{symbol}
										</Text>
										{isImported && (
											<View style={styles.importedWrapper}>
												<Text style={styles.importedText}>Imported</Text>
											</View>
										)}

									</View>
								</View>
							</View>
						</TouchableOpacity>
					);
				})}
			</View>
		);
	};
}
