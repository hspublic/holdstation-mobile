import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity, InteractionManager, Image, Dimensions, Modal } from 'react-native';
import { swapsUtils } from '@metamask/swaps-controller';
import { connect } from 'react-redux';
import Engine from '../../../core/Engine';
import Analytics from '../../../core/Analytics';
import AnalyticsV2 from '../../../util/analyticsV2';
import AppConstants from '../../../core/AppConstants';
import { strings } from '../../../../locales/i18n';

import { swapsLivenessSelector } from '../../../reducers/swaps';
import { showAlert } from '../../../actions/alert';
import { protectWalletModalVisible } from '../../../actions/user';
import { toggleAccountsModal, toggleReceiveModal } from '../../../actions/modals';
import { newAssetTransaction } from '../../../actions/transaction';

import Device from '../../../util/device';
import { ANALYTICS_EVENT_OPTS } from '../../../util/analytics';
import { renderFiat } from '../../../util/number';
import { renderAccountName } from '../../../util/address';
import { getEther } from '../../../util/transactions';
import { doENSReverseLookup, doENSLookup, isDefaultAccountName } from '../../../util/ENSUtils';
import { isSwapsAllowed } from '../Swaps/utils';

import Identicon from '../Identicon';
import AssetActionButton from '../AssetActionButton';
import EthereumAddress from '../EthereumAddress';
import { fontStyles, baseStyles } from '../../../styles/common';
import { allowedToBuy } from '../FiatOrders';
import AssetSwapButton from '../Swaps/components/AssetSwapButton';
import ClipboardManager from '../../../core/ClipboardManager';
import { ThemeContext, mockTheme } from '../../../util/theme';
import Icon from 'react-native-vector-icons/FontAwesome';

import Logger from '../../../util/Logger';
import AccountBackupStep1B from '../../Views/AccountBackupStep1B';
import SecureKeychain from '../../../core/SecureKeychain';
import { win32 } from 'path';
import {BACKGROUND_COLOR} from '../../../constants/backgroundColor'
import Avatar from '../../../util/avatar';

const barcodeIcon = require('../../../images-new/receive-icons/barcode.png');

const win = Dimensions.get('window');

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			// flex: 1,
			paddingTop: 10,
			paddingBottom: 0,
			backgroundColor: '#1B1B23',
			textColor: '#FFFFFF',
		},
		warningWrapper: {
			backgroundColor: colors.error.muted,
			margin: 20,
			marginTop: - win.height / 100,
			borderRadius: 16,
			borderWidth: 0.1,
			borderColor: colors.error.default,
			paddingHorizontal: win.width / 50,
			height: 30,
			justifyContent: 'center',
			alignItems: 'center',
			
		},
		warningRowWrapper: {
			// flex: 1,
			flexDirection: 'row',
			alignContent: 'center',
			alignItems: 'center',
		},
		warningText: {
			marginTop: 10,
			color: colors.error.default,
			...fontStyles.normal,
		},
		scrollView: {
			// flex: 1,
			backgroundColor: colors.background.default,
		},
		info: {
			flex: 1,
			justifyContent: 'center',
			alignItems: 'center',
			textAlign: 'center',
		},
		data: {
			flex: 1,
			minWidth: 99,
			textAlign: 'center',
			paddingTop: 7,
		},
		label: {
			fontSize: 24,
			textAlign: 'center',
			...fontStyles.normal,
			color: colors.text.default,
		},
		labelInput: {
			marginBottom: Device.isAndroid() ? -10 : 0,
		},
		labelWrapper: {
			flexDirection: 'row',
		},
		tag: {
			flexDirection: 'row',
			alignItems: 'center',
			marginTop: 2,
			padding: 4,
			paddingHorizontal: 8,
			borderWidth: 1,
			borderColor: colors.text.default,
			height: 28,
			borderRadius: 14,
		},
		tagText: {
			fontSize: 12,
			...fontStyles.bold,
			minWidth: 32,
			textAlign: 'center',
			color: colors.text.default,
		},
		amountFiat: {
			fontSize: 24,
			padding: 5,
			//color: colors.text.alternative,
			color: '#FFFFFF',
			...fontStyles.normal,
			fontWeight: '600',
			marginTop: - win.height / 65,
			marginBottom: win.height / 65
		},
		onboardingWizardLabel: {
			borderWidth: 2,
			borderRadius: 4,
			paddingVertical: Device.isIos() ? 2 : -4,
			paddingHorizontal: Device.isIos() ? 5 : 5,
			top: Device.isIos() ? 0 : -2,
		},
		actions: {
			flex: 1,
			justifyContent: 'center',
			alignItems: 'flex-start',
			flexDirection: 'row',
		},
		accountNameWrapper: {
			flexDirection: 'row',
			justifyContent: 'center',
		},
		accountLabel: {
			position: 'relative',
			fontSize: 16,
			color: '#FFFFFF',
			alignItems: 'center',
			fontWeight: '600',
			...fontStyles.bold,
		},
		angleDown: {
			alignSelf: 'flex-end',
			color: '#FFFFFF',
			borderColor: 'red',
		},
		addressBlock: {
			marginBottom: 20,
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
		},
		addressWrapper: {
			borderRadius: 40,
			//marginTop: 5,
			// marginBottom: 20,
			paddingVertical: 7,
			paddingHorizontal: 3,
			// textColor: '#A0A0B1',
		},
		address: {
			fontSize: 12,
			color: '#A0A0B1',
			...fontStyles.normal,
			letterSpacing: 0.8,
		},
		receiveButton: {
			paddingVertical: 7,
			paddingHorizontal: 3,
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
		},
		receiveButtonIcon: {
			width: 12,
			height: 12,
		},
		receiveButtonText: {
			color: '#7E73FF',
			fontSize: 12,
			marginLeft: 3,
			...fontStyles.semiBold,
		},
		hWrapper: {
			flex: 1,
			flexDirection: 'row',
			height: 45,
			width: '100%',
			marginBottom: win.height / 80
		},
		identiconBorder: {
			flex: 2,
			borderRadius: 24,
			// borderWidth: 2,
			padding: 1,
			alignItems: 'center',
			marginBottom: win.height / 50
			// borderColor: colors.primary.default,
			// shadowColor: 
		},
		iconSwipe: {
			flex: 4.5,
			justifyContent: 'center',
		},
		iconSwipeLeft: {
			alignItems: 'flex-start',
			marginLeft: win.width / 20
		},
		iconSwipeRight: {
			alignItems: 'flex-end',
			marginRight: win.width / 20
		},
		iconImage: {
			height: 30,
			resizeMode: 'contain',
		},
		iconSetting: {
			height: 30,
			resizeMode: 'contain',
			marginLeft: - win.width / 30
		},
		iconBlockie: {
			backgroundColor: 'red',

		},
		textContainer: {
			alignItems: 'center',
			justifyContent: 'center',
			// backgroundColor: colors.background.alternative,
			borderRadius: 8,
		},
		groupImage: {
			borderRadius: 24,
			width: 48,
			height: 48,
			overflow: 'hidden',
		},
		textWrapperIcon: {
			textAlign: 'center',
			fontSize: 18,
			color: '#FFF',
			marginTop: -win.height/24,
			...fontStyles.boldHight
		},
		warningWrapperIcon: {
			// marginTop:  win.height / 150,
			// marginLeft: win.width / 200,
			// marginRight: win.width / 100
			marginRight: win.width / 100
		},
		imageWarning:{
			width: 12,
			height: 12,
			// marginTop: win.height / 130 
		},
		warningMessageText:{
			color : '#EB5252',
			...fontStyles.semiBold,
			fontSize: 12,
			// marginTop: win.height / 130,
		},
	
		copyWrapper:{
			width: win.width/2.5,
			// height: win.height/20,
			resizeMode: 'contain',
			position: 'absolute',
			left: win.width/3.5,
			top: -win.height/25,
		},
		zoomInItem: {
			transform: [{scale: 0.97}]
		},
		arrowRight: {
			marginLeft: win.width / 150
		}
	});

/**
 * View that's part of the <Wallet /> component
 * which shows information about the selected account
 */
class AccountOverview extends PureComponent {
	static propTypes = {
		/**
		 * String that represents the selected address
		 */
		selectedAddress: PropTypes.string,
		/**
		/* Identities object required to get account name
		*/
		identities: PropTypes.object,
		/**
		 * Object that represents the selected account
		 */
		account: PropTypes.object,
		/**
		/* Selected currency
		*/
		currentCurrency: PropTypes.string,
		/**
		/* Triggers global alert
		*/
		showAlert: PropTypes.func,
		/**
		 * Action that toggles the accounts modal
		 */
		toggleAccountsModal: PropTypes.func,
		/**
		 * whether component is being rendered from onboarding wizard
		 */
		onboardingWizard: PropTypes.bool,
		/**
		 * Used to get child ref
		 */
		onRef: PropTypes.func,
		/**
		 * Prompts protect wallet modal
		 */
		protectWalletModalVisible: PropTypes.func,
		/**
		 * Start transaction with asset
		 */
		newAssetTransaction: PropTypes.func,
		/**
		/* navigation object required to access the props
		/* passed by the parent component
		*/
		navigation: PropTypes.object,
		/**
		 * Action that toggles the receive modal
		 */
		toggleReceiveModal: PropTypes.func,
		/**
		 * Chain id
		 */
		chainId: PropTypes.string,
		/**
		 * Wether Swaps feature is live or not
		 */
		swapsIsLive: PropTypes.bool,
		/**
		 * ID of the current network
		 */
		network: PropTypes.string,
		/**
		 * Current provider ticker
		 */
		ticker: PropTypes.string,
		seedphraseBackedUp: PropTypes.bool,
		screen: PropTypes.string,
	};

	state = {
		accountLabelEditable: false,
		accountLabel: '',
		originalAccountLabel: '',
		ens: undefined,
		avt: this.props.account.address,
		modalCopy: false,
		isLeftPress: false,
		isRightPress: false,
	};

	editableLabelRef = React.createRef();
	scrollViewContainer = React.createRef();
	mainView = React.createRef();

	animatingAccountsModal = false;

	toggleAccountsModal = () => {
		const { onboardingWizard } = this.props;
		if (!onboardingWizard && !this.animatingAccountsModal) {
			this.animatingAccountsModal = true;
			this.props.toggleAccountsModal();
			setTimeout(() => {
				this.animatingAccountsModal = false;
			}, 500);
		}
	};

	input = React.createRef();

	componentDidMount = () => {
		const { identities, selectedAddress, onRef } = this.props;
		const accountLabel = renderAccountName(selectedAddress, identities);
		this.setState({ accountLabel });
		onRef && onRef(this);
		InteractionManager.runAfterInteractions(() => {
			this.doENSLookup();
		});
	};

	pressIconLeft = () => {
		// this.props.navigation.navigate('Activity');
		this.props.screen === 'transaction-screen' ?  this.props.navigation.navigate('SettingsView') : this.props.navigation.navigate('TransactionsHome');
	};

	pressIconRight = () => {
		// this.props.navigation.navigate('Activity');
		this.props.screen === 'transaction-screen' && this.props.navigation.navigate('WalletView')
	};

	componentDidUpdate(prevProps) {
		if (prevProps.account.address !== this.props.account.address || prevProps.network !== this.props.network) {
			requestAnimationFrame(() => {
				this.doENSLookup();
			});
		}
	}

	setAccountLabel = () => {
		const { PreferencesController } = Engine.context;
		const { selectedAddress } = this.props;
		const { accountLabel } = this.state;
		PreferencesController.setAccountLabel(selectedAddress, accountLabel);
		this.setState({ accountLabelEditable: false });
	};

	onAccountLabelChange = (accountLabel) => {
		this.setState({ accountLabel });
	};

	setAccountLabelEditable = () => {
		const { identities, selectedAddress } = this.props;
		const accountLabel = renderAccountName(selectedAddress, identities);
		this.setState({ accountLabelEditable: true, accountLabel });
		setTimeout(() => {
			this.input && this.input.current && this.input.current.focus();
		}, 100);
	};

	cancelAccountLabelEdition = () => {
		const { identities, selectedAddress } = this.props;
		const accountLabel = renderAccountName(selectedAddress, identities);
		this.setState({ accountLabelEditable: false, accountLabel });
	};

	copyAccountToClipboard = async () => {
		const biometryType = await SecureKeychain.getSupportedBiometryType();
		Logger.error(biometryType);
		const { selectedAddress } = this.props;
		await ClipboardManager.setString(selectedAddress);
		// this.props.showAlert({
		// 	isVisible: true,
		// 	autodismiss: 1500,
		// 	content: 'clipboard-alert',
		// 	data: { msg: strings('account_details.account_copied_to_clipboard') },
		// });
		this.setState({
			modalCopy: true,
		  });
		  setTimeout(() => {
			this.setState({
				modalCopy: false,
			});
		  }, 1000);
		// setTimeout(() => this.props.protectWalletModalVisible(), 2000);
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_COPIED_ADDRESS);
		});
	};

	onReceive = () => this.props.toggleReceiveModal();

	onSend = () => {
		const { newAssetTransaction, navigation, ticker } = this.props;
		newAssetTransaction(getEther(ticker));
		navigation.navigate('SendFlowView');
	};

	onBuy = () => {
		this.props.navigation.navigate('FiatOnRamp');
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_BUY_ETH);
			AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.ONRAMP_OPENED, {
				button_location: 'Home Screen',
				button_copy: 'Buy',
			});
		});
	};

	goToSwaps = () =>
		this.props.navigation.navigate('Swaps', {
			screen: 'SwapsAmountView',
			params: {
				sourceToken: swapsUtils.NATIVE_SWAPS_TOKEN_ADDRESS,
			},
		});

	goToSeedphrase = () => {
		//Logger.log(JSON.stringify(this.props))
		this.props.navigation.navigate('Seedphrases', {
			screen: 'AccountBackupStep1',
			params: {},
		});
	};

	doENSLookup = async () => {
		const { network, account } = this.props;
		try {
			const ens = await doENSReverseLookup(account.address, network);
			this.setState({ ens });
		} catch { }
	};

	render() {
		const {
			account: { address, name },
			currentCurrency,
			onboardingWizard,
			chainId,
			swapsIsLive,
			seedphraseBackedUp,
		} = this.props;
		const colors = this.context.colors || mockTheme.colors;
		//const themeAppearance = this.context.themeAppearance || 'light';
		const styles = createStyles(colors);

		const fiatBalance = `${renderFiat(Engine.getTotalFiatAccountBalance(), currentCurrency)}`;

		if (!address) return null;
		//const { accountLabelEditable, accountLabel, ens } = this.state;
		const { ens } = this.state;

		//const isQRHardwareWalletAccount = isQRHardwareAccount(address);

		return (
			<View style={styles.wrapper} ref={this.scrollViewContainer} collapsable={false}>
				<ScrollView
					bounces={false}
					keyboardShouldPersistTaps={'never'}
					style={styles.scrollView}
					contentContainerStyle={styles.wrapper}
					testID={'account-overview'}
				>
					<View style={styles.info} ref={this.mainView}>
						<View style={styles.hWrapper}>
							<View style={[styles.iconSwipe, styles.iconSwipeLeft, this.state.isLeftPress && styles.zoomInItem]}>
								<TouchableOpacity 
									onPress={this.pressIconLeft}
									onPressIn={() => this.setState({isLeftPress: true})}
									onPressOut={() => this.setState({isLeftPress: false})} 
									activeOpacity={1}
								>
									{this.props.screen === 'transaction-screen' ? 
										<Image
											source={require('../../../images-new/setting.png')}
											style={styles.iconSetting}
											resizeMethod={'auto'}
										/>:
										<Image
											source={require('../../../images/icon-swipe-left.png')}
											style={styles.iconImage}
											resizeMethod={'auto'}
									/>}
								</TouchableOpacity>
							</View>
							<View style={{marginTop: - win.height / 300}}>
								{/* <TouchableOpacity
									style={styles.identiconBorder}
									disabled={onboardingWizard}
									testID={'wallet-account-identicon'}
								> */}
									<Avatar address={this.props.account.address}/>

								{/* </TouchableOpacity> */}
							</View>
							<View style={[styles.iconSwipe, styles.iconSwipeRight]}>
								<TouchableOpacity 
									onPress={this.pressIconRight}
									onPressIn={() => this.setState({isRightPress: true})}
									onPressOut={() => this.setState({isRightPress: false})}
									style={this.state.isRightPress && {transform : [{scale: 0.9}]}} 
									activeOpacity={1}
								>
									<Image
										source={require('../../../images/icon-swipe-right.png')}
										style={styles.iconImage}
										resizeMethod={'auto'}
									/>	
								</TouchableOpacity>
							</View>
						</View>
						<View ref={this.editableLabelRef} style={styles.data} collapsable={false}>
							<TouchableOpacity
								style={styles.accountInfo}
								onPress={this.toggleAccountsModal}
								testID={'navbar-account-button'}
							>
								<View style={styles.accountNameWrapper}>
									<Text style={styles.accountLabel} numberOfLines={1}>
										{!!ens ? ens : name}
									</Text>
									<View style={{marginLeft: win.width / 70, marginTop: win.height/ 120}}>
										{/* <Icon name="angle-down" size={20} style={styles.angleDown} /> */}
										<Image source={require('../../../images-new/dropdown.png')} style={{
											width: 10,
											height: 5,
										}}></Image>
									</View>
								</View>
							</TouchableOpacity>
						</View>
						<View style={styles.addressBlock}>
							<TouchableOpacity style={styles.addressWrapper} onPress={this.copyAccountToClipboard}>
								<EthereumAddress address={address} style={styles.address} type={'short'} />
							</TouchableOpacity>
							<TouchableOpacity style={styles.receiveButton} onPress={this.onReceive}>
								<Image style={styles.receiveButtonIcon} source={barcodeIcon} />
								<Text style={styles.receiveButtonText}>{strings('receive_request.title')}</Text>
							</TouchableOpacity>
						</View>
						<Modal
							animationType="fade"
							transparent
							visible={this.state.modalCopy}
							onRequestClose={() => {
								this.setState({modalCopy:false})
						}}>
							{/* <Image source={require('../../../images-new/copied.png')} style={styles.copyWrapper} ></Image> */}
							<View
								style={{
									backgroundColor: '#FFFFFF',
									// width: win.width/7.13,
									// height: win.height/28.9,
									position: 'absolute',
									left: win.width/2.5,
									top: win.height/6,
									borderRadius: 10,
								}}>
									<View style={{
										position:'absolute',
										left: win.width/22,
										top:  win.height/30,
										width: 0,
										height: 0,
										borderTopWidth: 12,
										borderRightWidth: 12,
										borderBottomWidth: 0,
										borderLeftWidth: 12,
										borderTopColor: "#FFFFFF",
										borderRightColor: 'transparent',
										borderBottomColor: 'transparent',
										borderLeftColor: 'transparent',
									
									}}>
									</View>
									<Text style={{fontSize: 14, color: '#1B1B23', alignSelf:'center', justifyContent: 'center', ...fontStyles.normal
									,  paddingHorizontal: 12, paddingVertical: 8}}>
										{strings(`${'Copied'}`)}
									</Text>
							</View>
						
						</Modal>
						{!seedphraseBackedUp ? (
							<View style={styles.warningWrapper}>
								<TouchableOpacity
									onPress={this.goToSeedphrase}
									style={[styles.warningRowWrapper]}
								>
									<View style={styles.warningWrapperIcon}>
										<Image
											source={require('../../../images/icon-warning.png')}
											style={styles.imageWarning}
											resizeMethod={'auto'}
										/>
									</View>
									<View>
										<Text style={styles.warningMessageText}>{strings(`wallet.un_secure`)}</Text>
									</View>
									<View style={styles.arrowRight}>
										<Image
											source={require('../../../images-new/arrow-right.png')}
											style={[styles.imageWarning, {tintColor: '#EB5252'}]}
											resizeMethod={'auto'}
										/>
									</View>
								</TouchableOpacity>
							</View>
						) : null}
						<Text style={styles.amountFiat}>{fiatBalance}</Text>
					</View>
				</ScrollView>
			</View>
		);
	}
}

const mapStateToProps = (state) => ({
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
	identities: state.engine.backgroundState.PreferencesController.identities,
	currentCurrency: state.engine.backgroundState.CurrencyRateController.currentCurrency,
	chainId: state.engine.backgroundState.NetworkController.provider.chainId,
	ticker: state.engine.backgroundState.NetworkController.provider.ticker,
	network: state.engine.backgroundState.NetworkController.network,
	swapsIsLive: swapsLivenessSelector(state),
	seedphraseBackedUp: state.user.seedphraseBackedUp,
});

const mapDispatchToProps = (dispatch) => ({
	showAlert: (config) => dispatch(showAlert(config)),
	toggleAccountsModal: () => dispatch(toggleAccountsModal()),
	protectWalletModalVisible: () => dispatch(protectWalletModalVisible()),
	newAssetTransaction: (selectedAsset) => dispatch(newAssetTransaction(selectedAsset)),
	toggleReceiveModal: (asset) => dispatch(toggleReceiveModal(asset)),
});

AccountOverview.contextType = ThemeContext;

export default connect(mapStateToProps, mapDispatchToProps)(AccountOverview);
