import React, { PureComponent } from 'react';
import {
	SafeAreaView,
	View,
	StyleSheet,
	Text,
	ScrollView,
	Dimensions,
	Image,
	TouchableOpacity,
	Linking,
	FlatList,
	TextInput,
} from 'react-native';
import { fontStyles, GLOBAL_ITEM_FONT_SIZE } from '../../../styles/common';
import { connect } from 'react-redux';
import DefaultTabBar from 'react-native-scrollable-tab-view/DefaultTabBar';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import PropTypes from 'prop-types';
import { strings } from '../../../../locales/i18n';
import { getNetworkNavbarOptions } from '../../UI/Navbar';
import { NetworksChainId } from '@metamask/controllers';
import CollectibleDetectionModal from '../../UI/CollectibleDetectionModal';
import { isMainNet } from '../../../util/networks';
import { ThemeContext, mockTheme } from '../../../util/theme';
import SearchTokenAutocomplete from '../SearchTokenAutocomplete';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { rgbaColor } from 'react-native-reanimated/src/reanimated2/Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import collectible from '../../Views/Collectible';
import Engine from '../../../core/Engine';
import CollectibleMedia from '../CollectibleMedia';
import { getEther } from '../../../util/transactions';
import Trait from './Trait';
import Share from 'react-native-share';
import Modal from 'react-native-modal';
import StyledButton from '../StyledButton/index';
import WalletModal from '../WalletModal';
import CameraRoll from '@react-native-community/cameraroll';
import NotificationManager from '../../../core/NotificationManager';
import Clipboard from '@react-native-clipboard/clipboard';
import { win32 } from 'path';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			// flex: 1,
			// minHeight: '90%',
			minHeight: '85%',
			backgroundColor: '#1B1B23',
			borderTopLeftRadius: 20,
			borderTopRightRadius: 20,
		},
		titleWrapper: {
			width: '100%',
			height: 33,
			alignItems: 'center',
			justifyContent: 'center',
			//borderBottomWidth: StyleSheet.hairlineWidth,
			//borderColor: colors.border.muted,
		},

		dragger: {
			width: 48,
			height: 5,
			borderRadius: 4,
			backgroundColor: colors.border.default,
			opacity: 0.6,
		},
		scrollView: {
			// marginHorizontal: 0,
			flex: 1
		},

		mainSection: {
			width: screenWidth - 24,
			height: (screenWidth * 3) / 2,
			marginHorizontal: 12,
			backgroundColor: '#282832',
			borderRadius: 12,
			borderColor: 'rgba(255,255,255,0.15)',
			borderWidth: 1,
			flexDirection: 'column',
			alignItems: 'center',
		},

		titleSection: {
			color: 'white',
			fontSize: 16,
			fontWeight: '600',
		},

		sectionStyle: {
			flexDirection: 'column',
			marginHorizontal: 20,
			marginTop: 20,
		},

		socialStyle: {
			flexDirection: 'row',
			marginHorizontal: 20,
			marginTop: 20,
		},

		infoSection: {
			color: '#A0A0B1',
			fontSize: 16,
			fontWeight: '400',
			marginTop: 8,
			lineHeight: 20
		},
		textStyle: {
			fontSize: 16,
			letterSpacing: 0.5,
			...fontStyles.bold,
		},

		roundButton1: {
			width: 91,
			height: 36,
			justifyContent: 'center',
			alignItems: 'center',
			flexDirection: 'row',
			// padding: 10,
			borderRadius: 15,
			backgroundColor: rgbaColor(255, 255, 255, 0.15),
		},
		roundButton1Disable: {
			width: 91,
			height: 34,
			justifyContent: 'center',
			alignItems: 'center',
			flexDirection: 'row',
			// padding: 10,
			borderRadius: 15,
			backgroundColor: rgbaColor(255, 255, 255, 0.15),
			opacity: 0.1,
		},
		roundButton2: {
			width: 34,
			height: 36,
			justifyContent: 'center',
			alignItems: 'center',
			// padding: 10,
			marginHorizontal: 5,
			borderRadius: 100,
			backgroundColor: rgbaColor(255, 255, 255, 0.15),
			opacity: 1,
		},
		iconImage: {
			height: 12,
			resizeMode: 'contain',
			marginTop: 8,
		},
		imagePreviewStyle: {
			marginTop: 12,
			width: screenWidth - 48,
			height: screenWidth - 48,
			borderRadius: 12,
		},
		arrowImage: {
			height: 16,
			resizeMode: 'contain',
			marginTop: 8,
			marginRight: 8,
		},
		collectibleContractPreview: {
			width: screenWidth - 48,
			height: screenWidth - 48,
		},
		valuePrice: {
			color: 'white',
			fontSize: 20,
			fontWeight: '700',
			marginTop: 5,
		},
		titlePrice: {
			color: '#A0A0B1',
			fontSize: 14,
			fontWeight: '500',
		},
		buttonSend: {
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
			width: 153,
			height: 44,
			flex: 1,
			marginHorizontal: 12,
		},
		titleBtnSend: {
			color: 'white',
			fontSize: 16,
			fontWeight: '500',
			marginHorizontal: 2,
		},
		imageBtnSend: {
			width: screenWidth / 2.5,
			// height: screenHeight / 17,
		},
		imageBtnOpenSea: {
			width: screenWidth / 2.5,
			// height: screenHeight/ 17,
		},
		wrapperModal: {
			flex: 1,
			paddingHorizontal: 16,
			backgroundColor: '#1B1B23',
			borderRadius: 12,
			borderWidth: 1,
			borderColor: 'rgba(255,255,255,0.15)',
			zIndex: 99
		},
		titleModal: {
			fontSize: 14,
			fontWeight: '500',
			color: 'white',
			marginVertical: 8,
		},
		buttonModal: {
			flexDirection: 'column',
			justifyContent: 'center',
			paddingVertical: 16
		},
		traitsContain: {
			flexDirection: 'row',
			flexWrap: 'wrap',
			marginTop: 5,
		},
		fontTextModal: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			...fontStyles.semiBold,
			lineHeight: 18,
			color: '#ffffff'
		},
		copyText: {
			fontSize: 12,
			fontWeight: '500',
			color: 'white',
			opacity: 0.6,
		},
		modalStyle: {
			width: '80%',
			flex: 1,
			marginLeft: 'auto',
			top: -26,
		},
		actionContainerStyle: {
			paddingHorizontal: 0,
			paddingVertical: 0
		},
		modalContent: {
			position: 'relative',
		},
		modalItem: {
			position: 'absolute', 
			bottom: 50, 
			width: 200, 
			right: 0
		},
		zoomInItem: {
			transform: [{scale: 0.95}]
		}
	});

class CollectiblesDetails extends PureComponent {
	state = {
		address: '',
		symbol: '',
		decimals: '',
		collectible: null,
		modalVisible: false,
		isPressOpenSea: false,
		isPressSend: false,
	};

	static propTypes = {
		/**
		/* navigation object required to push new views
		 */
		navigation: PropTypes.object,
		/**
		 * Chain id
		 */
		chainId: PropTypes.string,
		/**
		 * Chain Name
		 */
		chainName: PropTypes.string,
		/**
		 * Object that represents the current route info like params passed to it
		 */
		route: PropTypes.object,
		/**
		 * Boolean to show if NFT detection is enabled
		 */
		useCollectibleDetection: PropTypes.bool,
	};

	copyToClipboard = () => {
		const tokenID = this.state.collectible.token_id;
		Clipboard.setString(tokenID);
		this.setState({ modalVisible: false });
		NotificationManager.showSimpleNotification({
			status: 'success',
			duration: 5000,
			title: 'Copied',
			description: 'Token ' + tokenID + ' has been copied to clipboard',
		});
	};

	gotoEtherscan = () => {

		// test add collectible
		// const col = this.props.route.params?.collectible;
		// const tokenID = col.token_id;
		// const { CollectiblesController } = Engine.context;
		// CollectiblesController.addCollectible('0x0A1dB2a73647d5242FAce0D70365ee9d34A4f725', tokenID);

		this.goToBrowserUrl('https://etherscan.io/address/' + this.state.collectible.token_address);
	};

	fetchCopiedText = async () => {
		const text = await Clipboard.getString();
		setCopiedText(text);
	};

	downloadImage = async (url) => {
		const collectible = this.state.collectible;
		if (collectible !== null) {
			const url = collectible.metadata?.image;
			try {
				const image = CameraRoll.save(url, 'photo');
				// console.log('da save ' + image);
				if (image) {
					this.setState({ modalVisible: false });
					NotificationManager.showSimpleNotification({
						status: 'success',
						duration: 5000,
						title: 'Image saved',
						description: 'Successfully saved image to your gallery.',
					});
				}
			} catch (e) {
				console.log('save anh error ' + e);
			}
		}
	};

	onPress(selectedItems) {
	}

	updateNavBar = () => {
		// const { route } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		// navigation.setOptions(
		// 	getNetworkNavbarOptions(
		// 		`add_asset.${route.params.assetType === 'token' ? 'title' : 'title_nft'}`,
		// 		true,
		// 		navigation,
		// 		colors
		// 	)
		// );
	};

	componentDidMount = () => {
		this.fetchColectiblePrice();
		this.updateNavBar();
	};

	componentDidUpdate = () => {
		this.updateNavBar();
	};

	fetchColectiblePrice = async () => {
		const { CollectibleDetectionController } = Engine.context;
		// const collectible = await CollectibleDetectionController.getNFTsDetail(tokenID, addressAsset);
		// const collectible = await CollectibleDetectionController.getNFTsDetail(
		// 	'7430896416987029961621409596952728493162983252170241538492573436136719908865',
		// 	'0x88b48f654c30e99bc2e4a1559b4dcf1ad93fa656'
		// );
		let collectible = { ...this.props.route};
		collectible.priceData = {
			price: null,
			lowestPrice: null,
		};
		this.setState({ collectible: { ...collectible } });
		const collectiblePrice = await CollectibleDetectionController.getMoralisCollectiblePrice(
			this.props.chainName,
			collectible.token_address
		);
		collectible.priceData = Object.assign({}, collectible.priceData, { price: collectiblePrice });
		this.setState({ collectible: { ...collectible } });
	};

	goToBrowserUrl(url) {
		if (!url) {
			return;
		}
		Linking.openURL(url).catch((err) => console.error('Error', err));
	}

	// onSend = async () => {
	// 	this.props.navigation.navigate('SendFlowView');
	// };

	share = async (customOptions = options) => {
		try {
			await Share.open(customOptions);
		} catch (err) {
			console.log(err);
		}
	};

	toggleWalletModal = () => this.setState((state) => ({ modalVisible: !state.modalVisible }));

	render = () => {

		const {
			// route: {
			// 	params: { assetType, collectibleContract },
			// },
			// navigation,
			chainId,
			useCollectibleDetection,
		} = this.props;

		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		const collectible = this.state.collectible;
		if(!collectible) {
			return <></>;
		}

		let tokenID = '';
		let address = '';
		let name = '--';
		let contractName = '--';
		let price = '--';
		let description = '--';
		let descriptionContract = '--';
		let traits = [];
		let homepagelink = '';
		let scanplaceName = '';
		let scanplaceUrl = '';
		let marketplaceName = '';
		let marketplaceUrl = '';
		let discordLink = null;
		let fblink = null;
		let twlink = null;
		let telegramlink = null;

		const shareLink = {};

		if (!!collectible) {
			tokenID = collectible.token_id;
			address = collectible.token_address;
			name = collectible.metadata?.name;
			contractName = collectible.name || strings('collectible.untitled_collection');
			if (collectible.priceData.price) {
				price = collectible.priceData.price.nativePrice.value + ' ' + collectible.priceData.price.nativePrice.symbol;
			}
			description = collectible.metadata?.description;
			descriptionContract = description || 'No information';
			switch (this.props.chainName) {
				case 'eth':
				case 'ropsten':
				case 'kovan':
				case 'rinkeby':
				case 'goerli':
					marketplaceName = 'OpenSea';
					marketplaceUrl = `https://opensea.io/assets/ethereum/${collectible.token_address}/${collectible.token_id}`;
					scanplaceName = `${strings('collectibles.watch_on')} Etherscan`;
					scanplaceUrl = `https://etherscan.io/address/${collectible.token_address}`;
					break;
				case 'BSC':
					marketplaceName = 'Galler';
					scanplaceName = `${strings('collectibles.watch_on')} bscscan`;
					marketplaceUrl = '';
					scanplaceUrl = `https://bscscan.com/address/${collectible.token_address}`;
					break;
				default:
					break;
			}
			homepagelink = collectible.metadata?.external_url;
			discordLink = collectible.metadata?.discord_url;
			fblink = collectible.metadata?.instagram_username;
			twlink = collectible.metadata?.twitter_username;
			telegramlink = collectible.metadata?.telegram_url;
			traits = collectible.metadata?.attributes || [];
			// shareLink.url = homepagelink;
			// shareLink.title = name;
			// shareLink.message = description || '';
			shareLink.message = marketplaceUrl;
		}
		return (
			<SafeAreaView style={styles.wrapper} testID={`collectible-details`}>
				<View style={styles.titleWrapper}>
					<View style={styles.dragger} testID={'addasset-dragger'} />
				</View>
				<View style={styles.wrapper}>
					<ScrollView style={styles.scrollView}>
						<View style={styles.mainSection}>
							{collectible !== null ? (
								<CollectibleMedia
									iconStyle={styles.collectibleContractPreview}
									collectible={{
										name: strings('collectible.untitled_collection'),
										...collectible,
										image: collectible.metadata?.image,
									}}
									big
								/>
							) : (
								<Image
									source={require('../../../images/sampleNFT.png')}
									style={styles.imagePreviewStyle}
									resizeMethod={'auto'}
								/>
							)}

							<View
								style={{
									flexDirection: 'row',
									marginHorizontal: 12,
									marginTop: 16,
								}}
							>
								<View
									style={{
										flex: 1,
										flexDirection: 'column',
									}}
								>
									<Text
										numberOfLines={1}
										style={{
											color: 'white',
											fontSize: 20,
											fontWeight: '700',
										}}
									>
										{/* {`${name || `${contractName} #${tokenID}`}`} */}
										{`${name || `${contractName}`}`}

									</Text>
									<View
										style={{
											flexDirection: 'row',
										}}
									>
										<Text
											style={{
												color: '#A0A0B1',
												fontSize: 14,
												fontWeight: '500',
												marginEnd: 5,
												marginTop: 5,
											}}
										>
											{contractName}
										</Text>
										<Image
											source={require('../../../images/blue-check.png')}
											style={styles.iconImage}
										/>
										<Image
											source={require('../../../images/arrow-right.png')}
											style={styles.iconImage}
										/>
									</View>
								</View>
								<View
									style={{
										flex: 1,
										flexDirection: 'row',
										alignItems: 'center',
										justifyContent: 'flex-end',
										alignSelf: 'center',
									}}
								>
									{/* <TouchableOpacity onPress={() => { }} style={styles.roundButton2}>
										<FontAwesome name={'heart'} size={20} color={'white'} />
									</TouchableOpacity> */}

									<TouchableOpacity
										onPress={async () => {
											await this.share(shareLink);
										}}
										style={styles.roundButton2}
									>
										<FontAwesome name={'share'} size={20} color={'white'} />
									</TouchableOpacity>


									{/* Show modal content */}
									<View style={styles.modalContent}>

										<View style={this.state.modalVisible ? [styles.wrapperModal, styles.modalItem] : [styles.wrapperModal, styles.modalItem, {display: 'none'}]}>
											<TouchableOpacity style={styles.buttonModal} onPress={this.copyToClipboard}>
												<Text style={styles.fontTextModal}>{strings('collectibles.copy_token_id')}</Text>
												<Text style={[styles.copyText, {marginTop: 4}]} >{tokenID}</Text>
											</TouchableOpacity>
											<View
												style={{
													height: 1,
													paddingHorizontal: 12,
													backgroundColor: 'rgba(255,255,255,0.15)',
												}}
											/>
											<TouchableOpacity style={styles.buttonModal} onPress={this.downloadImage}>
												<Text style={styles.fontTextModal}>{strings('collectibles.save_picture')}</Text>
											</TouchableOpacity>
											<View
												style={{
													height: 1,
													paddingHorizontal: 12,
													backgroundColor: 'rgba(255,255,255,0.15)',
												}}
											/>
											<TouchableOpacity style={styles.buttonModal} onPress={() => this.goToBrowserUrl(scanplaceUrl)}>
												<Text style={styles.fontTextModal}>{scanplaceName}</Text>
											</TouchableOpacity>
										</View>

										<TouchableOpacity onPress={this.toggleWalletModal} style={styles.roundButton2}>
											<FontAwesome name={'ellipsis-h'} size={20} color={'white'} />
										</TouchableOpacity>
									</View>

									{/*<OptionsMenu*/}
									{/*	customButton={(<FontAwesome name={'ellipsis-h'} size={20} color={'white'} />)}*/}
									{/*	// destructiveIndex={1}*/}
									{/*	options={["Sao chép ID Token", "Lưu Ảnh", "Xem trên Etherscan" ,"Cancel"]}*/}
									{/*	actions={[this.onSend, this.onSend]}/>*/}
								</View>
							</View>
							<View
								style={{
									width: screenWidth - 48,
									height: 1,
									backgroundColor: '#3D3D4B',
									marginTop: 16,
								}}
							/>
							{/*Gia*/}
							<View
								style={{
									flexDirection: 'row',
									marginHorizontal: 12,
									marginTop: 16,
								}}
							>
								<View
									style={{
										flex: 1,
										flexDirection: 'column',
									}}
								>
									<Text style={styles.titlePrice}>{strings('collectibles.nearest_price')}</Text>
									<Text style={styles.valuePrice}>{price}</Text>
								</View>
								<View
									style={{
										flex: 1,
										flexDirection: 'column',
										alignItems: 'flex-end',
									}}
								>
									<Text style={styles.titlePrice}>{strings('collectibles.floor_price')}</Text>
									<Text style={styles.valuePrice}>{price}</Text>
								</View>
							</View>

							{/*Button Send*/}
							<View
								style={{
									flex: 1,
									flexDirection: 'row',
									marginTop: 20,
									alignItems: 'center',
									justifyContent: 'center'
								}}
							>
								<TouchableOpacity
									onPress={() => this.goToBrowserUrl(marketplaceUrl)}
									style={[{
										marginTop: - screenHeight / 50},
										this.state.isPressOpenSea && styles.zoomInItem
									]}
									onPressIn={() => this.setState({isPressOpenSea: true})}
									onPressOut={() => this.setState({isPressOpenSea: false})}
									activeOpacity={1}
								>
									{/* <Text style={styles.titleBtnSend}>{marketplaceName}</Text> */}
									<Image
										source={require('../../../images-new/openSea.png')}
										style={styles.imageBtnOpenSea}
										resizeMode={'contain'}
									/>
								</TouchableOpacity>

								<TouchableOpacity onPress={this.onSend}
									style={[{
										marginTop: - screenHeight / 50,
										marginLeft: screenWidth / 30
										},
										this.state.isPressSend && styles.zoomInItem
									]}
									onPressIn={() => this.setState({isPressSend: true})}
									onPressOut={() => this.setState({isPressSend: false})}
									activeOpacity={1}
								>
									<Image
										source={require('../../../images-new/send.png')}
										style={styles.imageBtnSend}
										resizeMode={'contain'}
									/>
								</TouchableOpacity>
							</View>
						</View>

						{/*Mo ta*/}
						<View style={styles.sectionStyle}>
							<Text style={styles.titleSection}>{strings('collectibles.description')}</Text>
							<Text style={styles.infoSection}>{description}</Text>
						</View>

						{/*Thuoc tinh*/}
						{traits.length > 0 && (
							<View style={styles.sectionStyle}>
								<Text style={styles.titleSection}>{strings('wallet.nft_attributes')}</Text>
								<View style={styles.traitsContain}>
									{
										traits.map((item, index) => {
											return <Trait key={`iii${index}`} trait={item} contractName={contractName} index={index}/>
										})
									}
								</View>
								{/* <FlatList
									style={{
										marginVertical: 12,
									}}
									numColumns={3}
									data={traits}
									keyExtractor={(item, index) => index.toString()}
									renderItem={({ item }) => <Trait trait={item} contractName={contractName} />}
								/> */}
							</View>
						)}

						{/*Thong tin ve*/}
						{/*<View style={styles.sectionStyle}>*/}
						{/*	<Text style={styles.titleSection}>Thông tin về {contractName}</Text>*/}
						{/*	<Text style={styles.infoSection}>{descriptionContract}</Text>*/}
						{/*</View>*/}

						{/*Button social*/}
						<View style={styles.socialStyle}>
							{!!homepagelink && (
								<TouchableOpacity
									onPress={() => this.goToBrowserUrl(homepagelink)}
									style={[styles.roundButton1, {marginRight: screenWidth / 70}]}
								>
									<Image source={require('../../../images-new/website.png')} style={{width: 14, height: 14}}></Image>
									<Text
										style={{
											fontSize: 14,
											fontWeight: '500',
											color: 'white',
											paddingLeft: 5,
										}}
									>
										{strings('collectibles.website')}
									</Text> 
								</TouchableOpacity>
							)}

							{!!discordLink && (
								<TouchableOpacity
									onPress={() => this.goToBrowserUrl(homepagelink)}
									style={styles.roundButton2}
								>
									<Image source={require('../../../images-new/discord.png')} style={{width: 20, height: 15}} resizeMode={'cover'}></Image>
								</TouchableOpacity>
							)}

							{!!twlink && (
								<TouchableOpacity
									onPress={() =>
										this.goToBrowserUrl(
											'https://twitter.com/' + this.state.details.links.twitter_screen_name
										)
									}
									style={styles.roundButton2}
								>
									<Image source={require('../../../images-new/twitter.png')} style={{width: 20, height: 16}} resizeMode={'cover'}></Image>
								</TouchableOpacity>
							)}

							{/* {!!fblink && (
								<TouchableOpacity
									onPress={() =>
										this.goToBrowserUrl(
											'https://www.facebook.com/' + this.state.details.links.facebook_username
										)
									}
									style={styles.roundButton2}
								>
																	<Image source={require('../../../images-new/twitter.png')} style={{width: 20, height: 16.49}} resizeMode={'cover'}></Image>
								</TouchableOpacity>
							)} */}

							{!!telegramlink && (
								<TouchableOpacity
									onPress={() =>
										this.goToBrowserUrl(
											'https://t.me/' + this.state.details.links.telegram_channel_identifier
										)
									}
									style={styles.roundButton2}
								>
									{/* <FontAwesome name={'telegram'} size={20} color={'white'} /> */}
									<Image source={require('../../../images-new/tele.png')} style={{width: 20, height: 16.49}} resizeMode={'cover'}></Image>
								</TouchableOpacity>
							)}
						</View>
					</ScrollView>
				</View>

				{/* <WalletModal
					warningModalVisible={this.state.modalVisible}
					// warningModalVisible={true}
					onCancelPress={this.onSend}
					onRequestClose={this.toggleWalletModal}
					onConfirmPress={this.toggleWalletModal}
					cancelButtonDisabled={false}
					confirmText={strings('accounts.create_new_wallet')}
					cancelText={strings('accounts.cancel')}
					displayConfirmButton={false}
					displayCancelButton={false}
					modalStyle={styles.modalStyle}
					actionContainerStyle={styles.actionContainerStyle}
				>
					<View style={styles.wrapperModal}>
						<TouchableOpacity style={styles.buttonModal} onPress={this.copyToClipboard}>
							<Text style={styles.fontTextModal}>{strings('collectibles.copy_token_id')}</Text>
							<Text style={[styles.copyText, {marginTop: 4}]} >{tokenID}</Text>
						</TouchableOpacity>
						<View
							style={{
								height: 1,
								paddingHorizontal: 12,
								backgroundColor: 'rgba(255,255,255,0.15)',
							}}
						/>

						<TouchableOpacity style={styles.buttonModal} onPress={this.downloadImage}>
							<Text style={styles.fontTextModal}>{strings('collectibles.save_picture')}</Text>
						</TouchableOpacity>
						<View
							style={{
								height: 1,
								paddingHorizontal: 12,
								backgroundColor: 'rgba(255,255,255,0.15)',
							}}
						/>

						<TouchableOpacity style={styles.buttonModal} onPress={() => this.goToBrowserUrl(scanplaceUrl)}>
							<Text style={styles.fontTextModal}>{scanplaceName}</Text>
						</TouchableOpacity>
					</View>
				</WalletModal> */}
			</SafeAreaView>
		);
	};
}

CollectiblesDetails.contextType = ThemeContext;

const mapStateToProps = (state) => {
	let chainName = '';
	switch (state.engine.backgroundState.NetworkController.provider.type) {
		case 'mainnet':
			chainName = 'eth';
			break;
		case 'ropsten':
		case 'kovan':
		case 'rinkeby':
		case 'goerli':
			chainName = state.engine.backgroundState.NetworkController.provider.type;
			break;
		case 'rpc':
			if (!state.engine.backgroundState.NetworkController.provider.nickname) {
				chainName = '';
			} else {
				switch (state.engine.backgroundState.NetworkController.provider.chainId) {
					case '56':
						chainName = 'bsc';
					case '137':
						chainName = 'polygon';
					default:
						chainName = '';
				}
			}
		default:
			break;
	}
	return {
		chainId: state.engine.backgroundState.NetworkController.provider.chainId,
		chainName,
		useCollectibleDetection: state.engine.backgroundState.PreferencesController.useCollectibleDetection,
		detailTokenModalVisible: state.modals.detailTokenModalVisible,
		// route: state.modals.detailTokenModalVisible,
		
	};
};

export default connect(mapStateToProps)(CollectiblesDetails);
