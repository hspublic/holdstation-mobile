import React from 'react';
import { Linking, Text, TouchableOpacity, View } from "react-native";
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { toDateOnlyFormat } from '../../../util/date';
import { strings } from '../../../../locales/i18n';


class Trait extends React.Component {
	state = {
		visible: false,
	};
	render() {
		let { trait_type, value } = this.props.trait;
		if (value && typeof value === 'number' && value.toString().length === 13 && new Date(value).getTime() > 0) {
			value = toDateOnlyFormat(value);
		}
		const { contractName } = this.props;
		return (
			<View
				style={{
					flexDirection: 'column',
					backgroundColor: '#282832',
					borderRadius: 11,
					borderColor: '#3D3D4B',
					borderWidth: 1,
					marginRight: 10,
					marginVertical: 5,
					padding: 5
				}}
			>
				<TouchableOpacity
					onPress={() => {
						this.setState({ visible: true });
					}}
					disabled={true}
				>
					<Text
						style={{
							color: '#B1B5FF',
							fontSize: 12,
							fontWeight: '500',
							marginHorizontal: 5,
							marginVertical: 5,
						}}
					>
						{trait_type}
					</Text>
					<Text
						style={{
							color: 'white',
							fontSize: 16,
							fontWeight: '600',
							marginHorizontal: 5,
							marginBottom: 5,
						}}
					>
						{value}
					</Text>
				</TouchableOpacity>
				<Dialog
					visible={this.state.visible}
					onTouchOutside={() => {
						this.setState({ visible: false });
					}}
					width={300}
					height={70}
					contentStyle={{backgroundColor: 'red'}}
				>
					<DialogContent transparent={true} style={{
						justifyContent: 'center',
						alignItems: 'center',
						backgroundColor: '#1B1B23',
						borderColor: 'rgba(255,255,255,0.25)',
						borderRadius: 12,
						borderWidth: 1,
					}}>
						<TouchableOpacity
							style={{
							backgroundColor: '#1B1B23',
							flexDirection: 'row',
							width: 300,
							height: 70,
							justifyContent: 'center',
							alignItems: 'center',
						}}
							onPress={() => {
								const api = 'https://opensea.io/collection/';
								const url = `${api}${contractName}?search[stringTraits][0][name]=${trait_type}&search[stringTraits][0][values][0]=${value}`;
								Linking.openURL(url).catch((err) => console.error('Error', err));
								this.setState({ visible: false });
							}}
						>
							<MaterialCommunityIcons name={"link-box-variant"} size={20} color={"white"} />
							<Text style={{
								color: 'white',
								fontSize: 14,
								fontWeight: '500',
								marginHorizontal: 12
							}}>{strings('collectible.view_all_properties')}</Text>
						</TouchableOpacity>
					</DialogContent>
				</Dialog>
			</View>
		);
	}
}

export default Trait;
