import PropTypes from 'prop-types';
import {
	Text,
	Button,
	View,
	StyleSheet,
	TouchableOpacity,
	TouchableWithoutFeedback,
	TextInput,
	InteractionManager,
	Image,
	TouchableNativeFeedback,
} from 'react-native';
import React, { PureComponent } from 'react';
import { strings } from '../../../../locales/i18n';
import { fontStyles } from '../../../styles/common';
import { useAppThemeFromContext, mockTheme, ThemeContext } from '../../../util/theme';
//import Button from '../../UI/Button';
import StyledButton from '../../UI/StyledButton';
import Analytics from '../../../core/Analytics';
import { ANALYTICS_EVENT_OPTS } from '../../../util/analytics';
import { connect } from 'react-redux';

const createStyles = (colors) =>
	StyleSheet.create({
		hintWrapper: {
			boxSizing: 'border-box',
			display: 'flex',
			flexDirection: 'column',
			//justifyContent: 'center',
			alignItems: 'center',
			padding: 20,
			gap: 20,
			background: 'rgba(27, 27, 35, 1)',
			borderColor: '#FFFFFF1A',
			border: '1px solid rgba(255, 255, 255, 0.15)',
			boxShadow: '0px 4px 39px rgba(0, 0, 0, 0.4)',
			borderWidth: 1,
			borderRadius: 12,
			minHeight: 187,
			minWidth: 335,
			margin: 20,
		},
		hintHeader: {
			fontStyle: 'normal',
			fontSize: 14,
			lineHeight: 18,
			textAlign: 'center',
			color: '#FFFFFF',
			width: 200,
			height: 36,
			flexGrow: 0,
			margin: 10,
		},
		btnView: {
			display: 'flex',
			flexDirection: 'row',
			alignItems: 'flex-start',
			padding: 8,
			width: 118,
			height: 32,
			background: 'linear-gradient(135deg, #58FF55 0%, #009FC1 53.65%, #007CD6 100%)',
			borderRadius: 12,
			flexGrow: 0,
		},
		btnIcon: {
			width: 15,
			height: 15,
		},
		buttonText: {
			color: '#FFFFFF',
		},
		addButton: {
			fontSize: 18,
			textAlign: 'center',
			...fontStyles.normal,
		},
	});

class NoticeDeposit extends PureComponent {
	static propTypes = {
		navigation: PropTypes.object,
	};

	goToAddToken = () => {
		// this.setState({ isAddTokenEnabled: false });
		this.props.navigation.push('AddAsset', { assetType: 'token' });
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_ADD_TOKENS);
			this.setState({ isAddTokenEnabled: true });
		});
	};

	render = () => {
		const colors = this.context.colors || mockTheme.colors;
		const themeAppearance = this.context.themeAppearance;
		const styles = createStyles(colors);

		return (
			<View style={styles.hintWrapper}>
				<Text style={styles.hintHeader}>{strings('wallet.no_asset_warning')}</Text>
				<StyledButton type={'blue'} onPress={this.goToAddToken} testID={'add-contact-button'}>
					<Image
						style={styles.btnIcon}
						source={require('../../../images/icon-dolla.png')}
						resizeMethod={'auto'}
					/>
					<Text style={styles.buttonText}>{'  ' + strings('wallet.receive_tokens')}</Text>
				</StyledButton>

				<StyledButton type={'inverse-transparent'} onPress={this.goToAddToken} testID={'add-contact-button'}>
					<Image
						style={styles.btnIcon}
						source={require('../../../images/add-circle.png')}
						resizeMethod={'auto'}
					/>
					<Text style={styles.buttonText}>{'  ' + strings('wallet.add_tokens')}</Text>
				</StyledButton>
			</View>
		);
	};
}
//
// const propTypes = {
// 	onConfirm: PropTypes.func.isRequired,
// };
// const defaultProps = {
// 	modalVisible: false,
// };
//
// NoticeDeposit.propTypes = propTypes;
// NoticeDeposit.defaultProps = defaultProps;
//
// export default NoticeDeposit;

NoticeDeposit.contextType = ThemeContext;

export default NoticeDeposit;
