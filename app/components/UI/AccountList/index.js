import React, { PureComponent } from 'react';
import { KeyringTypes } from '@metamask/controllers';
import Engine from '../../../core/Engine';
import PropTypes from 'prop-types';
import {
	Alert,
	ActivityIndicator,
	InteractionManager,
	FlatList,
	TouchableOpacity,
	StyleSheet,
	Text,
	TextInput,
	Image,
	View,
	SafeAreaView,
	SectionList, Dimensions

} from "react-native";
import { fontStyles } from '../../../styles/common';
import Device from '../../../util/device';
import { strings } from '../../../../locales/i18n';
import { toChecksumAddress } from 'ethereumjs-util';
import Logger from '../../../util/Logger';
import Analytics from '../../../core/Analytics';
import AnalyticsV2 from '../../../util/analyticsV2';
import { ANALYTICS_EVENT_OPTS } from '../../../util/analytics';
import { doENSReverseLookup } from '../../../util/ENSUtils';
import AccountElement from './AccountElement';
import { connect } from 'react-redux';
import { ThemeContext, mockTheme } from '../../../util/theme';
import WalletModal from '../../UI/WalletModal';
import { useNavigation } from '@react-navigation/native';
import { toggleImportFromSeedModalVisible } from '../../../actions/modals';
import ImportFromSeed from '../../../components/Views/ImportFromSeed';


import Modal from 'react-native-modal';
import {
	BIOMETRY_CHOICE_DISABLED,
	ONBOARDING_WIZARD,
	ENCRYPTION_LIB,
	TRUE,
	ORIGINAL,
	EXISTING_USER,
} from '../../../constants/storage';
import AsyncStorage from '@react-native-community/async-storage';
import StyledButton from '../../UI/StyledButton';
import NotificationManager from '../../../core/NotificationManager';

const win = Dimensions.get('window');

const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			backgroundColor: '#1B1B23',
			borderTopLeftRadius: 10,
			borderTopRightRadius: 10,
			minHeight: '90%',
		},
		title: {
			fontSize: 15,
			color: '#FFFFFF',
			justifyContent: 'center',
			textAlign: 'center',
		},
		titleWrapper: {
			width: '100%',
			height: 33,
			alignItems: 'center',
			justifyContent: 'center',
			//borderBottomWidth: StyleSheet.hairlineWidth,
			//borderColor: colors.border.muted,
		},
		wrapperModal: {
			flex: 1,
			paddingHorizontal: 32,
		},
		titleModal: {
			width: '100%',
			height: 33,
			alignItems: 'center',
			justifyContent: 'center',
			borderBottomWidth: StyleSheet.hairlineWidth,
			borderColor: colors.border.muted,
		},
		input: {
			...fontStyles.normal,
			fontSize: 12,
			paddingTop: 2,
			color: 'white',
			borderRadius: 10,
			borderWidth: 1,
			borderColor: colors.border.default,
			textAlign: 'center',
			minHeight: 40,
		},
		container: {
			justifyContent: 'center',
			alignItems: 'center',
			marginTop: 20,
			marginBottom: 20,
		},
		mainWrapper: {
			backgroundColor: '#1B1B23',
			flex: 1,
		},
		wrapperWelcome: {
			flex: 1,
			paddingHorizontal: 32,
		},
		containerWelcome: {
			justifyContent: 'center',
			alignItems: 'center',
		},
		likeIcon: {
			marginTop: 250,
			marginBottom: 30,
		},
		ctaWrapper: {
			marginTop: 250,
			marginBottom: 30,
		},
		walletTitle: {
			fontSize: 15,
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
			color: 'white',
		},
		errorPhrase: {
			fontSize: 12,
			borderRadius: 10,
			...fontStyles.normal,
			paddingTop: 10,
			justifyContent: 'center',
			alignItems: 'center',
		},
		errorLabel: {
			color: 'red',
		},
		fieldRow: {
			flexDirection: 'row',
			alignItems: 'flex-end',
			justifyContent: 'center',
			textAlign: 'center',
		},
		dragger: {
			width: 48,
			height: 5,
			borderRadius: 4,
			backgroundColor: colors.border.default,
			opacity: Device.isAndroid() ? 0.6 : 0.5,
		},
		accountsWrapper: {
			flex: 1,
		},
		footer: {
			height: Device.isIphoneX() ? 150 : 130,
			paddingBottom: Device.isIphoneX() ? 30 : 0,
			justifyContent: 'flex-start',
			flexDirection: 'column',
			alignItems: 'flex-start',
		},
		btnText: {
			fontSize: 14,
			color: '#FFF',
			lineHeight: 17,
			...fontStyles.semiBold,
			marginLeft: 5,
			marginTop: win.height / 250
		},
		footerButton: {
			width: '100%',
			marginLeft: win.width / 17,
			padding: 10,
			height: 55,
			alignItems: 'flex-start',
			justifyContent: 'flex-start',
			//borderTopWidth: StyleSheet.hairlineWidth,
			borderColor: colors.border.muted,
		},
		addTokenImg: {
			height: 20,
			width: 20,
			flexDirection: 'row',
			marginTop: 2
		},
		iconWrapper: {
			flexDirection: 'row',
			justifyContent: 'flex-start',
			marginLeft: - win.width / 30
		},
		textWallet: {
			color: '#FFFFFF', 
			fontSize: 16, 
			...fontStyles.bold,
			lineHeight: 19,
		},
		wrapperTextWallet: {
			justifyContent:'center',
			alignItems: 'center'
		},
		zoominItem: {
			transform:[{scale: 0.95}]
		},
		bottomModal: {
			justifyContent: 'flex-end',
			margin: 0,
		}
	});

/**
 * View that contains the list of all the available accounts
 */
class AccountList extends PureComponent {
	static propTypes = {
		/**
		/* navigation object required to push new views
		*/
		navigation: PropTypes.object,
		/**
		 * Map of accounts to information objects including balances
		 */
		accounts: PropTypes.object,
		/**
		 * An object containing each identity in the format address => account
		 */
		identities: PropTypes.object,
		/**
		 * A string representing the selected address => account
		 */
		selectedAddress: PropTypes.string,
		/**
		 * An object containing all the keyrings
		 */
		keyrings: PropTypes.array,
		/**
		 * function to be called when switching accounts
		 */
		onAccountChange: PropTypes.func,
		/**
		 * function to be called when importing an account
		 */
		onImportAccount: PropTypes.func,
		/**
		 * function to be called when connect to a QR hardware
		 */
		onConnectHardware: PropTypes.func,
		/**
		 * Current provider ticker
		 */
		ticker: PropTypes.string,
		/**
		 * Whether it will show options to create or import accounts
		 */
		enableAccountsAddition: PropTypes.bool,
		/**
		 * function to generate an error string based on a passed balance
		 */
		getBalanceError: PropTypes.func,
		/**
		 * Indicates whether third party API mode is enabled
		 */
		thirdPartyApiMode: PropTypes.bool,
		/**
		 * ID of the current network
		 */
		network: PropTypes.string,
		toggleImportFromSeedModalVisible: PropTypes.func
	};

	state = {
		selectedAccountIndex: 0,
		loading: false,
		error: null,
		orderedAccounts: {},
		accountsENS: {},
		modalVisible: false,
		showWelcomeScreen: false,
		isPressAddWallet: false,
		isPressImportWallet: false,
	};

	flatList = React.createRef();
	lastPosition = 0;
	updating = false;

	getInitialSelectedAccountIndex = () => {
		const { identities, selectedAddress } = this.props;
		Object.keys(identities).forEach((address, i) => {
			if (selectedAddress === address) {
				this.mounted && this.setState({ selectedAccountIndex: i });
			}
		});
	};

	componentDidMount() {
		this.mounted = true;
		this.getInitialSelectedAccountIndex();
		const orderedAccounts = this.getAccounts();
		InteractionManager.runAfterInteractions(() => {
			this.assignENSToAccounts(orderedAccounts);
			if (orderedAccounts.length > 4) {
				this.scrollToCurrentAccount();
			}
		});
		this.mounted && this.setState({ orderedAccounts });
	}

	componentWillUnmount = () => {
		this.mounted = false;
	};

	scrollToCurrentAccount() {
		// eslint-disable-next-line no-unused-expressions
		this.flatList?.current?.scrollToIndex({ index: this.state.selectedAccountIndex, animated: true });
	}

	onAccountChange = async (newIndex) => {
		const previousIndex = this.state.selectedAccountIndex;
		const { PreferencesController } = Engine.context;
		const { keyrings, accounts } = this.props;

		requestAnimationFrame(async () => {
			try {
				this.mounted && this.setState({ selectedAccountIndex: newIndex });

				const allKeyrings =
					keyrings && keyrings.length ? keyrings : Engine.context.KeyringController.state.keyrings;
				const accountsOrdered = allKeyrings.reduce((list, keyring) => list.concat(keyring.accounts), []);

				// If not enabled is used from address book so we don't change accounts
				if (!this.props.enableAccountsAddition) {
					this.props.onAccountChange(accountsOrdered[newIndex]);
					const orderedAccounts = this.getAccounts();
					this.mounted && this.setState({ orderedAccounts });
					return;
				}

				PreferencesController.setSelectedAddress(accountsOrdered[newIndex]);

				this.props.onAccountChange();

				this.props.thirdPartyApiMode &&
					InteractionManager.runAfterInteractions(async () => {
						setTimeout(() => {
							Engine.refreshTransactionHistory();
						}, 1000);
					});
			} catch (e) {
				// Restore to the previous index in case anything goes wrong
				this.mounted && this.setState({ selectedAccountIndex: previousIndex });
				Logger.error(e, 'error while trying change the selected account'); // eslint-disable-line
			}
			InteractionManager.runAfterInteractions(() => {
				setTimeout(() => {
					// Track Event: "Switched Account"
					AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.SWITCHED_ACCOUNT, {
						number_of_accounts: Object.keys(accounts ?? {}).length,
					});
				}, 1000);
			});
			const orderedAccounts = this.getAccounts();
			this.mounted && this.setState({ orderedAccounts });
		});
	};

	importAccount = () => {
		this.props.onImportAccount();
		// this.props.toggleImportFromSeedModalVisible();
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.ACCOUNTS_IMPORTED_NEW_ACCOUNT);
		});
		// this.props.navigation.navigate('Welcome', { type: 'import' });
	};

	connectHardware = () => {
		this.props.onConnectHardware();
		AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.CONNECT_HARDWARE_WALLET);
	};

	addAccount = async () => {
		const walletName = this.state.wallet;
		if (walletName != null && walletName.length > 18) {
			this.setState({ error: strings('accounts.input_max_length') });
			return;
		}

		if (this.state.loading) return;
		this.mounted && this.setState({ loading: true });
		const { KeyringController } = Engine.context;
		requestAnimationFrame(async () => {
			try {
				await KeyringController.addNewAccountWithName(walletName);
				const { PreferencesController } = Engine.context;
				const newIndex = Object.keys(this.props.identities).length - 1;
				const selected = Object.keys(this.props.identities)[newIndex];
				// PreferencesController.setSelectedAddress(selected);
				this.mounted && this.setState({ selectedAccountIndex: newIndex });
				setTimeout(() => {
					this.flatList && this.flatList.current && this.flatList.current.scrollToEnd();
					this.mounted && this.setState({ loading: false });
				}, 500);
				const orderedAccounts = this.getAccounts();
				this.mounted && this.setState({ orderedAccounts });
				this.toggleWalletModal();
				this.props.navigation.reset({ routes: [{ name: 'Welcome' }] });
				// this.setState({ showWelcomeScreen: true });
			} catch (e) {
				// Restore to the previous index in case anything goes wrong
				Logger.error(e, 'error while trying to add a new account'); // eslint-disable-line
				this.mounted && this.setState({ loading: false });
			}
		});
		
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.ACCOUNTS_ADDED_NEW_ACCOUNT);
		});
	};

	isImported(allKeyrings, address) {
		let ret = false;
		for (const keyring of allKeyrings) {
			if (keyring.accounts.includes(address)) {
				ret = keyring.type === KeyringTypes.simple;
				break;
			}
		}
		return ret;
	}

	isQRHardware(allKeyrings, address) {
		let ret = false;
		for (const keyring of allKeyrings) {
			if (keyring.accounts.includes(address)) {
				ret = keyring.type === KeyringTypes.qr;
				break;
			}
		}

		return ret;
	}

	onLongPress = async (address, imported, index) => {
		const orderedAccounts = this.getAccounts();
		const { KeyringController } = Engine.context;
		if (orderedAccounts.length > 1) {
			await Engine.context.KeyringController.removeAccount(address);
			this.props.onAccountChange();

			NotificationManager.showSimpleNotification({
				status: 'success',
				duration: 5000,
				title: 'Notification',
				description: strings('accounts.remove_success'),
			});
		} else {
			try {
				await Engine.resetState();
				await KeyringController.createNewVaultAndKeychain(`${Date.now()}`);
				await KeyringController.setLocked();
				this.deleteExistingUser();
			} catch (error) {
				Logger.log(error, `Failed to createNewVaultAndKeychain: ${error}`);
			}
		}
	};

	deleteExistingUser = async () => {
		try {
			await AsyncStorage.removeItem(EXISTING_USER);
			// We need to reset instead of navigate here otherwise, OnboardingRootNav remembers the last screen that it was on, which is most likely not OnboardingNav.
			this.props.navigation?.reset({
				routes: [
					{
						name: 'OnboardingRootNav',
						state: {
							routes: [
								{ name: 'OnboardingNav', params: { screen: 'Onboarding', params: { delete: true } } },
							],
						},
					},
				],
			});
		} catch (error) {
			Logger.log(error, `Failed to remove key: ${EXISTING_USER} from AsyncStorage`);
		}
	};

	onStartButton = () => {
		this.setState({ showWelcomeScreen: false });
		this.toggleWalletModal();
		this.props.onAccountChange();
	};

	renderItem = ({ item, index }) => {
		const { ticker } = this.props;
		const { accountsENS } = this.state;
		return (
			<AccountElement
				onPress={this.onAccountChange}
				onLongPress={this.onLongPress}
				item={{ ...item, ens: accountsENS[item.address] }}
				ticker={ticker}
				disabled={Boolean(item.balanceError)}
				index={index}
			/>
		);
	};

	getAccounts() {
		const { accounts, identities, selectedAddress, keyrings, getBalanceError } = this.props;
		// This is a temporary fix until we can read the state from @metamask/controllers
		const allKeyrings = keyrings && keyrings.length ? keyrings : Engine.context.KeyringController.state.keyrings;

		const accountsOrdered = allKeyrings.reduce((list, keyring) => list.concat(keyring.accounts), []);
		return accountsOrdered
			.filter((address) => !!identities[toChecksumAddress(address)])
			.map((addr, index) => {
				const checksummedAddress = toChecksumAddress(addr);
				const identity = identities[checksummedAddress];
				const { name, address } = identity;
				const identityAddressChecksummed = toChecksumAddress(address);
				const isSelected = identityAddressChecksummed === selectedAddress;
				const isImported = this.isImported(allKeyrings, identityAddressChecksummed);
				const isQRHardware = this.isQRHardware(allKeyrings, identityAddressChecksummed);
				let balance = 0x0;
				if (accounts[identityAddressChecksummed]) {
					balance = accounts[identityAddressChecksummed].balance;
				}

				const balanceError = getBalanceError ? getBalanceError(balance) : null;
				return {
					index,
					name,
					address: identityAddressChecksummed,
					balance,
					isSelected,
					isImported,
					isQRHardware,
					balanceError,
				};
			});
	}

	assignENSToAccounts = (orderedAccounts) => {
		const { network } = this.props;
		orderedAccounts.forEach(async (account) => {
			try {
				const ens = await doENSReverseLookup(account.address, network);
				this.setState((state) => ({
					accountsENS: {
						...state.accountsENS,
						[account.address]: ens,
					},
				}));
			} catch {
				// Error
			}
		});
	};

	toggleWalletModal = () => this.setState((state) => ({ modalVisible: !state.modalVisible }));

	keyExtractor = (item) => item.address;

	render() {
		const { orderedAccounts, error, wallet } = this.state;
		const { enableAccountsAddition } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		const themeAppearance = this.context.themeAppearance || 'light';

		const onReach_MAX_Length = (temp) => {
			this.setState({ wallet: temp });
			let error = null;
			if (temp.length > 18) {
				error = strings('accounts.input_max_length');
			}
			this.setState({ error });
		};
		return (
			<SafeAreaView style={styles.wrapper} testID={'account-list'}>
				<View style={styles.titleWrapper}>
					<View style={styles.dragger} testID={'account-list-dragger'} />
				</View>
				<View style={styles.wrapperTextWallet}>
					<Text style={styles.textWallet}>{strings('address_book.my_wallet')}</Text>
				</View>
				<FlatList
					data={orderedAccounts}
					keyExtractor={this.keyExtractor}
					renderItem={this.renderItem}
					ref={this.flatList}
					style={styles.accountsWrapper}
					testID={'account-number-button'}
					getItemLayout={(_, index) => ({ length: 80, offset: 80 * index, index })} // eslint-disable-line
				/>

				<WalletModal
					warningModalVisible={this.state.modalVisible}
					onCancelPress={this.addAccount}
					onRequestClose={this.toggleWalletModal}
					onConfirmPress={this.toggleWalletModal}
					cancelButtonDisabled={error != null}
					confirmText={strings('accounts.create_new_wallet')}
					cancelText={strings('accounts.cancel')}
				>
					<View style={styles.wrapperModal}>
						<View style={styles.container}>
							<Text style={styles.walletTitle}>{strings('accounts.naming_account_title')}</Text>
						</View>
						<TextInput
							style={styles.input}
							maxLength={19}
							onChangeText={(item) => onReach_MAX_Length(item)}
							value={this.state.wallet}
							placeholder={strings('accounts.naming_account_placeholder')}
							placeholderTextColor={colors.text.muted}
							blurOnSubmit
							returnKeyType="next"
							keyboardAppearance={themeAppearance}
						/>
						{!!error && (
							<View style={styles.errorPhrase}>
								<Text style={styles.errorLabel}>{error}</Text>
							</View>
						)}
					</View>

					<Modal animationType="fade" visible={this.state.showWelcomeScreen} style={{ margin: 0 }}>
						<View style={styles.mainWrapper}>
							<View style={styles.wrapperWelcome} resetScrollToCoords={{ x: 0, y: 0 }}>
								<View style={styles.containerWelcome}>
									<Image style={styles.likeIcon} source={require('../../../images-new/like.png')} />
									<Text style={styles.title}>{strings('wellcome.finish_wallet_1')}</Text>
									<Text style={styles.title}>{strings('wellcome.finish_wallet_2')}</Text>
								</View>

								<View style={styles.ctaWrapper}>
									<StyledButton
										type={'blue'}
										style={styles.onStartButton}
										onPress={this.onStartButton}
										testID={'submit'}
										disabled={error}
									>
										{strings('wellcome.start_button')}
									</StyledButton>
								</View>
							</View>
						</View>
					</Modal>
				</WalletModal>

				{enableAccountsAddition && (
					<View style={styles.footer}>
						<TouchableOpacity
							style={[styles.footerButton]}
							testID={'create-account-button'}
							onPress={this.toggleWalletModal}
							onPressIn={()=> {this.setState({isPressAddWallet:true})
						}}
							onPressOut={() => {this.setState({isPressAddWallet:false})
						}}
							activeOpacity={1}
						>
							{this.state.loading ? (
								<ActivityIndicator size="small" color={colors.primary.default} />
							) : (
								<View style={[styles.iconWrapper, this.state.isPressAddWallet && styles.zoominItem]}>
									<Image
										source={require('../../../images/add-circle.png')}
										style={styles.addTokenImg}
										resizeMethod={'auto'}
									/>
									<Text style={styles.btnText}>{strings('accounts.create_new_account')}</Text>
								</View>
							)}
						</TouchableOpacity>
						<TouchableOpacity
							onPress={this.importAccount}
							style={[styles.footerButton]}
							testID={'import-account-button'}
							onPressIn={() => this.setState({isPressImportWallet:true})}
							onPressOut={() => this.setState({isPressImportWallet:false})}
							activeOpacity={1}
						>
							<View style={[styles.iconWrapper, this.state.isPressImportWallet && styles.zoominItem]}>
								<Image
									source={require('../../../images/import.png')}
									style={styles.addTokenImg}
									resizeMethod={'auto'}
								/>
								<Text style={styles.btnText}>{strings('accounts.import_account')}</Text>
							</View>
						</TouchableOpacity>
					</View>
				)}
				
			</SafeAreaView>
		);
	}
}

AccountList.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	accounts: state.engine.backgroundState.AccountTrackerController.accounts,
	thirdPartyApiMode: state.privacy.thirdPartyApiMode,
	keyrings: state.engine.backgroundState.KeyringController.keyrings,
	network: state.engine.backgroundState.NetworkController.network,
});

const mapDispatchToProps = (dispatch) => ({
	toggleImportFromSeedModalVisible: () => dispatch(toggleImportFromSeedModalVisible()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AccountList);
