import React, { createRef, PureComponent } from 'react';
import Identicon from '../../Identicon';
import PropTypes from 'prop-types';
import { SafeAreaView, StyleSheet, Text, TextInput, View, Image, Animated, Dimensions } from 'react-native';
import { fontStyles } from '../../../../styles/common';
import { renderFromWei } from '../../../../util/number';
import { getTicker } from '../../../../util/transactions';
import { isDefaultAccountName } from '../../../../util/ENSUtils';
import { strings } from '../../../../../locales/i18n';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { ThemeContext, mockTheme } from '../../../../util/theme';
import WalletModal from '../../../UI/WalletModal';
import Engine from '../../../../core/Engine';
import EthereumAddress from '../../EthereumAddress';
import PINCode, { hasUserSetPinCode } from '@haskkor/react-native-pincode';
import AsyncStorage from '@react-native-community/async-storage';
import { BIOMETRY_CHOICE } from '../../../../constants/storage';
import { isNull } from 'url/util';
import Modal from 'react-native-modal';
import {
	MANUAL_BACKUP_STEPS,
	SEED_PHRASE,
	CONFIRM_PASSWORD,
	WRONG_PASSWORD_ERROR,
} from '../../../../constants/onboarding';
import SecureKeychain from '../../../../core/SecureKeychain';
import { GestureHandlerRootView, Swipeable, TouchableOpacity } from 'react-native-gesture-handler';
import NotificationManager from '../../../../core/NotificationManager';
import { BACKGROUND_COLOR } from '../../../../constants/backgroundColor';
import { win32 } from 'path';
import { KeyringController } from '@metamask/controllers';
import Avatar from '../../../../util/avatar';
import Logger from '../../../../util/Logger';

const EMPTY = '0x0';
const BALANCE_KEY = 'balance';
const win = Dimensions.get('window');

const createStyles = (colors) =>
	StyleSheet.create({
		account: {
			borderColor: colors.border.muted,
			flexDirection: 'row',
			paddingHorizontal: 20,
			paddingVertical: 20,
			height: 80,
		},
		walletInfo: {
			borderColor: colors.border.muted,
			flexDirection: 'row',
			// paddingHorizontal: 20,
			// paddingVertical: 20,
			height: 80,
			marginLeft : win.width / 4.4
			// justifyContent: 'center',
			// justifyContent: 'center'
		},
		walletInfoWarning: {
			borderColor: colors.border.muted,
			flexDirection: 'row',
			flex: 1,
			width : win.width * 0.9,
			// height: 80,
			// marginLeft : win.width / 4.5
			// // justifyContent: 'center',
			// // justifyContent: 'center'
		},
		disabledAccount: {
			opacity: 0.5,
		},
		accountInfo: {
			marginLeft: 15,
			marginRight: 0,
			flex: 1,
			flexDirection: 'row',
			marginTop: win.height / 300,
		},
		accountLabel: {
			fontSize: 16,
			color: '#FFFFFF',
			...fontStyles.bold,
		},
		accountLabelConfirm: {
			fontSize: 18,
			color: 'white',
			...fontStyles.normal,
		},
		accountBalanceWrapper: {
			display: 'flex',
			flexDirection: 'row',
			marginTop: -win.height / 250,
		},
		accountBalance: {
			paddingTop: 5,
			fontSize: 10,
			color: '#A0A0B1',
			...fontStyles.normal,
		},
		accountBalanceError: {
			color: colors.error.default,
			marginLeft: 4,
		},
		importedView: {
			flex: 0.5,
			alignItems: 'flex-start',
			marginTop: 2,
		},
		accountMain: {
			flex: 1,
			flexDirection: 'column',
			marginTop: win.height / 130,
			marginLeft: -win.width / 100,
		},
		selectedWrapper: {
			flex: 0.2,
			alignItems: 'flex-end',
			width: 12,
			height: 12,
			marginTop: win.height / 50,
		},
		importedText: {
			color: colors.text.alternative,
			fontSize: 10,
			...fontStyles.bold,
		},
		importedWrapper: {
			paddingHorizontal: 10,
			paddingVertical: 3,
			borderRadius: 10,
			borderWidth: 1,
			borderColor: colors.icon.default,
		},
		wrapperModal: {
			flex: 1,
			paddingHorizontal: 32,
		},
		titleModal: {
			width: '100%',
			height: 33,
			alignItems: 'center',
			justifyContent: 'center',
			borderBottomWidth: StyleSheet.hairlineWidth,
			borderColor: colors.border.muted,
		},
		input: {
			...fontStyles.normal,
			fontSize: 16,
			paddingTop: 2,
			color: '#FFFFFF',
			borderRadius: 12,
			borderWidth: 1,
			textAlign: 'center',
			minHeight: 47,
			width: win.width * 0.785,
			marginLeft: -win.width / 50,
		},
		wrapperError: {
			borderColor: '#EB5252'
		},
		wrapperNormal: {
			borderColor: '#FFFFFF26',
		},
		container: {
			justifyContent: 'center',
			alignItems: 'center',
			marginTop: 20,
			marginBottom: 20,
		},
		walletTitle: {
			fontSize: 16,
			justifyContent: 'center',
			textAlign: 'center',
			...fontStyles.bold,
			color: 'white',
		},
		errorPhrase: {
			fontSize: 12,
			borderRadius: 10,
			...fontStyles.normal,
			paddingTop: 20,
			textAlign: 'center',
		},
		errorLabel: {
			color: '#EB5252',
			textAlign: 'center',
		},
		addressWrapper: {
			borderRadius: 40,
			//marginTop: 5,
			marginBottom: 20,
			paddingVertical: 7,
			paddingHorizontal: 15,
			textColor: '#A0A0B1',
		},
		address: {
			fontSize: 10,
			color: '#A0A0B1',
			...fontStyles.normal,
			letterSpacing: 0.8,
		},
		hWrapper: {
			flex: 1,
			flexDirection: 'row',
			height: 45,
			width: '100%',
		},
		identiconBorder: {
			flex: 2,
			borderRadius: 80,
			borderWidth: 2,
			padding: 1,
			alignItems: 'center',
			borderColor: colors.primary.default,
		},
		iconSwipe: {
			flex: 4.5,
			justifyContent: 'center',
		},
		iconSwipeLeft: {
			alignItems: 'flex-start',
		},
		iconSwipeRight: {
			alignItems: 'flex-end',
		},
		iconImage: {
			height: 30,
			resizeMode: 'contain',
		},
		accountNameWrapper: {
			flexDirection: 'row',
			justifyContent: 'space-between',
		},
		warningWrapper: {
			flexDirection: 'row',
			backgroundColor: 'rgba(222, 172, 44, 0.13)',
			textAlign: 'center',
			borderRadius: 14,
			padding: 10,
			width : win.width * 0.785,
			marginLeft: - win.width / 50 
		},
		warningImage: {
			marginTop: win.width / 30,
			width: 20,
			height: 20
		},
		warningMessageText: {
			flexDirection: 'row',
			fontSize: 12,
			color: '#FFBC39',
			...fontStyles.normal,
			textAlign: 'left',
		},
		rightAction: {
			backgroundColor: '#1B1B23',
			justifyContent: 'center',
			flexDirection: 'row',
			flexWrap: 'wrap',
			alignItems: 'center',
			// marginRight:
		},
		box: {
			width: 20,
			height: 20,
			marginTop: win.height / 25,
			marginLeft: 5,
			marginRight: 8,
		},
		textContainer: {
			alignItems: 'center',
			justifyContent: 'center',
			// backgroundColor: colors.background.alternative,
			borderRadius: 8,
		},
		groupImage: {
			borderRadius: 19,
			width: 38,
			height: 38,
		},
		textWrapperIcon: {
			textAlign: 'center',
			fontSize: 18,
			color: '#FFF',
			...fontStyles.boldHight,
			marginTop: -win.height / 24,
		},
		onlineIcon: {
			width: 12,
			height: 12,
		},
		dots: {
			width: 12,
			height: 12,
			borderRadius: 6,
			backgroundColor: '#1B1B23',
			borderColor: '#FFFFFF',
			borderWidth: 2,
		},
		titlePinCode: {
			fontSize: 24,
			...fontStyles.bold,
			lineHeight: 29,
			color: '#FFFFFF',
			opacity: 1,
		},
		subtitlePinCode: {
			fontSize: 16,
			...fontStyles.normal,
			lineHeight: 20,
			color: '#58586C',
			opacity: 1,
		},
		pinCodeTextButtonCircle: {
			fontSize: 20,
			...fontStyles.normal,
			color: '#FFFFFF',
			opacity: 1,
		},
		pinCodeButtonCircle: {
			backgroundColor: '#1B1B23',
			borderWidth: 1,
			borderColor: '#3D3D4B',
			// width: 72,
			// height: 72,
			borderRadius: 36,
			opacity: 1,
		},
		pinCodeDeleteButtonText: {
			color: '#FFFFFF',
			fontSize: 16,
			lineHeight: 20,
			...fontStyles.normal,
		},
	});

/**
 * View that renders specific account element in AccountList
 */
class AccountElement extends PureComponent {
	static propTypes = {
		/**
		 * Callback to be called onPress
		 */
		onPress: PropTypes.func.isRequired,
		/**
		 * Callback to be called onLongPress
		 */
		onLongPress: PropTypes.func.isRequired,
		/**
		 * Current ticker
		 */
		ticker: PropTypes.string,
		/**
		 * Whether the account element should be disabled (opaque and not clickable)
		 */
		disabled: PropTypes.bool,
		item: PropTypes.object,
		/**
		 * Updated balance using stored in state
		 */
		updatedBalanceFromStore: PropTypes.string,
		index: PropTypes.number,

		editWalletAddress: PropTypes.string,
		edit: PropTypes.func,
	};

	state = {
		modalVisible: false,
		removeModalVisible: false,
		confirmRemoveModalVisible: false,
		walletError: null,
		removeWalletError: null,
		showPINCode: false,
		wallet: '',
		name: '',
		removeWalletName: '',
		warningIncorrectPassword: undefined,
		view: '',
		ready: false,
		openSwipe: false,
		currentIndexSwipe: -1,
	};

	constructor(props) {
		super(props);

		this.swipeableRef = createRef(null);
	}

	componentDidUpdate() {
		if (this.props.editWalletAddress === this.props.item.address) {
			return;
		}

		if (this.swipeableRef.current === null) {
			return;
		}

		this.swipeableRef.current.close();
	}

	onPress = () => {
		const { onPress } = this.props;
		const { index } = this.props.item;
		onPress && onPress(index);
		// this.toggleRemoveWalletModal();
		// this.toggleConfirmRemoveWalletModal();
	};

	onLongPress = () => {
		const { onLongPress } = this.props;
		const { address, isImported, index } = this.props.item;
		onLongPress && onLongPress(address, isImported, index);
	};

	toggleWalletModal = () => this.setState((state) => ({ modalVisible: !state.modalVisible }));

	toggleRemoveWalletModal = () => this.setState((state) => ({ removeModalVisible: !state.removeModalVisible }));

	toggleConfirmRemoveWalletModal = () =>
		this.setState((state) => ({ confirmRemoveModalVisible: !state.confirmRemoveModalVisible }));

	onRightPress = (ref, item) => {
		ref.close();
	};

	updateWalletName = async () => {
		const walletName = this.state.wallet;
		const { PreferencesController } = Engine.context;
		let error = null;

		if (walletName === '') {
			error = strings('accounts.error_empty_message');
			this.setState({ walletError: error });
			return;
		}

		if (walletName != null && walletName.length > 18) {
			this.setState({ walletError: strings('accounts.input_max_length') });
			return;
		}

		PreferencesController.setAccountLabel(this.props.item.address, walletName);
		this.setState({ name: walletName });
		this.toggleWalletModal();

		NotificationManager.showSimpleNotification({
			status: 'success',
			duration: 5000,
			title: 'Notification',
			description: strings('accounts.rename_success'),
		});
		this.onPress();
	};

	removeWallet = async () => {
		let currentWallet = this.props.item.name;
		currentWallet = currentWallet.replace('...', '…');
		const inputWallet = this.state.removeWalletName;
		let error = null;

		if (currentWallet !== inputWallet) {
			error = strings('accounts.wrong_wallet');
		} else {
			this.toggleConfirmRemoveWalletModal();
		}
		this.setState({ removeWalletError: error });
	};

	confirmRemoveWallet = async () => {
		const biometryType = await SecureKeychain.getSupportedBiometryType();
		const biometryChoice = await AsyncStorage.getItem(BIOMETRY_CHOICE);
		if (isNull(biometryType) || isNull(biometryChoice) || !biometryChoice) {
			this.setState({ showPINCode: true, ready: true });
		} else {
			try {
				const credentials = await SecureKeychain.getGenericPassword();
				if (credentials) {
					this.onLongPress();
					this.toggleRemoveWalletModal();
				} else {
					this.setState({ showPINCode: true });
				}
			} catch (e) {
				this.setState({ showPINCode: true });
			}
			this.setState({ ready: true });
		}
	};

	tryUnlockWithPassword = async (password) => {
		this.setState({ ready: false });
		try {
			this.setState({ showPINCode: false, ready: true });
			this.onLongPress();
			this.toggleRemoveWalletModal();
			KeyringController.removeWalletName;
		} catch (e) {
			let msg = strings('reveal_credential.warning_incorrect_password');
			if (e.toString().toLowerCase() !== WRONG_PASSWORD_ERROR.toLowerCase()) {
				msg = strings('reveal_credential.unknown_error');
			}
			this.setState({
				warningIncorrectPassword: msg,
				ready: true,
			});
		}
	};

	tryUnlock = async (pin) => {
		// const { password } = this.state;
		const hasPin = await hasUserSetPinCode();
		if (hasPin) {
			this.tryUnlockWithPassword(pin);
		}
	};

	onSwipeableClose = () => {
		this.setState({ openSwipe: false, currentIndexSwipe: -1 });
		// Logger.log('onSwipeableClose', this.props.item.address);
	};

	onSwipeableWillOpen = () => {
		this.setState({ openSwipe: true });
		this.setState({ currentIndexSwipe: this.props.index });

		this.props.edit(this.props.item.address);
	};

	render() {
		const { disabled, updatedBalanceFromStore, ticker } = this.props;
		const { address, name, ens, isSelected, isImported, balanceError, isQRHardware } = this.props.item;
		const { walletError, removeWalletError, wallet, view, ready, showPINCode, openSwipe } = this.state;

		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		const themeAppearance = this.context.themeAppearance || 'light';
		const selected = isSelected ? (
			// <Icon name="check-circle" size={30} color={colors.primary.default} />
			<Image source={require('../../../../images/online.png')} style={styles.onlineIcon}></Image>
		) : null;

		const onReach_MAX_Length = (temp) => {
			this.setState({ wallet: temp });
			let error = null;

			if (temp.length > 18) {
				error = strings('accounts.input_max_length');
			} else {
				error = null;
			}
			this.setState({ walletError: error });
		};

		const onChangeTextRemoveWalletName = (temp) => {
			this.setState({ removeWalletName: temp , removeWalletError: null});
			// let currentWallet = this.props.item.name;
			// currentWallet = currentWallet.replace('...', '…');
			// let error = null;

			// if (temp.length === 0 || (temp.length > 0 && currentWallet !== temp)) {
			// 	error = strings('accounts.wrong_wallet');
			// } else {
			// 	error = null;
			// }
			// this.setState({ removeWalletError: error });
		};

		const RightActions = ({ progress, dragX, onPressEdit, onPressDelete }) => (
			<View style={styles.rightAction}>
				<TouchableOpacity onPress={onPressEdit}>
					<Image
						source={require('../../../../images/icon-pencil.png')}
						style={styles.box}
						resizeMethod={'auto'}
					/>
				</TouchableOpacity>
				<TouchableOpacity onPress={onPressDelete}>
					<Image
						source={require('../../../../images/icon-trash.png')}
						style={[styles.box, { marginRight: win.width / 15 }]}
						resizeMethod={'auto'}
					/>
				</TouchableOpacity>
			</View>
		);

		return (
			<SafeAreaView
				style={{
					marginBottom: -win.height / 70,
				}}
			>
				<GestureHandlerRootView>
					<Swipeable
						ref={this.swipeableRef}
						onSwipeableClose={this.onSwipeableClose}
						friction={8}
						containerStyle={{ overflow: 'hidden' }}
						onSwipeableWillOpen={this.onSwipeableWillOpen}
						renderRightActions={(progress, dragX) => {
							return (
								<RightActions
									progress={progress}
									dragX={dragX}
									onPressEdit={() => {
										this.toggleWalletModal();
										this.onRightPress(this.swipeableRef.current);
									}}
									onPressDelete={() => {
										this.toggleRemoveWalletModal();
										this.onRightPress(this.swipeableRef.current);
									}}
								/>
							);
						}}
						overshootLeft={false}
						overshootRight={false}
						leftThreshold={win.width / 160}
						rightThreshold={win.width / 160}
					>
						<TouchableOpacity
							style={[
								styles.account,
								disabled ? styles.disabledAccount : null,
								openSwipe && { marginLeft: win.width / 6.2 },
							]}
							key={`account-${address}`}
							onPress={this.onPress}
							// onLongPress={this.onLongPress}
							disabled={disabled}
						>
							<Avatar address={address} />
							<View style={styles.accountInfo}>
								<View style={styles.accountMain}>
									<Text numberOfLines={1} style={[styles.accountLabel]}>
										{isDefaultAccountName(name) && ens
											? ens
											: this.state.name === ''
											? name
											: this.state.name}
									</Text>
									<View style={styles.accountBalanceWrapper}>
										<EthereumAddress
											address={address}
											style={styles.accountBalance}
											type={'short'}
										/>
									</View>
								</View>
								{/* {!!tag && tag} */}
								{!openSwipe && <View style={styles.selectedWrapper}>{selected}</View>}
							</View>
						</TouchableOpacity>
					</Swipeable>

					<WalletModal
						warningModalVisible={this.state.modalVisible}
						onCancelPress={this.updateWalletName}
						onRequestClose={() => {this.toggleWalletModal();
							this.setState({wallet: '', walletError: null})
						}}
						onConfirmPress={() => {this.toggleWalletModal();
							this.setState({wallet: '', walletError: null})
						}}
						cancelButtonDisabled={walletError !== null || this.state.wallet === '' || this.state.wallet === name}
						confirmText={strings('accounts.ok')}
						cancelText={strings('accounts.cancel')}
					>
						<View style={styles.wrapperModal}>
							<View style={styles.container}>
								<Text style={styles.walletTitle}>{strings('accounts.changing_wallet')}</Text>
							</View>
							<TextInput
								style={[styles.input, walletError !== null ? styles.wrapperError : styles.wrapperNormal]}
								maxLength={19}
								onChangeText={(item) => onReach_MAX_Length(item)}
								placeholder={strings('accounts.naming_account_placeholder')}
								placeholderTextColor={colors.text.muted}
								blurOnSubmit
								returnKeyType="next"
								keyboardAppearance={themeAppearance}
							/>
							{!!walletError && (
								<View style={styles.errorPhrase}>
									<Text style={styles.errorLabel}>{walletError}</Text>
								</View>
							)}
						</View>
					</WalletModal>

					<WalletModal
						warningModalVisible={this.state.removeModalVisible}
						onCancelPress={this.removeWallet}
						onRequestClose={() => {this.toggleRemoveWalletModal();
							this.setState({removeWalletError : null, removeWalletName : ''})
						}}
						onConfirmPress={() => {this.toggleRemoveWalletModal();
							this.setState({removeWalletError : null, removeWalletName : ''})
						}}
						cancelButtonDisabled={removeWalletError !== null || this.state.removeWalletName === ''}
						confirmText={strings('accounts.continue')}
						cancelText={strings('accounts.cancel')}
					>
						<View style={styles.wrapperModal}>
							<View style={styles.container}>
								<Text style={styles.walletTitle}>{strings('accounts.removing_wallet')}</Text>
							</View>
							<TextInput
								style={[styles.input, removeWalletError !== null ? styles.wrapperError : styles.wrapperNormal]}
								maxLength={19}
								onChangeText={(item) => onChangeTextRemoveWalletName(item)}
								placeholder={this.props.item.name}
								placeholderTextColor={colors.text.muted}
								blurOnSubmit
								returnKeyType="next"
								keyboardAppearance={themeAppearance}
							/>
							{!!removeWalletError && (
								<View style={styles.errorPhrase}>
									<Text style={styles.errorLabel}>{removeWalletError}</Text>
								</View>
							)}
						</View>

						<WalletModal
							warningModalVisible={this.state.confirmRemoveModalVisible}
							onCancelPress={this.confirmRemoveWallet}
							onRequestClose={()=> {this.toggleConfirmRemoveWalletModal();this.toggleRemoveWalletModal();
								this.setState({removeWalletName: ''});
							}}
							onConfirmPress={()=> {this.toggleConfirmRemoveWalletModal();this.toggleRemoveWalletModal();
								this.setState({removeWalletName: ''});
							}}
							confirmText={strings('accounts.remove')}
							cancelText={strings('accounts.no')}
							type={'confirm_delete'}
						>
							<Modal
								animationType="fade"
								visible={showPINCode}
								style={{ backgroundColor: 'black', margin: 0 }}
							>
								<PINCode
									titleChoose={strings('pin_code.title_choose')}
									subtitleChoose={strings('pin_code.subtitle')}
									titleConfirm={strings('pin_code.title_confirm')}
									subtitleConfirm={strings('pin_code.subtitle_confirm')}
									titleConfirmFailed={strings('pin_code.title_confirm_failed')}
									subtitleError={strings('pin_code.subtitl_error')}
									buttonDeleteText={strings('pin_code.button_delete_text')}
									passwordLength={6}
									disableLockScreen
									status={'enter'}
									touchIDDisabled
									finishProcess={(pin) => this.tryUnlock(pin)}
									stylePinCodeTextTitle={styles.titlePinCode}
									stylePinCodeTextSubtitle={styles.subtitlePinCode}
									stylePinCodeCircle={styles.dots}
									stylePinCodeTextButtonCircle={styles.pinCodeTextButtonCircle}
									stylePinCodeButtonCircle={styles.pinCodeButtonCircle}
									stylePinCodeDeleteButtonText={styles.pinCodeDeleteButtonText}
								/>
							</Modal>

							<View style={styles.wrapperModal}>
								<View style={styles.container}>
									<Text style={styles.walletTitle}>{strings('accounts.confirm_remove_wallet')}</Text>
								</View>

								<View style={[styles.walletInfo, {marginTop:  - win.height / 100}]}>
									<Avatar address={address}/>
									<View style={styles.accountInfo}>
										<View style={styles.accountMain}>
											<View style={styles.accountNameWrapper}>
												<Text style={styles.accountLabelConfirm} numberOfLines={1}>
													{name}
												</Text>
											</View>
											<View style={styles.accountBalanceWrapper}>
												<EthereumAddress
													address={address}
													style={styles.address}
													type={'short'}
												/>
											</View>
										</View>
									</View>
								</View>

								<View style={(styles.warningWrapper)}>
									<Image
										source={require('../../../../images/icon-warning-yellow.png')}
										style={styles.warningImage}

									/>
									<View style={styles.accountInfo}>
										<Text style={styles.warningMessageText}>
											{strings('accounts.warning_message')}
										</Text>
									</View>
								</View>
							</View>
						</WalletModal>
					</WalletModal>
				</GestureHandlerRootView>
			</SafeAreaView>
		);
	}
}

const mapStateToProps = (
	{
		engine: {
			backgroundState: { PreferencesController, AccountTrackerController },
		},
		myWallet,
	},
	{ item: { balance, address } }
) => {
	const { selectedAddress } = PreferencesController;
	const { accounts } = AccountTrackerController;
	const selectedAccount = accounts[selectedAddress];
	const selectedAccountHasBalance =
		selectedAccount && Object.prototype.hasOwnProperty.call(selectedAccount, BALANCE_KEY);
	const updatedBalanceFromStore =
		balance === EMPTY && selectedAddress === address && selectedAccount && selectedAccountHasBalance
			? selectedAccount[BALANCE_KEY]
			: balance;

	return {
		updatedBalanceFromStore,
		editWalletAddress: myWallet.editWalletAddress,
	};
};

AccountElement.contextType = ThemeContext;

export default connect(mapStateToProps, {
	edit: (address) => ({
		type: 'EDIT_MY_WALLET',
		payload: { address },
	}),
	cancelEdit: () => ({
		type: 'CANCEL_EDIT_WALLET',
	}),
})(AccountElement);
