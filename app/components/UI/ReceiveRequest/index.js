import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { InteractionManager, TouchableOpacity, SafeAreaView, Dimensions, StyleSheet, View, Alert, Image } from 'react-native';
import Modal from 'react-native-modal';
import Share from 'react-native-share';
import QRCode from 'react-native-qrcode-svg';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { connect } from 'react-redux';

import Analytics from '../../../core/Analytics';
import AnalyticsV2 from '../../../util/analyticsV2';
import Logger from '../../../util/Logger';
import Device from '../../../util/device';
import { strings } from '../../../../locales/i18n';
import { ANALYTICS_EVENT_OPTS } from '../../../util/analytics';
import { generateUniversalLinkAddress } from '../../../util/payment-link-generator';
import { getTicker } from '../../../util/transactions';
import { allowedToBuy } from '../FiatOrders';
import { showAlert } from '../../../actions/alert';
import { toggleReceiveModal } from '../../../actions/modals';
import { protectWalletModalVisible } from '../../../actions/user';

import { fontStyles } from '../../../styles/common';
import Text from '../../Base/Text';
import ModalHandler from '../../Base/ModalHandler';
import ModalDragger from '../../Base/ModalDragger';
import AddressQRCode from '../../Views/AddressQRCode';
import EthereumAddress from '../EthereumAddress';
import GlobalAlert from '../GlobalAlert';
import StyledButton from '../StyledButton';
import ClipboardManager from '../../../core/ClipboardManager';
import { ThemeContext, mockTheme } from '../../../util/theme';
import Networks from "../../../util/networks";

const logoImage = require('../../../images-new/receive-icons/logo.png');
const iconCopy = require('../../../images-new/receive-icons/copy.png');
const iconShare = require('../../../images-new/receive-icons/share.png');
const iconDash = require('../../../images-new/receive-icons/dash.png');
const iconBlock = require('../../../images-new/receive-icons/block.png');

const createStyles = (colors) =>
	StyleSheet.create({
		daggerWrapper: {
			width: '100%',
			height: 40,
			alignItems: 'center',
			justifyContent: 'flex-end',
		},
		dagger: {
			width: 40,
			height: 4,
			borderRadius: 20,
			backgroundColor: '#A0A0B1',
			marginBottom: 4,
		},
		wrapper: {
			// backgroundColor: colors.background.default,
			backgroundColor: '#1B1B23',
			borderTopLeftRadius: 20,
			borderTopRightRadius: 20,
		},
		body: {
			alignItems: 'center',
			paddingHorizontal: 15,
		},
		qrWrapper: {
			margin: 15,
			marginTop: 40,
			justifyContent: 'center',
			alignItems: 'center'
		},
		qrContent: {
			padding: 25,
			backgroundColor: '#FFF',
			borderRadius: 20,
			justifyContent: 'center',
			alignItems: 'center',
		},
		qrLogo: {
			height: 35,
			width: 29,
			marginBottom: 22,
		},
		qrText: {
			marginTop: 22,
			color: '#58586C',
			fontSize: 12,
			fontWeight: '500',
		},
		addressWrapper: {
			flexDirection: 'row',
			alignItems: 'center',
			margin: 15,
			padding: 9,
			paddingHorizontal: 15,
			backgroundColor: colors.background.alternative,
			borderRadius: 30,
		},
		copyButton: {
			backgroundColor: colors.icon.muted,
			color: colors.text.default,
			borderRadius: 12,
			overflow: 'hidden',
			paddingVertical: 3,
			paddingHorizontal: 6,
			marginHorizontal: 6,
		},
		actionRow: {
			flexDirection: 'row',
			marginBottom: 15,
		},
		actionButton: {
			flex: 1,
			marginHorizontal: 8,
		},
		title: {
			...fontStyles.normal,
			// color: colors.text.default,
			color: '#FFF',
			fontSize: 16,
			fontWeight: '600',
			flexDirection: 'row',
			alignSelf: 'center',
		},
		titleWrapper: {
			marginTop: 18,
		},
		chainBlock: {
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
			paddingVertical: 10,
			paddingHorizontal: 20,
			borderWidth: 1,
			borderColor: 'rgba(255, 255, 255, 0.15)',
			borderRadius: 20,
			marginTop: 15,
		},
		chainBlockIconDash: {
			width: 6,
			height: 2,
		},
		chainBlockIconBlock: {
			width: 12,
			height: 12,
			marginLeft: 10,
		},
		chainBlockText: {
			color: '#FFF',
			fontSize: 12,
			fontWeight: '500',
			marginLeft: 10,
		},
		addressBlock: {
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
			paddingVertical: 5,
			paddingHorizontal: 15,
			marginTop: 15,
		},
		addressBlockText: {
			color: '#FFF',
			fontSize: 12,
			fontWeight: '500',
		},
		addressBlockIcon: {
			width: 18,
			height: 18,
			marginLeft: 5,
		},
		buttonShare: {
			backgroundColor: '#58586C',
			borderRadius: 20,
			paddingVertical: 12,
			paddingHorizontal: 57,
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
			marginVertical: 40,
		},
		buttonShareImage: {
			width: 18,
			height: 18,
		},
		buttonShareText: {
			color: '#FFF',
			fontSize: 16,
			fontWeight: '500',
			marginLeft: 10,
		},
	});

/**
 * PureComponent that renders receive options
 */
class ReceiveRequest extends PureComponent {
	static propTypes = {
		/**
		 * The navigator object
		 */
		navigation: PropTypes.object,
		/**
		 * Selected address as string
		 */
		selectedAddress: PropTypes.string,
		/**
		 * Asset to receive, could be not defined
		 */
		receiveAsset: PropTypes.object,
		/**
		 * Action that toggles the receive modal
		 */
		toggleReceiveModal: PropTypes.func,
		/**
		/* Triggers global alert
		*/
		showAlert: PropTypes.func,
		/**
		 * Network Controller
		 */
		networkController: PropTypes.object,
		/**
		 * Network id
		 */
		network: PropTypes.string,
		/**
		 * Native asset ticker
		 */
		ticker: PropTypes.string,
		/**
		 * Prompts protect wallet modal
		 */
		protectWalletModalVisible: PropTypes.func,
		/**
		 * Hides the modal that contains the component
		 */
		hideModal: PropTypes.func,
		/**
		 * redux flag that indicates if the user
		 * completed the seed phrase backup flow
		 */
		seedphraseBackedUp: PropTypes.bool,
	};

	state = {
		qrModalVisible: false,
		buyModalVisible: false,
	};

	/**
	 * Share current account public address
	 */
	onShare = () => {
		const { selectedAddress } = this.props;
		Share.open({
			// message: generateUniversalLinkAddress(selectedAddress),
			message: selectedAddress,
		})
			.then(() => {
				this.props.hideModal();
				setTimeout(() => this.props.protectWalletModalVisible(), 1000);
			})
			.catch((err) => {
				Logger.log('Error while trying to share address', err);
			});
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.RECEIVE_OPTIONS_SHARE_ADDRESS);
		});
	};

	onShareQRCode = () => {
		this.QRCode.toDataURL((dataUrl) => {
			Share.open({
				url: `data:image/png;base64,${dataUrl}`,
			})
				.then(() => {
					this.props.hideModal();
					setTimeout(() => this.props.protectWalletModalVisible(), 1000);
				})
				.catch((err) => {
					Logger.log('Error while trying to share address', err);
				});
		});
	};

	/**
	 * Shows an alert message with a coming soon message
	 */
	onBuy = async () => {
		const { navigation, toggleReceiveModal, network } = this.props;
		if (!allowedToBuy(network)) {
			Alert.alert(strings('fiat_on_ramp.network_not_supported'), strings('fiat_on_ramp.switch_network'));
		} else {
			toggleReceiveModal();
			navigation.navigate('FiatOnRamp');
			InteractionManager.runAfterInteractions(() => {
				Analytics.trackEvent(ANALYTICS_EVENT_OPTS.WALLET_BUY_ETH);
				AnalyticsV2.trackEvent(AnalyticsV2.ANALYTICS_EVENTS.ONRAMP_OPENED, {
					button_location: 'Receive Modal',
					button_copy: `Buy ${this.props.ticker}`,
				});
			});
		}
	};

	copyAccountToClipboard = async () => {
		const { selectedAddress } = this.props;
		ClipboardManager.setString(selectedAddress);
		this.props.showAlert({
			isVisible: true,
			autodismiss: 1500,
			content: 'clipboard-alert',
			data: { msg: strings('account_details.account_copied_to_clipboard') },
		});
		if (!this.props.seedphraseBackedUp) {
			setTimeout(() => this.props.hideModal(), 1000);
			setTimeout(() => this.props.protectWalletModalVisible(), 1500);
		}
	};

	/**
	 * Closes QR code modal
	 */
	closeQrModal = (toggleModal) => {
		this.props.hideModal();
		toggleModal();
	};

	/**
	 * Opens QR code modal
	 */
	openQrModal = () => {
		this.setState({ qrModalVisible: true });
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.RECEIVE_OPTIONS_QR_CODE);
		});
	};

	onReceive = () => {
		this.props.toggleReceiveModal();
		this.props.navigation.navigate('PaymentRequestView', {
			screen: 'PaymentRequest',
			params: { receiveAsset: this.props.receiveAsset },
		});
		InteractionManager.runAfterInteractions(() => {
			Analytics.trackEvent(ANALYTICS_EVENT_OPTS.RECEIVE_OPTIONS_PAYMENT_REQUEST);
		});
	};

	render() {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		const { networkController } = this.props;
		let chainFullname = null;

		if (networkController.provider.nickname) {
			chainFullname = networkController.provider.nickname;
		} else {
			chainFullname =
				(Networks[networkController.provider.type] && Networks[networkController.provider.type].name) ||
				{ ...Networks.rpc, color: null }.name;
		}

		return (
			<>
				<View style={styles.daggerWrapper}>
					<View style={styles.dagger} />
				</View>
				<SafeAreaView style={styles.wrapper}>
					{/*<ModalDragger />*/}
					<View style={styles.titleWrapper}>
						<Text style={styles.title} testID={'receive-request-screen'}>
							{strings('receive_request.my_address')}
						</Text>
					</View>
					<View style={styles.body}>
						<View style={styles.chainBlock}>
							<Image style={styles.chainBlockIconDash} source={iconDash} />
							<Image style={styles.chainBlockIconBlock} source={iconBlock} />
							<Text style={styles.chainBlockText}>{chainFullname}</Text>
						</View>
						<ModalHandler>
							{({ isVisible, toggleModal }) => (
								<>
									<TouchableOpacity
										style={styles.qrWrapper}
										// eslint-disable-next-line react/jsx-no-bind
										onPress={() => {
											// toggleModal();
											// InteractionManager.runAfterInteractions(() => {
											// 	Analytics.trackEvent(ANALYTICS_EVENT_OPTS.RECEIVE_OPTIONS_QR_CODE);
											// });
											this.onShareQRCode();
										}}
									>
										<View style={styles.qrContent}>
											<Image style={styles.qrLogo} source={logoImage} />
											<QRCode
												getRef={(QRCode) => (this.QRCode = QRCode)}
												value={`ethereum:${this.props.selectedAddress}@${this.props.network}`}
												// size={Dimensions.get('window').width / 2}
												size={Dimensions.get('window').width / 2.3}
												// color={colors.text.default}
												// backgroundColor={colors.background.default}
												color={'#000'}
												backgroundColor={'#FFF'}
											/>
											<Text style={styles.qrText}>{strings('receive_request.press_to_share_qr')}</Text>
										</View>
									</TouchableOpacity>
									<Modal
										isVisible={isVisible}
										onBackdropPress={toggleModal}
										onBackButtonPress={toggleModal}
										onSwipeComplete={toggleModal}
										swipeDirection={'down'}
										propagateSwipe
										testID={'qr-modal'}
										backdropColor={colors.overlay.default}
										backdropOpacity={1}
									>
										<AddressQRCode closeQrModal={() => this.closeQrModal(toggleModal)} />
									</Modal>
								</>
							)}
						</ModalHandler>

						{/*<Text>{strings('receive_request.scan_address')}</Text>*/}

						{/*<TouchableOpacity*/}
						{/*	style={styles.addressWrapper}*/}
						{/*	onPress={this.copyAccountToClipboard}*/}
						{/*	testID={'account-address'}*/}
						{/*>*/}
						{/*	<Text>*/}
						{/*		<EthereumAddress address={this.props.selectedAddress} type={'short'} />*/}
						{/*	</Text>*/}
						{/*	<Text style={styles.copyButton} small>*/}
						{/*		{strings('receive_request.copy')}*/}
						{/*	</Text>*/}
						{/*	<TouchableOpacity onPress={this.onShare}>*/}
						{/*		<EvilIcons*/}
						{/*			name={Device.isIos() ? 'share-apple' : 'share-google'}*/}
						{/*			size={25}*/}
						{/*			color={colors.icon.default}*/}
						{/*		/>*/}
						{/*	</TouchableOpacity>*/}
						{/*</TouchableOpacity>*/}
						<TouchableOpacity
							style={styles.addressBlock}
							onPress={this.copyAccountToClipboard}
						>
							<Text style={styles.addressBlockText}>
								<EthereumAddress address={this.props.selectedAddress} type={'full'} />
							</Text>
							<Image style={styles.addressBlockIcon} source={iconCopy} />
						</TouchableOpacity>
						<TouchableOpacity
							style={styles.buttonShare}
							onPress={this.onShare}
						>
							<Image style={styles.buttonShareImage} source={iconShare} />
							<Text style={styles.buttonShareText}>{strings('receive_request.share')}</Text>
						</TouchableOpacity>
						{/*<View style={styles.actionRow}>*/}
						{/*	{allowedToBuy(this.props.network) && (*/}
						{/*		<StyledButton type={'blue'} containerStyle={styles.actionButton} onPress={this.onBuy}>*/}
						{/*			{strings('fiat_on_ramp.buy', { ticker: getTicker(this.props.ticker) })}*/}
						{/*		</StyledButton>*/}
						{/*	)}*/}
						{/*	<StyledButton*/}
						{/*		type={'normal'}*/}
						{/*		onPress={this.onReceive}*/}
						{/*		containerStyle={styles.actionButton}*/}
						{/*		testID={'request-payment-button'}*/}
						{/*	>*/}
						{/*		{strings('receive_request.request_payment')}*/}
						{/*	</StyledButton>*/}
						{/*</View>*/}
					</View>

					<GlobalAlert />
				</SafeAreaView>
			</>
		);
	}
}

ReceiveRequest.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	networkController: state.engine.backgroundState.NetworkController,
	network: state.engine.backgroundState.NetworkController.network,
	ticker: state.engine.backgroundState.NetworkController.provider.ticker,
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
	receiveAsset: state.modals.receiveAsset,
	seedphraseBackedUp: state.user.seedphraseBackedUp,
});

const mapDispatchToProps = (dispatch) => ({
	toggleReceiveModal: () => dispatch(toggleReceiveModal()),
	showAlert: (config) => dispatch(showAlert(config)),
	protectWalletModalVisible: () => dispatch(protectWalletModalVisible()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ReceiveRequest);
