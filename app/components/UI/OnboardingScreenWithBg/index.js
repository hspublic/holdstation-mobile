import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, ImageBackground, View, Image, Dimensions } from "react-native";
import { useAppThemeFromContext, mockTheme } from '../../../util/theme';
import { colors as importedColors } from "../../../styles/common";
import Svg, {
	G,
	Path,
	Ellipse,
	Defs,
	LinearGradient,
	Stop,
	ClipPath,
} from "react-native-svg";
import LottieView from "lottie-react-native";

const win = Dimensions.get('window')

const createStyles = (colors) =>
	StyleSheet.create({
		flex: {
			flex: 1,
			backgroundColor: '#1B1B23',
			shadowColor: importedColors.transparent,
		},
		wrapper: {
			top: 0,
			bottom: 0,
			left: 0,
			right: 0,
			position: 'absolute',
			borderTopWidth: 0,
			flex: 1,
			width: null,
			height: null,
		},
		logo: {
			//alignSelf: 'center',
			top: -100,
			flex: 0.435,
			alignItems: 'center',
			justifyContent: 'flex-end',
			zIndex: 1,
			height : win.height / 5,
			// backgroundColor: 'red'
		},
		logoImage: {
			width: win.width / 2,
			height: win.height / 10
		}
	});

const images = {
	a: require('../../../images/welcome-bg1.png'), // eslint-disable-line
	b: require('../../../images/welcome-bg2.png'), // eslint-disable-line
	c: require('../../../images/welcome-bg3.png'), // eslint-disable-line
	d: require('../../../images/welcome-bg4.png'), // eslint-disable-line
	e: require('../../../images/bgStart.png'), // eslint-disable-line
	carousel: null,
};

const OnboardingScreenWithBg = (props) => {
	const { colors } = useAppThemeFromContext() || mockTheme;
	const styles = createStyles(colors);

	return (
		<View style={styles.flex}>

			<View style={styles.logo}>
				<Image
					source={require('../../../images-new/hs-logo.png') }
					style={styles.logoImage}
				/>
			</View>
			<ImageBackground source={images["e"]} style={styles.wrapper} resizeMode={'stretch'}>
					{props.children}
			</ImageBackground>
		
		</View>
	);
};

OnboardingScreenWithBg.propTypes = {
	/**
	 * String specifying the image
	 * to be used
	 */
	screen: PropTypes.string,
	/**
	 * Children components of the GenericButton
	 * it can be a text node, an image, or an icon
	 * or an Array with a combination of them
	 */
	children: PropTypes.any,
};

export default OnboardingScreenWithBg;
