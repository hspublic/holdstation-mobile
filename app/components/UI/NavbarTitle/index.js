import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TouchableOpacity, View, StyleSheet, Text, Dimensions } from 'react-native';
import { fontStyles, colors as importedColors, GLOBAL_ITEM_FONT_SIZE } from '../../../styles/common';
import Networks from '../../../util/networks';
import { toggleNetworkModal } from '../../../actions/modals';
import { strings } from '../../../../locales/i18n';
import Device from '../../../util/device';
import { ThemeContext, mockTheme } from '../../../util/theme';
import { Image } from 'react-native-animatable';

const win = Dimensions.get('window');
const dropdownIcon = require('../../../images-new/dropdown.png')
const boxMainnet = require('../../../images-new/BoxMainnet.png')


const createStyles = (colors) =>
	StyleSheet.create({
		wrapper: {
			alignItems: 'center',
			justifyContent: 'center',
			borderColor: '#ffffff26',
			borderWidth: 1,
			borderRadius: 12,
			paddingHorizontal: win.width / 35, 
			height: 32
		},
		network: {
			flexDirection: 'row',
			justifyContent: 'center',
			
		},
		networkName: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			//color: colors.text.alternative,
			color: '#FFFFFF',
			...fontStyles.semiBold,
			lineHeight: 18,
		},
		networkIcon: {
			width: 8,
			height: 3,
			borderRadius: 4,
			marginRight: 5,
			marginTop: Device.isIos() ? 7 : 8,
			backgroundColor: '#22D07D',
		},
		title: {
			fontSize: 18,
			...fontStyles.normal,
			//color: colors.text.default,
			color: '#FFFFFF',
		},
		otherNetworkIcon: {
			backgroundColor: importedColors.transparent,
			borderColor: colors.border.default,
			borderWidth: 1,
		},
		boxMainnet: {
			width:9,
			height:11,
			marginTop: win.height/300,
			marginRight: win.width/40,
			marginLeft: win.width/70,
		},
		dropdownIcon: {
			height: 5,
			width: 8,
			marginTop: win.height/130, 
			marginLeft: win.width/70
		}
	});

/**
 * UI PureComponent that renders inside the navbar
 * showing the view title and the selected network
 */
class NavbarTitle extends PureComponent {
	static propTypes = {
		/**
		 * Object representing the selected the selected network
		 */
		network: PropTypes.object.isRequired,
		/**
		 * Name of the current view
		 */
		title: PropTypes.string,
		/**
		 * Action that toggles the network modal
		 */
		toggleNetworkModal: PropTypes.func,
		/**
		 * Boolean that specifies if the title needs translation
		 */
		translate: PropTypes.bool,
		/**
		 * Boolean that specifies if the network can be changed
		 */
		disableNetwork: PropTypes.bool,
	};

	static defaultProps = {
		translate: true,
	};

	animating = false;

	openNetworkList = () => {
		if (!this.props.disableNetwork) {
			if (!this.animating) {
				this.animating = true;
				this.props.toggleNetworkModal();
				setTimeout(() => {
					this.animating = false;
				}, 500);
			}
		}
	};

	render = () => {
		const { network, title, translate } = this.props;
		let name = null;
		const color = (Networks[network.provider.type] && Networks[network.provider.type].color) || null;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (network.provider.nickname) {
			name = network.provider.nickname;
		} else {
			name =
				(Networks[network.provider.type] && Networks[network.provider.type].name) ||
				{ ...Networks.rpc, color: null }.name;
		}

		const realTitle = translate ? strings(title) : title;

		return (
			<TouchableOpacity
				onPress={this.openNetworkList}
				style={styles.wrapper}
				activeOpacity={this.props.disableNetwork ? 1 : 0.2}
				testID={'open-networks-button'}
			>
				{title ? (
					<Text numberOfLines={1} style={styles.title}>
						{realTitle}
					</Text>
				) : null}

				<View style={styles.network}>
					{/* color ? { backgroundColor: color } : styles.otherNetworkIcon] */}
					<View style={[styles.networkIcon]} />
					<Image source={boxMainnet} style={styles.boxMainnet} resizeMode="stretch" />
					<Text numberOfLines={1} style={styles.networkName} testID={'navbar-title-network'}>
						{name}
					</Text>
					<Image source={dropdownIcon} style={styles.dropdownIcon} resizeMode="stretch" />
				</View>
			</TouchableOpacity>
		);
	};
}

NavbarTitle.contextType = ThemeContext;

const mapStateToProps = (state) => ({
	network: state.engine.backgroundState.NetworkController,
});
const mapDispatchToProps = (dispatch) => ({
	toggleNetworkModal: () => dispatch(toggleNetworkModal()),
});
export default connect(mapStateToProps, mapDispatchToProps)(NavbarTitle);
