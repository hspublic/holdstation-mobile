import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, TouchableHighlight, StyleSheet, Image, Text, View, Dimensions } from 'react-native';
import { fontStyles, GLOBAL_ITEM_FONT_SIZE } from '../../../styles/common';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import { strings } from '../../../../locales/i18n';
import { toDateFormat, isToday, isYesterday, getTodayText, getYesterdayText, toDateOnlyFormat } from '../../../util/date';
import TransactionDetails from './TransactionDetails';
import { safeToChecksumAddress } from '../../../util/address';
import { connect } from 'react-redux';
import StyledButton from '../StyledButton';
import Modal from 'react-native-modal';
import decodeTransaction from './utils';
import { getTokenByTicker, TRANSACTION_TYPES } from "../../../util/transactions";
import ListItem from '../../Base/ListItem';
import StatusText from '../../Base/StatusText';
import DetailsModal from '../../Base/DetailsModal';
import { isMainNet } from "../../../util/networks";
import { WalletDevice, util } from '@metamask/controllers/';
import { ThemeContext, mockTheme } from '../../../util/theme';
import { getTokenList } from "../../../reducers/tokens";
const { weiHexToGweiDec, isEIP1559Transaction } = util;
import AssetIcon from '../AssetIcon';
import Identicon from '../Identicon';
import NetworkMainAssetLogo from '../NetworkMainAssetLogo';

const win = Dimensions.get('window')

const createStyles = (colors) =>
	StyleSheet.create({
		row: {
			// backgroundColor: colors.background.default,
			backgroundColor: '#1B1B23',
			flex: 1,
			// borderBottomWidth: StyleSheet.hairlineWidth,
			// borderColor: colors.border.muted,
		},
		actionContainerStyle: {
			height: 25,
			padding: 0,
		},
		speedupActionContainerStyle: {
			marginRight: 10,
		},
		actionStyle: {
			fontSize: 10,
			padding: 0,
			paddingHorizontal: 10,
		},
		icon: {
			width: 28,
			height: 28,
		},
		summaryWrapper: {
			padding: 15,
		},
		fromDeviceText: {
			color: colors.text.alternative,
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			marginBottom: 10,
			...fontStyles.normal,
		},
		importText: {
			color: colors.text.alternative,
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			...fontStyles.bold,
			alignContent: 'center',
		},
		importRowBody: {
			alignItems: 'center',
			backgroundColor: colors.background.alternative,
			paddingTop: 10,
		},
		txElementWrapper: {
			padding: 15,
			marginLeft: win.width / 100,
			marginRight: win.width / 100,
		},
		txElementTime: {
			color: '#FFF',
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			fontFamily: 'EuclidCircularB-Regular',
			fontWeight: '600',
			marginBottom: 20
		},
		txElementLineBreak: {
			flexDirection: 'row',
		},
		txElementLineBreakIcon: {
			width: 30,
			height: 30,
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'center',
		},
		txElementLineBreakIconImg: {
			width: 12,
			height: 12,
		},
		txElementContent: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		txElementBlock1: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		txElementAvatar: {
			width: 32,
			height: 32,
		},
		txElementBlock2: {
			flex: 1,
			marginLeft: 15,
		},
		txElementAction: {
			flexDirection: 'row',
			alignItems: 'center',
		},
		txElementActionIcon: {
			width: 15,
			height: 15,
		},
		txElementActionIconApprove: {
			width: 12,
			height: 12,
		},
		txElementActionText: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			fontWeight: '500',
			color: '#FFF',
			marginLeft: 5,
		},
		txElementTokenName: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			fontWeight: '400',
			marginTop: 5,
			color: '#A0A0B1',
		},
		txElementBlock3: {
			// flex: 1,
			alignItems: 'flex-end',
			width: win.width/ 2, 
			// backgroundColor: 'red'
		},
		txElementValue: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			fontWeight: '700',
			color: '#FFF',
			// ...fontStyles.normal,
			lineHeight: 18,
		},
		valueRender: {
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			...fontStyles.boldHight,
			lineHeight: 18,
			marginTop: win.height / 200,
			// width : win.width / 2,
		},
		valuePending: {
			color: '#A0A0B1',
		},
		valueSuccess: {
			color: '#22D07D',
		},
		valueFailed: {
			color: '#EB5252',
		},
		valueNormal: {
			color: '#FFFFFF',
		},
		bottomModal: {
			justifyContent: 'flex-end',
			margin: 0,
		},
		modalDetailsContainer: {
			width: '100%',
			backgroundColor: '#1B1B23',
			borderTopLeftRadius: 25,
			borderTopRightRadius: 25,
			paddingVertical: 20,
		},
		modalDetailsView: {
			flexDirection: 'column',
			justifyContent: 'center',
			alignItems: 'center',
		},
		modalDaggerWrapper: {
			width: '100%',
			height: 40,
			alignItems: 'center',
			justifyContent: 'flex-end',
		},
		modalDagger: {
			width: 40,
			height: 4,
			borderRadius: 20,
			backgroundColor: '#A0A0B1',
			marginBottom: 4,
		},
		modalDetailsHeader: {
			flexDirection: 'row',
			paddingHorizontal: 16,
		},
		modalDetailsTitle: {
			flex: 1,
			textAlign: 'center',
			fontSize: GLOBAL_ITEM_FONT_SIZE,
			color: '#FFF',
			lineHeight: 17,
			...fontStyles.bold,
		},
		normalColor: {
			color: '#A0A0B1'
		},
		failColor: {
			color: '#EB5252'
		},	
		failColorIcon: {
			tintColor: '#EB5252'
		}
	});

/* eslint-disable import/no-commonjs */
// const transactionIconApprove = require('../../../images/transaction-icons/approve.png');
// const transactionIconInteraction = require('../../../images/transaction-icons/interaction.png');
// const transactionIconSent = require('../../../images/transaction-icons/send.png');
// const transactionIconReceived = require('../../../images/transaction-icons/receive.png');
//
// const transactionIconApproveFailed = require('../../../images/transaction-icons/approve-failed.png');
// const transactionIconInteractionFailed = require('../../../images/transaction-icons/interaction-failed.png');
// const transactionIconSentFailed = require('../../../images/transaction-icons/send-failed.png');
// const transactionIconReceivedFailed = require('../../../images/transaction-icons/receive-failed.png');

const transactionIconSentPending = require('../../../images-new/transaction-icons/send-pending.png');
const transactionIconSentConfirmed = require('../../../images-new/transaction-icons/send-confirmed.png');
const transactionIconSentFailed = require('../../../images-new/transaction-icons/send-failed.png');

const transactionIconReceivedPending = require('../../../images-new/transaction-icons/receive-pending.png');
const transactionIconReceivedConfirmed = require('../../../images-new/transaction-icons/receive-confirmed.png');
const transactionIconReceivedFailed = require('../../../images-new/transaction-icons/receive-failed.png');

// const transactionIconApprovePending = require('../../../images-new/transaction-icons/approve-pending.png');
const transactionIconApproveConfirmed = require('../../../images-new/transaction-icons/approve-confirmed.png');
// const transactionIconApproveFailed = require('../../../images-new/transaction-icons/approve-failed.png');

const transactionIconDepositPending = require('../../../images-new/transaction-icons/deposit-pending.png');
const transactionIconDepositConfirmed = require('../../../images-new/transaction-icons/deposit-confirmed.png');
const transactionIconDepositFailed = require('../../../images-new/transaction-icons/deposit-failed.png');

const transactionIconWithdrawPending = require('../../../images-new/transaction-icons/withdraw-pending.png');
const transactionIconWithdrawConfirmed = require('../../../images-new/transaction-icons/withdraw-confirmed.png');
const transactionIconWithdrawFailed = require('../../../images-new/transaction-icons/withdraw-failed.png');

const transactionIconAddLiquidityPending = require('../../../images-new/transaction-icons/add-liquidity-pending.png');
const transactionIconAddLiquidityConfirmed = require('../../../images-new/transaction-icons/add-liquidity-confirmed.png');
const transactionIconAddLiquidityFailed = require('../../../images-new/transaction-icons/add-liquidity-failed.png');

const transactionIconRemoveLiquidityPending = require('../../../images-new/transaction-icons/remove-liquidity-pending.png');
const transactionIconRemoveLiquidityConfirmed = require('../../../images-new/transaction-icons/remove-liquidity-confirmed.png');
const transactionIconRemoveLiquidityFailed = require('../../../images-new/transaction-icons/remove-liquidity-failed.png');

const transactionIconSwapPending = require('../../../images-new/transaction-icons/swap-pending.png');
const transactionIconSwapConfirmed = require('../../../images-new/transaction-icons/swap-confirmed.png');
const transactionIconSwapFailed = require('../../../images-new/transaction-icons/swap-failed.png');
const transactionIconSwapIcon = require('../../../images-new/transaction-icons/swap-icon.png');
const transactionIconUnknown = require('../../../images-new/transaction-icons/unknown.png');

/* eslint-enable import/no-commonjs */

/**
 * View that renders a transaction item part of transactions list
 */
class TransactionElement extends PureComponent {
	static propTypes = {
		assetSymbol: PropTypes.string,
		/**
		 * Asset object (in this case ERC721 token)
		 */
		tx: PropTypes.object,
		/**
		 * String of selected address
		 */
		selectedAddress: PropTypes.string,
		/**
		/* Identities object required to get import time name
		*/
		identities: PropTypes.object,
		/**
		 * Current element of the list index
		 */
		i: PropTypes.number,
		visibleTime: PropTypes.bool,
		/**
		 * Callback to render transaction details view
		 */
		onPressItem: PropTypes.func,
		/**
		 * Callback to speed up tx
		 */
		onSpeedUpAction: PropTypes.func,
		/**
		 * Callback to cancel tx
		 */
		onCancelAction: PropTypes.func,
		swapsTransactions: PropTypes.object,
		swapsTokens: PropTypes.arrayOf(PropTypes.object),
		/**
		 * Chain Id
		 */
		chainId: PropTypes.string,
		signQRTransaction: PropTypes.func,
		cancelUnsignedQRTransaction: PropTypes.func,
		isQRHardwareAccount: PropTypes.bool,
		tokenList: PropTypes.object,
	};

	state = {
		actionKey: undefined,
		cancelIsOpen: false,
		speedUpIsOpen: false,
		detailsModalVisible: false,
		importModalVisible: false,
		transactionGas: { gasBN: undefined, gasPriceBN: undefined, gasTotal: undefined },
		transactionElement: undefined,
		transactionDetails: undefined,
	};

	mounted = false;

	componentDidMount = async () => {
		const [transactionElement, transactionDetails] = await decodeTransaction({
			...this.props,
			swapsTransactions: this.props.swapsTransactions,
			swapsTokens: this.props.swapsTokens,
			assetSymbol: this.props.assetSymbol,
		});
		// console.log(this.props.i, transactionElement)
		this.mounted = true;
		this.mounted && this.setState({ transactionElement, transactionDetails });
	};

	componentWillUnmount() {
		this.mounted = false;
	}

	onPressItem = () => {
		const { tx, i, onPressItem } = this.props;
		onPressItem(tx.id, i);
		this.setState({ detailsModalVisible: true });
	};

	onPressImportWalletTip = () => {
		this.setState({ importModalVisible: true });
	};

	onCloseImportWalletModal = () => {
		this.setState({ importModalVisible: false });
	};

	onCloseDetailsModal = () => {
		this.setState({ detailsModalVisible: false });
	};

	// renderTxTime = () => {
	// 	const { tx, selectedAddress } = this.props;
	// 	const incoming = safeToChecksumAddress(tx.transaction.to) === selectedAddress;
	// 	const selfSent = incoming && safeToChecksumAddress(tx.transaction.from) === selectedAddress;
	// 	return `${
	// 		(!incoming || selfSent) && tx.deviceConfirmedOn === WalletDevice.MM_MOBILE
	// 			? `#${parseInt(tx.transaction.nonce, 16)} - ${toDateFormat(tx.time)} ${strings(
	// 					'transactions.from_device_label'
	// 					// eslint-disable-next-line no-mixed-spaces-and-tabs
	// 			  )}`
	// 			: `${toDateFormat(tx.time)}
	// 		`
	// 	}`;
	// };

	renderTxTime = () => {
		const { tx, selectedAddress } = this.props;
		const incoming = safeToChecksumAddress(tx.transaction.to) === selectedAddress;
		const selfSent = incoming && safeToChecksumAddress(tx.transaction.from) === selectedAddress;
		const txDateString = isToday(tx.time) ? getTodayText()
							: isYesterday(tx.time) ? getYesterdayText()
							: toDateOnlyFormat(tx.time)
		return `${txDateString}`;
	};

	/**
	 * Function that evaluates tx to see if the Added Wallet label should be rendered.
	 * @returns Account added to wallet view
	 */
	renderImportTime = () => {
		const { tx, identities, selectedAddress } = this.props;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		const accountImportTime = identities[selectedAddress]?.importTime;
		if (tx.insertImportTime && accountImportTime) {
			return (
				<>
					<TouchableOpacity onPress={this.onPressImportWalletTip} style={styles.importRowBody}>
						<Text style={styles.importText}>
							{`${strings('transactions.import_wallet_row')} `}
							<FAIcon name="info-circle" style={styles.infoIcon} />
						</Text>
						<ListItem.Date>{toDateFormat(accountImportTime)}</ListItem.Date>
					</TouchableOpacity>
				</>
			);
		}
		return null;
	};

	renderTxElementAvatar = (transactionElement) => {
		const { transferInformation } = transactionElement;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		if (transferInformation) {
			const tokenInformation = Object.keys(this.props.tokenList)
				.map((tokenAdress) => this.props.tokenList[tokenAdress])
				.find(({ address }) => address.toLowerCase() === transferInformation.contractAddress.toLowerCase());
			if (tokenInformation) {
				return <AssetIcon logo={tokenInformation.iconUrl || tokenInformation.thumbnails} customStyle={styles.txElementAvatar} />;
			}
			return <Identicon address={transferInformation.contractAddress} customStyle={styles.txElementAvatar} />;
		}
		return <NetworkMainAssetLogo big style={styles.txElementAvatar} />;
	};

	// renderTxElementIcon = (transactionElement, status) => {
	// 	const { transactionType } = transactionElement;
	// 	const colors = this.context.colors || mockTheme.colors;
	// 	const styles = createStyles(colors);
	//
	// 	const isFailedTransaction = status === 'cancelled' || status === 'failed';
	// 	let icon;
	// 	switch (transactionType) {
	// 		case TRANSACTION_TYPES.SENT_TOKEN:
	// 		case TRANSACTION_TYPES.SENT_COLLECTIBLE:
	// 		case TRANSACTION_TYPES.SENT:
	// 			icon = isFailedTransaction ? transactionIconSentFailed : transactionIconSent;
	// 			break;
	// 		case TRANSACTION_TYPES.RECEIVED_TOKEN:
	// 		case TRANSACTION_TYPES.RECEIVED_COLLECTIBLE:
	// 		case TRANSACTION_TYPES.RECEIVED:
	// 			icon = isFailedTransaction ? transactionIconReceivedFailed : transactionIconReceived;
	// 			break;
	// 		case TRANSACTION_TYPES.SITE_INTERACTION:
	// 			icon = isFailedTransaction ? transactionIconInteractionFailed : transactionIconInteraction;
	// 			break;
	// 		case TRANSACTION_TYPES.APPROVE:
	// 			icon = isFailedTransaction ? transactionIconApproveFailed : transactionIconApprove;
	// 			break;
	// 	}
	// 	return <Image source={icon} style={styles.icon} resizeMode="stretch" />;
	// };

	renderTxElementIcon = (transactionElement, status) => {
		const { transactionType } = transactionElement;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		const isPendingTransaction = status === 'pending' || status === 'submitted';
		const isFailedTransaction = status === 'cancelled' || status === 'failed';
		const isApprove = transactionType === 'approve';
		let icon;
		switch (transactionType) {
			case 'send':
				icon = isPendingTransaction ? transactionIconSentPending : isFailedTransaction ? transactionIconSentFailed : transactionIconSentPending;
				break;
			case 'receive':
				icon = isPendingTransaction ? transactionIconReceivedPending : isFailedTransaction ? transactionIconReceivedFailed : transactionIconReceivedConfirmed;
				break;
			case 'transfer':
				icon = isPendingTransaction ? transactionIconSentPending : isFailedTransaction ? transactionIconSentFailed : transactionIconSentPending;
				break;
			case 'approve':
				icon = transactionIconApproveConfirmed;
				break;
			case 'deposit':
				icon = isPendingTransaction ? transactionIconDepositPending : isFailedTransaction ? transactionIconDepositFailed : transactionIconDepositConfirmed;
				break;
			case 'withdraw':
				icon = isPendingTransaction ? transactionIconWithdrawPending : isFailedTransaction ? transactionIconWithdrawFailed : transactionIconWithdrawConfirmed;
				break;
			case 'add_liquidity':
				icon = isPendingTransaction ? transactionIconAddLiquidityPending : isFailedTransaction ? transactionIconAddLiquidityFailed : transactionIconAddLiquidityConfirmed;
				break;
			case 'remove_liquidity':
				icon = isPendingTransaction ? transactionIconRemoveLiquidityPending : isFailedTransaction ? transactionIconRemoveLiquidityFailed : transactionIconRemoveLiquidityConfirmed;
				break;
			case 'swap':
				icon = isPendingTransaction ? transactionIconSwapPending : isFailedTransaction ? transactionIconSwapFailed : transactionIconSwapConfirmed;
				break;
			case 'renew':
				icon = isPendingTransaction ? transactionIconSentPending : isFailedTransaction ? transactionIconSentFailed : transactionIconSentConfirmed;
				break;
			case 'receive_nft':
				icon = isPendingTransaction ? transactionIconReceivedPending : isFailedTransaction ? transactionIconReceivedFailed : transactionIconReceivedConfirmed;
				break;
			case 'unknown':
				icon = transactionIconUnknown;
				break;
		}
		return <Image source={icon} style={[!isApprove ? styles.txElementActionIcon: styles.txElementActionIconApprove, isFailedTransaction && styles.failColorIcon]} resizeMode="stretch" />;
	};

	renderTxElementValue = (transactionElement, status) => {
		const { transactionType , value} = transactionElement;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		const isPendingTransaction = status === 'pending' || status === 'submitted';
		const isFailedTransaction = status === 'cancelled' || status === 'failed';
		let valueRender;
		let styleRender;
		switch (transactionType) {
			case 'send':
				valueRender = isPendingTransaction ? value : isFailedTransaction ? value : '-' + value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;
				break;
			case 'receive':
				valueRender = isPendingTransaction ? value : isFailedTransaction ? value : '+' + value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueSuccess;
				break;
			case 'transfer':
				valueRender = isPendingTransaction ? value : isFailedTransaction ? value : '-' + value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;
				break;
			case 'approve':
				valueRender = null;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;
				break;
			case 'deposit':
				valueRender = value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;
				break;
			case 'withdraw':
				valueRender = value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;
				break;
			case 'add_liquidity':
				valueRender = value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;
				break;
			case 'remove_liquidity':
				valueRender = value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;
				break;
			case 'swap':
				valueRender = isPendingTransaction ? value : isFailedTransaction ? value : '-' + value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;
				break;
			case 'renew':
				valueRender = isPendingTransaction ? value : isFailedTransaction ? value : '-' + value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;
				break;
			case 'receive_nft':
				valueRender = isPendingTransaction ? value : isFailedTransaction ? value : '-' + value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;

				break;
			case 'unknown':
				valueRender = isPendingTransaction ? value : isFailedTransaction ? value : value;
				styleRender = isPendingTransaction ? styles.valuePending : isFailedTransaction ? styles.valueFailed : styles.valueNormal;
				break;
		}
		return <Text style={[styleRender, styles.valueRender, {marginTop: - win.height / 400}]}>{valueRender}</Text>;
	};

	renderTxElementTokenName = (transactionElement, status) => {
		const { transferInformation } = transactionElement;
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		const isFailedTransaction = status === 'cancelled' || status === 'failed';
		if (transferInformation) {
			const tokenInformation = Object.keys(this.props.tokenList)
				.map((tokenAdress) => this.props.tokenList[tokenAdress])
				.find(({ address }) => address.toLowerCase() === transferInformation.contractAddress.toLowerCase());
			if (tokenInformation) {
				return <Text style={[styles.txElementTokenName, isFailedTransaction && styles.failColor]}>{tokenInformation.name}</Text>;
			}
			else if (transferInformation?.name){
				return <Text style={[styles.txElementTokenName, isFailedTransaction && styles.failColor]}>{transferInformation.name}</Text>;
			}
			return <Text style={[styles.txElementTokenName, isFailedTransaction && styles.failColor]}>{transferInformation?.symbol ? transferInformation?.symbol : ''}</Text>;
		}
		let tokenMainnet;
		if (this.props.ticker) {
			tokenMainnet = getTokenByTicker(this.props.ticker);
		}
		else {
			tokenMainnet = getTokenByTicker('ETH');
		}
		return <Text style={[styles.txElementTokenName, isFailedTransaction && styles.failColor]}>{tokenMainnet.name}</Text>;

	};

	/**
	 * Renders an horizontal bar with basic tx information
	 *
	 * @param {object} transactionElement - Transaction information to render, containing addressTo, actionKey, value, fiatValue, contractDeployment
	 */
	renderTxElement = (transactionElement) => {
		const {
			identities,
			chainId,
			selectedAddress,
			isQRHardwareAccount,
			tx: { time, status },
		} = this.props;
		// console.log(this.props.i === 0 ? transactionElement : this.props.i)
		const { value, fiatValue = false, actionKey } = transactionElement;
		const renderNormalActions = status === 'submitted' || (status === 'approved' && !isQRHardwareAccount);
		const renderUnsignedQRActions = status === 'approved' && isQRHardwareAccount;
		const accountImportTime = identities[selectedAddress]?.importTime;
		// return (
		// 	<>
		// 		{accountImportTime > time && this.renderImportTime()}
		// 		<ListItem>
		// 			<ListItem.Date>{this.renderTxTime()}</ListItem.Date>
		// 			<ListItem.Content>
		// 				<ListItem.Icon>{this.renderTxElementIcon(transactionElement, status)}</ListItem.Icon>
		// 				<ListItem.Body>
		// 					<ListItem.Title numberOfLines={1}>{actionKey}</ListItem.Title>
		// 					<StatusText status={status} />
		// 				</ListItem.Body>
		// 				{Boolean(value) && (
		// 					<ListItem.Amounts>
		// 						<ListItem.Amount>{value}</ListItem.Amount>
		// 						{isMainNet(chainId) && <ListItem.FiatAmount>{fiatValue}</ListItem.FiatAmount>}
		// 					</ListItem.Amounts>
		// 				)}
		// 			</ListItem.Content>
		// 			{renderNormalActions && (
		// 				<ListItem.Actions>
		// 					{this.renderSpeedUpButton()}
		// 					{this.renderCancelButton()}
		// 				</ListItem.Actions>
		// 			)}
		// 			{renderUnsignedQRActions && (
		// 				<ListItem.Actions>
		// 					{this.renderQRSignButton()}
		// 					{this.renderCancelUnsignedButton()}
		// 				</ListItem.Actions>
		// 			)}
		// 		</ListItem>
		// 		{accountImportTime <= time && this.renderImportTime()}
		// 	</>
		// );

		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		// if (transactionElement.transactionType !== 'swap') return <></>;
		const {visibleTime} = this.props;
		const isFailedTransaction = status === 'cancelled' || status === 'failed';
		return (
			<>
				<View style={styles.txElementWrapper}>
					{visibleTime &&<Text style={styles.txElementTime}>{this.renderTxTime()}</Text>}
					<View style={styles.txElementContent}>
						<View style={styles.txElementBlock1}>
							{this.renderTxElementAvatar(transactionElement)}
						</View>
						<View style={styles.txElementBlock2}>
							<View style={styles.txElementAction}>
								{this.renderTxElementIcon(transactionElement, status)}
								{/*{console.log(actionKey)}*/}
								<Text style={[styles.txElementActionText, isFailedTransaction && styles.failColor]} numberOfLines={1}>{actionKey}</Text>
							</View>
							{this.renderTxElementTokenName(transactionElement, status)}
						</View>
						<View style={styles.txElementBlock3}>
							{this.renderTxElementValue(transactionElement, status)}
							{/* <Text style={styles.txElementValue}>{value}</Text> */}
							<StatusText status={status} />
						</View>
					</View>
					{Boolean(transactionElement.swappedInformation) && (
						<>
							<View style={styles.txElementLineBreak}>
								<View style={styles.txElementLineBreakIcon}>
									<Image style={styles.txElementLineBreakIconImg} source={transactionIconSwapIcon} />
								</View>
							</View>
							<View style={styles.txElementContent}>
								<View style={styles.txElementBlock1}>
									{this.renderTxElementAvatar(transactionElement.swappedInformation)}
								</View>
								<View style={styles.txElementBlock2}>
									<View style={styles.txElementAction}>
										{this.renderTxElementIcon(transactionElement.swappedInformation, status)}
										<Text style={styles.txElementActionText} numberOfLines={1}>{transactionElement.swappedInformation.actionKey}</Text>
									</View>
									{this.renderTxElementTokenName(transactionElement.swappedInformation)}
								</View>
								<View style={styles.txElementBlock3}>
									{/*{this.renderTxElementValue(transactionElement, status)}*/}
									<Text style={[styles.valuePending, styles.valueRender, {marginTop: -win.height/200}]}>{transactionElement.swappedInformation.value}</Text>
									<StatusText status={status} />
								</View>
							</View>
						</>
					)}
				</View>
			</>
		);
	};

	renderCancelButton = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<StyledButton
				type={'cancel'}
				containerStyle={styles.actionContainerStyle}
				style={styles.actionStyle}
				onPress={this.showCancelModal}
			>
				{strings('transaction.cancel')}
			</StyledButton>
		);
	};

	parseGas = () => {
		const { tx } = this.props;

		let existingGas = {};
		const transaction = tx?.transaction;
		if (transaction) {
			if (isEIP1559Transaction(transaction)) {
				existingGas = {
					isEIP1559Transaction: true,
					maxFeePerGas: weiHexToGweiDec(transaction.maxFeePerGas),
					maxPriorityFeePerGas: weiHexToGweiDec(transaction.maxPriorityFeePerGas),
				};
			} else {
				const existingGasPrice = tx.transaction ? tx.transaction.gasPrice : '0x0';
				const existingGasPriceDecimal = parseInt(existingGasPrice === undefined ? '0x0' : existingGasPrice, 16);
				existingGas = { gasPrice: existingGasPriceDecimal };
			}
		}
		return existingGas;
	};

	showCancelModal = () => {
		const existingGas = this.parseGas();

		this.mounted && this.props.onCancelAction(true, existingGas, this.props.tx);
	};

	showSpeedUpModal = () => {
		const existingGas = this.parseGas();

		this.mounted && this.props.onSpeedUpAction(true, existingGas, this.props.tx);
	};

	hideSpeedUpModal = () => {
		this.mounted && this.props.onSpeedUpAction(false);
	};

	showQRSigningModal = () => {
		this.mounted && this.props.signQRTransaction(this.props.tx);
	};

	cancelUnsignedQRTransaction = () => {
		this.mounted && this.props.cancelUnsignedQRTransaction(this.props.tx);
	};

	renderSpeedUpButton = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		return (
			<StyledButton
				type={'normal'}
				containerStyle={[styles.actionContainerStyle, styles.speedupActionContainerStyle]}
				style={styles.actionStyle}
				onPress={this.showSpeedUpModal}
			>
				{strings('transaction.speedup')}
			</StyledButton>
		);
	};

	renderQRSignButton = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		return (
			<StyledButton
				type={'normal'}
				containerStyle={[styles.actionContainerStyle, styles.speedupActionContainerStyle]}
				style={styles.actionStyle}
				onPress={this.showQRSigningModal}
			>
				{strings('transaction.sign_with_keystone')}
			</StyledButton>
		);
	};

	renderCancelUnsignedButton = () => {
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);
		return (
			<StyledButton
				type={'cancel'}
				containerStyle={[styles.actionContainerStyle, styles.speedupActionContainerStyle]}
				style={styles.actionStyle}
				onPress={this.cancelUnsignedQRTransaction}
			>
				{strings('transaction.cancel')}
			</StyledButton>
		);
	};
	render() {
		const { tx } = this.props;
		const { detailsModalVisible, importModalVisible, transactionElement, transactionDetails } = this.state;
		// console.log(this.props.i === 0 ? transactionElement : this.props.i)
		const colors = this.context.colors || mockTheme.colors;
		const styles = createStyles(colors);

		if (!transactionElement || !transactionDetails) return null;
		return (
			<>
				<TouchableHighlight
					style={styles.row}
					onPress={this.onPressItem}
					// underlayColor={colors.background.alternative}
					underlayColor={'#262630'}
					activeOpacity={1}
				>
					{this.renderTxElement(transactionElement)}
				</TouchableHighlight>
				{/*{detailsModalVisible && (*/}
				{/*	<Modal*/}
				{/*		isVisible={detailsModalVisible}*/}
				{/*		onBackdropPress={this.onCloseDetailsModal}*/}
				{/*		onBackButtonPress={this.onCloseDetailsModal}*/}
				{/*		onSwipeComplete={this.onCloseDetailsModal}*/}
				{/*		swipeDirection={'down'}*/}
				{/*		backdropColor={colors.overlay.default}*/}
				{/*		backdropOpacity={1}*/}
				{/*	>*/}
				{/*		<DetailsModal>*/}
				{/*			<DetailsModal.Header>*/}
				{/*				<DetailsModal.Title onPress={this.onCloseDetailsModal}>*/}
				{/*					{transactionElement?.actionKey}*/}
				{/*				</DetailsModal.Title>*/}
				{/*				<DetailsModal.CloseIcon onPress={this.onCloseDetailsModal} />*/}
				{/*			</DetailsModal.Header>*/}
				{/*			<TransactionDetails*/}
				{/*				transactionObject={tx}*/}
				{/*				transactionDetails={transactionDetails}*/}
				{/*				showSpeedUpModal={this.showSpeedUpModal}*/}
				{/*				showCancelModal={this.showCancelModal}*/}
				{/*				close={this.onCloseDetailsModal}*/}
				{/*			/>*/}
				{/*		</DetailsModal>*/}
				{/*	</Modal>*/}
				{/*)}*/}
				{detailsModalVisible && (
					<Modal
						isVisible={detailsModalVisible}
						style={styles.bottomModal}
						onBackdropPress={this.onCloseDetailsModal}
						onBackButtonPress={this.onCloseDetailsModal}
						onSwipeComplete={this.onCloseDetailsModal}
						swipeDirection={'down'}
						backdropColor={colors.overlay.default}
						backdropOpacity={1}
					>
						<View style={styles.modalDetailsView}>
							<View style={styles.modalDaggerWrapper}>
								<View style={styles.modalDagger} />
							</View>
							<View style={styles.modalDetailsContainer}>
								<View style={styles.modalDetailsHeader} >
									<Text style={styles.modalDetailsTitle}>{strings('transactions.transaction_details')}</Text>
								</View>
								<TransactionDetails
									transactionObject={tx}
									transactionDetails={transactionDetails}
									showSpeedUpModal={this.showSpeedUpModal}
									showCancelModal={this.showCancelModal}
									close={this.onCloseDetailsModal}
								/>
							</View>
						</View>
					</Modal>
				)}
				<Modal
					isVisible={importModalVisible}
					onBackdropPress={this.onCloseImportWalletModal}
					onBackButtonPress={this.onCloseImportWalletModal}
					onSwipeComplete={this.onCloseImportWalletModal}
					swipeDirection={'down'}
					backdropColor={colors.overlay.default}
					backdropOpacity={1}
				>
					<DetailsModal>
						<DetailsModal.Header>
							<DetailsModal.Title onPress={this.onCloseImportWalletModal}>
								{strings('transactions.import_wallet_label')}
							</DetailsModal.Title>
							<DetailsModal.CloseIcon onPress={this.onCloseImportWalletModal} />
						</DetailsModal.Header>
						<View style={styles.summaryWrapper}>
							<Text style={styles.fromDeviceText}>{strings('transactions.import_wallet_tip')}</Text>
						</View>
					</DetailsModal>
				</Modal>
			</>
		);
	}
}

const mapStateToProps = (state) => ({
	ticker: state.engine.backgroundState.NetworkController.provider.ticker,
	chainId: state.engine.backgroundState.NetworkController.provider.chainId,
	identities: state.engine.backgroundState.PreferencesController.identities,
	primaryCurrency: state.settings.primaryCurrency,
	selectedAddress: state.engine.backgroundState.PreferencesController.selectedAddress,
	swapsTransactions: state.engine.backgroundState.TransactionController.swapsTransactions || {},
	swapsTokens: state.engine.backgroundState.SwapsController.tokens,
	tokenList: getTokenList(state),
});

TransactionElement.contextType = ThemeContext;

export default connect(mapStateToProps)(TransactionElement);
