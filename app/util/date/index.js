import { strings } from '../../../locales/i18n';

export function toLocaleDateTime(timestamp) {
	const dateObj = new Date(timestamp);
	const date = dateObj.toLocaleDateString();
	const time = dateObj.toLocaleTimeString();
	return `${date} ${time}`;
}

export function toDateFormat(timestamp) {
	const date = new Date(timestamp);
	const month = strings(`date.months.${date.getMonth()}`);
	const day = date.getDate();
	let hours = date.getHours();
	let minutes = date.getMinutes();
	const ampm = hours >= 12 ? 'pm' : 'am';
	hours %= 12;
	hours = hours || 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	return `${month} ${day} ${strings('date.connector')} ${hours}:${minutes} ${ampm}`;
}

export function toDateOnlyFormat(timestamp) {
	const date = new Date(timestamp);
	const year = date.getFullYear();
	const month = date.getMonth() + 1;
	const day = date.getDate();
	return `${day > 9 ? day : `0${day}`}/${month > 9 ? month : `0${month}`}/${year}`;
}

export function toLocaleDate(timestamp) {
	return new Date(timestamp).toLocaleDateString();
}

export function toLocaleTime(timestamp) {
	return new Date(timestamp).toLocaleTimeString();
}

export function isToday(timestamp) {
	const date = new Date(timestamp);
	const today = new Date();
	return date.getDate() === today.getDate() &&
		date.getMonth() === today.getMonth() &&
		date.getFullYear() === today.getFullYear();
}

export function getTodayText() {
	return strings('date.today');
}

export function isYesterday(timestamp) {
	const date = new Date(timestamp);
	const yesterday = new Date();
	yesterday.setDate(yesterday.getDate() - 1);
	return date.getDate() === yesterday.getDate() &&
		date.getMonth() === yesterday.getMonth() &&
		date.getFullYear() === yesterday.getFullYear();
}

export function getYesterdayText() {
	return strings('date.yesterday');
}
