import {
	ApolloClient,
	HttpLink,
	InMemoryCache,
} from '@apollo/client';

const defaultOptions = {
	query: {
		errorPolicy: 'all',
		fetchPolicy: 'no-cache',
	},
	watchQuery: {
		errorPolicy: 'ignore',
		fetchPolicy: 'no-cache',
	},
};

export const ensClient = new ApolloClient({
	...defaultOptions,
	cache: new InMemoryCache(),
	defaultOptions: {
		query: {
			fetchPolicy: 'no-cache',
		},
	},
	link: new HttpLink({
		uri: 'https://api.thegraph.com/subgraphs/name/ensdomains/ens',
	}),
});
