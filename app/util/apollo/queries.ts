import { AddressZero } from '@ethersproject/constants';
import gql from 'graphql-tag';

export const ENS_SUGGESTIONS = gql`
  query lookup($name: String!, $amount: Int!) {
    domains(
      first: $amount
      where: { name_starts_with: $name, resolvedAddress_not: \"${AddressZero}\" }
      orderBy: labelName
      orderDirection: asc
    ) {
      name
      resolver {
        texts
        addr {
          id
        }
      }
      owner {
        id
      }
    }
  }
`;
