import { View, Text, Dimensions, Image } from 'react-native'
import React from 'react'
import { BACKGROUND_COLOR } from '../../constants/backgroundColor'
import { fontStyles } from '../../styles/common'

const win = Dimensions.get('window')

const Avatar = ({address}) => {
  return (
    <View style={{
        shadowColor: BACKGROUND_COLOR[
            (address.charCodeAt(address.length - 3) * 100 + address.charCodeAt(address.length - 2) * 10
                +  address.charCodeAt(address.length - 1)) % 20
        ],
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.3,
        shadowRadius: 20,
    }}>
        <Image source={require('../../images-new/hexagon.svg')} style={{
            tintColor: BACKGROUND_COLOR[
                (address.charCodeAt(address.length - 3) * 100 + address.charCodeAt(address.length - 2) * 10
                    +  address.charCodeAt(address.length - 1)) % 20
            ],
            width: win.width/8.625,
            height: win.height/ 17.23,
            zIndex: -1,
        }}>
        </Image>
        <Text style={{
            textAlign: 'center',
			fontSize: 18,
			color: '#FFF',
			marginTop: -win.height/24,
			...fontStyles.boldHight
        }}>
                {address.toUpperCase().slice(address.length - 3)}
        </Text>
    </View>
  )
}


export default Avatar