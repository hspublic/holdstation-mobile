import Engine from '../core/Engine';
import networkMap from 'ethjs-ens/lib/network-map.json';
import ENS from 'ethjs-ens';
import { toLowerCaseEquals } from '../util/general';
import HttpProvider from 'ethjs-provider-http'
/**
 * Utility class with the single responsibility
 * of caching ENS names
 */
class ENSCache {
	static cache = {};
}

export async function searchENSLookup(ensName, network) {
	const provider = new HttpProvider('https://mainnet.infura.io')
	const ens = new ENS({ provider, network: network })
	ens.lookup(ensName)
		.then((address) => {
			console.log('address : ', address)
			// const expected = '0x5f8f68a0d1cbc75f6ef764a44619277092c32df0'

			// if (address === expected) {
			// 	alert("That's how you do it!")
			// }
		})
		.catch((reason) => {
			console.log('reason : ', reason)
		})
}

export async function doENSReverseLookup(address, network) {
	const cache = ENSCache.cache[network + address];
	if (cache) {
		return Promise.resolve(cache);
	}

	const { provider } = Engine.context.NetworkController;
	// console.log('provider : ', provider)
	const networkHasEnsSupport = Boolean(networkMap[network]);

	if (networkHasEnsSupport) {
		this.ens = new ENS({ provider, network });
		try {
			const name = await this.ens.reverse(address);
			// console.log('name : ', name)
			const resolvedAddress = await this.ens.lookup(name);
			if (toLowerCaseEquals(address, resolvedAddress)) {
				ENSCache.cache[network + address] = name;
				return name;
			}
			// eslint-disable-next-line no-empty
		} catch (e) { }
	}
}

export async function doENSLookup(ensName, network) {
	const { provider } = Engine.context.NetworkController;

	const networkHasEnsSupport = Boolean(networkMap[network]);

	if (networkHasEnsSupport) {
		this.ens = new ENS({ provider, network });
		try {
			const resolvedAddress = await this.ens.lookup(ensName);
			return resolvedAddress;
			// eslint-disable-next-line no-empty
		} catch (e) { }
	}
}

export function isDefaultAccountName(name) {
	return /^Account \d*$/.test(name);
}
