import AsyncStorage from "@react-native-community/async-storage";

const tokenAddress = (address: string, chainId: number) => `${address}.${chainId}`;

export const getListToken = async (address: string, chainId: number) => {
	const result = await AsyncStorage.getItem(tokenAddress(address, chainId));
	// const expireAt = getExpireDate(expireInMinutes);
	// @ts-ignore
    console.log('json', JSON.parse(result))

	if (result) {
		return JSON.parse(result);
	}
	return null;
};

export const saveTokenList = async (address: string, chainId:number, value: Object) =>{
	// @ts-ignore
	await AsyncStorage.setItem(tokenAddress(address, chainId), JSON.stringify(value));
}