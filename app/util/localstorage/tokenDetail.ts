import AsyncStorage from '@react-native-community/async-storage';

const tokenAddress = (address: string) => `token.${address}`;

export const getTokenDetail = async (address: string) => {
	const result = await AsyncStorage.getItem(tokenAddress(address));
	// const expireAt = getExpireDate(expireInMinutes);
	// @ts-ignore
	if (result && new Date(result.expireAt) < new Date()) {
		await AsyncStorage.removeItem(tokenAddress(address));
		return null;
	}
	if (result) {
		return JSON.parse(result);
	}
	return null;
};

export const saveTokenDetail = async (address: string, value: Object, date: Date, expireInMinutes: 30) =>{
	// @ts-ignore
	value.expireAt = new Date(date.getTime() + expireInMinutes * 60000);
	await AsyncStorage.setItem(tokenAddress(address), JSON.stringify(value));
}
//
// /**yar
//  *
//  * @param expireInMinutes
//  * @returns {Date}
//  */
// const getExpireDate = (expireInMinutes: number) => {
// 	const now = new Date();
// 	const expireTime = new Date(now);
// 	expireTime.setMinutes(now.getMinutes() + expireInMinutes);
// 	return expireTime;
// };
