const PopularList = [
	{
		symbol: "ETH",
		name: "Ethereum",
		chainId: 1,
		coinGeckoId: "ethereum",
		verified: true,
		thumbnail:"https://assets.coingecko.com/coins/images/279/small/ethereum.png?1595348880"
	},
	{
		symbol:"AVAX",
		name:"Avalanche",
		chainId: 43114,
		coinGeckoId:"avalanche-2",
		verified:true,
		thumbnail:"https://assets.coingecko.com/coins/images/12559/large/coin-round-red.png?1604021818"
	},
	{
		symbol: "BNB",
		name: "Binance Coin",
		chainId: 56,
		coinGeckoId: "binancecoin",
		verified: true,
		thumbnail: "https://assets.coingecko.com/coins/images/825/large/bnb-icon2_2x.png?1644979850"
	},
	{
		symbol: "FTM",
		name: "Fantom",
		coinGeckoId: "fantom",
		chainId: 250,
		verified: true,
		thumbnail: "https://assets.coingecko.com/coins/images/4001/small/Fantom.png?1558015016"
	},
	{
		symbol: "ONE",
		name: "Harmony",
		coinGeckoId: "harmony",
		chainId: 1666600000,
		verified: true,
		thumbnail: "https://assets.coingecko.com/coins/images/4344/large/Y88JAze.png?1565065793"
	},
	{
		symbol: "MATIC",
		name: "Polygon",
		chainId: 137,
		coinGeckoId: "matic-network",
		verified: true,
		thumbnail: "https://assets.coingecko.com/coins/images/4713/large/matic-token-icon.png?1624446912"
	}
];

export default PopularList;
