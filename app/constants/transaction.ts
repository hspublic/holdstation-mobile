export const TX_UNAPPROVED = 'unapproved';
export const TX_SUBMITTED = 'submitted';
export const TX_SIGNED = 'signed';
export const TX_PENDING = 'pending';
export const TX_CONFIRMED = 'confirmed';

export const TX_HISTORY_LIMIT = 100;
export const TX_HISTORY_LIMIT_GET = 100;
export const SWAP_ADDRESS_FEE_RECEIVER = '0x4921e251416b81A1e68269A320CD19E62808758B'
export const SWAP_CHARGE_FEE = 'currency_in'
export const SWAP_FEE_AMOUNT = '55'
export const SWAP_CLIENT_DATA = '{ "source": "holdstation" }'
export const SWAP_APPROVE_MAX_VALUE = 2**256
export const USE_MAX_PERCENT = 0.9

// Values
export const UINT256_HEX_MAX_VALUE = 'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';
